const gulp = require('gulp');
const gutil = require('gulp-util');
const clean = require('gulp-clean');
const uglify = require('gulp-uglify');
const removeLog = require('gulp-remove-logging');
const replace = require('gulp-replace');
const rename = require('gulp-rename');
const version = require('gulp-version-number');
const del = require('del');
const cleanCSS = require('gulp-clean-css');
const imagemin = require('gulp-imagemin');
const minifyejs = require('gulp-minify-ejs');
const runSequence = require('run-sequence');
const zip = require('gulp-zip');
const useref = require('gulp-useref');
const browserSync = require('browser-sync').create();
const nodemon = require('gulp-nodemon');

const views = './views/**';
const pub = './public/**';

const ejs = ['./dist/views/**/*.ejs', '!./dist/views/**/seja-um-professor.ejs'];
const js = ['./dist/public/**/*.js', '!./dist/public/pdfviewer/**/*.js'];
const css = './dist/public/**/*.css';
const imgs = ['./public/img/**/*.png', './public/img/**/*.jpg'];

const files = [
  '!node_modules/**',

  '!dist/**',

  '!gulp*.js',

  '!public/**',

  '!views/**',

  '!.git',
  '!.gitignore',

  '!.env',
  '!.env.example',

  '!*.zip',

  '!version',

  '!sitemap-generator/**',

  '!docs/**',

  './**'
];

gulp.task('deploy', function(done) {
  runSequence('clean', 'mini', 'zip', 'clean');
});

gulp.task('clean', function() {
  return del('dist');
});

// Tarefa de minificação
gulp.task('mini', function(callback) {
  runSequence(
    'concatenate-all',
    'copy-public-from-concatenate',
    'clean-old-public',
    'copy-public',
    'fix-views-asset-reference',
    'add-versioning-to-views',
    'mini-js',
    'mini-css',
    'mini-img',
    'transfer-files',
    callback
  );
});

gulp.task('concatenate-all', function() {
  return gulp
    .src(views, { dot: true })
    .pipe(
      useref({
        transformPath: function(filePath) {
          //fix directory reference
          return filePath.replace('views', 'public');
        }
      })
    )
    .pipe(gulp.dest('dist/views/.'));
});

gulp.task('clean-old-public', function() {
  return del('dist/views/public');
});

gulp.task('copy-public-from-concatenate', function() {
  return gulp.src('./dist/views/public/**', { dot: true }).pipe(gulp.dest('dist/public/.'));
});

gulp.task('copy-public', function() {
  return gulp.src('./public/**', { dot: true }).pipe(gulp.dest('dist/public/.'));
});

gulp.task('fix-views-asset-reference', function() {
  return gulp
    .src('./dist/views/**', { dot: true })
    .pipe(replace('public/', '/'))
    .pipe(gulp.dest('./dist/views/.'));
});

gulp.task('add-versioning-to-views', function() {
  return gulp
    .src('./dist/views/**/*.ejs', { base: './' })
    .pipe(
      version({
        value: '%MDS%',
        append: {
          key: 'v',
          to: ['css', 'js']
        }
      })
    )
    .pipe(gulp.dest('./'));
});

gulp.task('mini-js', function() {
  return (
    gulp
      .src(js, { base: './' })
      // .pipe(removeLog())
      // .pipe(gulp.dest('dist/.'))
      .pipe(
        uglify().on('error', function(e) {
          console.log(e);
        })
      )
      .pipe(gulp.dest('./'))
      .pipe(replace('.includes', '.indexOf')) //bug internet explorer
      .pipe(gulp.dest('./'))
  );
});

gulp.task('mini-css', function() {
  return gulp
    .src(css, { base: './' })
    .pipe(cleanCSS())
    .pipe(gulp.dest('./'));
});

//nao usado - elimina espaços necessarios!
gulp.task('mini-ejs', function() {
  return gulp
    .src(ejs, { base: './' })
    .pipe(minifyejs())
    .pipe(gulp.dest('./'));
});

gulp.task('mini-img', function() {
  return gulp
    .src(imgs, { base: './' })
    .pipe(imagemin())
    .pipe(gulp.dest('./dist/public/img/.'));
});

gulp.task('transfer-files', function() {
  return gulp.src(files, { dot: true }).pipe(gulp.dest('dist/.'));
});

// deprecated
gulp.task('zip', function() {
  return gulp
    .src('dist/**/*.*', { dot: true })
    .pipe(zip('colmeia_website_dist.zip'))
    .pipe(gulp.dest('.'));
});

//////////////////////////////  browser sync  /////////////////////////////////
// gulp.task('serve', function() {
//   browserSync.init(null, {
//     proxy: 'localhost:4000'
//   });

//   gulp.watch(views).on('change', browserSync.reload);
//   gulp.watch(pub).on('change', browserSync.reload);
// });

// gulp.task('default', ['serve']);
