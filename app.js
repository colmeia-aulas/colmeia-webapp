const app = require('./config/express');
const { debug, port } = require('./config/vars');

app.listen(port, () =>
  console.log(`Running on ${port} in ${debug ? 'development' : 'production'}.`)
);
