router.get('/professores/:name', function(req, res) {

  var name = req.params.name;

  db.getProfessoresColmeiaParam('name_identifier', name, function(result) {
    if (result.length > 0) {
      res.render('professor-unico', {professor: result});
    }
    else {
      res.status(404).render("./404");
    }
  });
});

router.get('/professores/:materia/:cidade/:bairro', function(req, res) {

  if(req.params.cidade != 'brasilia' && req.params.cidade != 'brasilia' )
  {
    res.status(404).render("./404", {user: req.session.user});
  }

  else
  {
    var data = {
      materia: materiasMap[req.params.materia],
      bairro: bairrosMap[req.params.bairro]
    };

    if (data.materia == 'Qualquer' && data.bairro == 'Qualquer')
    {
      db.getProfessoresColmeia({}, function(results){
        res.render('professores-colmeia', {professores: results});
      });
    }
    else if (data.materia == 'Qualquer') {
      db.getProfessoresColmeiaParam('bairros', data.bairro, function(results){
        res.render('professores-colmeia', {professores: results});
      });
    }
    else if (data.bairro == 'Qualquer') {
      db.getProfessoresColmeiaParam('topicos', data.materia, function(results){
        res.render('professores-colmeia', {professores: results});
      });
    }
    else {
      db.getProfessoresColmeia(data, function(results){
        res.render('professores-colmeia', {professores: results});
      });
    }
  }
});
