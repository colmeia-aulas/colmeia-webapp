Bootstrap v3.3		- http://getbootstrap.com/docs/3.3/components/
Bootstrap Datepicker 	- https://eonasdan.github.io/bootstrap-datetimepicker/
Bootstrap filestyle 	- http://markusslima.github.io/bootstrap-filestyle/
Bootstrap select	- http://silviomoreto.github.io/bootstrap-select
Bootbox			- http://bootboxjs.com/examples.html
jQuery			- https://api.jquery.com/
jQuery input mask	- http://igorescobar.github.io/jQuery-Mask-Plugin/docs.html
Loading overlay		- http://gasparesganga.com/labs/jquery-loading-overlay/
Moment 			- https://momentjs.com/docs/
Pagarme 		- https://docs.pagar.me/
Particles js		- https://github.com/VincentGarreau/particles.js/