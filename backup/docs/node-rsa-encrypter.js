var NodeRSA = require('node-rsa');
var cardKey = new NodeRSA('-----BEGIN PUBLIC KEY-----\n' +
'MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAk8aNKmOs7Ikia6sRPF2b\n' +
'FD8TslQAvJ3xdvNdjXzCWhpYlGEAqLwzLkXrlOlxe/nQJSF4+/4k5LRMyV0VpqC+\n' +
'TOVdeMgVWTk1nDOKJpfH6FDRC1WCm3Gshce8yNcmHFb852P1ryTN/dKwT9rOFk0Z\n' +
'n5BYbbwq3D6t1TTrKbamE4l/6ouf0r+XAT9xiU4T2sKQXEkrQEbLPYeE21/Ic+xl\n' +
'0BFlbamNgmvCQjSz5CiZU10rZjetu+xmPGEVlS4YzTnqZSvToNWTd0oks+fbVzpv\n' +
'xU94pQ+T3YKOQw0cyEoJpTyvEcsLOER0UQY7RSmFTNexbN5EoZBV6rZfpPoGKQCL\n' +
'vLZoV3LJUMkxFai4YPKRtGPT3KF/B8YB9vwYXFxuv6MqVqnDMA7mwFgRg+k8f19L\n' +
'g2SJoYXGsKhbCtAgkZOzP6IRBqJueY2Y/b0YkbU/tUuTT8EsjbGtd7QbgrwXJwdx\n' +
'DX43BhaH8yGBqGYje41HlULIrXJkHc+LwEGNunt7h8nXZDriObRDWuSUuCccuLL4\n' +
'gCvMnfMPgRjWF7IqSjZ3bJN85LXkvMmFL9CvPKuNBaHC4GRMKmznTngF24snKk84\n' +
'shFF8nVhx77eExbwt4yyzAovU2aS+ApM7hg5IHDc4wc8icf0hCXSaenOUj3X//NR\n' +
'e0ibAWHMHp//ZAkMfd/dIskCAwEAAQ==\n' +
'-----END PUBLIC KEY-----');
var passwordKey = new NodeRSA('-----BEGIN PUBLIC KEY-----\n' +
'MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEA1ICF67t7HyK0uqDJ70ib\n' +
'RKuf520nd4wBiOVwzLSJid9pYZC6qa51EH61int128vKe0NahNcSXPR/spjnBhQ1\n' +
'9ZZVD5nK/pMb/a9LdDZKiVluPYJ8qy2orvwEWPplB/+murCe2ShBJ3xvS+vKPdSV\n' +
'ZjLsmaXhiB+MOB1PzT4wQ59L/7mZeXsWMW+3piLeUs6Bm+AFCE82vswHDocFiSk4\n' +
'pvkubGwyQ6uDHSGrQ2EWNol4yydKyCdcopWBkpo0TebMuHY3X5H6iGbV7LnSYRv6\n' +
'1JoMmUQQLSw44TMxw3KcuXvXqrZ8wwEhPZMY4p9u8F1PwJ72y13Bc7tu5BYbnS7i\n' +
'7IWoPWo2/lzxeNBQ9Y1ycGDAUVCITb5KV3EdqadLbsBDEM994hvmNDCFXhvQTp6+\n' +
'6R+IMp9FR77gcIKIKQvPoFWFrFZzEbutDZTMtckPAcCUwME1ASAqM2MEfMXNdxRg\n' +
'MvX6c4p44kjBnsKwQStGFJYmr81jvXJK3XnwNhObGS63Vlhzfw/+RStX8fNUHtET\n' +
'/RpJRHEysnuvY60z0y1DRXYpQWfgheBV9iSMYyXwQ3A45p5Iusd9kcGQgmXuhwFG\n' +
'P7pvvzoAMC9BYD/Zt2nFMuU/TaoFahfabs0S2jo4SgNnyIYAV4zYFr6uzzyKQ/Dh\n' +
'ukAvvfZUWU95aWmCj0Owh70CAwEAAQ==\n' +
'-----END PUBLIC KEY-----');

window.NodeRsaEncryptString = function(keySelector, message) {
	if(keySelector.toLowerCase() === 'card') {
		return cardKey.encrypt(message, 'base64');
	}
	if(keySelector.toLowerCase() === 'password') {
		return passwordKey.encrypt(message, 'base64');
	}
	return 'Invalid keySelector parameter.';
}
