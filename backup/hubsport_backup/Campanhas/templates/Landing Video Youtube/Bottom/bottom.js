$('.section-1__cta').on('click', function() {
  scrollToTop();
});

$('.section-2__cta').on('click', function() {
  scrollToTop();
});

$('.section-video__cta').on('click', function() {
  scrollToTop();
});

$('.section-video__cta_mobile').on('click', function() {
  scrollToTop();
});

function scrollToTop() {
  $('html, body').animate({ scrollTop: 0 }, 'medium');
}
