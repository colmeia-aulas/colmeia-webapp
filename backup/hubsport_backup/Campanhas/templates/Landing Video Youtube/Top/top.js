$(function() {
  setupMask();
  setupRappi();

  $('form').submit(function(e) {
    e.preventDefault();

    if (
      $('.form__input--nome').val() == '' ||
      $('.form__input--telefone').val() == '' ||
      $('.form__input--email').val() == ''
    ) {
      alert('Por favor, preencha todos os campos!');
      return;
    }

    if ($('.form__input--idioma').length && !$('.form__input--idioma').val()) {
      alert('Por favor, selecione um idioma!');
      return;
    }
    $.ajax({
      url: 'https://aulascolmeia.com.br/landing-campanha',
      type: 'POST',
      data: {
        url: window.location.href || window.location.search,
        nome: $('.form__input--nome').val(),
        telefone: '+55' + $('.form__input--telefone').cleanVal(),
        email: $('.form__input--email').val(),
        ga_id: '',
        origin: ('landing-campanha-' + pageId).toUpperCase(),
        navigator: {},
        info: $('.form__input--idioma').val() || ''
      }
    })
      .done(function(result) {
        if (result.success) {
          dataLayer.push({
            event: 'cadastroCriado'
          });
          $('form')
            .find('.form__input')
            .val('');

          alert('Dados recebidos com sucesso!');

          fbq('track', 'CompleteRegistration');
        } else {
          alert('Desculpe, houve um erro ao receber seus dados.');
        }
      })
      .fail(function(jqXHR, textStatus, errorThrown) {
        alert('Desculpe, houve um erro ao receber seus dados.');
      });
  });

  function setupMask() {
    var maskBehavior = function(val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
      },
      options = {
        onKeyPress: function(val, e, field, options) {
          field.mask(maskBehavior.apply({}, arguments), options);
        }
      };

    $('.form__input--telefone').mask(maskBehavior, options);
  }

  function setupRappi() {
    if (window.location.pathname.indexOf('rappi') > -1) {
      var selectContent =
        '<select style="-webkit-appearance: none; -moz-appearance: none; background: #fff url(https://aulascolmeia.com.br/img/dropdown-arrow.png) 95% center no-repeat; background-size: 20px;" class="form__input form__input--idioma"><option value="" disabled selected>Idioma</option><option value="INGLES">Inglês</option><option value="ESPANHOL">Espanhol</option><option value="INGLES+ESPANHOL">Inglês e Espanhol</option></select>';

      $('.form__input--telefone').after(selectContent);
      $('.form__button, .section-2__cta').css({
        'background-color': '#ED6865',
        'border-color': '#ED6865'
      });

      $('.form__button')
        .mouseenter(function() {
          $(this).css('color', '#fff');
        })
        .mouseleave(function() {
          $(this).css('color', '#fff');
        });
    }
  }
});
