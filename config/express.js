// libs
const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cookieSession = require('cookie-session');
const skipMap = require('skip-map');
const morgan = require('morgan');
const compression = require('compression');
const cors = require('cors');
const helmet = require('helmet');
const flash = require('connect-flash');

// config
const { debug } = require('./vars');
const { morganLog, morganConfig, corsConfig, cookieConfig } = require('./libs');

// middleware
const errorMiddleware = require('../middlewares/error');
const webMiddleware = require('../middlewares/web');
const localsMiddleware = require('../middlewares/locals');
const debugAuthMiddleware = require('../middlewares/debugAuth');

// routes
const userRoutes = require('../routes/user');
const miscRoutes = require('../routes/misc');
const contentRoutes = require('../routes/content');
const campaignRoutes = require('../routes/campaigns');
const classRoutes = require('../routes/class');
const addressRoutes = require('../routes/address');
const paymentRoutes = require('../routes/payment');
const tutorRoutes = require('../routes/tutors');
const formsRoutes = require('../routes/forms');
const reactRoutes = require('../routes/reactRoutes');
const notFoundRoutes = require('../routes/notFound');

const app = express();

app.set('view engine', 'ejs');

app.use(helmet());
app.use(compression());
app.use(skipMap());

app.use(morgan(morganLog, morganConfig));
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));

// public
app.use(express.static(path.join(__dirname, '../public')));

// react
app.use('/static', express.static(path.join(__dirname, '../react-web/build', 'static')));

app.use(cookieSession(cookieConfig));

app.use(flash());

app.use(webMiddleware.forceHttps);
app.use(webMiddleware.removeWww);

if (debug) {
  // auto log in
  // app.use(debugAuthMiddleware);
} else {
  app.use(cors(corsConfig));
  app.use(webMiddleware.cacheResources);
}

// locals
app.use(localsMiddleware);

// routes
app
  .use(miscRoutes)
  .use(userRoutes)
  .use(contentRoutes)
  .use(classRoutes)
  .use(addressRoutes)
  .use(paymentRoutes)
  .use(campaignRoutes)
  .use(tutorRoutes)
  .use(formsRoutes)
  .use(reactRoutes);

// error
app.use(notFoundRoutes).use(errorMiddleware.errorHandler);

module.exports = app;
