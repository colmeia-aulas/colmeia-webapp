const path = require('path');
const multer = require('multer');
const { debug } = require('./vars');
const { createDomainWhiteList } = require('../util/domain');

module.exports = {
  morganLog: debug ? 'dev' : ':method :url :status :response-time ms :remote-addr [:date[clf]]',
  morganConfig: {
    skip: (req, res) =>
      req.path.indexOf('/img/') !== -1 ||
      req.path.indexOf('/video/') !== -1 ||
      req.path.indexOf('/js/') !== -1 ||
      req.path.indexOf('/css/') !== -1 ||
      req.path.indexOf('/assets/') !== -1 ||
      req.path.indexOf('/fonts/') !== -1 ||
      req.path.indexOf('/manifest.json') !== -1 ||
      req.path.indexOf('/favicon.ico') !== -1 ||
      req.path.indexOf('/resize-image') !== -1
  },

  corsConfig: {
    origin: (origin, callback) => {
      const whiteList = createDomainWhiteList();

      if (debug || whiteList.indexOf(origin) !== -1 || !origin) {
        callback(null, true);
      } else {
        callback(new Error(`Origem ${origin} não permitida!`));
      }
    }
  },

  cookieConfig: {
    name: 'session', // req.session
    secret: '451AUTHORIZATION:#COLMEIA541',

    maxAge: 24 * 60 * 60 * 1000 * 365 // 24 * 365 -> 1 year
  },

  multerConfig: {
    storage: multer.diskStorage({
      destination: (req, file, cb) => {
        cb(null, path.join(__dirname, '../public/uploads'));
      },
      filename: (req, file, cb) => {
        const ext = file.originalname.split('.').pop();
        cb(null, `${file.fieldname}-${Date.now()}.${ext}`);
      }
    })
  }
};
