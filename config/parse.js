const { Parse } = require('parse/node');
const config = require('./vars');

Parse.initialize(config.parse.appId, config.parse.jsKey, config.parse.masterKey);
Parse.serverURL = config.parse.serverUrl;

module.exports = Parse;
