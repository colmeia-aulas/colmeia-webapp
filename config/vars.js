const path = require('path');
const dotenv = require('dotenv-safe');

if (!process.env.NODE_ENV || process.env.NODE_ENV !== 'production') {
  // import .env variables
  dotenv.load({
    path: path.join(__dirname, '../.env'),
    sample: path.join(__dirname, '../.env.example')
  });
}

module.exports = {
  env: process.env.NODE_ENV,
  port: process.env.PORT || 4000,
  debug: process.env.DEBUG_ENV ? true : false,
  parse: {
    appId: process.env.DEBUG_ENV ? process.env.PARSE_DEBUG_APP_ID : process.env.PARSE_PROD_APP_ID,
    jsKey: process.env.DEBUG_ENV ? process.env.PARSE_DEBUG_JS_KEY : process.env.PARSE_PROD_JS_KEY,
    masterKey: process.env.DEBUG_ENV
      ? process.env.PARSE_DEBUG_MASTER_KEY
      : process.env.PARSE_PROD_MASTER_KEY,
    serverUrl: process.env.DEBUG_ENV
      ? process.env.PARSE_DEBUG_SERVER_URL
      : process.env.PARSE_PROD_SERVER_URL
  },
  sendGridKey: process.env.SENDGRID_KEY,
  twilio: {
    accountSid: process.env.TWILIO_ACCOUNT_SID,
    apiKey: process.env.TWILIO_API_KEY,
    apiSecret: process.env.TWILIO_API_SECRET,
    chatService: process.env.TWILIO_CHAT_SERVICE_SID,
    outgoingApplicationSid: process.env.TWILIO_TWIML_APP_SID,
    incomingAllow: process.env.TWILIO_ALLOW_INCOMING_CALLS === 'true'
  }
};
