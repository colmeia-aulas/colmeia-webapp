const { Address } = require('../models');

module.exports.find = async (req, res, next) => {
  const { id } = req.body;
  const { token } = req.session;

  try {
    const addresses = await Address.findUserAddresses(id, token);
    res.json(addresses);
  } catch (error) {
    const err = new Error('ADDRESS_FIND');
    err.originalError = error;
    next(err);
  }
};

module.exports.create = async (req, res, next) => {
  const data = {
    endereco: req.body.endereco,
    bairro: req.body.bairro,
    complemento: req.body.complemento,
    numero: req.body.numero,
    estado: req.body.estado,
    cidade: req.body.cidade
  };

  const { id } = req.session.user;
  const { token } = req.session;

  try {
    const address = await Address.create(data, id, token);
    res.json(address);
  } catch (error) {
    const err = new Error('ADDRESS_CREATE');
    err.originalError = error;
    next(err);
  }
};

module.exports.delete = async (req, res, next) => {
  const { id } = req.body;
  const { token } = req.session;

  try {
    const result = await Address.delete(id, token);
    res.json(result);
  } catch (error) {
    const err = new Error('ADDRESS_DELETE');
    err.originalError = error;
    next(err);
  }
};
