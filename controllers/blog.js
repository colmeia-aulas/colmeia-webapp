const moment = require('moment-timezone');
const showdown = require('showdown');

const showdownConverter = new showdown.Converter({ tables: true, strikethrough: true });

const { BlogPost } = require('../models');

moment()
  .tz('America/Sao_Paulo')
  .format('YYY/MMM/DD - HH:mm');
const monthsName = ['Jan', 'Fev', 'Mar', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'];

const previewColumns = ['postPath', 'previewImage', 'topic', 'title', 'previewText', 'postedAt'];

function getRandomInt(_min, _max) {
  const min = Math.ceil(_min);
  const max = Math.floor(_max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function formatDateForReadability(momentDate) {
  const date = moment(momentDate);
  const month = monthsName[date.month()];
  const year = date.year();
  const day = date.date();
  const postedAt = `${day} de ${month} de ${year}`;

  return postedAt;
}

async function getBlogPreviews(additionalPreviewConstraints) {
  const rawBlogPreviewData = await BlogPost.select(previewColumns, additionalPreviewConstraints);
  const blogPreviewData = [];

  if (rawBlogPreviewData.length) {
    rawBlogPreviewData.forEach(preview => {
      const postedAt = formatDateForReadability(preview.postedAt);

      const { postPath, previewImage, topic, title, previewText } = preview;

      blogPreviewData.push({
        topic,
        title,
        postPath,
        previewText,
        previewImageUrl: previewImage._url,
        postedAt
      });
    });
  }

  return blogPreviewData;
}

module.exports.main = async (req, res, next) => {
  try {
    const additionalPreviewConstraints = [
      { constraint: 'descending', value: 'postedAt' },
      { constraint: 'limit', value: 7 }
    ];

    const blogPreviewData = await getBlogPreviews(additionalPreviewConstraints);

    const count = await BlogPost.count();

    const previewsToSkip = 4;
    const additionalRandomPreviewConstraints = [
      { constraint: 'skip', value: getRandomInt(0, count - previewsToSkip) },
      { constraint: 'limit', value: previewsToSkip }
    ];

    const randomBlogPreviewData = await getBlogPreviews(additionalRandomPreviewConstraints);

    res.render('blog', { recentPreviews: blogPreviewData, recommendedPreviews: randomBlogPreviewData });
  } catch (error) {
    const err = new Error('BLOGPREVIEW_DESCRIPTION');
    err.originalError = error;
    next(err);
  }
};

module.exports.article = async (req, res, next) => {
  const axios = require('axios').default;
  const { id } = req.params;

  async function createPostContent(rawBlogPostData) {
    const { data: markdown } = await axios.get(rawBlogPostData.post._url);
    const blogPostData = {
      topic: rawBlogPostData.topic,
      title: rawBlogPostData.title,
      author: rawBlogPostData.author,
      description: rawBlogPostData.description,
      previewText: rawBlogPostData.previewText,
      previewImage: rawBlogPostData.previewImage._url,
      postPath: rawBlogPostData.postPath,
      html: showdownConverter.makeHtml(markdown),
      postedAt: formatDateForReadability(rawBlogPostData.postedAt)
    };

    console.log(markdown);

    return blogPostData;
  }

  try {
    const rawBlogPostData = await BlogPost.select(
      ['post', 'topic', 'title', 'author', 'previewText', 'previewImage', 'postPath', 'description', 'postedAt'],
      [{ constraint: 'equalTo', value: ['postPath', id] }]
    );

    if (rawBlogPostData.length === 1) {
      const blogPostData = await createPostContent(rawBlogPostData[0]);
      const count = await BlogPost.count();

      const previewsToSkip = 3;
      const additionalPreviewConstraints = [
        { constraint: 'skip', value: getRandomInt(0, count - previewsToSkip) },
        { constraint: 'limit', value: previewsToSkip }
      ];

      const randomBlogPreviewData = await getBlogPreviews(additionalPreviewConstraints);

      console.log(rawBlogPostData[0]);
      res.render('blog-post', { post: blogPostData, previews: randomBlogPreviewData });
    } else {
      res.status(404).redirect('/');
    }
  } catch (error) {
    const err = new Error('BLOGPOST_DESCRIPTION');
    err.originalError = error;
    next(err);
  }
};

module.exports.getBlogPostsUrls = async () => {
  const result = await BlogPost.select(['postPath']);
  const urls = result.map(row => row.postPath);
  return urls;
};
