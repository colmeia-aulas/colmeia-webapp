const { campaigns } = require('../data/pages.json');
const { Leads, Leads2, LeadCalculadora } = require('../models');

module.exports.setupCampaignPages = router => {
  Object.keys(campaigns).forEach(key => {
    router.get([key, `${key}/:end`], (req, res, next) => {
      if (req.params.end !== undefined && req.params.end !== 'obrigado') {
        return next(new Error('NOT_FOUND_GET'));
      }

      const page = req.params.end === 'obrigado' ? `${campaigns[key]}-obrigado` : campaigns[key];

      return res.render(page);
    });
  });

  return router;
};

module.exports.leadCalculadora = async (req, res, next) => {
  const data = {
    name: req.body.nome,
    email: req.body.email,
    phone: req.body.telefone,
    ano: req.body.ano,
    escola: req.body.escola,
    url: req.body.url,
    argumento: req.body.argumento
  };

  try {
    await LeadCalculadora.create(data);
    res.json({ success: true });
  } catch (error) {
    const err = new Error('LEAD_CALCULADORA');
    err.originalError = error;
    next(err);
  }
};

module.exports.leadAcompanhamento = async (req, res, next) => {
  const data = {
    name: req.body.nome,
    phone: req.body.telefone,
    ga_id: req.body.ga_id,
    origin: 'ACOMPANHAMENTO'
  };

  try {
    await Leads.create(data);
    res.json({ success: true });
  } catch (error) {
    const err = new Error('LEAD_ACOMPANHAMENTO');
    err.originalError = error;
    next(err);
  }
};

module.exports.lead = async (req, res, next) => {
  console.log(`Lead: ${JSON.stringify(req.body, null, 2)}`);

  const data = {
    name: req.body.nome,
    phone: req.body.telefone,
    email: req.body.email,
    info: req.body.info,
    ga_id: req.body.ga_id,
    origin: req.body.origin,
    navigator: req.body.navigator,
    url: req.body.url,
    lessonType: req.body.lessonType,
    comoConheceu: req.body.comoConheceu,
    school: req.body.school,
    schoolYear: req.body.schoolYear,
    leadType: req.body.leadType
  };

  try {
    await Leads.create(data);
    await Leads2.create(data);
    res.json({ success: true });
  } catch (error) {
    const err = new Error('LEAD');
    err.originalError = error;
    next(err);
  }
};

module.exports.setupCampaignPagesWithId = (req, res, next) => {
  const scriptHead = id =>
    `<!-- Google Tag Manager -->
          <script>
              (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
              new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
              j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
              'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
              })(window,document,'script','dataLayer','${id}');
          </script>
          `;
  const scriptBody = id =>
    `<!-- Google Tag Manager (noscript) -->
          <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=${id}"
          height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
          <!-- End Google Tag Manager (noscript) -->        
          `;

  const data = {
    acompanhamento: {
      keys: {
        'sao-paulo': {
          name: ' em São Paulo',
          phone: '(11) 4949-0115',
          scriptHead: scriptHead('GTM-N6TNXN2'),
          scriptBody: scriptBody('GTM-N6TNXN2')
        },
        brasilia: {
          name: ' em Brasília',
          phone: '(61) 9 9805-2073',
          scriptHead: scriptHead('GTM-N6TNXN2'),
          scriptBody: scriptBody('GTM-N6TNXN2')
        }
      }
    },
    aula: {
      keys: {
        matematica: {
          name: ' de Matemática',
          phone: '(61) 9 9805-2073',
          scriptHead: scriptHead('GTM-N6TNXN2'),
          scriptBody: scriptBody('GTM-N6TNXN2')
        },
        portugues: {
          name: ' de Português',
          phone: '(61) 9 9805-2073',
          scriptHead: scriptHead('GTM-N6TNXN2'),
          scriptBody: scriptBody('GTM-N6TNXN2')
        },
        'sao-paulo': {
          name: ' em São Paulo',
          phone: '(11) 4949-0115',
          scriptHead: scriptHead('GTM-N6TNXN2'),
          scriptBody: scriptBody('GTM-N6TNXN2')
        },
        brasilia: {
          name: ' em Brasília',
          phone: '(61) 9 9805-2073',
          scriptHead: scriptHead('GTM-N6TNXN2'),
          scriptBody: scriptBody('GTM-N6TNXN2')
        },
        online: {
          name: ' online',
          type: 'aula-particular',
          phone: '(11) 4949-0115',
          scriptHead: scriptHead('GTM-N6TNXN2'),
          scriptBody: scriptBody('GTM-N6TNXN2')
        }
      },
      title: 'aulas particulares',
      cardTitle: 'Aulas particulares'
    },
    reforco: {
      keys: {
        brasilia: {
          name: ' em Brasília',
          phone: '(61) 9 9805-2073',
          scriptHead: scriptHead('GTM-N6TNXN2'),
          scriptBody: scriptBody('GTM-N6TNXN2')
        },
        online: {
          name: ' online',
          type: 'reforco-escolar',
          phone: '(11) 4949-0115',
          scriptHead: scriptHead('GTM-N6TNXN2'),
          scriptBody: scriptBody('GTM-N6TNXN2')
        }
      },
      title: 'reforço escolar',
      cardTitle: 'Aulas de reforço'
    },
    material: {
      keys: {
        download: {
          name: ' - Crie uma rotina eficiente',
          phone: '(61) 9 9805-2073',
          scriptHead: scriptHead('GTM-N6TNXN2'),
          scriptBody: scriptBody('GTM-N6TNXN2')
        }
      },
      title: 'material de apoio',
      cardTitle: 'Material de apoio'
    }
  };

  let selectedData;

  const path = req.baseUrl + req.path;

  if (path.indexOf('reforco-escolar') !== -1) {
    selectedData = data.reforco;
  } else if (path.indexOf('acompanhamento-escolar') !== -1) {
    selectedData = data.acompanhamento;
  } else if (path.indexOf('ebook') !== -1) {
    selectedData = data.material;
  } else {
    selectedData = data.aula;
  }

  const ids = Object.keys(selectedData.keys);
  let { id } = req.params;

  // todas as páginas não possuem mais aula grátis
  let abTest = true;

  if (id === 'brasilia-1') {
    id = 'brasilia';
    abTest = true;
  }

  if (req.path.indexOf('cartao') !== -1) {
    abTest = false;
  }

  if ((req.params.end !== undefined && req.params.end !== 'obrigado') || !ids.includes(id)) {
    return next(new Error('NOT_FOUND_GET'));
  }

  if (req.path.indexOf('cartao') !== -1 && req.params.id !== 'brasilia') {
    return next(new Error('NOT_FOUND_GET'));
  }

  let page;

  if (path.includes('acompanhamento-escolar')) {
    page = 'landing-pedagoga';
  } else if (path.includes('online')) {
    page = 'landing-aulaonline';
  } else if (path.includes('ebook-2')) {
    page = 'landing-ebook';
  }else {
    page = 'new-landing';
  }

  return res.render(page, {
    pageId: id,
    pageData: selectedData.keys[id],
    title: selectedData.title,
    cardTitle: selectedData.cardTitle,
    capitalize: s => s.charAt(0).toUpperCase() + s.slice(1),
    abTest
  });
};

module.exports.setupCampaignPageReforco = (req, res, next) => {
  if (req.params.end !== undefined && req.params.end !== 'obrigado') {
    return next(new Error('NOT_FOUND_GET'));
  }

  const scriptHead = id =>
    `<!-- Google Tag Manager -->
          <script>
              (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
              new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
              j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
              'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
              })(window,document,'script','dataLayer','${id}');
          </script>
          `;
  const scriptBody = id =>
    `<!-- Google Tag Manager (noscript) -->
          <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=${id}"
          height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
          <!-- End Google Tag Manager (noscript) -->        
          `;

  const data = {
    brasilia: {
      name: ' em Brasília',
      phone: '(61) 9 9805-2073',
      scriptHead: scriptHead('GTM-N6TNXN2'),
      scriptBody: scriptBody('GTM-N6TNXN2')
    }
  };

  const ids = Object.keys(data);
  const { id } = req.params;

  if (!ids.includes(id)) {
    return next(new Error('NOT_FOUND_GET'));
  }

  const page =
    req.params.end === 'obrigado'
      ? 'landing-campanha-reforco-obrigado'
      : 'landing-campanha-reforco';

  return res.render(page, {
    pageId: `reforco-${id}`,
    pageData: data[id]
  });
};
