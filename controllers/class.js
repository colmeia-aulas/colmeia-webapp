const fs = require('fs');
const helper = require('../helper/helper');
const { CONST } = require('../public/js/constantes.js');

const { Preco, Professor, Escola, Cartoes, Aulas } = require('../models');

module.exports.newAgendamento = (req, res) => {
  res.render('new-agendamento');
};

module.exports.buscaProfessores = async (req, res, next) => {
  // reseta dados da aula
  req.session.aula = {};

  const cidade = CONST.CIDADES[req.params.cidade];
  const bairro = CONST.BAIRROS[req.params.cidade][req.params.bairro];
  const topico = CONST.MATERIAS_ALL[req.params.materia];
  const ano = CONST.ANOS[req.params.ano];
  const nivel = CONST.ANOS_NIVEIS[req.params.ano];

  const tipo = req.params.materia === 'ingles' ? 'Línguas' : 'Escolar';

  const dados = {
    tipo,
    ano,
    nivel,
    topico,
    endereco: {
      bairro,
      cidade
    }
  };

  const variation = req.path.split('/')[1];
  let title = CONST.CHAMADAS[variation];

  let titleGoogle = title;
  let infoContainerH1 = title;
  let infoContainerH2 = title;
  let professoresHeader = 'Professores';
  let infoContainerH3 = `Colmeia - ${title} - ${cidade}`;
  let avaliacaoH2 = 'Veja como nossos alunos avaliaram nossas aulas';
  const avaliacaoH3 = `A melhor forma de aprender com ${title}.`;
  const bottomContainerH3 = `A melhor forma de aprender com ${title}.`;
  const simpleH2 = `Colmeia - ${title} de qualidade em sua casa.`;
  let reasonsH2;
  const reasonsP = `A Colmeia tem os melhores professores em toda ${cidade} para levar até você aulas de reforço escolar de qualidade para Ensino médio e Ensino fundamental.`;

  const tagsLocation = [cidade];
  const tagsProfessor = [cidade];

  if (topico !== 'Todas as matérias') {
    title += ` de ${topico}`;
    titleGoogle += ` | ${topico}`;
    infoContainerH1 += ` de ${topico}`;
    infoContainerH3 += ` - ${topico}`;
    avaliacaoH2 += ` de ${topico}`;
    professoresHeader += ` de ${topico}`;
    tagsProfessor.push(topico);
  }

  infoContainerH3 += ` - ${ano}`;

  tagsProfessor.push(ano);

  professoresHeader += ` em ${cidade}`;

  if (bairro !== 'Todos os bairros') {
    title += ` no bairro ${bairro}`;
    titleGoogle += ` | ${bairro}`;
    infoContainerH1 += ` no bairro ${bairro}`;
    infoContainerH2 += ` no bairro ${bairro}`;
    infoContainerH3 += ` - ${bairro} - ${CONST.CHAMADAS[variation]} em ${bairro}`;
    avaliacaoH2 += ` em ${bairro}`;
    reasonsH2 = `Nós estamos no bairro ${bairro} e em toda ${cidade} para te atender.`;
    tagsLocation.push(bairro);
    tagsProfessor.push(bairro);
  } else {
    title += ` em ${cidade}`;
    titleGoogle += ` | ${cidade}`;
    infoContainerH2 += ` em ${cidade}`;
    avaliacaoH2 += ` em ${cidade}`;
    reasonsH2 = `Nós estamos em toda ${cidade} para te atender.`;
  }

  if (variation === 'professor-particular') {
    infoContainerH2 += ' com segurança e conforto na sua casa.';
  } else {
    infoContainerH2 += ' na sua casa com professores selecionados.';
  }

  infoContainerH1 += ` em ${cidade}.`;

  const pageTitle = `👉 ${titleGoogle} - Colmeia`;

  const description = `${title}. Aulas para Ensino Fundamental e Médio com os MELHORES professores. Agende aulas por R$ 59,90/h!`;

  const url = `${req.protocol}://${req.get('host')}${req.originalUrl}`;

  const tagsBottomBairro = [];
  const tagsBottomMateria = [];
  const tagsBottomAnos = [];
  const tagsBottomCidade = [];

  const keys = Object.keys(CONST.BAIRROS[req.params.cidade]);
  const index = keys.indexOf(req.params.bairro);

  for (let i = 0; i < 6; i += 1) {
    let j = (index + i + 1) % keys.length;

    if (keys[j] === 'todos-os-bairros') {
      j = (index + i + 1 + 1) % keys.length;
    }

    const bairroName = CONST.BAIRROS[req.params.cidade][keys[j]];

    const link = `/${variation}/${req.params.materia}/${req.params.cidade}/${keys[j]}/${
      req.params.ano
    }`;

    tagsBottomBairro.push({
      title: bairroName,
      link
    });
  }

  Object.keys(CONST.MATERIAS_ALL).forEach(materia => {
    if (materia !== 'todas-as-materias') {
      const materiaName = CONST.MATERIAS_ALL[materia];

      // if (materia === 'ingles' && req.params.materia !== 'ingles') {
      //   selectedNivel = 'basico';
      // }

      // if (materia !== 'ingles' && req.params.materia === 'ingles') {
      //   selectedNivel = 'fundamental';
      // }

      const link = `/${variation}/${materia}/${req.params.cidade}/${req.params.bairro}/${
        req.params.ano
      }`;

      tagsBottomMateria.push({ title: materiaName, link });
    }
  });

  Object.keys(CONST.ANOS).forEach(ano => {
    const anoName = CONST.ANOS[ano];

    const link = `/${variation}/${req.params.materia}/${req.params.cidade}/${
      req.params.bairro
    }/${ano}`;

    tagsBottomAnos.push({ title: anoName, link });
  });

  Object.keys(CONST.CIDADES).forEach(cid => {
    const cidadeName = CONST.CIDADES[cid];

    const link = `/${variation}/${req.params.materia}/${cid}/todos-os-bairros/${req.params.ano}`;

    tagsBottomCidade.push({ title: cidadeName, link });
  });

  try {
    const [precos, professores] = await Promise.all([
      Preco.getAllPrices(),
      Professor.buscaAulas(dados)
    ]);

    res.render('professores', {
      title: pageTitle,
      professores,
      // dados: dados,
      precos,
      description,
      url,
      professoresHeader,
      infoContainerH1,
      infoContainerH2,
      infoContainerH3,
      avaliacaoH2,
      avaliacaoH3,
      simpleH2,
      bottomContainerH3,
      reasonsH2,
      reasonsP,
      tagsBottomBairro,
      tagsBottomMateria,
      tagsBottomAnos,
      tagsBottomCidade,
      tagsLocation,
      tagsProfessor,
      topico,
      cidade,
      bairro
    });
  } catch (error) {
    const err = new Error('PROFESSORES_EXTERNO');
    err.originalError = error;
    next(err);
  }
};

module.exports.selectTime = (req, res) => {
  // reseta novamente dados de aula.
  req.session.aula = {};

  req.session.aula.ano = CONST.ANOS[req.body.ano]; // nome regular
  req.session.aula.topico = CONST.MATERIAS_ALL[req.body.topico]; // nome regular
  req.session.aula.ensino = CONST.ANOS_NIVEIS[req.body.ano];
  req.session.aula.tipo = req.body.tipo;
  req.session.aula.cidade = req.body.cidade;
  req.session.aula.bairro = req.body.bairro;
  req.session.aula.professor = req.body.professor;
  req.session.aula.agendamentoUrl = req.body.url;

  req.session.aula.dia = `${req.body.dia} ${req.body.hora}`;
  req.session.aula.duracao = req.body.duration;

  res.sendStatus(200);
};

module.exports.summary = async (req, res, next) => {
  const { tipo, cidade } = req.session.aula;
  const { token } = req.session;

  try {
    const escolasResponse = await Escola.find(
      {
        where: {
          cidade
        }
      },
      ['name'],
      token
    );
    const escolas = escolasResponse.map(e => e.name);

    // does not have price
    if (!req.session.aula.discountCode) {
      const { id } = req.session.user;
      const { preco, hasEscolaAno } = await Preco.getPrice(tipo, id, token);

      req.session.aula.preco = preco;
      req.session.aula.precoBase = preco;
      req.session.aula.resumo = true;
      req.session.aula.hasEscolaAno = hasEscolaAno;
    }

    res.render('resumo', {
      dados: req.session.aula,
      escolas
    });
  } catch (error) {
    const err = new Error('RESUMO');
    err.originalError = error;
    next(err);
  }
};

module.exports.verifyCode = async (req, res, next) => {
  const data = {
    userID: req.session.user.id,
    type: req.body.type,
    code: req.body.code,
    issuer: req.body.issuer
  };
  const { token } = req.session;
  try {
    const result = await Preco.getPriceWithCode(data, token);
    if (result.price) {
      // verifica desconto ativo
      if (req.session.aula.preco < result.price) {
        res.json({
          success: false,
          error: {
            message: 'Você já possui um desconto superior ativo.'
          }
        });
      } else {
        req.session.aula.discountCode = req.body.code;
        req.session.aula.preco = result.price;
        res.json({ price: result.price });
      }
    } else if (result.error) {
      res.json({ error: 'Este código expirou ou é inválido.' });
    }
  } catch (error) {
    if (String(error.code) === '141') {
      res.json({ error: error.message });
    } else {
      const err = new Error('VERIFY_CODE');
      err.originalError = error;
      next(err);
    }
  }
};

module.exports.scheduleClass = async (req, res, next) => {
  const imgFile = req.files[0]
    ? {
        originalName: req.files[0].originalName,
        size: req.files[0].size,
        base64: Buffer.from(fs.readFileSync(req.files[0].path)).toString('base64'),
        mimetype: req.files[0].mimetype,
        filename: req.files[0].filename
      }
    : null;

  // Verifica se algum cartão foi selecionado
  if (!req.body.cardId) {
    return res.json({
      success: false,
      message: 'Por favor, selecione um cartão de crédito.'
    });
  }

  const dados = {};
  const slots = helper.calculateTimeSlots(req.session.aula.dia, req.session.aula.duracao);
  const preco = req.session.aula.preco * req.session.aula.duracao;
  const { id } = req.session.user;
  const { token } = req.session;

  // veio da req.body
  dados.cardId = req.body.cardId;
  dados.enderecoId = req.body.endId;
  dados.observacao = req.body.observacao;
  dados.nomeEscola = req.body.nomeEscola;
  dados.anoEscola = req.body.anoEscola;
  dados.cidade = req.body.cidade;

  dados.professor = {
    id: req.session.aula.professor.id,
    userId: req.session.aula.professor.userId
  };

  dados.topico = req.session.aula.topico;
  dados.ano = req.session.aula.ano;
  dados.ensino = req.session.aula.ensino;
  dados.tipo = req.session.aula.tipo;
  dados.duracao = req.session.aula.duracao;
  dados.dia = req.session.aula.dia;
  dados.horarios = slots;
  dados.preco = preco;
  dados.discount = req.session.aula.discountCode;
  dados.image = imgFile;

  try {
    const response = await Aulas.create(dados, id, token);
    if (response.error) {
      const error = JSON.parse(response.error.message);
      return res.json({ success: false, message: error.message });
    }

    req.session.aula = null;

    res.json(response);
  } catch (error) {
    const err = new Error('MARCAR_AULA');
    err.originalError = error;
    next(err);
  } finally {
    if (imgFile) fs.unlinkSync(req.files[0].path);
  }
};

module.exports.requestClassCancel = async (req, res, next) => {
  try {
    const { id } = req.body;
    const { token } = req.session;
    const result = await Aulas.requestCancel(id, token);
    if (result.error === undefined) {
      res.json({ success: true });
    } else {
      // payed cancel
      res.json({
        success: false,
        ...result.error
      });
    }
  } catch (error) {
    const err = new Error('CANCELAR_AULA');
    err.originalError = error;
    next(err);
  }
};

module.exports.cancelClass = async (req, res, next) => {
  try {
    const { id } = req.body;
    const { token } = req.session;
    const result = await Aulas.confirmCancel(id, token);
    if (result.error === undefined) {
      res.json({ success: true });
    } else {
      const err = new Error('CANCELAR_AULA_CONFIRMAR');
      err.originalError = result.error;
      next(err);
    }
  } catch (error) {
    const err = new Error('CANCELAR_AULA_CONFIRMAR');
    err.originalError = error;
    next(err);
  }
};
