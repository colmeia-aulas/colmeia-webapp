const { pages } = require('../data/pages.json');

module.exports.setupLandingPages = router => {
  Object.keys(pages).forEach(key => {
    router.get(key, (req, res) => res.render(pages[key]));
  });

  return router;
};
