const axios = require('axios').default;

module.exports.get = (req, res, next) => {
  return res.render('formulario');
};

module.exports.sendToGoogleSheets = async (req, res, next) => {
  const {
    email,
    faixaEtaria,
    sexo,
    naoPoderUsarColmeiaEh,
    entendendoRespostaANaoPoderUsarColmeiaEh,
    casoColmeiaNaoEstivesseDisponivel,
    beneficioDaColmeia,
    pessoaBeneficiadaPelosServicosDaColmeia,
    melhoriasAColmeia,
    id
  } = req.body;

  const baseURL =
    'https://script.google.com/macros/s/AKfycbyhIPdW7pL_LEph53wZxa4u9iIHRCbnWUu5mWu1z07htW-9EmY/exec';
  /* const googleSheetsEndpoint = `${baseURL}?email=${email}&faixaEtaria=${faixaEtaria}&sexo=${sexo}&naoPoderUsarColmeiaEh=${naoPoderUsarColmeiaEh}&entendendoRespostaANaoPoderUsarColmeiaEh=${entendendoRespostaANaoPoderUsarColmeiaEh}&casoColmeiaNaoEstivesseDisponivel=${casoColmeiaNaoEstivesseDisponivel}&beneficioDaColmeia=${beneficioDaColmeia}&pessoaBeneficiadaPelosServicosDaColmeia=${pessoaBeneficiadaPelosServicosDaColmeia}&melhoriasAColmeia=${melhoriasAColmeia}&id=${id}&action=insert`;

  console.log(googleSheetsEndpoint);
  const result = await axios.get(googleSheetsEndpoint); */
  const result = await axios.post(baseURL, {
    email,
    faixaEtaria,
    sexo,
    naoPoderUsarColmeiaEh,
    entendendoRespostaANaoPoderUsarColmeiaEh,
    casoColmeiaNaoEstivesseDisponivel,
    beneficioDaColmeia,
    pessoaBeneficiadaPelosServicosDaColmeia,
    melhoriasAColmeia,
    id
  });
  console.log(result);
  res.json({ success: true });
};

module.exports.sendStreamFeedback = async (req, res, next) => {
  const { streamAvaliation, feedback, username, roomName, errors } = req.body;
  const baseURL = 'https://script.google.com/macros/s/AKfycbz3zA1u4RbNQM5DGBCAo2F3SG-xj7hFxaqc5Dlihgsw2tembQH8/exec';

  try {
    const result = await axios.post(baseURL, {
      avaliation: streamAvaliation,
      feedback,
      username,
      roomName,
      errors: errors || 'nenhum erro'
    });

    res.json({ success: result.statusText === 'OK' });
  } catch (error) {
    const err = new Error('FEEDBACK_AULAONLINE');
    err.originalError = error;
    next(err);
  }
};

module.exports.restrictLP = (req, res, next) => {
  const scriptHead = id =>
    `<!-- Google Tag Manager -->
      <script>
          (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
          new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
          j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
          'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
          })(window,document,'script','dataLayer','${id}');
      </script>
    `;
  const scriptBody = id =>
    `<!-- Google Tag Manager (noscript) -->
      <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=${id}"
      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
      <!-- End Google Tag Manager (noscript) -->        
    `;

  try {
    return res.render('lp-comunidade-de-estudos', {
      pageData: {
        phone: '(11) 4949-0115',
        scriptHead: scriptHead('GTM-N6TNXN2'),
        scriptBody: scriptBody('GTM-N6TNXN2')
      }
    });
  } catch (error) {
    const err = new Error('LP_PAGINA_RESTRITA');
    err.originalError = error;
    next(err);
  }
};
