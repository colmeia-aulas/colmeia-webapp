const path = require('path');
const sharp = require('sharp');
const rp = require('request-promise');
const sgMail = require('@sendgrid/mail');

const config = require('../config/vars');

const calculadoraUtil = require('../util/calculadora');
const { cacheTime } = require('../config/app');

const { videoToken } = require('../config/twilio-token');

const {
  NovoCadastroSite,
  NovoCadastroIdiomasSite,
  Job,
  EmailMarketing,
  InscritosNewsletter
} = require('../models');
const { userInfo } = require('os');

sgMail.setApiKey(config.sendGridKey);

const scriptHead = id =>
  `<!-- Google Tag Manager -->
      <script>
          (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
          new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
          j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
          'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
          })(window,document,'script','dataLayer','${id}');
      </script>
    `;
const scriptBody = id =>
  `<!-- Google Tag Manager (noscript) -->
      <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=${id}"
      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
      <!-- End Google Tag Manager (noscript) -->        
    `;

module.exports.dataStudio = (req, res) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
  res.header('X-Frame-Options', 'ALLOW-FROM https://datastudio.google.com');

  res.sendFile(path.join(__dirname, '/../data/todos.html'));
};

module.exports.emailNovoAgendamento = async (req, res, next) => {
  const {
    materia,
    conteudo,
    escola,
    ano,
    transtorno,
    dias,
    horarios,
    cidade,
    bairro,
    nome,
    email,
    telefone,
    origem
  } = req.body.data;

  try {
    await NovoCadastroSite.create({
      materia,
      conteudo,
      escola,
      ano,
      transtorno,
      dias,
      horarios,
      cidade,
      bairro,
      nome,
      email,
      telefone,
      origem
    });

    const html = `
    <div>
    <strong>SOLICITAÇÃO DE AGENDAMENTO</strong>
    <br />
    <br />

    Cliente: ${nome}
    <br />
    Email: ${email}
    <br />
    Telefone: ${telefone}
    <br />
    De onde veio: ${origem}
    <br />
    <br />

    Cidade: ${cidade}
    <br />
    Bairro: ${bairro}
    <br />
    <br />

    Matéria: ${materia}
    <br />
    Conteúdo: ${conteudo}
    <br />
    Transtorno: ${transtorno}
    <br />
    <br />

    Dias: ${dias.join(', ')}
    <br />
    Horários: ${horarios.join(', ')}
    <br />
    <br />

    Escola: ${escola}
    <br />
    Ano: ${ano}
    <br />

    </div>
    `;

    const msg = {
      to: config.debug ? 'victorlandim5@gmail.com' : 'relacionamento@aulascolmeia.com.br',
      from: 'contato@aulascolmeia.com.br',
      subject: 'SOLICITAÇÃO DE AGENDAMENTO',
      text: ' ',
      html
    };

    sgMail.send(msg);

    return res.json({ success: true });
  } catch (error) {
    const err = new Error('NOVO_AGENDAMENTO_SITE');
    err.originalError = error;
    next(err);
  }
};

module.exports.emailNovoAgendamentoIdiomas = async (req, res, next) => {
  const { idiomas, horarios: turnos, nome, email, telefone, empresa } = req.body.data;

  try {
    await NovoCadastroIdiomasSite.create({
      idiomas,
      turnos,
      nome,
      email,
      telefone,
      empresa
    });

    const html = `
    <div>
    <strong>SOLICITAÇÃO DE AGENDAMENTO DE IDIOMAS</strong>
    <br />
    <br />

    Cliente: ${nome}
    <br />
    Email: ${email}
    <br />
    Telefone: ${telefone}
    <br />
    Empresa: ${empresa}
    <br />
    <br />

    ${idiomas
      .map(
        e =>
          `
        Idioma: ${e.idioma}
        <br />
        Nivel: ${e.niveis.join(', ')}
        <br />
        <br />
      `
      )
      .join(' ')}

    Turnos: ${turnos.join(', ')}
    <br />
    <br />

    </div>
    `;

    const msg = {
      to: config.debug ? 'victorlandim5@gmail.com' : 'relacionamento@aulascolmeia.com.br',
      from: 'contato@aulascolmeia.com.br',
      subject: 'SOLICITAÇÃO DE AGENDAMENTO IDIOMAS',
      text: ' ',
      html
    };

    sgMail.send(msg);

    return res.json({ success: true });
  } catch (error) {
    const err = new Error('NOVO_AGENDAMENTO_SITE');
    err.originalError = error;
    next(err);
  }
};

module.exports.privacyPolicy = (req, res) =>
  res.sendFile(path.join(__dirname, '../public/pdfviewer/web', 'viewer.html'));

module.exports.calculateScorePAS = (req, res) => {
  const { pas1, pas2, pas3 } = req.body;

  if (
    pas1.escore == 'NaN' ||
    pas1.redacao == 'NaN' ||
    pas2.escore == 'NaN' ||
    pas2.redacao == 'NaN' ||
    pas3.escore == 'NaN' ||
    pas3.redacao == 'NaN'
  ) {
    res.json({ success: false });
  } else {
    const argumento = calculadoraUtil.calculateArg(pas1, pas2, pas3);
    res.json({ success: true, argumento });
  }
};

module.exports.favicon = (req, res) => {
  const favicon = Buffer.from(
    `iVBORw0KGgoAAAANSUhEUgAAACAAAAAcCAMAAAA3HE0QAAAAM1BMVEX////0fyD0fyD0
          fyD0fyD0fyD0fyD0fyD0fyD0fyD0fyD0fyD0fyD0fyD0fyD0fyD0fyDrogAZAAAAEHRS
          TlMAECAwQFBgcICQoLDA0ODwVOCoyAAAALBJREFUeF6FU1kShSAMY1MRauX+p31OfRJt
          mSFfZBpKuuD+SKSQ3BdHU+BvfGsG2zvuTys4vQP2NsCOeJBA7jw1QegCUim5Cejhi9DV
          Ol7eFw7tGKVmIdKYShcYPjMuVEh1qUVOYdyN4lxErtIs4l0ieydSCxoLlot7FsHwiQLH
          UZmE9yCk6DKRrnapaZRoE6Rotdkw1sMCVgzLjBvtp/HCwFaYr9x8aedrP/8406/3AyKV
          Hx/nEzMmAAAAAElFTkSuQmCC`,
    'base64'
  );
  res.statusCode = 200;
  res.setHeader('Content-Length', favicon.length);
  res.setHeader('Content-Type', 'image/x-icon');
  res.setHeader('Cache-Control', `public, max-age=cacheTime`);
  res.setHeader('Expires', new Date(Date.now() + cacheTime * 1000).toUTCString());
  res.end(favicon);
};

module.exports.resizeImage = async (req, res, next) => {
  const { url } = req.body;
  try {
    const b = await rp(url, { encoding: null });
    const buff = await sharp(b)
      .resize(70, 70)
      .toFormat('png')
      .toBuffer();

    res.json({ success: true, buffer: buff.toString('base64') });
  } catch (error) {
    const err = new Error('RESIZE_IMG');
    err.originalError = error;
    next(err);
  }
};

module.exports.emailMarketing = async (req, res, next) => {
  const data = {
    name: req.body.name,
    ga_id: req.body.ga_id,
    email: req.body.email
  };

  try {
    await EmailMarketing.create(data);
    res.json({ success: true });
  } catch (error) {
    const err = new Error('EMAIL_MARKETING');
    err.originalError = error;
    next(err);
  }
};

module.exports.newsletter = async (req, res, next) => {
  const data = {
    nome: req.body.nome,
    email: req.body.email
  };

  try {
    await InscritosNewsletter.create(data);
    res.json({ success: true });
  } catch (error) {
    const err = new Error('NEWSLETTER');
    err.originalError = error;
    next(err);
  }
};

module.exports.jobController = async (req, res, next) => {
  const { id } = req.params;

  try {
    const jobData = await Job.find({
      where: {
        slug: id
      }
    });

    if (jobData.length === 1) {
      res.render('job-details', { data: jobData[0] });
    } else {
      res.status(404).redirect('/');
    }
  } catch (error) {
    const err = new Error('JOB_DESCRIPTION');
    err.originalError = error;
    next(err);
  }
};
module.exports.allJobsController = async (req, res, next) => {
  try {
    const jobData = await Job.find({
      where: {
        active: true
      }
    });

    res.render('jobs', { data: jobData });
  } catch (error) {
    const err = new Error('JOBS');
    err.originalError = error;
    next(err);
  }
};

module.exports.live = (req, res, next) => {
  try {
    res.render('aulao', {
      pageData: {
        phone: '(11) 4949-0115',
        scriptHead: scriptHead('GTM-N6TNXN2'),
        scriptBody: scriptBody('GTM-N6TNXN2')
      }
    });
  } catch (error) {
    const err = new Error('AULAO');
    err.originalError = error;
    next(err);
  }
};

module.exports.restrictPage = (req, res, next) => {
  const { id } = req.params;

  const videoData = [
    {
      img: '/img/01-card-barbara-biologia.png',
      embed: '3M9kIs2WpEc',
      data: '14/04/20',
      disciplina: 'Biologia',
      descricao: 'A professora Bárbara Akemi vai dar o contexto inicial sobre o coronavírus e depois vai apresentar algumas questões a respeito da temática vírus.'
    },
    {
      img: '/img/02-card-gabriel-matematica.png',
      embed: '5PfIKr2uYuY',
      data: '21/04/20',
      disciplina: 'Matemática',
      descricao: 'O professor Gabriel Mello dará aula de matemática com exemplos e exercícios aplicados com a temática coronavírus.'
    },
    {
      img: '/img/03-card-sayoanara-redacao.png',
      embed: 'TCXNY3nnmbI',
      data: '28/04/20',
      disciplina: 'Português',
      descricao: 'A professora Sayoanara Mihalache dará aula de redação sobre a temática coronavírus, competências avaliadas e como produzir uma redação nota 10.'
    },
    {
      img: '/img/04-card-allan-fisica.png',
      embed: '-nTc_X8vevM',
      data: '05/05/20',
      disciplina: 'Física',
      descricao: 'O professor Allan Luís dará aula na área de física sobre mecânica dinâmica e cinemática e abordará soluções ao coronavírus.'
    },
    {
      img: '/img/05-card-gustavo-quimica.png',
      embed: 'I4Jisp-KKZE',
      data: '12/05/20',
      disciplina: 'Química',
      descricao: 'O professor Gustavo Benevuto dará aula na área de química sobre a temática coronavírus trabalhando os conceitos de hidrocarbonetos, classificação de cadeias, classificação do carbono e função oxigenada álcool.'
    },
    {
      img: '/img/06-card-mariana-historia.png',
      embed: 'ULDDKv-8xIg',
      data: '19/05/20',
      disciplina: 'História',
      descricao: 'A professor Mariana Gonçalves dará aula na área de história sobre a temática coronavírus vinculando com revolta da vacina, gripe espanhola no Brasil e movimento sanitarista.'
    },
    {
      img: '/img/07-card-luis-quimica.png',
      embed: 'EgCeQDoBwzQ',
      data: '12/06/20',
      disciplina: 'Química',
      descricao: 'O professor Luís de Sousa abordará a área dos gases, falando sobre suas leis, transformações e as equações gerais dos gases ideais.',
      pdf: '/quimica-aulao3.pdf'
    },
    {
      img: '/img/08-card-sthefani-interpretacao.png',
      embed: 'ofKGWwLD6PI',
      data: '18/06/20',
      disciplina: 'Português',
      descricao: 'A professora Sthefani Silva trabalhará Língua Portuguesa nessa semana. Abordando os seguintes temas: Morfologia, interpretação e análise de texto.',
      pdf: '/interpretacao-aulao2.pdf'
    },
    {
      img: '/img/09-card-henrique-quimica.png',
      embed: 'CrCCoq1YBI4',
      data: '24/06/20',
      disciplina: 'Química',
      descricao: 'O professor Henrique do Nascimento trabalhará Radioatividade, abordando os seguintes temas: Transformações Nucleares, Conceitos fundamentais da radioatividade, Reações de fusão e fissão nuclear, Desintegração Radioativa e radioisótopos.'
    },
    {
      img: '/img/10-card-marcelo-geografia.png',
      embed: '3bdWPtHEWao',
      data: '01/07/20',
      disciplina: 'Geografia',
      descricao: 'O professor Marcelo Agner trabalhará Globalização, abordando os seguintes temas: "O que é a globalização?", "Fatores Condicionantes da globalização" e "A globalização como Fábula".'
    },
    {
      img: '/img/11-card-luis-quimica.png',
      embed: 'MNPA_gNaKr4',
      data: '08/07/20',
      disciplina: 'Química',
      descricao: 'O professor Luis de Sousa trabalhará Transformações Químicas, abordando: "Energia calorífica", "Calor de reação", "Entalpia", "Equações termoquímicas" e "Lei de Hess".'
    }
  ];

  const questionData = [
    {
      img: '/img/card-questoes-biologia.png',
      disciplina: 'Biologia',
      pdf: 'biologia'
    },
    {
      img: '/img/card-questoes-quimica.png',
      disciplina: 'Química',
      pdf: 'quimica'
    },
    {
      img: '/img/card-questoes-historia.png',
      disciplina: 'História',
      pdf: 'historia'
    },
    {
      img: '/img/card-questoes-portugues.png',
      disciplina: 'Português',
      pdf: 'portugues'
    }
  ];

  try {
    if (id) {
      return res.render('restrict-video', {
        pageData: {
          id,
          phone: '(11) 4949-0115',
          video: videoData,
          scriptHead: scriptHead('GTM-N6TNXN2'),
          scriptBody: scriptBody('GTM-N6TNXN2')
        }
      });
    }

    res.render('restrict-page', {
      pageData: {
        phone: '(11) 4949-0115',
        video: videoData,
        question: questionData,
        scriptHead: scriptHead('GTM-N6TNXN2'),
        scriptBody: scriptBody('GTM-N6TNXN2')
      }
    });
  } catch (error) {
    const err = new Error('PAGINA_RESTRITA');
    err.originalError = error;
    next(err);
  }
};

module.exports.twilioAuth = (req, res, next) => {
  function sendTokenResponse(token) {
    res.set('Content-Type', 'application/json');
    res.send(
      JSON.stringify({
        token: token.toJwt()
      })
    );
  }

  try {
    const { identity } = req.body;
    const { room } = req.body;
    const token = videoToken(identity, room, config);
    return sendTokenResponse(token);
  } catch (error) {
    const err = new Error('VIDEO-LESSON');
    err.originalError = error;
    next(err);
  }
};

module.exports.restrictPdf = (req, res, next) => {
  const questionData = {
    biologia: [
      {
        img: '/img/card-questoes-biologia.png',
        disciplina: 'Biologia I',
        pdf: '/biologia-aulao1.pdf',
        tema: 'Seres Vivos'
      },
      {
        img: '/img/card-questoes-biologia.png',
        disciplina: 'Biologia II',
        pdf: '/biologia-aulao2.pdf',
        tema: 'Seres vivos'
      }
    ],
    historia: [
      {
        img: '/img/card-questoes-historia.png',
        disciplina: 'História I',
        pdf: '/historia-aulao1.pdf',
        tema: 'Questões gerais'
      },
      {
        img: '/img/card-questoes-historia.png',
        disciplina: 'História II',
        pdf: '/historia-aulao2.pdf',
        tema: 'Questões gerais'
      }
    ],
    quimica: [
      {
        img: '/img/card-questoes-quimica.png',
        disciplina: 'Química I',
        pdf: '/quimica-aulao1.pdf',
        tema: 'Questões gerais'
      },
      {
        img: '/img/card-questoes-quimica.png',
        disciplina: 'Química II',
        pdf: '/quimica-aulao2.pdf',
        tema: 'Evidências e interpretação de transformações químicas'
      },
      {
        img: '/img/card-questoes-quimica.png',
        disciplina: 'Química III',
        pdf: '/quimica-aulao3.pdf',
        tema: 'Introdução a Radioatividade'
      }
    ],
    portugues: [
      {
        img: '/img/card-questoes-gramatica.png',
        disciplina: 'Português I',
        pdf: '/gramatica-aulao1.pdf',
        tema: 'Gramática'
      },
      {
        img: '/img/card-questoes-gramatica.png',
        disciplina: 'Português II',
        pdf: '/gramatica-aulao2.pdf',
        tema: 'Gramática'
      },
      {
        img: '/img/card-questoes-literatura.png',
        disciplina: 'Português III',
        pdf: '/literatura-aulao.pdf',
        tema: 'Literatura'
      },
      {
        img: '/img/card-questoes-interpretacao.png',
        disciplina: 'Português IV',
        pdf: '/interpretacao-aulao1.pdf',
        tema: 'Interpretação de Texto'
      },
      {
        img: '/img/card-questoes-interpretacao.png',
        disciplina: 'Português V',
        pdf: '/interpretacao-aulao2.pdf',
        tema: 'Morfologia, interpretação e dicas'
      }
    ]
  };

  const { id } = req.params;

  try {
    return res.render('restrict-pdf', {
      pageData: {
        id,
        phone: '(11) 4949-0115',
        question: questionData,
        scriptHead: scriptHead('GTM-N6TNXN2'),
        scriptBody: scriptBody('GTM-N6TNXN2')
      }
    });
  } catch (error) {
    const err = new Error('PAGINA_RESTRITA_LEITOR');
    err.originalError = error;
    next(err);
  }
};

module.exports.videoTest = (req, res) => {
  return res.render('video-test');
};

module.exports.cadastroOnline = (req, res, next) => {
  try {
    return res.render('cadastro-online', {
      pageData: {
        phone: '(11) 4949-0115',
        scriptHead: scriptHead('GTM-N6TNXN2'),
        scriptBody: scriptBody('GTM-N6TNXN2')
      }
    });
  } catch (error) {
    const err = new Error('CADASTRO-ONLINE');
    err.originalError = error;
    next(err);
  }
};

module.exports.clienteOnline = (req, res, next) => {
  try {
    return res.render('cliente-online', {
      pageData: {
        phone: '(11) 4949-0115',
        scriptHead: scriptHead('GTM-N6TNXN2'),
        scriptBody: scriptBody('GTM-N6TNXN2')
      }
    });
  } catch (error) {
    const err = new Error('CLIENTE-ONLINE');
    err.originalError = error;
    next(err);
  }
};

module.exports.curriculo = async (req, res, next) => {
  const { CONST } = require('../public/js/constantes.js');
  const { Professor, Rating, User } = require('../models');

  const { profId } = req.params;
  const professorData = await Professor.get(profId,
    ['nomeCompleto', 'topicos', 'anos', 'curriculo', 'image', 'createdAt', 'cidade', 'schedule']
  );

  function reduceRuleByDay(rules) {
    var result = { SU: [], MO: [], TU: [], WE: [], TH: [], FR: [], SA: [] };
    for (var i = 0; i < rules.length; i++) {
      const rule = rules[i],
        rrule = rule.RRULE,
        days = rrule.BYDAY,
        duration = rrule.DURATION,
        date =
          rule.DTSTART.iso === undefined
            ? rule.DTSTART + '/' + duration
            : rule.DTSTART.iso + '/' + duration;
      for (var j = 0; j < days.length; j++) {
        const day = days[j];
        result[day].push(date);
      }
    }
    return result;
  }

  const schedule = reduceRuleByDay(professorData.schedule.rules === undefined ? [] : professorData.schedule.rules);

  const professor = {
    nome: professorData.nomeCompleto,
    disciplinas: professorData.topicos,
    cidade: professorData.cidade,
    niveis: professorData.anos,
    curriculo: professorData.curriculo,
    imagem: professorData.image._url,
    desde: professorData.createdAt.getFullYear(),
    schedule,
    weekDays: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S']
  };

  const ratings = await Rating.find({
    where: {
      status: 'SHOWING',
      professor: {
        __type: 'Pointer',
        className: 'Professor',
        objectId: profId
      }
    }
  });

  const commentsData = await Promise.all(
    ratings.map(async rating => {
      if (!rating.comment || rating.comment.length < 3) return;
      const user = await User.get(rating.user.id, ['nome', 'image']);      
      const defaultImage = 'https://parsefiles.back4app.com/H5h9AeZtIyrDlVYX9hI1ODxM9npPa3qohJnH5H7a/12ad887e280f09dfbf7a2e3fe045e71c_user-icon.png';
      const image = user.image ? user.image._url : defaultImage;

      return {
        text: rating.comment,
        client: user.nome,
        image
      };
    })
  );

  try {
    return res.render('curriculo', {
      pageData: {
        phone: '(11) 4949-0115',
        scriptHead: scriptHead('GTM-N6TNXN2'),
        scriptBody: scriptBody('GTM-N6TNXN2'),
        professor,
        comments: commentsData.filter((comment) => comment != undefined)
      }
    });
  } catch (error) {
    const err = new Error('CURRICULO');
    err.originalError = error;
    next(err);
  }
};
