const { Cartoes, User } = require('../models');

module.exports.find = async (req, res, next) => {
  const { id } = req.body;
  const { token } = req.session;

  try {
    const cards = await Cartoes.getCreditCards(id, token);
    res.json(cards);
  } catch (error) {
    const err = new Error('CARTOES_FIND');
    err.originalError = error;
    next(err);
  }
};

module.exports.create = async (req, res, next) => {
  const hash = req.body.card_hash;
  const cpf = req.body.cpf;
  const name = req.session.user.nome;
  const phone = req.session.user.phone;
  const email = req.session.user.email;
  let moipId = req.session.user.moipId;

  const { id } = req.session.user;
  const { token } = req.session;

  try {
    if (!moipId) {
      const response = await User.createMoipClient(name, email, cpf, phone, id, token);

      if (response.success) {
        req.session.user.moipId = response.moipId;
        moipId = response.moipId;
        // TODO: Improve error handling
      } else if (response.code == 4001) {
        return res.json({ success: false, path: '/aluno/sair' });
      } else {
        return res.json({ success: false, message: 'Erro ao adicionar o cartão.' });
      }
    }

    const card = await Cartoes.create({ hash, moipId, id }, token);

    if (card.code == 4001) {
      res.json({ success: false, path: '/aluno/sair' });
    } else {
      res.json(card);
    }
  } catch (error) {
    const err = new Error('CARTOES_CREATE');
    err.originalError = error;
    next(err);
  }
};

module.exports.delete = async (req, res, next) => {
  try {
    const { id } = req.body;
    const { token } = req.session;
    const result = await Cartoes.delete(id, token);
    res.json(result);
  } catch (error) {
    const err = new Error('CARTOES_DELETE');
    err.originalError = error;
    next(err);
  }
};
