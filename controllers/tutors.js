const fs = require('fs');
const { faqContent } = require('../data/professores');
const { Professor, ProfessorCandidato, File } = require('../models');

module.exports.showHome = (req, res) => res.render('professor', { faqContent });
module.exports.showSimpleHome = (req, res) => res.render('seja-um-professor-simples');
module.exports.showSimpleHomeEnd = (req, res) => res.render('seja-um-professor-simples-finalizado');

module.exports.getProfessor = async (req, res) => {
  const { id } = req.params;
  const professor = await Professor.find({ where: { name_identifier: id } });

  if (!professor) {
    return res.status(404).redirect('/');
  }

  res.render('professor-details', { prof: professor[0] });
};

module.exports.showRegisterPage = async (req, res, next) => {
  try {
    const { id } = req.session.user;
    const { token } = req.session;
    const professorCandidatoId = await ProfessorCandidato.getUserProfessorCandidato(id, token);

    const response = await ProfessorCandidato.get(
      professorCandidatoId,
      [
        'finalizado',
        'nome',
        'passoCadastro',
        'telefone',
        'step',
        'username',
        'passoCadastro',
        'materiasIdiomas'
      ],
      token
    );

    if (
      response.finalizado == 'Sim' ||
      (response.step[0] == 'true' && response.step[1] == 'true' && response.step[3] == 'true')
    ) {
      if (response.passoCadastro == 'Pré aprovado') {
        if (response.step[4] == 'true' && response.step[5] == 'true') {
          res.render('cadastro-finalizado-cont', {
            response
          });
        } else {
          res.render('cadastro-professor-cont', {
            response
          });
        }
      } else {
        res.render('cadastro-finalizado', {
          response
        });
      }
    } else {
      // TODO: Add error handling
      res.render('cadastro-professor', { response });
    }
  } catch (error) {
    const err = new Error('CADASTRO_PROFESSOR_SHOW');
    err.originalError = error;
    next(err);
  }
};

module.exports.update = async (req, res, next) => {
  try {
    const { data, step } = req.body;
    const { id } = req.session.user;
    const { token } = req.session;
    const professorCandidatoId = await ProfessorCandidato.getUserProfessorCandidato(id, token);

    await ProfessorCandidato.update(professorCandidatoId, step, data);
    res.json({ success: true });
  } catch (error) {
    const err = new Error('CADASTRO_PROFESSOR_UPDATE');
    err.originalError = error;
    next(err);
  }
};

module.exports.updateFinal = async (req, res, next) => {
  if (!req.files) {
    const err = new Error('CADASTRO_PROFESSOR_FINAL_FILE');
    return next(err);
  }

  const { step } = req.body;

  if (step !== '1') {
    const err = new Error('CADASTRO_PROFESSOR_STEP');
    return next(err);
  }

  try {
    const { id } = req.session.user;
    const { token } = req.session;
    const professorCandidatoId = await ProfessorCandidato.getUserProfessorCandidato(id, token);

    const fileData = {
      filename: req.files[0].filename,
      base64: Buffer.from(fs.readFileSync(req.files[0].path)).toString('base64'),
      mimetype: req.files[0].mimetype
    };

    const curriculoArquivo = await File.create(fileData, token);

    const data = {
      nome: req.body.nome,
      nomeMae: req.body.nome_mae,
      nascimento: req.body.nascimento,
      cpf: req.body.cpf,
      rg: req.body.rg_numero,
      rgIssuer: req.body.rg_orgao,
      rgIssueDate: req.body.rg_data,
      comoConheceu: req.body.como_conheceu,
      carro: req.body.possui_carro,
      endereco: req.body.logradouro,
      endereco_cidade: req.body.cidade,
      addressStreetNumber: req.body.numero,
      addressDistrict: req.body.bairro,
      addressState: req.body.estado,
      addressZipCode: req.body.cep,
      finalizado: 'Sim',
      curriculoArquivo
    };

    await ProfessorCandidato.updateFinal(professorCandidatoId, data);

    fs.unlinkSync(req.files[0].path);

    res.json({ success: true });
  } catch (error) {
    const err = new Error('CADASTRO_PROFESSOR_FINAL');
    err.originalError = error;
    next(err);
  }
};
