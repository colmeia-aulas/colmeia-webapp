const fs = require('fs');
const { User, Address, Aulas, Cartoes, PreCadastro, File } = require('../models');

module.exports.showLogin = (req, res) => {
  if (!req.session.nextPage) {
    req.session.nextPage = '/';
  }
  res.render('login', { next: req.session.nextPage });
};

module.exports.showRegister = (req, res) => {
  if (!req.session.nextPage) {
    req.session.nextPage = '/';
  }

  res.render('registrar', { next: req.session.nextPage });
};

module.exports.showCadastroExpress = (req, res) => res.render('cadastro-express');
module.exports.showPasswordReset = (req, res) => res.render('recuperar-senha');

module.exports.showProfile = (req, res) => res.render('perfil');
module.exports.showSchedule = (req, res) => res.render('agenda');
module.exports.showHistory = (req, res) => res.render('historico');

const login = (email, password) => {
  return new Promise(async (resolve, reject) => {
    try {
      const userResponse = await User.login({ email, password });
      // await User.setCurrentUser(userResponse.token);
      const favoriteAddress = await Address.getFavoriteAddress(
        userResponse.user.id,
        userResponse.token
      );
      resolve({
        success: true,
        user: {
          ...userResponse.user,
          fav: favoriteAddress.favorite
        },
        token: userResponse.token
      });
    } catch (error) {
      reject(error);
    }
  });
};

module.exports.login = login;

module.exports.postLogin = async (req, res, next) => {
  const userData = {
    email: req.body.email,
    password: req.body.password
  };

  try {
    const loginResponse = await login(userData.email, userData.password);
    req.session.token = loginResponse.token;
    req.session.user = loginResponse.user;

    res.json(loginResponse);
  } catch (error) {
    const err = new Error('USER_LOGIN');
    err.originalError = error;
    next(err);
  }
};

module.exports.postRegister = async (req, res, next) => {
  const data = {
    email: req.body.email,
    username: req.body.email,
    password: req.body.password,
    telefone: req.body.telefone,
    nome: req.body.nome || '',
    ga_id: req.body.ga_id || '',
    digits_id: req.body.digits_id || '',
    url: req.body.url || '',
    tipoCliente: req.body.tipoCliente || undefined
  };

  try {
    await User.create(data);

    const loginResponse = await login(data.email, data.password);
    req.session.token = loginResponse.token;
    req.session.user = loginResponse.user;

    res.json(loginResponse);
  } catch (error) {
    let err = new Error('USER_REGISTER');

    if (String(error.code) === '202' || String(error.code) === '203') {
      err = new Error('USER_REGISTER_EMAIL_TAKEN');
    }
    err.originalError = error;
    next(err);
  }
};

module.exports.update = async (req, res, next) => {
  const { id } = req.session.user;
  const { token } = req.session;
  const { data } = req.body;

  try {
    await User.update(id, data, token);
    res.json({ success: true });
  } catch (error) {
    const err = new Error('USER_UPDATE');
    err.originalError = error;
    next(err);
  }
};

module.exports.postPasswordReset = async (req, res, next) => {
  try {
    const { email } = req.body;
    await User.resetPassord(email);
    res.json({ success: true });
  } catch (error) {
    const err = new Error('USER_PASSWORD_RESET');
    err.originalError = error;
    next(err);
  }
};

module.exports.postLogout = async (req, res, next) => {
  try {
    req.session.user = {};
    req.session.token = undefined;
    req.session.aula = {};
    req.session.nextPage = '/';

    res.redirect('/');
  } catch (error) {
    const err = new Error('USER_LOGOUT');
    err.originalError = error;
    next(err);
  }
};

module.exports.postProfilePicture = async (req, res, next) => {
  try {
    const imageData = {
      originalName: req.files[0].originalName,
      size: req.files[0].size,
      base64: Buffer.from(fs.readFileSync(req.files[0].path)).toString('base64'),
      mimetype: req.files[0].mimetype,
      filename: req.files[0].filename
    };

    const { id } = req.session.user;
    const { token } = req.session;

    const image = await File.create(imageData, token);

    await User.update(id, { image }, token);
    fs.unlinkSync(req.files[0].path);

    res.json({ success: true });
  } catch (error) {
    const err = new Error('USER_PICTURE');
    err.originalError = error;
    next(err);
  }
};

module.exports.getProfile = async (req, res, next) => {
  try {
    const { id } = req.session.user;
    const { token } = req.session;

    const response = Promise.all([
      User.get(id, ['nome', 'email', 'telefone', 'image']),
      Address.findUserAddresses(id, token),
      Cartoes.getFavoriteCreditCard(id, token)
    ]);

    const [perfil, enderecos, cartaoFavorito] = await response;
    res.json({
      success: true,
      perfil,
      enderecos,
      cartaoFavorito
    });
  } catch (error) {
    const err = new Error('USER_PROFILE');
    err.originalError = error;
    next(err);
  }
};
module.exports.getSchedule = async (req, res, next) => {
  const { id } = req.session.user;
  const { token } = req.session;

  try {
    const aulas = await Aulas.getAgenda(id, 'agenda', token);
    res.json({ success: true, aulas });
  } catch (error) {
    const err = new Error('AGENDA');
    err.originalError = error;
    next(err);
  }
};

module.exports.getHistory = async (req, res, next) => {
  const { id } = req.session.user;
  const { token } = req.session;

  try {
    const aulas = await Aulas.getAgenda(id, 'historico', token);
    res.json({ success: true, aulas });
  } catch (error) {
    const err = new Error('HISTORICO');
    err.originalError = error;
    next(err);
  }
};

module.exports.emailPreRegister = async (req, res, next) => {
  const { email } = req.body;

  try {
    await PreCadastro.create({ email });
    res.json({ success: true });
  } catch (error) {
    next(new Error({ type: 'USER_PREREGISTER', error }));
  }
};
