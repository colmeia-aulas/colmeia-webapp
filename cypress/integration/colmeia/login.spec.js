describe('Log in', () => {
  context('Unauthorized', () => {
    it('redirects to login page', () => {
      cy.visit('localhost:4000/reforco-escolar/resumo');

      cy.url().should('not.include', 'reforco-escolar/resumo');
    });
  });

  context('Authorized', () => {
    beforeEach(() => cy.visit('localhost:4000/aluno/entrar'));

    it('Logs in with correct credentials', () => {
      const user = 'matrpedreira@gmail.com';
      const password = 'colmeia123';

      cy.get('#login-email').type(user);
      cy.get('#login-senha').type(`${password}{enter}`);

      cy.url().should('not.include', 'aluno/entrar');
    });
    it('Does not log in with incorrect credentials', () => {
      const user = 'matrpedreira@gmail.com';
      const password = '---------';

      cy.get('#login-email').type(user);
      cy.get('#login-senha').type(`${password}{enter}`);

      cy.url().should('include', 'aluno/entrar');
    });
  });
});
