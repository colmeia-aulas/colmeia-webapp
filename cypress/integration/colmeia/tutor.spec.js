/* eslint-disable no-undef */
describe('Cadastro professor', () => {
  context('register', () => {
    // it('registers successfully with valid credentials', () => {
    //   cy.visit('localhost:4000/professor');

    //   cy.get('#check-termos').check();

    //   cy.get('#nome').type('Teste');
    //   cy.get('#email').type(`teste${Math.random()}@gmail.com`);
    //   cy.get('#telefone').type('61999999999');
    //   cy.get('#senha').type('senhasenha{enter}');

    //   cy.url().should('include', 'professor/cadastro');
    // });

    it('completes step 1', () => {
      cy.visit('localhost:4000/professor');

      cy.get('#check-termos').check();

      cy.get('#nome').type('Teste');
      cy.get('#email').type(`teste${Math.random()}@gmail.com`);
      cy.get('#telefone').type('61999999999');
      cy.get('#senha').type('senhasenha{enter}');

      cy.wait(6000);

      cy.get('.introduction-container button.orange-button').click();

      cy.get('input[name="radio-tipo-materia"]')
        .first()
        .check();
      cy.get('.materias-1 button.orange-button').click();

      cy.get('input[type="checkbox"][value="Matemática"]').check();
      cy.get('input[type="checkbox"][value="Português"]').check();
      cy.get('input[type="checkbox"][value="Biologia"]').check();

      cy.get('.materias-2 button.orange-button').click();

      cy.get('input[type="checkbox"][value="Fundamental"]').check();

      cy.get('.materias-3 button.orange-button').click();

      cy.get('.bairros-1 h2.greeting-heading')
        .should('be.visible')
        .and(
          'contain',
          'Na segunda etapa, vamos te perguntar algumas informações sobre onde tem interesse em atender e sobre seu deslocamento.'
        );

      cy.get('input[name="radio-cidade"]')
        .first()
        .check();

      cy.get('.bairros-1 button.orange-button').click();

      cy.get('.bairros-2 input[type="checkbox"][value="Asa Sul"]').check();
      cy.get('.bairros-2 input[type="checkbox"][value="Asa Norte"]').check();
      cy.get('.bairros-2 input[type="checkbox"][value="Taguatinga"]').check();
      cy.get('.bairros-2 button.orange-button').click();

      cy.get('.pessoal-1 h2.greeting-heading')
        .should('be.visible')
        .and(
          'contain',
          'Na etapa final, vamos te perguntar mais sobre você. Especialmente seus dados pessoais para o registro em nossa plataforma.'
        );

      cy.get('.pessoal-1 input[name="nome"]').type('TESTES TESTE DA SILVA');
      cy.get('.pessoal-1 button.orange-button').click();

      cy.get('.pessoal-2 input[name="nascimento"]').type('05031997');
      cy.get('.pessoal-2 button.orange-button').click();

      cy.get('.pessoal-3 input[name="cpf"]').type('05973624161');
      cy.get('.pessoal-3 button.orange-button').click();

      cy.get('.pessoal-4 input[name="rg_numero"]').type('3158045');
      cy.get('.pessoal-4 input[name="rg_orgao"]').type('SSP/DF');
      cy.get('.pessoal-4 input[name="rg_data"]').type('05973624161');
      cy.get('.pessoal-4 button.orange-button').click();

      cy.get('.pessoal-5 input[name="logradouro"]').type('SQN 104 BLOCO H');
      cy.get('.pessoal-5 input[name="numero"]').type('103');
      cy.get('.pessoal-5 input[name="cidade"]').type('Brasília');
      cy.get('.pessoal-5 select[name="estado"]').select('DF');
      cy.get('.pessoal-5 input[name="bairro"]').type('Asa Norte');
      cy.get('.pessoal-5 input[name="cep"]').type('70733080');
      cy.get('.pessoal-5 button.orange-button').click();

      cy.get('.pessoal-6 input[name="radio-carro"]')
        .first()
        .check();
      cy.get('.pessoal-6 button.orange-button').click();

      cy.get('.pessoal-7 input[name="nome_mae"]').type('Nome Mãe');
      cy.get('.pessoal-7 button.orange-button').click();

      cy.get('.pessoal-8 input[name="como_conheceu"]').type('Facebook');
      cy.get('.pessoal-8 button.orange-button').click();
    });
  });
});
