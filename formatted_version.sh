CURR_VERSION=$(cat colmeia.version);
# Incrementando versão do projeto
if [ "$CURR_VERSION" -lt "100" ]; then
  printf "%03d" $CURR_VERSION ;
else
  printf $CURR_VERSION;
fi
