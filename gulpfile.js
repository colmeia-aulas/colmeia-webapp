const gulp = require('gulp');
const sass = require('gulp-sass');
const browserSync = require('browser-sync').create();
const del = require('del');
const imagemin = require('gulp-imagemin');
const version = require('gulp-version-number');
const replace = require('gulp-replace');
const uglify = require('gulp-uglify');
const useref = require('gulp-useref');
const zip = require('gulp-zip');
const postCss = require('gulp-postcss');
const autoPrefixer = require('autoprefixer');
const cssNano = require('cssnano');

const source = {
  ejs: './views/**/*.ejs',
  js: './public/js/**/*.js',
  css: './public/css/**/*.css',
  sass: './react-web/src/sass/**/*.scss',
  sassMain: './react-web/src/sass/main.scss'
};

const dest = {
  css: './public/css/',
  ejs: './dist/views/.'
};

const ejs = ['./dist/views/**/*.ejs', '!./dist/views/**/seja-um-professor.ejs'];
const js = ['./dist/public/**/*.js', '!./dist/public/pdfviewer/**/*.js'];
const css = './dist/public/**/*.css';
const imgs = ['./public/img/**/*.png', './public/img/**/*.jpg'];

// inclusion must come before exclusion!
const files = [
  './**',
  '!node_modules/**',
  '!dist/**',
  '!test/**',
  '!script/**',
  '!gulp*.js',
  '!public/**',
  '!views/**',
  '!sass/**',
  '!sitemap-generator/**',
  '!docs/**',
  '!.vscode/**',
  '!.git/**',
  '!.gitignore',
  '!.env.example',
  // '!.env',
  '!*.zip',
  '!version'
];

// Compile sass into CSS & auto-inject into browsers
const sassTask = () =>
  gulp
    .src(source.sassMain)
    .pipe(sass())
    .on('error', sass.logError)
    .pipe(gulp.dest(dest.css))
    .pipe(browserSync.stream());

const reloadTask = () => browserSync.reload();

// Static Server + watching scss/html files
const watchTask = () => {
  browserSync.init(null, {
    proxy: 'localhost:4000'
  });

  sassTask();

  gulp.watch(source.sass, sassTask);
  gulp.watch(source.ejs, reloadTask);
  gulp.watch(source.js, reloadTask);
};

const cleanTask = () => del('dist');

const concatenateAllTask = () =>
  gulp
    .src(source.ejs, { dot: true })
    .pipe(
      useref({
        // fix directory reference
        transformPath: filePath => filePath.replace('views', 'public')
      })
    )
    .pipe(gulp.dest(dest.ejs));

const cleanOldPublicTask = () => del('dist/views/public');

const copyPublicFromConcatenateTask = () =>
  gulp.src('./dist/views/public/**', { dot: true }).pipe(gulp.dest('dist/public/.'));

const copyPublicTask = () =>
  gulp.src('./public/**', { dot: true }).pipe(gulp.dest('dist/public/.'));

const fixViewsAssetReferenceTask = () =>
  gulp
    .src('./dist/views/**', { dot: true })
    .pipe(replace('public/', '/'))
    .pipe(gulp.dest('./dist/views/.'));

const addVersioningToViewsTask = () =>
  gulp
    .src('./dist/views/**/*.ejs', { base: './' })
    .pipe(
      version({
        value: '%MDS%',
        append: {
          key: 'v',
          to: ['css', 'js']
        }
      })
    )
    .pipe(gulp.dest('./'));

const miniJsTask = () =>
  gulp
    .src(js, { base: './' })
    // .pipe(removeLog())
    // .pipe(gulp.dest('dist/.'))
    .pipe(uglify().on('error', e => console.log(e)))
    .pipe(gulp.dest('./'))
    .pipe(replace('.includes', '.indexOf')) // internet explorer bug
    .pipe(gulp.dest('./'));

const miniCssTask = () =>
  gulp
    .src(css, { base: './' })
    .pipe(postCss([autoPrefixer(), cssNano()]))
    .pipe(gulp.dest('./'));

// const miniEjsTask = () =>
//   gulp
//     .src(ejs, { base: './' })
//     .pipe(miniEjs())
//     .pipe(gulp.dest('./'));

const miniImgTask = () =>
  gulp
    .src(imgs, { base: './' })
    .pipe(imagemin())
    .pipe(gulp.dest('./dist/public/img/.'));

const transferFilesTask = () => gulp.src(files, { base: '.', dot: true }).pipe(gulp.dest('dist/.'));

const miniTask = gulp.series(
  concatenateAllTask,
  copyPublicFromConcatenateTask,
  cleanOldPublicTask,
  copyPublicTask,
  fixViewsAssetReferenceTask,
  addVersioningToViewsTask,
  gulp.parallel(miniJsTask, miniCssTask, miniImgTask),
  transferFilesTask
);

const zipTask = () =>
  gulp
    .src('dist/**/*.*', { dot: true })
    .pipe(zip('colmeia_website_dist.zip'))
    .pipe(gulp.dest('.'));

const deployTask = gulp.series(cleanTask, miniTask, zipTask, cleanTask);

exports.watch = watchTask;
exports.default = deployTask;
