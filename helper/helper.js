var moment = require('moment-timezone');
moment.tz.setDefault('America/Sao_Paulo');

module.exports = {
    // Calcula os slots da aula
    // startTime deve ser um objeto moment ou uma string no formato DD/MM/YYYY HH:mm
    // duration é o numero de slots(de 30 minutos) da aula
    calculateTimeSlots: function(startTime, duration) {
        if (!moment.isMoment(startTime))
            // Checa se startTime é um objeto moment
            time = moment(startTime, 'DD/MM/YYYY HH:mm');
        else time = startTime;

        var weekDay = time.day() + 1; // Domingo no moment é 0, ajusta pra 1
        var timeSlots = [];

        for (var i = 0; i <= duration * 2; i++) {
            var slot = weekDay + '/' + time.format('HH:mm');
            timeSlots.push(slot);
            time.add(30, 'm');
        }

        return timeSlots;
    },

    isJson: function(item) {
        item = typeof item !== 'string' ? JSON.stringify(item) : item;

        try {
            item = JSON.parse(item);
        } catch (e) {
            return false;
        }

        if (typeof item === 'object' && item !== null) {
            return true;
        }

        return false;
    }
};
