// this piece of could could be used in a future refactor
// it will promote better code reuse in the controller
// it will automatically catch error and forward them to the error handler

const asyncHandler = fn => (req, res, next) => Promise.resolve(fn(req, res, next)).catch(next);

module.exports = asyncHandler;
