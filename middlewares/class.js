const { CONST } = require('../public/js/constantes.js');

module.exports.validateParamsBusca = (req, res, next) => {
  if (req.params.ano === 'fundamental') {
    const url = req.originalUrl.replace('/fundamental', '/9o-ano-fundamental');
    return res.redirect(url);
  }

  if (req.params.ano === 'medio') {
    const url = req.originalUrl.replace('/medio', '/3o-ano-medio');
    return res.redirect(url);
  }

  if (req.params.cidade === 'goiania') {
    return next(new Error('INVALID_SEARCH_PARAMS'));
  }

  const topico = CONST.MATERIAS_ALL[req.params.materia];
  const ano = CONST.ANOS[req.params.ano];

  const title = CONST.CHAMADAS[req.path.split('/')[1]];

  const cidade = CONST.CIDADES[req.params.cidade];
  const bairro = CONST.BAIRROS[req.params.cidade][req.params.bairro];

  if (!cidade || !bairro || !topico || !ano || !title) {
    return next(new Error('INVALID_SEARCH_PARAMS'));
  }

  if (cidade && !CONST.BAIRROS[req.params.cidade][req.params.bairro]) {
    return next(new Error('INVALID_SEARCH_PARAMS'));
  }

  if (
    req.params.materia == 'ingles' &&
    req.params.nivel != 'basico' &&
    req.params.nivel != 'intermediario' &&
    req.params.nivel != 'avancado'
  ) {
    return next(new Error('INVALID_SEARCH_PARAMS'));
  }

  if (
    req.params.materia != 'ingles' &&
    (req.params.nivel == 'basico' ||
      req.params.nivel == 'intermediario' ||
      req.params.nivel == 'avancado')
  ) {
    return next(new Error('INVALID_SEARCH_PARAMS'));
  }

  return next();
};
