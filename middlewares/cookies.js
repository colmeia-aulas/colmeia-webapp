const isKeyInCookie = (key, redirectPath) => {
  return (req, res, next) => {
    if (req.session[key]) {
      next();
    } else if (redirectPath) {
      res.redirect(redirectPath);
    } else {
      res.json({ success: false, message: 'Você não possui permissão para acessar esta página.' });
    }
  };
};

module.exports.initializeCookies = (req, res, next) => {
  if (req.session.user === undefined) req.session.user = {};
  if (req.session.aula === undefined) req.session.aula = {};
  if (req.session.token === undefined) req.session.token = '';
  if (req.session.nextPage === undefined) req.session.nextPage = '/';
  next();
};

const isLoggedIn = redirectUrl => (req, res, next) => {
  if (req.session.user && 'id' in req.session.user) {
    next();
  } else if (redirectUrl) {
    req.session.nextPage = req.originalUrl;
    // req.flash('error', 'Por favor, faça login para prosseguir com seu agendamento.');
    res.redirect(redirectUrl);
  } else {
    res.json({ success: false, message: 'Você não possui permissão para acessar esta página.' });
  }
};

module.exports.estaNoFluxo = isKeyInCookie('aula', '/');
module.exports.estaNoFluxoAjax = isKeyInCookie('aula', null);

module.exports.aulaMarcada = isKeyInCookie('aula', '/');

module.exports.isLoggedIn = isLoggedIn('/aluno/entrar');
module.exports.isLoggedInAjax = isLoggedIn(null);

module.exports.isProfLoggedIn = isLoggedIn('/professor');
module.exports.isProfLoggedInAjax = isLoggedIn(null);
