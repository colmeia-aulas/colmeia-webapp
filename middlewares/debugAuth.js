const { login } = require('../controllers/user');

module.exports = async (req, res, next) => {
  if (req.session.user) return next();

  const userData = {
    email: 'matrpedreira@gmail.com',
    password: 'colmeia123'
  };

  try {
    const loginResponse = await login(userData.email, userData.password);
    req.session.token = loginResponse.token;
    req.session.user = loginResponse.user;

    console.log(`Logged in as: ${userData.email}`);

    res.redirect(req.originalUrl);
  } catch (error) {
    next(error);
  }
};
