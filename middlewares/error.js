const sgMail = require('@sendgrid/mail');
const { debug, sendGridKey } = require('../config/vars');

sgMail.setApiKey(sendGridKey);

const sendErrorEmail = async error => {
  // const adminEmail = 'victorlandim5@gmail.com';
  const adminEmail = 'douglasrdsousa1@gmail.com';

  const html = `
  <div>
    <strong>ERRO CRÍTICO NO WEBSITE COLMEIA</strong>
    <p>Um erro crítico ocorreu no website. O erro completo consta abaixo.</p>
    <br />
    <p>-------------------------------------------------------</p>
    <pre>${JSON.stringify(error, null, 4)}</pre>
    <p>-------------------------------------------------------</p>
    <br />
    <br />
  </div>
  `;

  const msg = {
    to: adminEmail,
    from: 'contato@aulascolmeia.com.br',
    subject: 'ERRO COLMEIA',
    text: ' ',
    html
  };

  try {
    await sgMail.send(msg);
  } catch (e) {
    console.log(e);
  }
};

module.exports.errorHandler = (error, req, res, next) => {
  const { message, originalError } = error;

  console.log(`Thrown error: ${message}`);
  console.log(`Message: ${originalError && originalError.message}`);
  console.log(`UserId: ${req.session.user && req.session.user.id}`);

  // if (debug) {
  console.log(originalError);
  // }

  if (originalError && String(originalError.code) === '209') {
    req.session.user = {};
    req.session.token = null;
    return res.redirect('/aluno/entrar');
  }

  switch (message) {
    // TODO: standardize error response
    // currently the error messages are
    // being adapted to what the frontend is expecting
    case 'PROFESSORES_INTERNO':
    case 'PROFESSORES_EXTERNO':
      req.flash(
        'error',
        'Houve um erro ao realizar sua busca. Por favor, entre em contato conosco pelo WhatsApp (61) 9 9805-2073.'
      );
      return res.redirect('/');
    case 'RESUMO':
      req.flash(
        'error',
        'Houve um erro ao prosseguir com seu agendamento. Por favor, entre em contato conosco pelo WhatsApp (61) 9 9805-2073.'
      );
      sendErrorEmail({ message, originalError });
      return res.redirect('/');
    case 'USER_LOGOUT':
      req.flash('error', 'Houve um erro ao realizar seu logout, tente novamente mais tarde.');
      sendErrorEmail({ message, originalError });
      return res.redirect('/');

    case 'VERIFY_CODE':
      sendErrorEmail({ message, originalError });
      return res.json({
        error: 'Houve um erro ao verificar seu código, tente novamente mais tarde.'
      });

    case 'MARCAR_AULA':
      sendErrorEmail({ message, originalError });
      return res.json({
        success: false,
        message:
          'Houve um erro ao agendar sua aula. Por favor, entre em contato conosco pelo WhatsApp (61) 9 9805-2073.'
      });
    case 'CANCELAR_AULA':
    case 'CANCELAR_AULA_CONFIRMAR':
      sendErrorEmail({ message, originalError });
      if (originalError.code === 141) {
        return res.json({
          ...JSON.parse(originalError.message)
        });
      }
      return res.json({
        success: false,
        error:
          'Houve um erro ao cancelar sua aula. Por favor, entre em contato conosco pelo WhatsApp (61) 9 9805-2073.'
      });

    case 'ADDRESS_FIND':
      sendErrorEmail({ message, originalError });
      return res.json({
        success: false,
        error: 'Houve um erro ao obter seus endereços, tente novamente mais tarde.'
      });
    case 'ADDRESS_CREATE':
      sendErrorEmail({ message, originalError });
      return res.json({
        success: false,
        error: 'Houve um erro ao adicionar seu endereço, tente novamente mais tarde.'
      });
    case 'ADDRESS_DELETE':
      sendErrorEmail({ message, originalError });
      return res.json({
        success: false,
        error: 'Houve um erro ao excluir seu endereço, tente novamente mais tarde.'
      });

    case 'CARTOES_FIND':
      sendErrorEmail({ message, originalError });
      return res.json({
        success: false,
        error: 'Houve um erro ao obter seus cartões, tente novamente mais tarde.'
      });
    case 'CARTOES_CREATE':
      sendErrorEmail({ message, originalError });
      return res.json({
        success: false,
        message:
          'Houve um erro ao adicionar seu cartão. Por favor, entre em contato conosco pelo WhatsApp (61) 9 9805-2073.'
      });
    case 'CARTOES_DELETE':
      sendErrorEmail({ message, originalError });
      return res.json({
        success: false,
        error: 'Houve um erro ao excluir seu cartão, tente novamente mais tarde.'
      });

    case 'USER_LOGIN':
      return res.json({
        success: false,
        message:
          'Houve um erro ao realizar seu login. Por favor, confira se os dados inseridos estão corretos. Se o problema persistir, tente novamente mais tarde ou entre em contato conosco pelo WhatsApp (61) 9 9805-2073.'
      });
    case 'USER_REGISTER':
      sendErrorEmail({ message, originalError });
      return res.json({
        success: false,
        message:
          'Houve um erro ao realizar seu cadastro. Por favor, confira se os dados inseridos estão corretos. Se o problema persistir, tente novamente mais tarde ou entre em contato conosco pelo WhatsApp (61) 9 9805-2073.'
      });
    case 'USER_REGISTER_EMAIL_TAKEN':
      return res.json({
        success: false,
        message:
          'Já existe uma conta cadastrada com o e-mail que você inseriu. Insira outro e-mail ou realize login para continuar.'
      });
    case 'USER_UPDATE':
      sendErrorEmail({ message, originalError });
      return res.json({
        success: false,
        message: 'Houve um erro ao atualizar suas informações, tente novamente mais tarde.'
      });
    case 'USER_PASSWORD_RESET':
      return res.json({
        success: false,
        message:
          'Houve um erro ao solicitar a redefinição da sua senha, tente novamente mais tarde.'
      });
    case 'USER_PICTURE':
      return res.json({
        success: false,
        message: 'Houve um erro ao alterar sua imagem de perfil, tente novamente mais tarde.'
      });

    case 'USER_PROFILE':
      return res.json({
        success: false,
        message: 'Houve um erro ao obter seu perfil, tente novamente mais tarde.'
      });

    case 'AGENDA':
      return res.json({
        success: false,
        message: 'Houve um erro ao obter sua agenda, tente novamente mais tarde.'
      });
    case 'HISTORICO':
      return res.json({
        success: false,
        message: 'Houve um erro ao obter seu histórico de aulas, tente novamente mais tarde.'
      });

    case 'USER_PREREGISTER':
      return null; // do nothing

    case 'EMAIL_MARKETING':
    case 'NEWSLETTER':
      return res.json({
        success: false,
        message:
          'Houve um erro ao te cadastrar em nossa lista de emails. Por favor, entre em contato conosco pelo WhatsApp (61) 9 9805-2073.'
      });
    case 'NOVO_AGENDAMENTO_SITE':
      sendErrorEmail({ message, originalError });
      return res.json({
        success: false,
        message:
          'Houve um erro ao realizar seu agendamento. Por favor, entre em contato conosco pelo WhatsApp (61) 9 9805-2073.'
      });
    case 'RESIZE_IMG':
      return res.json({ success: false, error: originalError.message });

    case 'NOT_FOUND':
      return res.status(404).json('Not found.');

    case 'NOT_FOUND_GET':
      return res.status(404).render('404');

    case 'INVALID_SEARCH_PARAMS':
      req.flash('error', 'Desculpe, você tentou realizar uma busca inválida.');
      return res.status(404).redirect('/');

    case 'CADASTRO_PROFESSOR_SHOW':
      sendErrorEmail({ message, originalError });
      req.flash(
        'error',
        'Houve um erro ao acessar a página de cadastro. Por favor, entre em contato conosco pelo WhatsApp (61) 9 9805-2073.'
      );
      return res.redirect('/');
    case 'CADASTRO_PROFESSOR_UPDATE':
      sendErrorEmail({ message, originalError });
      return res.json({
        success: false,
        message:
          'Houve um erro ao enviar suas informações. Por favor, entre em contato conosco pelo WhatsApp (61) 9 9805-2073.'
      });
    case 'CADASTRO_PROFESSOR_STEP':
      sendErrorEmail({ message, originalError });
      return res.json({
        success: false,
        message: 'Houve um erro ao enviar suas informações. Recebemos dados inválidos.'
      });
    case 'CADASTRO_PROFESSOR_FINAL_FILE':
      sendErrorEmail({ message, originalError });
      return res.json({
        success: false,
        message:
          'Houve um erro ao enviar seu arquivo. Por favor, entre em contato conosco pelo WhatsApp (61) 9 9805-2073.'
      });
    case 'CADASTRO_PROFESSOR_FINAL':
      sendErrorEmail({ message, originalError });
      return res.json({
        success: false,
        message:
          'Houve um erro ao finalizar seu cadastro. Por favor, entre em contato conosco pelo WhatsApp (61) 9 9805-2073.'
      });

    case 'LEAD':
    case 'LEAD_ACOMPANHAMENTO':
    case 'LEAD_CALCULADORA':
      sendErrorEmail({ message, originalError });
      return res.json({
        success: false,
        message:
          'Desculpe, houve um erro ao receber suas informações. Por favor, entre em contato conosco pelo WhatsApp (61) 9 9805-2073.'
      });
    case 'JOB_DESCRIPTION':
    case 'JOBS':
      return res.redirect('/');
    default:
      req.flash('error', 'Desculpe, parece que algo deu errado!');
      return res.redirect('/');
  }
};
