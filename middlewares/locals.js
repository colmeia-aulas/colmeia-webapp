const { debug, env } = require('../config/vars');

module.exports = (req, res, next) => {
  res.locals.env = env;
  res.locals.isDebug = debug;
  res.locals.user = req.session.user || {};
  res.locals.message = {
    success: req.flash('success'),
    error: req.flash('error'),
    info: req.flash('info')
  };
  next();
};
