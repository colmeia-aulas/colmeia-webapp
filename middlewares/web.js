const { debug, env } = require('../config/vars');
const { cacheTime } = require('../config/app');

module.exports = {
  cacheResources: (req, res, next) => {
    if (
      (!debug && req.url.indexOf('/img/') === 0) ||
      req.url.indexOf('/css/') === 0 ||
      req.url.indexOf('/js/') === 0
    ) {
      res.setHeader('Cache-Control', `public, max-age=cacheTime`);
      res.setHeader('Expires', new Date(Date.now() + cacheTime * 1000).toUTCString());
    }
    next();
  },

  forceHttps: (req, res, next) => {
    if (env === 'production' && !req.secure && req.get('X-Forwarded-Proto') !== 'https') {
      res.redirect(`https://${req.get('Host')}${req.url}`);
    } else next();
  },

  removeWww: (req, res, next) => {
    if (req.headers.host.match(/^www/) !== null) {
      res
        .status(301)
        .redirect(`${req.protocol}://${req.headers.host.replace(/^www\./, '')}${req.url}`);
    } else next();
  }
};
