/* eslint-disable no-var */
const DbBase = require('./db');

class Endereco extends DbBase {
  constructor() {
    super('Address');
  }

  create({ endereco, bairro, complemento, numero, estado, cidade }, id, sessionToken) {
    return new Promise(async (resolve, reject) => {
      const address = new this.DbObj();

      const bairros = this.Parse.Object.extend('Bairro');
      const query = new this.Parse.Query(bairros);
      console.log(bairro);

      query.equalTo('nome', bairro);
      try {
        const result = await query.find({ sessionToken });


        const baiId = result[0].id;

        const userPointer = {
          __type: 'Pointer',
          className: '_User',
          objectId: id
        };

        const bairroPointer = {
          __type: 'Pointer',
          className: 'Bairro',
          objectId: baiId
        };

        address.set('endereco', endereco);
        address.set('complemento', complemento);
        address.set('numero', numero);
        address.set('estado', estado);
        address.set('cidade', cidade);

        address.set('bairro', bairroPointer);
        address.set('user', userPointer);

        const add = await address.save(null, { sessionToken });

        const end = {
          bairro,
          complemento,
          numero,
          endereco,
          estado,
          cidade,
          id: add.id
        };

        resolve({ success: true, end });
      } catch (error) {
        reject(error);
      }
    });
  }

  findUserAddresses(id, sessionToken) {
    return new Promise(async (resolve, reject) => {
      const query = new this.Parse.Query(this.DbObj);

      const userPointer = {
        __type: 'Pointer',
        className: '_User',
        objectId: id
      };

      query.include('bairro');
      query.include('user.endereco_preferido');
      query.include('user');

      query.equalTo('user', userPointer);
      try {
        const results = await query.find({ sessionToken });

        let favorite;
        if (results.length > 0) favorite = results[0].get('user').get('endereco_preferido');

        const favorite_id = favorite != undefined ? favorite.id : '';

        const ends = results.map(e => ({
          bairro: e.get('bairro').get('nome'),
          complemento: e.get('complemento'),
          numero: e.get('numero'),
          endereco: e.get('endereco'),
          id: e.id
        }));

        resolve({ success: true, ends, favorite: favorite_id });
      } catch (error) {
        reject(error);
      }
    });
  }

  delete(id, sessionToken) {
    return new Promise(async (resolve, reject) => {
      const query = new this.Parse.Query(this.DbObj);

      query.include('user');

      try {
        const result = await query.get(id, { sessionToken });

        // result.unset('user');
        result.set('user', undefined);
        await result.save(null, { sessionToken });
        resolve({ success: true });
      } catch (error) {
        reject(error);
      }
    });
  }

  getFavoriteAddress(userId, sessionToken) {
    return new Promise(async (resolve, reject) => {
      const query = new this.Parse.Query(this.DbObj);

      const userPointer = {
        __type: 'Pointer',
        className: '_User',
        objectId: userId
      };

      query.include('user');
      query.include('user.endereco_preferido');
      query.include('user.endereco_preferido.bairro');
      query.equalTo('user', userPointer);

      try {
        const results = await query.find({ sessionToken });

        if (results.length > 0 && results[0].get('user').get('endereco_preferido') !== undefined) {
          const favorite = {
            bairro: results[0]
              .get('user')
              .get('endereco_preferido')
              .get('bairro')
              .get('nome'),
            cidade: results[0]
              .get('user')
              .get('endereco_preferido')
              .get('bairro')
              .get('cidade')
          };
          resolve({ favorite });
        } else {
          resolve({ favorite: undefined });
        }
      } catch (error) {
        reject(error);
      }
    });
  }
}

module.exports = Endereco;
