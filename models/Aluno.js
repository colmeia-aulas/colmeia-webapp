const DbBase = require('./db');

class Aluno extends DbBase {
  constructor() {
    super('Aluno');
  }
}

module.exports = Aluno;
