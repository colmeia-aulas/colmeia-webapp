const moment = require('moment-timezone');
const DbBase = require('./db');
const Aluno = require('./Aluno');
const File = require('./File');
const User = require('./User');

moment.tz.setDefault('America/Sao_Paulo');

class Aulas extends DbBase {
  constructor() {
    super('Aulas');
  }

  create(dados, user, sessionToken) {
    return new Promise(async (resolve, reject) => {
      try {
        const aula = new this.DbObj();

        const userPointer = {
          __type: 'Pointer',
          className: '_User',
          objectId: user
        };

        const enderecoPointer = {
          __type: 'Pointer',
          className: 'Address',
          objectId: dados.enderecoId
        };

        const professorPointer = {
          __type: 'Pointer',
          className: 'Professor',
          objectId: dados.professor.id
        };

        const professorUserPointer = {
          __type: 'Pointer',
          className: '_User',
          objectId: dados.professor.userId
        };

        const cardPointer = {
          __type: 'Pointer',
          className: 'Cartoes',
          objectId: dados.cardId
        };

        const dia = moment(dados.dia, 'DD/MM/YYYY HH:mm Z');

        aula.set('topico', dados.topico);
        aula.set('ensino', dados.ensino);
        aula.set('ano', dados.ano);
        aula.set('tipo', dados.tipo);

        aula.set('endereco', enderecoPointer);
        aula.set('aluno', userPointer);
        aula.set('professor_user', professorUserPointer);
        aula.set('professor', professorPointer);
        aula.set('duracao', Number(dados.duracao));
        aula.set('dia', dia.toDate());
        aula.set('horarios', dados.horarios); // Slots
        aula.set('card', cardPointer);
        aula.set('preco', dados.preco);

        aula.set('observacao', dados.observacao);
        aula.set('tipoDeAgendamento', 'Site');

        if (dados.image) {
          const file = await new File().create(dados.image, sessionToken);
          aula.set('image', file);
        }

        if (dados.discount) {
          aula.set('codigo_promocional', dados.discount);
        }

        const result = await aula.save(null, { sessionToken });

        // Setar cartão preferido
        // setCurrentUser(token, function(current) {
        //   current.set('cartao_preferido', cardPointer);
        //   current.save();
        //   callback({aula: a});
        // });

        if (dados.nomeEscola && dados.anoEscola) {
          const alunoData = {
            escola: dados.nomeEscola,
            serie: dados.anoEscola,
            cidade: dados.cidade,
            user: userPointer
          };

          try {
            const alunoResponse = await new Aluno().create(alunoData, sessionToken);

            const alunoPointer = {
              __type: 'Pointer',
              className: 'Aluno',
              objectId: alunoResponse.id
            };

            await new User().update(
              user,
              {
                aluno: alunoPointer
              },
              sessionToken
            );

            resolve({ aula: result });
          } catch (error) {
            reject(error);
          }
        } else {
          resolve({ aula: result });
        }
      } catch (error) {
        reject(error);
      }
    });
  }

  requestCancel(aulaId, sessionToken) {
    return this.runCloudCode('cancelarAula', { aulaId }, sessionToken);
  }

  confirmCancel(aulaId, sessionToken) {
    return this.runCloudCode('confirmarCancelamento', { aulaId }, sessionToken);
  }

  getAgenda(userId, tipo, sessionToken) {
    return new Promise(async (resolve, reject) => {
      try {
        const query = new this.Parse.Query(this.DbObj);

        const userPointer = {
          __type: 'Pointer',
          className: '_User',
          objectId: userId
        };

        query.include('professor');
        query.include(['endereco', 'endereco.bairro']);
        query.include('card');
        query.equalTo('aluno', userPointer);

        if (tipo === 'agenda') {
          query.containedIn('status', ['Criada', 'Confirmada']);
        } else {
          query.containedIn('status', ['Finalizada', 'Cancelada']);
        }

        const results = await query.find({ sessionToken });

        const aulas = [];

        for (let i = 0; i < results.length; i += 1) {
          let foto = '';
          if (results[i].get('professor').get('image') != undefined)
            foto = results[i]
              .get('professor')
              .get('image')
              .url();

          const aula = {
            id: results[i].id,
            dia: results[i].get('dia'),
            duracao: results[i].get('duracao'),
            ensino: results[i].get('ensino'),
            preco: results[i].get('preco'),
            tipo: results[i].get('tipo'),
            topico: results[i].get('topico'),
            observacao: results[i].get('observacao'),
            status: results[i].get('status'),
            card:
              results[i].get('card') == undefined
                ? undefined
                : {
                    digits: results[i].get('card').get('digits'),
                    brand: results[i].get('card').get('brand')
                  },
            endereco:
              results[i].get('endereco') == undefined
                ? undefined
                : {
                    bairro: results[i]
                      .get('endereco')
                      .get('bairro')
                      .get('nome'),
                    endereco: results[i].get('endereco').get('endereco'),
                    numero: results[i].get('endereco').get('numero'),
                    complemento: results[i].get('endereco').get('complemento')
                  },
            prof: {
              nome: results[i].get('professor').get('nome'),
              foto,
              avaliacao: results[i].get('professor').get('avaliacao')
            }
          };
          aulas.push(aula);
        }
        resolve(aulas);
      } catch (error) {
        reject(error);
      }
    });
  }
}

module.exports = Aulas;
