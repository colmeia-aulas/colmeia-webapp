const DbBase = require('./db');

class BlogImage extends DbBase {
  constructor() {
    super('BlogImage');
  }
}

module.exports = BlogImage;
