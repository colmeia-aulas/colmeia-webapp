const DbBase = require('./db');

class BlogPost extends DbBase {
  constructor() {
    super('BlogPost');
  }

  count() {
    return new Promise(async (resolve, reject) => {
      try {
        const query = new this.Parse.Query(this.DbObj);
        const count = await query.count();
        resolve(count);
      } catch (error) {
        reject(error);
      }
    });
  }

  select(keysWanted = [], additionalConstraints = []) {
    return new Promise(async (resolve, reject) => {
      try {
        const query = new this.Parse.Query(this.DbObj);

        query.select(keysWanted);

        if (additionalConstraints.length) {
          additionalConstraints.forEach(({ constraint, value }) => {
            const shouldDeconstruct = typeof value === 'object';
            shouldDeconstruct ? query[constraint](...value) : query[constraint](value);
          });
        }

        const blogPost = await query.find();

        const response = blogPost.map(post => {
          const keys = {};

          const selectedFields = Object.keys(post.attributes);
          selectedFields.forEach(key => {
            keys[key] = post.get(key);
          });
          return keys;
        });

        resolve(response);
      } catch (error) {
        reject(error);
      }
    });
  }
}

module.exports = BlogPost;
