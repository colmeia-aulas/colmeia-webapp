const DbBase = require('./db');

function getRandomInt(_min, _max) {
  const min = Math.ceil(_min);
  const max = Math.floor(_max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

class BlogPost extends DbBase {
  constructor() {
    super('BlogPost');
  }

  count() {
    return new Promise(async (resolve, reject) => {
      try {
        const query = new this.Parse.Query(this.DbObj);
        const count = await query.count();
        resolve(count);
      } catch (error) {
        reject(error);
      }
    });
  }

  select(keysWanted = [], additionalConstraints = []) {
    return new Promise(async (resolve, reject) => {
      try {
        const query = new this.Parse.Query(this.DbObj);

        query.select(keysWanted);

        if (additionalConstraints.length) {
          additionalConstraints.forEach(({ constraint, value }) => {
            const shouldDeconstruct = typeof value === 'object';
            shouldDeconstruct ? query[constraint](...value) : query[constraint](value);
          });
        }

        const blogPost = await query.find();

        const response = blogPost.map(post => {
          const keys = {};

          const selectedFields = Object.keys(post.attributes);
          selectedFields.forEach(key => {
            keys[key] = post.get(key);
          });
          return keys;
        });

        resolve(response);
      } catch (error) {
        reject(error);
      }
    });
  }
}

describe('Extensions to the BlogPost class', () => {
  test("Check if select implementation doesn't bring unwanted values", async () => {
    const blogPost = new BlogPost();

    const wantedProps = ['topic', 'previewText'];
    const additionalConstraints = [
      { constraint: 'descending', value: 'createdAt' },
      { constraint: 'limit', value: 1 }
    ];

    const data = await blogPost.select(wantedProps, additionalConstraints);
    // console.log(data);
    expect(data.hasOwnProperty('previewImage')).toBeFalsy();
  });

  test('Check if ternary constraints like equalTo(key, value) work', async () => {
    const blogPost = new BlogPost();

    const wantedProps = ['topic', 'previewText'];
    const additionalTernaryConstraint = [
      { constraint: 'equalTo', value: ['objectId', 'Okyrk2WY7f'] }
    ];
    const data = await blogPost.select(wantedProps, additionalTernaryConstraint);
    // console.log(data);
    expect(data.length).toBeTruthy();
  });

  test('Get random objects from table', async () => {
    const blogPost = new BlogPost();

    const count = await blogPost.count();

    const wantedProps = ['topic'];
    const additionalConstraints = [
      { constraint: 'skip', value: getRandomInt(0, count - 3) },
      { constraint: 'limit', value: 3 }
    ];

    const data = await blogPost.select(wantedProps, additionalConstraints);
    console.log(data);
  });
});

describe.only('BlogPost urls', () => {
  test('Get all blogpost urls', async () => {
    const blogPost = new BlogPost();

    const result = await blogPost.select(['postPath']);
    const urls = result.map(row => row.postPath);
    console.log(urls);

    expect(!!result[0].postPath).toBeTruthy();
  });
});
