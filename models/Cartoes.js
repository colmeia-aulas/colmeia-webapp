const DbBase = require('./db');
const helper = require('../helper/helper');

class Cartoes extends DbBase {
  constructor() {
    super('Cartoes');
  }

  delete(id, sessionToken) {
    return new Promise(async (resolve, reject) => {
      try {
        const query = new this.Parse.Query(this.DbObj);

        query.include('user');
        const cardToBeDeleted = await query.get(id, { sessionToken });

        cardToBeDeleted.get('user').unset('cartao_preferido');

        await cardToBeDeleted.get('user').save(null, { sessionToken });

        cardToBeDeleted.unset('user');
        await cardToBeDeleted.save(null, { sessionToken });

        resolve({ success: true });
      } catch (error) {
        reject(error);
      }
    });
  }

  getCreditCards(userId, sessionToken) {
    return new Promise(async (resolve, reject) => {
      try {
        const query = new this.Parse.Query(this.DbObj);

        const userPointer = {
          __type: 'Pointer',
          className: '_User',
          objectId: userId
        };

        query.include('user');
        query.include('user.cartao_preferido');
        query.equalTo('user', userPointer);
        query.exists('gateway');

        const results = await query.find({ sessionToken });

        let favorite;
        if (results.length > 0) favorite = results[0].get('user').get('cartao_preferido');

        const favorite_id = favorite != undefined ? favorite.id : null;
        resolve({ success: true, cards: results, favorite: favorite_id });
      } catch (error) {
        reject(error);
      }
    });
  }

  getFavoriteCreditCard(userId, sessionToken) {
    return new Promise(async (resolve, reject) => {
      try {
        const query = new this.Parse.Query(this.DbObj);

        const userPointer = {
          __type: 'Pointer',
          className: '_User',
          objectId: userId
        };

        query.include('user');
        query.include('user.cartao_preferido');
        query.equalTo('user', userPointer);
        query.exists('gateway');

        const results = await query.find({ sessionToken });

        if (results.length > 0) {
          let favorite = results[0].get('user').get('cartao_preferido');

          if (favorite == undefined) {
            favorite = results[0];
            results[0].get('user').set('cartao_preferido', results[0]);
            await results[0].get('user').save(null, { sessionToken });
          }

          resolve({ favorite });
        } else {
          resolve({ favorite: undefined });
        }
      } catch (error) {
        reject(error);
      }
    });
  }

  create({ hash, moipId, id }, sessionToken) {
    return new Promise(async (resolve, reject) => {
      try {
        const cardObj = {
          cardHash: hash,
          moipClientID: moipId,
          userID: id
        };

        const newCard = await this.runCloudCode('registerCardMoip', cardObj, sessionToken);
        if (helper.isJson(newCard) && typeof newCard == 'string' && JSON.parse(newCard).code) {
          resolve({ success: false, code: newCard.code });
        } else {
          newCard.get('user').set('cartao_preferido', newCard);
          await newCard.get('user').save(null, { sessionToken });
          resolve({ success: true, card: newCard });
        }
      } catch (error) {
        reject(error);
      }
    });
  }
}

module.exports = Cartoes;
