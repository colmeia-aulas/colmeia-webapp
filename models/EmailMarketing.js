const DbBase = require('./db');

class EmailMarketing extends DbBase {
  constructor() {
    super('EmailMarketing');
  }
}

module.exports = EmailMarketing;
