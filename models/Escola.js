const DbBase = require('./db');

class Escola extends DbBase {
  constructor() {
    super('Escola');
  }
}

module.exports = Escola;
