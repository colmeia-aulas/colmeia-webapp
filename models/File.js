const DbBase = require('./db');

class File extends DbBase {
  constructor() {
    super('Image');
  }

  create({ filename, base64, mimetype }, sessionToken) {
    return new Promise(async (resolve, reject) => {
      const imageFile = new this.Parse.File(filename, { base64 }, mimetype);
      try {
        const result = await imageFile.save(null, { sessionToken });
        resolve(result);
      } catch (error) {
        reject(error);
      }
    });
  }
}

module.exports = File;
