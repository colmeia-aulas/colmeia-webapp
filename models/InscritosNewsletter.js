const DbBase = require('./db');

class InscritosNewsletter extends DbBase {
  constructor() {
    super('InscritosNewsletter');
  }
}

module.exports = InscritosNewsletter;
