const DbBase = require('./db');

class Job extends DbBase {
  constructor() {
    super('Job');
  }
}

module.exports = Job;
