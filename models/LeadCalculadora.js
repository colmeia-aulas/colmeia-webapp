const DbBase = require('./db');

class LeadCalculadora extends DbBase {
  constructor() {
    super('LeadCalculadora');
  }
}

module.exports = LeadCalculadora;
