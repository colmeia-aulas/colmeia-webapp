const DbBase = require('./db');

class Leads extends DbBase {
  constructor() {
    super('Leads');
  }
}

module.exports = Leads;
