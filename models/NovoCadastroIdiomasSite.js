const DbBase = require('./db');

class NovoCadastroIdiomasSite extends DbBase {
  constructor() {
    super('NovoCadastroIdiomasSite');
  }
}

module.exports = NovoCadastroIdiomasSite;
