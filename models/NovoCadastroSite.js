const DbBase = require('./db');

class NovoCadastroSite extends DbBase {
  constructor() {
    super('NovoCadastroSite');
  }
}

module.exports = NovoCadastroSite;
