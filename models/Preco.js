const DbBase = require('./db');
const User = require('./User');

class Preco extends DbBase {
  constructor() {
    super('Preco');
  }

  getPrice(tipo, userId, sessionToken) {
    return new Promise(async (resolve, reject) => {
      try {
        const queryOptions = {
          where: {
            tipo
          }
        };

        const result = await this.find(queryOptions, ['preco'], sessionToken);

        const { aluno } = await new User().get(userId, ['aluno'], sessionToken);

        resolve({
          // assuming there is only one price per type
          ...result[0],
          hasEscolaAno: Boolean(aluno)
        });
      } catch (error) {
        reject(error);
      }
    });
  }

  getAllPrices() {
    return new Promise(async (resolve, reject) => {
      try {
        const queryOptions = {
          where: {
            tipo: 'Escolar'
          }
        };

        const results = await this.find(queryOptions, ['preco', 'cidade']);

        const prices = {};

        results.forEach(e => {
          prices[e.cidade] = e.preco;
        });

        resolve(prices);
      } catch (error) {
        reject(error);
      }
    });
  }

  getPriceWithCode({ userID, type, code, issuer }, sessionToken) {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await this.runCloudCode(
          'validarCodigo',
          {
            userID,
            type,
            code,
            issuer
          },
          sessionToken
        );
        resolve(response);
      } catch (error) {
        reject(error);
      }
    });
  }
}

module.exports = Preco;
