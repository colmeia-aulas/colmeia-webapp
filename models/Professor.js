const DbBase = require('./db');

class Professor extends DbBase {
  constructor() {
    super('Professor');
  }

  buscaAulas(dados) {
    return new Promise(async (resolve, reject) => {
      try {
        const query = new this.Parse.Query(this.DbObj);

        query.equalTo('cidade', dados.endereco.cidade);

        if (dados.endereco.bairro !== 'Todos os bairros') {
          query.containsAll('bairros', [dados.endereco.bairro]);
        }

        if (dados.topico !== 'Todas as matérias') {
          query.containsAll('topicos', [dados.topico]);
        }
        if (dados.endereco.cidade === 'Brasília' && dados.topico !== 'Inglês') {
          query.equalTo('isDisplacementSystem', true);
        }

        query.containsAll('anos', [dados.nivel]);

        query.equalTo('habilitado', true);
        query.descending('orderScore');

        // TODO: pagination
        query.limit(30);

        const results = await query.find();

        const professores = results.map(e => ({
          avaliacao: e.get('avaliacao'),
          horarios: e.get('horarios'),
          image: e.get('image'),
          anos: e.get('anos'),
          schedule: e.get('schedule'),
          topicos: e.get('topicos'),
          nivelLinguas: e.get('nivelLinguas'),
          bairros: e.get('bairros'),
          curriculo: e.get('curriculo'),
          nome: e.get('nome'),
          email: e.get('email'),
          id: e.id,
          userId: e.get('user').id,
          busyClassInterval: e.get('busyClassInterval')
        }));
        resolve(professores);
      } catch (error) {
        reject(error);
      }
    });
  }
}

module.exports = Professor;
