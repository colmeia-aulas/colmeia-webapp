const DbBase = require('./db');
const User = require('./User');

class ProfessorCandidato extends DbBase {
  constructor() {
    super('ProfessorCandidato');
  }

  update(id, step, data) {
    return new Promise(async (resolve, reject) => {
      try {
        const query = new this.Parse.Query(this.DbObj);

        const result = await query.get(id);

        if (!result) {
          reject(new Error({ message: 'Object not found.' }));
        } else {
          Object.keys(data).forEach(key => result.set(key, data[key]));

          const stepArray = result.get('step');
          stepArray[step - 1] = 'true';
          result.set('step', stepArray);

          const res = await result.save(null, {
            useMasterKey: true
          });

          resolve(res);
        }
      } catch (error) {
        reject(error);
      }
    });
  }

  updateFinal(id, data) {
    return new Promise(async (resolve, reject) => {
      try {
        const query = new this.Parse.Query(this.DbObj);

        const result = await query.get(id);

        if (!result) {
          reject(new Error({ message: 'Object not found.' }));
        } else {
          Object.keys(data).forEach(key => result.set(key, data[key]));

          const stepArray = result.get('step');
          stepArray[0] = 'true';
          result.set('step', stepArray);

          const res = await result.save(null, {
            useMasterKey: true
          });

          resolve(res);
        }
      } catch (error) {
        reject(error);
      }
    });
  }

  getUserProfessorCandidato(userId, sessionToken) {
    return new Promise(async (resolve, reject) => {
      try {
        const user = new User();
        const currentUser = await user.get(
          userId,
          ['professorCandidato', 'nome', 'telefone', 'username'],
          sessionToken
        );

        // if the user does not have a professorCandidato pointer, create one
        if (!currentUser.professorCandidato) {
          const initialData = {
            finalizado: 'Não',
            step: ['false', 'false', 'false', 'false', 'false', 'false', 'false'],
            nome: currentUser.nome,
            telefone: currentUser.telefone,
            username: currentUser.username
          };
          const { id } = await this.create(initialData, sessionToken);

          const professorCandidatoPointer = {
            __type: 'Pointer',
            className: 'ProfessorCandidato',
            objectId: id
          };

          await user.update(
            userId,
            { professorCandidato: professorCandidatoPointer },
            sessionToken
          );

          resolve(id);
        } else {
          resolve(currentUser.professorCandidato.id);
        }
      } catch (error) {
        reject(error);
      }
    });
  }
}

module.exports = ProfessorCandidato;
