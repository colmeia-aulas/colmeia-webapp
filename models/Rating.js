const DbBase = require('./db');

class Rating extends DbBase {
  constructor() {
    super('Rating');
  }
}

module.exports = Rating;
