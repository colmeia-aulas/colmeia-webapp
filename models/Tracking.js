const DbBase = require('./db');

class Tracking extends DbBase {
  create(data) {
    return new Promise(async (resolve, reject) => {
      try {
        const query = new this.Parse.Query(this.dbObj);
        query.equalTo('ga_id', data.ga_id);

        const response = await query.find();

        // no tracking object found
        if (response.length === 0) {
          this.dbObj.set('url', data.url);
          this.dbObj.set('source', data.utm_source);
          this.dbObj.set('medium', data.utm_medium);
          this.dbObj.set('content', data.utm_content);
          this.dbObj.set('name', data.utm_campaign);
          this.dbObj.set('term', data.utm_term);
          this.dbObj.set('ga_id', data.ga_id);
          this.dbObj.set('navigator', data.navigator);
          const tracking = await this.dbObj.save();
          resolve({ trackId: tracking.get('ga_id') });
        } else {
          resolve({ trackId: response[0].get('ga_id') });
        }
      } catch (error) {
        reject(error);
      }
    });
  }
}

module.exports = Tracking;
