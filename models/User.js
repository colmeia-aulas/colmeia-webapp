const moment = require('moment-timezone');
const DbBase = require('./db');
const Image = require('./File');
const helper = require('../helper/helper');

moment.tz.setDefault('America/Sao_Paulo');

class User extends DbBase {
  constructor() {
    super('_User');
  }

  create(data) {
    return new Promise(async (resolve, reject) => {
      try {
        const obj = new this.Parse.User();
        Object.keys(data).forEach(key => obj.set(key, data[key]));

        const result = await obj.signUp(null, {
          useMasterKey: true
        });

        resolve(result);
      } catch (error) {
        reject(error);
      }
    });
  }

  resetPassord(email) {
    return new Promise(async (resolve, reject) => {
      try {
        await this.Parse.User.requestPasswordReset(email);
        resolve();
      } catch (error) {
        const message =
          error.code === 205
            ? 'O email inserido não existe.'
            : 'Houve um erro ao encontrar seu email.';

        reject({ ...error, message });
      }
    });
  }

  createMoipClient(name, email, cpf, phone, userID, sessionToken) {
    return new Promise(async (resolve, reject) => {
      try {
        const result = await this.runCloudCode(
          'registerClientMoip',
          {
            name,
            email,
            cpf,
            phone,
            userID
          },
          sessionToken
        );

        if (helper.isJson(result) && typeof result === 'string' && JSON.parse(result).code) {
          resolve({ success: false, code: result.code });
        } else {
          resolve({ success: true, moipId: result });
        }
      } catch (error) {
        reject(error);
      }
    });
  }

  login({ email, password }) {
    return new Promise(async (resolve, reject) => {
      try {
        // if (this.Parse.User.current()) {
        //   await this.Parse.User.logOut();
        // }

        const user = await this.Parse.User.logIn(email, password);

        const userResponse = {
          id: user.id,
          email: user.get('email'),
          nome: user.get('nome'),
          phone: user.get('telefone'),
          moipId: user.get('moipID')
        };

        resolve({
          success: true,
          user: userResponse,
          token: user.getSessionToken()
        });
      } catch (error) {
        reject(error);
      }
    });
  }

  // logout() {
  //   return new Promise(async (resolve, reject) => {
  //     try {
  //       await this.Parse.User.logOut();
  //       resolve();
  //     } catch (error) {
  //       reject(error);
  //     }
  //   });
  // }

  // DEPRECATED!!!!
  // setCurrentUser(token) {
  //   return new Promise(async (resolve, reject) => {
  //     this.Parse.User.enableUnsafeCurrentUser();

  //     try {
  //       // await this.Parse.User.logOut();
  //       // console.log(token);
  //       await this.Parse.User.become(token);
  //       resolve();
  //     } catch (error) {
  //       reject({ error: true, message: error });
  //     }
  //   });
  // }
}

module.exports = User;
