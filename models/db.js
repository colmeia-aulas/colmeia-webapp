const Parse = require('../config/parse');

class dbBase {
  constructor(type) {
    this.type = type;
    this.DbObj = Parse.Object.extend(type);
    this.Parse = Parse;
  }

  create(data, sessionToken) {
    return new Promise(async (resolve, reject) => {
      try {
        const obj = new this.DbObj();
        Object.keys(data).forEach(key => obj.set(key, data[key]));

        const result = await obj.save(null, { sessionToken });
        resolve(result);
      } catch (error) {
        reject(error);
      }
    });
  }

  update(id, data, sessionToken) {
    return new Promise(async (resolve, reject) => {
      try {
        const query = new this.Parse.Query(this.DbObj);

        const result = await query.get(id, { sessionToken });

        if (!result) {
          reject(new Error({ message: 'Object not found.' }));
        } else {
          Object.keys(data).forEach(key => result.set(key, data[key]));

          const res = await result.save(null, {
            useMasterKey: true,
            sessionToken
          });

          resolve(res);
        }
      } catch (error) {
        reject(error);
      }
    });
  }

  get(id, fields, sessionToken) {
    return new Promise(async (resolve, reject) => {
      try {
        const query = new this.Parse.Query(this.DbObj);

        const obj = await query.get(id, { sessionToken });

        const response = {};

        fields.forEach(key => {
          response[key] = obj.get(key);
        });

        resolve(response);
      } catch (error) {
        reject(error);
      }
    });
  }

  find(options, fields, sessionToken) {
    return new Promise(async (resolve, reject) => {
      try {
        const params = options || {
          queryType: 'first',
          where: {}
        };

        const query = new this.Parse.Query(this.DbObj);

        Object.keys(params.where).forEach(key => query.equalTo(key, params.where[key]));

        const obj = await query.find({ sessionToken });

        const response = obj.map(e => {
          const keys = {};

          const selectedFields = fields || Object.keys(e.attributes);

          selectedFields.forEach(key => {
            keys[key] = e.get(key);
          });
          return keys;
        });
        resolve(response);
      } catch (error) {
        reject(error);
      }
    });
  }

  delete(data, type, sessionToken) {
    return new Promise(async (resolve, reject) => {
      try {
        const query = new this.Parse.Query(this.DbObj);

        query.equalTo(type, data);

        const result = await query.first({ sessionToken });

        if (!result) {
          reject(new Error({ message: 'Object not found.' }));
        } else {
          const res = await result.destroy({ sessionToken });
          resolve(res);
        }
      } catch (error) {
        reject(error);
      }
    });
  }

  runCloudCode(fn, params, sessionToken) {
    return this.Parse.Cloud.run(fn, params, { sessionToken });
  }
}

module.exports = dbBase;
