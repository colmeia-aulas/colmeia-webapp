const User = require('./User');
const File = require('./File');
const Address = require('./Address');
const Aulas = require('./Aulas');
const Cartoes = require('./Cartoes');
const PreCadastro = require('./PreCadastro');
const Leads = require('./Leads');
const Leads2 = require('./Leads2');
const EmailMarketing = require('./EmailMarketing');
const InscritosNewsletter = require('./InscritosNewsletter');
const Preco = require('./Preco');
const Professor = require('./Professor');
const Escola = require('./Escola');
const ProfessorCandidato = require('./ProfessorCandidato');
const NovoCadastroSite = require('./NovoCadastroSite');
const NovoCadastroIdiomasSite = require('./NovoCadastroIdiomasSite');
const LeadCalculadora = require('./LeadCalculadora');
const Job = require('./Job');
const BlogPost = require('./BlogPost');
const BlogImage = require('./BlogImage');
const Rating = require('./Rating');

const dbInterface = {
  User: new User(),
  Address: new Address(),
  Aulas: new Aulas(),
  File: new File(),
  Cartoes: new Cartoes(),
  PreCadastro: new PreCadastro(),
  Leads: new Leads(),
  Leads2: new Leads2(),
  EmailMarketing: new EmailMarketing(),
  InscritosNewsletter: new InscritosNewsletter(),
  Preco: new Preco(),
  Professor: new Professor(),
  Escola: new Escola(),
  ProfessorCandidato: new ProfessorCandidato(),
  NovoCadastroSite: new NovoCadastroSite(),
  NovoCadastroIdiomasSite: new NovoCadastroIdiomasSite(),
  LeadCalculadora: new LeadCalculadora(),
  Job: new Job(),
  BlogPost: new BlogPost(),
  BlogImage: new BlogImage(),
  Rating: new Rating(),
};

module.exports = dbInterface;
