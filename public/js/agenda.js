$(function() {
  moment()
    .tz('America/Sao_Paulo')
    .format();

  $('.mode-toggle').on('click', function() {
    $('.mode-selector').toggle();
  });

  getAgenda(function(aulas) {
    if (aulas.length == 0) {
      $('.sem-aulas-container').show();
      return;
    }

    $('#calendar').fullCalendar({
      events: aulas.map(function(a) {
        return { title: a.topico, start: a.dia.split('T')[0], aula: a };
      }),
      header: {
        left: 'prev',
        center: 'title',
        right: 'today,next'
      },
      defaultView: 'month',
      locale: 'pt-br',
      eventClick: function(calEvent, jsEvent, view) {
        loadAulaData(calEvent.aula);
        setupHandlers();
      }
    });

    $('button.fc-today-button').html('Hoje');
  });
});

function loadAulaData(aula) {
  var container = $('.selected-aula-container');

  var id = aula.id;
  var fotoProf = aula.prof.foto ? aula.prof.foto : '/img/profile_placeholder.png';
  var nomeProf = aula.prof.nome;
  var notaProf = aula.prof.avaliacao;
  var duracao = aula.duracao;
  var dia = moment(aula.dia);
  var hora =
    dia.clone().format('HH:mm') +
    ' - ' +
    dia
      .clone()
      .add(duracao, 'hours')
      .format('HH:mm');

  var diaString = dia.date() + ' de ' + moment.months()[dia.month()];
  var duracaoAula = '';
  if (duracao == 0.5) {
    duracaoAula = '30 minutos de aula';
  } else if (duracao == 1) {
    duracaoAula = '1 hora de aula';
  } else if (duracao == 1.5) {
    duracaoAula = '1 hora e 30 minutos de aula';
  } else if (duracao == 2) {
    duracaoAula = '2 horas de aula';
  } else if (duracao == 2.5) {
    duracaoAula = '2 horas e 30 minutos de aula';
  } else if (duracao == 3) {
    duracaoAula = '3 horas de aula';
  }

  var materia = aula.topico;
  var nivel = aula.ensino;
  var bairro = aula.endereco.bairro;
  var endereco =
    aula.endereco.endereco + ' ' + aula.endereco.numero + ' ' + aula.endereco.complemento;
  var preco = 'R$' + Number(aula.preco).toFixed(2);
  var card;
  if (aula.card) {
    var cartao = '<span class="card-number-gray">xxxx-xxxx-xxxx-</span>' + aula.card.digits;
    var bandeiraFoto = '/img/' + aula.card.brand.toLowerCase() + '-logo.png';

    card =
      "<div class='aula-cartao'>\
                <div class='card-numero'>\
                <img class='card-icon' src='/img/gray-card.png' />\
                    <div class='card-final'>" +
      cartao +
      "</div>\
                </div>\
                <img src='" +
      bandeiraFoto +
      "' alt='Cartao bandeira' class='card-bandeira'>\
                </div>";
  } else {
    card = '';
  }

  container.html(
    "<div class='aula-card' id=" +
      id +
      ">\
    <div class='cancel-free-template' display: hidden>\
        <div class='cancel-header'>Cancelar aula</div>\
        <div class='cancel-message'>Tem certeza que deseja cancelar a aula?</div>\
        <a href='javascript:void(0)' class='voltar-free'>Voltar</a>\
        <div class='vertical-separator'></div>\
        <a href='javascript:void(0)' class='cancelar-free'>Confirmar</a>\
    </div>\
    <div class='cancel-paid-template' display: hidden>\
        <div class='cancel-header'>Cancelar aula</div>\
        <div class='cancel-message'>Faltam menos de 3 horas para o início da aula. <br> Se deseja proceder com o cancelamento, será cobrada uma taxa de R$15,00.</div>\
        <a href='javascript:void(0)' class='voltar-paid'>Voltar</a>\
        <div class='vertical-separator'></div>\
        <a href='javascript:void(0)' class='cancelar-paid'>Confirmar</a>\
    </div>\
    <div class='cancel-success-template' display: hidden>\
        <div class='cancel-header'>Aula cancelada</div>\
        <div class='cancel-message'>Sua aula foi cancelada com sucesso!</div>\
        <a href='/aluno/agenda' class='cancel-success'>Voltar para <br> agenda</a>\
        <div class='vertical-separator'></div>\
        <a href='/aluno/historico' class='cancel-success-historico'>Ver no <br> histórico</a>\
    </div>\
    <div class='aula-content'>\
        <div class='aula-professor'>\
            <div class='professor-left'>\
                    <img src='" +
      fotoProf +
      "' />\
            </div>\
            <div class='professor-right'>\
                <div class='professor-nome'>" +
      nomeProf +
      "</div>\
                <div class='professor-rating'>\
                    <div class='professor-rating-value'>" +
      notaProf +
      "</div>\
                    <img src='/img/rating-star.png' class='professor-rating-img' />\
                </div>\
                <div class='aula-materia'>" +
      materia +
      "</div><br>\
                <div class='aula-nivel'>" +
      nivel +
      "</div>\
            </div>\
        </div>\
        <hr class='card-separator'>\
        <div class='aula-data'>\
            <div class='aula-dia'>" +
      diaString +
      "</div>\
            <div class='aula-horario'>" +
      hora +
      "</div>\
            <div class='aula-duracao'>" +
      duracaoAula +
      "</div>\
        </div>\
        <hr class='card-separator'>\
        <div class='aula-endereco'>\
            <div class='aula-bairro'> " +
      bairro +
      "</div>\
            <div class='aula-endereco-completo'>" +
      endereco +
      "</div>\
        </div>\
        <hr class='card-separator'>" +
      card +
      "<div class='aula-preco'>\
            Total:\
            <div class='aula-preco-value'>" +
      preco +
      "</div>\
        </div>\
        </div>\
            <a href='javascript:void(0)' class='cancelar-button'>Cancelar aula</a>\
        </div>\
    </div>\
    "
  );
}

function getAgenda(callback) {
  $.ajax({
    url: '/aluno/agenda/buscar',
    type: 'GET'
  })
    .done(function(result) {
      if (result.success) {
        callback(result.aulas);
      } else if (result.message) {
        alert(result.message);
      }
    })
    .fail(function() {
      alert('Houve um erro ao obter sua agenda, tente novamente mais tarde.');
    });
}

function setupHandlers() {
  $('.aula-card .cancelar-button').on('click', function() {
    var id = $(this)
      .closest('.aula-card')
      .attr('id');

    var container = $('#' + id);
    var aulaContent = container.find('.aula-content');
    var cancelFreeTemplate = container.find('.cancel-free-template');

    aulaContent.fadeTo('fast', 0, function() {
      cancelFreeTemplate.fadeIn('fast');
    });
  });

  $('.cancel-free-template .voltar-free').on('click', function() {
    var id = $(this)
      .closest('.aula-card')
      .attr('id');
    var container = $('#' + id);
    var aulaContent = container.find('.aula-content');
    var cancelFreeTemplate = container.find('.cancel-free-template');

    cancelFreeTemplate.fadeOut('fast', function() {
      aulaContent.fadeTo('fast', 1);
    });
  });

  $('.cancel-free-template .cancelar-free').on('click', function() {
    var id = $(this)
      .closest('.aula-card')
      .attr('id');
    cancelarAula(id);
  });

  $('.cancel-paid-template .voltar-paid').on('click', function() {
    var id = $(this)
      .closest('.aula-card')
      .attr('id');
    var container = $('#' + id);
    var aulaContent = container.find('.aula-content');
    var cancelPaidTemplate = container.find('.cancel-paid-template');

    cancelPaidTemplate.fadeOut('fast', function() {
      aulaContent.fadeTo('fast', 1);
    });
  });

  $('.cancel-paid-template .cancelar-paid').on('click', function() {
    var id = $(this)
      .closest('.aula-card')
      .attr('id');

    confirmarCancelamento(id);
  });
}

function cancelarAula(id) {
  $.ajax({
    url: '/reforco-escolar/aula/cancelar',
    type: 'POST',
    data: { id: id }
  })
    .done(function(response) {
      if (response.success) {
        showCancelSuccess(id);
      } else if (response.code && response.code == 1001) {
        //cancelamento pago
        var container = $('#' + id);
        var cancelFreeTemplate = container.find('.cancel-free-template');
        var cancelPaidTemplate = container.find('.cancel-paid-template');

        cancelFreeTemplate.fadeOut('fast', function() {
          cancelPaidTemplate.fadeIn('fast');
        });
      } else if (response.error) {
        alert(response.error);
      }
    })
    .fail(function(jqxhr, textStatus, error) {
      console.log(error);
      alert(
        'Houve um erro ao cancelar sua aula. Por favor, entre em contato conosco pelo WhatsApp (61) 9 9805-2073.'
      );
    });
}

function confirmarCancelamento(id) {
  $.ajax({
    url: '/reforco-escolar/aula/cancelar/confirmar',
    type: 'POST',
    data: { id: id }
  })
    .done(function(response) {
      if (response.error) {
        alert(response.error);
      } else {
        showCancelSuccess(id);
      }
    })
    .fail(function(jqxhr, textStatus, error) {
      console.log(error);
      alert(
        'Houve um erro ao cancelar sua aula. Por favor, entre em contato conosco pelo WhatsApp (61) 9 9805-2073.'
      );
    });
}

function showCancelSuccess(id) {
  var cancelFreeTemplate = $('#' + id).find('.cancel-free-template');
  var cancelPaidTemplate = $('#' + id).find('.cancel-paid-template');
  var cancelSuccessTemplate = $('#' + id).find('.cancel-success-template');

  //is visible
  if (!(cancelFreeTemplate.css('display') == 'none')) {
    cancelFreeTemplate.hide();
  } else if (!(cancelPaidTemplate.css('display') == 'none')) {
    cancelPaidTemplate.hide();
  }

  cancelSuccessTemplate.fadeIn('fast');
}
