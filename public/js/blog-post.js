$(function() {
  $('.blog-post')
    .find('a')
    .attr('target', '_blank');

  setupForm();

  function setupForm() {
    $('form.newsletter__form').on('submit', function(e) {
      e.preventDefault();

      if ($('.newsletter__input--nome').val() == '' || $('.newsletter__input--email').val() == '') {
        alert('Por favor, preencha todos os campos!');
        return;
      }

      $.ajax({
        url: '/newsletter',
        type: 'POST',
        data: {
          nome: $('.newsletter__input--nome').val(),
          email: $('.newsletter__input--email').val()
        }
      })
        .done(function(result) {
          $('.newsletter__form')
            .find('.newsletter__input')
            .val('');

          if (result.success) {
            alert('Cadastro realizado com sucesso!');
          } else {
            alert('Desculpe, houve um erro ao receber seus dados.');
          }
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
          alert('Desculpe, houve um erro ao receber seus dados.');
        });
    });
  }
});
