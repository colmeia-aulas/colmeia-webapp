$(function() {
  hideFields();
  setupMask();

  handleNovoEndereco();
  handleEnderecoClienteOnline();
  handleNovoCartao();

  var cadastroTipo = new URL(window.location.href).searchParams.get('tipo');

  $('.register-button').on('click', function() {
    if (validateRegister()) {
      register();
    }
  });

  $('#register-email, #register-senha').keyup(function(e) {
    if (e.keyCode === 13) {
      $('.register-button').click();
    }
  });

  $('.register-facebook-button').on('click', function() {
    fbLogin();
  });

  function register() {
    var values = {
      nome: $('#register-nome').val(),
      password: $('#register-senha').val(),
      email: $('#register-email')
        .val()
        .toLowerCase()
        .trim(),
      telefone: '+55' + $('#register-telefone').cleanVal(),
      digits_id: $('#register-digits-id').val()
    };

    $.ajax({
      url: '/aluno/registrar',
      type: 'POST',
      data: values
    })
      .done(function(data) {
        if (data.success) {
          // Checa se o Parse retornou algum erro

          try {
            woopra.identify({
              email: data.user.email,
              name: data.user.nome,
              phone: data.user.phone,
              idparse: data.user.id
            });

            woopra.track('signup', {
              facebook: false
            });
          } catch (e) {}

          ga('send', 'event', {
            eventCategory: 'Cadastro express exp.',
            eventAction: 'step1'
          });

          showEnderecoContainer();
        } else {
          alert(
            data.message ||
              'Houve um erro ao realizar seu cadastro. Por favor, entre em contato conosco pelo WhatsApp (61) 9 9805-2073.'
          );
        }
      })
      .fail(function(jqXHR, textStatus, errorThrown) {
        console.log('erro');
        console.log(textStatus);
        console.log(errorThrown);
        $('.error-container-register').html('Algo deu errado, tente novamente mais tarde.');
      });
  }

  function validateRegister() {
    $('.error-container-register').html('');

    var name = $('#register-nome').val();
    if (name === undefined || name === null || /\S/.test(name) === false) {
      // Checa se a string é vazia
      $('.error-container-register').html('Digite um nome para se cadastrar');
      return false;
    }

    var email = $('#register-email').val();
    if (email === undefined || email === null || /.+@.+/.test(email) === false) {
      // Checa se a string é vazia
      $('.error-container-register').html('O email inserido não é válido.');
      return false;
    }

    var password = $('#register-senha').val();
    if (password === undefined || password === null || password.length < 6) {
      $('.error-container-register').html('A senha deve ter no mínimo 6 caracteres.');
      return false;
    }

    var telefone = $('#register-telefone').val();
    if (telefone === undefined || telefone === null || telefone == '') {
      $('.error-container-register').html('Insira um número de telefone!');
      return false;
    }

    var termosAceitos = $("input[type='checkbox']").prop('checked');
    if (!termosAceitos) {
      $('.error-container-register').html('Você deve aceitar os termos para prosseguir!');
      return false;
    }

    return true;
  }

  function handleNovoEndereco() {
    $('.enderecos-container .colmeia-button').on('click', function() {
      $('.enderecos-container').fadeOut('fast', function() {
        $('#enderecos-header').html('Novo endereço');
        $('#novo-endereco').fadeIn('fast');
      });
    });

    $('#select-cidade').empty();
    $('#select-bairro').empty();

    Object.keys(CIDADES).forEach(function(key, index) {
      $('#select-cidade').append("<option value='" + key + "'>" + CIDADES[key] + '</option>');
    });

    var bairros = BAIRROS['brasilia'];

    $('#select-bairro').append('<option disabled selected value="">Selecione um bairro</option>');

    Object.keys(bairros).forEach(function(key, index) {
      if (key != 'todos-os-bairros') {
        $('#select-bairro').append("<option value='" + key + "'>" + bairros[key] + '</option>');
      }
    });

    $('#select-cidade').on('change', function() {
      var cidade = $(this).val();
      bairros = BAIRROS[cidade];

      $('#select-bairro').empty();

      $('#select-bairro').append('<option disabled selected value="">Selecione um bairro</option>');

      Object.keys(bairros).forEach(function(key, index) {
        if (key != 'todos-os-bairros') {
          $('#select-bairro').append("<option value='" + key + "'>" + bairros[key] + '</option>');
        }
      });
    });

    $('#enderecoContainer .colmeia-button.button-filled').on('click', function() {
      if (
        !$('#select-bairro').val() ||
        $('#input-endereco').val() == '' ||
        $('#input-numero').val() == ''
      ) {
        alert('Por favor, preencha todos os campos para continuar.');
      } else {
        var estados = {
          'brasilia': 'Distrito Federal',
          'sao-paulo': 'São Paulo'
        }

        var cidadeValue = $('#select-cidade').val();

        var bairroVal = $('#select-bairro').val();
        var cidadeText = $('#select-cidade option:selected').text();
        var bairro = BAIRROS[cidadeValue][bairroVal];
        var estado = estados[cidadeValue];

        var novoEnd = {
          endereco: $('#input-endereco').val(),
          numero: $('#input-numero').val(),
          complemento: $('#input-complemento').val(),
          bairro: bairro,
          cidade: cidadeText,
          estado: estado
        };

        $.ajax({
          url: '/reforco-escolar/endereco/adicionar',
          type: 'POST',
          data: novoEnd
        })
          .done(function(results) {
            if (results.success) {
              ga('send', 'event', {
                eventCategory: 'Cadastro express exp.',
                eventAction: 'step2'
              });

              showPagamentoContainer();
            } else {
              alert('Erro ao adicionar novo endereço!');
            }
          })
          .fail(function(jqXHR, textStatus, errorThrown) {
            alert('Erro ao adicionar novo endereço!');
          });
      }
    });
  }

  function handleEnderecoClienteOnline() {
    $('#select-estado-online').empty();

    Object.keys(ESTADOS).forEach(function(key, index) {
      $('#select-estado-online').append("<option value='" + key + "'>" + ESTADOS[key] + '</option>');
    });

    $('#select-estado-online').on('change', function() {
      $('#input-cidade-online').val('');
    });

    $('#enderecoContainerOnline .colmeia-button.button-filled').on('click', function() {
      if (
        !$('#select-estado-online').val() ||
        $('#input-cidade-online').val() == '' ||
        $('#input-endereco-online').val() == '' ||
        $('#input-numero-online').val() == ''
      ) {
        alert('Por favor, preencha todos os campos para continuar.');
      } else {
        var estado = $('#select-estado-online option:selected').text();
        var cidade = $('#input-cidade-online').val();
        var endereco = $('#input-endereco-online').val();
        var numero = $('#input-numero-online').val();
        var complemento = $('#input-complemento-online').val();

        $.ajax({
          url: '/reforco-escolar/endereco/adicionar',
          type: 'POST',
          data: {
            estado: estado,
            endereco: endereco,
            numero: numero,
            complemento: complemento,
            cidade: cidade,
            bairro: 'Online'
          }
        })
          .done(function(results) {
            if (results.success) {
              ga('send', 'event', {
                eventCategory: 'Cadastro express exp.',
                eventAction: 'step2'
              });

              showPagamentoContainer();
            } else {
              alert('Erro ao adicionar novo endereço!');
            }
          })
          .fail(function(jqXHR, textStatus, errorThrown) {
            alert('Erro ao adicionar novo endereço!');
          });
      }
    });
  }

  function handleNovoCartao() {
    $('#input-mes').mask('00');
    $('#input-ano').mask('00');
    $('#input-cpf-boleto').mask('000.000.000-00');
    $('#input-seguranca').mask('0000');
    $('#input-nascimento').mask('00/00/0000');

    //confirmar
    $('#pagamentoContainer .colmeia-button.button-filled').on('click', function() {
      if ($('#option-cartao').hasClass('card-checked')) {
        if (
          $('#input-nome').val() == '' ||
          $('#input-cpf').val() == '' ||
          $('#input-mes').val() == '' ||
          $('#input-ano').val() == '' ||
          $('#input-cartaonu').val() == '' ||
          $('#input-seguranca').val() == '' ||
          $('#input-nascimento').val() == ''
        ) {
          alert('Por favor, preencha todos os campos.');
          return;
        }

        if ($('#input-nascimento').val().length < 10) {
          alert('Insira uma data de nascimento válida.');
          return;
        }

        if ($('#input-cpf').val().length < 11) {
          alert('Insira um número de cpf válido.');
          return;
        }

        var nascimentoArray = $('#input-nascimento')
          .val()
          .split('/');
        var nascimento = nascimentoArray[2] + '-' + nascimentoArray[1] + '-' + nascimentoArray[0];

        var novoCard = {
          nome: $('#input-nome').val(),
          numero: $('#input-cartaonu').val(),
          mes: $('#input-mes').val(),
          ano: '20' + $('#input-ano').val(),
          cpf: $('#input-cpf').val(),
          seguranca: $('#input-seguranca').val(),
          nascimento: nascimento
        };

        hashCard(novoCard);
      } else {
        if ($('#input-nome-boleto').val() == '' || $('#input-cpf-boleto').val() == '') {
          alert('Preencha todos os campos para continuar.');
          return;
        }

        var data = {
          nomeCompleto: $('#input-nome-boleto').val(),
          cpf: $('#input-cpf-boleto').cleanVal()
        };

        $.ajax({
          url: '/aluno/update',
          type: 'POST',
          data: { data: data }
        })
          .done(function(data) {
            if (data.success) {
              ga('send', 'event', {
                eventCategory: 'Cadastro express exp.',
                eventAction: 'step3-boleto'
              });

              showFinalContainer();
            } else if (data.message) {
              alert(data.message);
            } else {
              alert('Algo deu errado, por favor, tente novamente.');
            }
          })
          .fail(function() {
            console.log('error');
            alert('Algo deu errado, por favor, tente novamente.');
          });
      }
    });
  }

  function hashCard(novoCard) {
    var isNumberValid = Moip.Validator.isValid(novoCard.numero);
    var isExpiryValid = Moip.Validator.isExpiryDateValid(novoCard.mes, novoCard.ano);
    var isCvcValid = Moip.Validator.isSecurityCodeValid(novoCard.numero, novoCard.seguranca);

    if (!isNumberValid) {
      alert(
        'O número do cartão que você inseriu não é válido. Verifique os campos e tente novamente.'
      );
      return false;
    }
    if (!isExpiryValid) {
      alert(
        'A data de expiração do cartão que você inseriu não é válida. Verifique os campos e tente novamente.'
      );
      return false;
    }
    if (!isCvcValid) {
      alert(
        'O código de segurança do cartão que você inseriu não é válido. Verifique os campos e tente novamente.'
      );
      return false;
    }

    var objectToEncryp = {
      expirationMonth: novoCard.mes,
      expirationYear: novoCard.ano,
      number: novoCard.numero,
      cvc: novoCard.seguranca,
      birthdate: novoCard.nascimento,
      fullname: novoCard.nome,
      cpf: novoCard.cpf
    };

    var encryptedCard = NodeRsaEncryptString('card', JSON.stringify(objectToEncryp));
    addCard(encryptedCard);
  }

  function addCard(hash) {
    var cpf = $('#input-cpf').val();

    $.ajax({
      url: '/reforco-escolar/pagamento/adicionar',
      type: 'POST',
      data: { card_hash: hash, cpf: cpf }
    })
      .done(function(data) {
        if (data.success) {
          try {
            woopra.identify({
              email: user.email,
              name: user.nome,
              phone: user.phone,
              idparse: user.id
            });

            woopra.track('add_card', {});
          } catch (e) {}

          ga('send', 'event', {
            eventCategory: 'Cadastro express exp.',
            eventAction: 'step3-card'
          });

          showFinalContainer();
        } else if (data.message) {
          alert(data.message);
        } else {
          alert('Houve um erro ao adicionar seu cartão, tente novamente mais tarde.');
        }
      })
      .fail(function() {
        alert('Houve um erro ao adicionar seu cartão, tente novamente mais tarde.');
      });
  }

  function showEnderecoContainer() {
    $('.header').html('Adicione seu endereço');

    $('#loginContainer').fadeOut('fast', function() {
      if (cadastroTipo === 'Online') {
        $('#enderecoContainerOnline').fadeIn('fast');
      } else {
        $('#enderecoContainer').fadeIn('fast');
      }
    });
  }

  function showFinalContainer() {
    $('.header').html('Cadastro finalizado');

    $('#pagamentoContainer').fadeOut('fast', function() {
      $('#finalContainer').fadeIn('fast');
    });
  }

  function showPagamentoContainer() {
    $('.header').html('Escolha sua forma de pagamento');

    if (cadastroTipo === 'Online') {
      $('#enderecoContainerOnline').fadeOut('fast', function() {
        $('#pagamentoContainer').fadeIn('fast');
      });
    } else {
      $('#enderecoContainer').fadeOut('fast', function() {
        $('#pagamentoContainer').fadeIn('fast');
      });
    }


    var url = new URL(window.location.href);
    var isCartao = url.searchParams.get('cartao');

    // type is set
    if (isCartao !== null) {
      $('#option-boleto')
        .parent()
        .hide();
      $('#option-cartao')
        .removeClass('card-unchecked')
        .addClass('card-checked');
      $('.cartao-container').show();
      $('#pagamentoContainer')
        .find('.colmeia-button')
        .show();
    } else {
      // show both cartao and boleto
      $('#option-boleto').on('click', function() {
        $(this)
          .removeClass('card-unchecked')
          .addClass('card-checked');

        $('#option-cartao')
          .removeClass('card-checked')
          .addClass('card-unchecked');

        $('.boleto-container').show();
        $('.cartao-container').hide();

        $('#pagamentoContainer .colmeia-button').show();
      });

      $('#option-cartao').on('click', function() {
        $(this)
          .removeClass('card-unchecked')
          .addClass('card-checked');

        $('#option-boleto')
          .removeClass('card-checked')
          .addClass('card-unchecked');

        $('.boleto-container').hide();
        $('.cartao-container').show();

        $('#pagamentoContainer .colmeia-button').show();
      });
    }
  }

  function setupMask() {
    var maskBehavior = function(val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
      },
      options = {
        onKeyPress: function(val, e, field, options) {
          field.mask(maskBehavior.apply({}, arguments), options);
        }
      };

    $('#register-telefone').mask(maskBehavior, options);
  }

  function hideFields() {
    var url = new URL(window.location.href);
    var name = url.searchParams.get('nome');
    var telefone = url.searchParams.get('telefone');

    if (name && telefone) {
      $('#register-nome')
        .val(name)
        .parent()
        .hide();
      $('#register-telefone')
        .val(telefone)
        .parent()
        .hide();

      ga('send', 'event', {
        eventCategory: 'Cadastro express exp.',
        eventAction: 'pageview'
      });
    }
  }
});
