var bancoContainer = $('.banco-container');
var banco1Container = $('.banco-1');
var banco2Container = $('.banco-2');
var banco3Container = $('.banco-3');

var horariosContainer = $('.horarios-container');
var horarios1Container = $('.horarios-1');
var horarios2Container = $('.horarios-2');
var horarios3Container = $('.horarios-3');
var horarios4Container = $('.horarios-4');
var horarios5Container = $('.horarios-5');
var horarios6Container = $('.horarios-6');
var horarios7Container = $('.horarios-7');
var horarios8Container = $('.horarios-8');

$(function() {
  setUp();

  introduction();
  step4();
  step5();

  handleCurrentStep();
  handleBackButton();
});

function setUp() {
  $('.select-banco').select2({
    placeholder: 'Selecione o seu banco',
    width: '100%',
    closeOnSelect: false
  });

  fixSelect();
}

function introduction() {
  var container = $('.introduction-container');
  var nextContainer = banco1Container;

  container.show();

  container.find('.orange-button').on('click', function() {
    container.fadeOut('fast', function() {
      nextContainer.fadeIn('fast', function() {
        scrollTo(nextContainer);
      });
    });
  });
}

function handleCurrentStep() {
  hideAll();

  var stepArray = response.step;

  //banco
  if (stepArray[4] == 'false') {
    $('.step-right').attr('src', '/img/step-4-side.png');
    $('.step-top').attr('src', '/img/step-4-top.png');
    var container = $('.introduction-container');
    container.fadeIn('fast', function() {
      // scrollTo(container);
    });
  } else {
    //horarios
    $('.step-right').attr('src', '/img/step-5-side.png');
    $('.step-top').attr('src', '/img/step-5-top.png');
    var container = $('.welcomeback-container');
    var nextContainer = horarios1Container;

    container.fadeIn('fast');

    container.find('.orange-button').on('click', function() {
      container.fadeOut('fast', function() {
        nextContainer.fadeIn('fast', function() {
          scrollTo(nextContainer);
        });
      });
    });

    container.find('.white-button').on('click', function() {
      container.fadeOut('fast', function() {
        banco1Container.fadeIn('fast', function() {
          scrollTo(banco1Container);
        });
      });
    });
  }
}

function step4() {
  banco1();
  banco2();
  banco3();
}

function step5() {
  horarios1();
  horarios2();
  horarios3();
  horarios4();
  horarios5();
  horarios6();
  horarios7();
  horarios8();
}

function banco1() {
  //nome do banco
  var container = banco1Container;
  var nextContainer = banco2Container;

  container.find('.orange-button').on('click', function() {
    var bancoSelecionado = $('.select-banco').val();

    if (!bancoSelecionado) {
      alert('Selecione uma opção para continuar.');
      return;
    }

    $('.step-right').attr('src', '/img/step-4-side.png');
    $('.step-right').fadeIn('fast');
    moveOn(container, nextContainer);
  });
}

function banco2() {
  //tipo de conta
  var container = banco2Container;
  var nextContainer = banco3Container;

  container.find('.orange-button').on('click', function() {
    var tipoSelecionado = $('input[name="radio-tipo-banco"]:checked').length > 0;

    if (!tipoSelecionado) {
      alert('Selecione uma opção para continuar.');
      return;
    }

    moveOn(container, nextContainer);
  });
}

function banco3() {
  //dados bancarios
  var container = banco3Container;

  container.find('.orange-button').on('click', function() {
    var agencia = $('input[name="agencia"]').val();
    var conta = $('input[name="conta"]').val();

    if (!agencia || !conta) {
      alert('Digite os dados do seu banco para continuar.');
      return;
    }

    sendStep4();
  });
}

function horarios1() {
  var container = horarios1Container;
  var nextContainer = horarios2Container;

  container.find('.orange-button').on('click', function() {
    $('.step-right').attr('src', '/img/step-5-side.png');
    $('.step-right').fadeIn('fast');
    moveOn(container, nextContainer);
  });
}

function horarios2() {
  //horario segunda
  var container = horarios2Container;
  var nextContainer = horarios3Container;

  container.find('.orange-button').on('click', function() {
    moveOn(container, nextContainer);
  });
}

function horarios3() {
  //horario terca
  var container = horarios3Container;
  var nextContainer = horarios4Container;

  container.find('.orange-button').on('click', function() {
    moveOn(container, nextContainer);
  });
}

function horarios4() {
  //horario quarta
  var container = horarios4Container;
  var nextContainer = horarios5Container;

  container.find('.orange-button').on('click', function() {
    moveOn(container, nextContainer);
  });
}

function horarios5() {
  //horario quinta
  var container = horarios5Container;
  var nextContainer = horarios6Container;

  container.find('.orange-button').on('click', function() {
    moveOn(container, nextContainer);
  });
}

function horarios6() {
  //horario sexta
  var container = horarios6Container;
  var nextContainer = horarios7Container;

  container.find('.orange-button').on('click', function() {
    moveOn(container, nextContainer);
  });
}

function horarios7() {
  //horario sabado
  var container = horarios7Container;
  var nextContainer = horarios8Container;

  container.find('.orange-button').on('click', function() {
    moveOn(container, nextContainer);
  });
}

function horarios8() {
  //horario domingo
  var container = horarios8Container;

  container.find('.orange-button').on('click', function() {
    sendStep5();
  });
}

function sendStep4() {
  var banco = $('.select-banco').val();
  var tipo = $('input[name="radio-tipo-banco"]').val();
  var agencia = $('input[name="agencia"]').val();
  var conta = $('input[name="conta"]').val();

  var agenciaDv = $('input[name="agencia-dv"]').val();
  var contaDv = $('input[name="conta-dv"]').val();

  var inf = {
    banco: banco,
    bankType: tipo,
    agencia: agencia,
    conta: conta,
    dvAgencia: agenciaDv,
    dvConta: contaDv
  };

  var dados = {
    step: 5,
    data: inf
  };

  $.ajax({
    url: '/professor/cadastro/update',
    type: 'post',
    data: dados
  })
    .done(function(r) {
      if (r == true) {
        try {
          woopra.identify({
            email: response.username,
            phone: response.telefone
          });

          woopra.track('cadastroProfessor', {
            passo: '4'
          });
        } catch (e) {}

        bancoContainer.fadeOut('fast', function() {
          horarios1Container.fadeIn('fast', function() {
            $('.step-right').hide();
            scrollTo(horarios1Container);
          });
        });
      } else {
        try {
          ga('send', 'exception', {
            exDescription: 'Parse retornou false para o pedido de post.',
            response: r,
            etapa: 'Enderecos'
          });
        } catch (error) {}

        alert('Algo deu errado, tente novamente mais tarde.');
      }
    })
    .fail(function(jqXHR, textStatus, errorThrown) {
      try {
        ga('send', 'exception', {
          exDescription: 'Ajax retornou fail para o pedido de post.',
          textStatus: textStatus,
          errorThrown: errorThrown,
          etapa: 'Enderecos'
        });
      } catch (error) {}

      alert('Algo deu errado, tente novamente mais tarde.');
    });
}

function sendStep5() {
  var daysHash = {
    2: 'segunda',
    3: 'terca',
    4: 'quarta',
    5: 'quinta',
    6: 'sexta',
    7: 'sabado',
    8: 'domingo'
  };

  var horarios = {
    segunda: [],
    terca: [],
    quarta: [],
    quinta: [],
    sexta: [],
    sabado: [],
    domingo: []
  };

  for (var i = 2; i <= 8; i++) {
    $('.horarios-' + i + ' input[type="checkbox"]:checked').each(function(el) {
      horarios[daysHash[i]].push($(this).val());
    });
  }

  var inf = {
    turnos: horarios
  };

  var dados = {
    step: 6,
    data: inf
  };

  $.ajax({
    url: '/professor/cadastro/update',
    type: 'post',
    data: dados
  })
    .done(function(r) {
      if (r == true) {
        try {
          woopra.identify({
            email: response.username,
            phone: response.telefone
          });

          woopra.track('cadastroProfessor', {
            passo: '5'
          });
        } catch (e) {}

        window.location = '/professor/cadastro';
      } else {
        try {
          ga('send', 'exception', {
            exDescription: 'Parse retornou false para o pedido de post.',
            response: r,
            etapa: 'Materias'
          });
        } catch (error) {}

        alert('Algo deu errado, tente novamente mais tarde.');
      }
    })
    .fail(function(jqXHR, textStatus, errorThrown) {
      try {
        ga('send', 'exception', {
          exDescription: 'Ajax retornou fail para o pedido de post.',
          textStatus: textStatus,
          errorThrown: errorThrown,
          etapa: 'Materias'
        });
      } catch (error) {}

      alert('Algo deu errado, tente novamente mais tarde.');
    });
}

function hideAll() {
  $('.step-right').hide();
  $('.introduction-container').hide();
  $('.welcomeback-container').hide();

  banco1Container.hide();
  banco2Container.hide();
  banco3Container.hide();

  horarios1Container.hide();
  horarios2Container.hide();
  horarios3Container.hide();
  horarios4Container.hide();
  horarios5Container.hide();
  horarios6Container.hide();
  horarios7Container.hide();
  horarios8Container.hide();
}

function handleBackButton() {
  $('.back-arrow').on('click', function() {
    if ($(this).attr('disabled') != true) {
      var target = $(this).attr('data-steptarget');
      var currentContainer = $(this).parent();
      moveBack(currentContainer, $(target));
      clearFields(currentContainer);
    }
  });
}

function moveOn(currentContainer, nextContainer) {
  if ($(nextContainer).css('opacity') != '1') {
    nextContainer.fadeTo('fast', 1, function() {
      currentContainer.fadeTo('fast', 0.3);
      disableElements(currentContainer);
      enableElements(nextContainer);
      scrollTo(nextContainer);
    });
  } else {
    nextContainer.fadeIn('fast', function() {
      currentContainer.fadeTo('fast', 0.3);
      disableElements(currentContainer);
      enableElements(nextContainer);
      scrollTo(nextContainer);
    });
  }
}

function moveBack(currentContainer, nextContainer) {
  //enable all previous elements
  scrollTo(nextContainer, function() {
    nextContainer.fadeTo('fast', 1, function() {
      enableElements(nextContainer);
      currentContainer.fadeTo('fast', 0);
    });
  });
}

function scrollTo(el, callback) {
  var elOffset = el.offset().top;
  var elHeight = el.height();
  var windowHeight = $(window).height();
  var offset;

  if (elHeight < windowHeight) {
    offset = elOffset - (windowHeight / 2 - elHeight / 2);
  } else {
    offset = elOffset;
  }

  $('html, body')
    .stop()
    .animate(
      {
        scrollTop: offset
      },
      500,
      function() {
        if (callback) callback();
      }
    );

  // window.scroll({
  //     top: Math.floor($(el).offset().top),
  //     left: 0,
  //     behavior: 'smooth'
  // });
}

function disableElements(parent) {
  $(parent)
    .find('input')
    .prop('disabled', 'true');
  $(parent)
    .find('button')
    .prop('disabled', 'true');
  $(parent)
    .find('.back-arrow')
    .prop('disabled', 'true');
}

function enableElements(parent) {
  $(parent)
    .find('input')
    .removeAttr('disabled');
  $(parent)
    .find('button')
    .removeAttr('disabled');
  $(parent)
    .find('.back-arrow')
    .removeAttr('disabled');
}

function clearFields(container) {
  $(container)
    .find('input[type="text"]')
    .val('');
  $(container)
    .find('input[type="checkbox"]')
    .prop('checked', false);
  $(container)
    .find('input[type="radio"]')
    .prop('checked', false);
  $(container)
    .find('.select-bairros')
    .val(null)
    .trigger('change');
}

function fixSelect() {
  var scrollTop;
  $('select').on('select2:selecting', function(event) {
    var $pr = $('#' + event.params.args.data._resultId).parent();
    scrollTop = $pr.prop('scrollTop');
  });
  $('select').on('select2:select', function(event) {
    var $pr = $('#' + event.params.data._resultId).parent();
    $pr.prop('scrollTop', scrollTop);
  });
}
