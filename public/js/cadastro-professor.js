var materiasContainer = $('.materias-container');
var materias1Container = $('.materias-1');
var materias2Container = $('.materias-2');
var materias3Container = $('.materias-3');
var materias4Container = $('.materias-4');
var materias5Container = $('.materias-5');
var materias6Container = $('.materias-6');
var materias7Container = $('.materias-7');
var materias8Container = $('.materias-8');

var bairrosContainer = $('.bairros-container');
var bairros1Container = $('.bairros-1');
var bairros2Container = $('.bairros-2');
var bairros3Container = $('.bairros-3');
var bairros4Container = $('.bairros-4');
var bairros5Container = $('.bairros-5');

var pessoalContainer = $('.pessoal-container');
var pessoal1Container = $('.pessoal-1');
var pessoal2Container = $('.pessoal-2');
var pessoal3Container = $('.pessoal-3');
var pessoal4Container = $('.pessoal-4');
var pessoal5Container = $('.pessoal-5');
var pessoal6Container = $('.pessoal-6');
var pessoal7Container = $('.pessoal-7');
var pessoal8Container = $('.pessoal-8');
var pessoal9Container = $('.pessoal-9');

var BAIRROS_BSB = {
  norte: [
    'Asa Norte',
    'Vila Planalto',
    'Lago Norte',
    'Noroeste',
    'Setor Militar',
    'Setor de Mansões do Lago Sorte',
    'Grande Colorado',
    'Sobradinho I'
  ],

  sul: [
    'Asa Sul',
    'Lago Sul',
    'Sudoeste',
    'Octogonal',
    'Cruzeiro',
    'Guará I',
    'Guará II',
    'Núcleo Bandeirante',
    'Candangolândia',
    'Águas Claras',
    'Vicente Pires',
    'Jardim Botânico',
    'Park Way',
    'Taguatinga'
  ]
};

var BAIRROS_SP = [
  'Água Branca',
  'Alto de Pinheiros',
  'Bela Vista',
  'Butantã',
  'Campo Belo',
  'Cidade Ademar',
  'Consolação',
  'Itaim Bibi',
  'Jabaquara',
  'Jardim Paulista',
  'Moema',
  'Mooca',
  'Morumbi',
  'Perdizes',
  'Pinheiros',
  'Samambaia',
  'Santo Amaro',
  'Saúde',
  'Vila Mariana'
];

var inglesSelected = false;

$(function () {
  setUp();

  introduction();
  step1();
  step2();
  step3();

  handleCurrentStep();
  handleBackButton();
});

function setUp() {
  $('.select-bairros').select2({
    placeholder: 'Selecione ou digite um bairro',
    width: '100%',
    closeOnSelect: false
  });

  $('input[name="cpf"]').mask('999.999.999-99');
  $('input[name="nascimento"]').mask('99/99/9999');
  $('input[name="rg_data"]').mask('99/99/9999');
  $('input[name="cep"]').mask('99999-999');

  $('input[name="numero"]').on('keypress', function (evt) {
    var charCode = evt.which ? evt.which : event.keyCode;
    return !(charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57));
  });

  $('input[name="numero"]').on('focusout', function () {
    var value = $(this).val();

    $(this).val(value.replace(/[^0-9]/g, ''));
  });

  fixSelect();
}

function introduction() {
  var container = $('.introduction-container');
  var nextContainer = materias1Container;

  container.find('.orange-button').on('click', function () {
    container.fadeOut('fast', function () {
      nextContainer.fadeIn('fast', function () {
        scrollTo(nextContainer);
      });
    });
  });
}

function handleCurrentStep() {
  hideAll();

  var stepArray = response.step;

  //materias
  if (stepArray[1] == 'false') {
    $('.step-right').attr('src', '/img/step-1-side.png');
    var container = $('.introduction-container');
    container.fadeIn('fast', function () {
      // scrollTo(container);
    });
  } else {
    var container = $('.welcomeback-container');
    container.fadeIn('fast');

    var nextContainer;

    if (stepArray[3] == 'false') {
      //bairros
      var nextContainer = bairros1Container;
    } else if (stepArray[0] == 'false') {
      //pessoal
      var nextContainer = pessoal1Container;
    } else {
      alert('Houve um erro, tente novamente mais tarde.');
      return;
    }

    container.find('.orange-button').on('click', function () {
      container.fadeOut('fast', function () {
        nextContainer.fadeIn('fast', function () {
          scrollTo(nextContainer);
        });
      });
    });

    container.find('.white-button').on('click', function () {
      container.fadeOut('fast', function () {
        materias1Container.fadeIn('fast', function () {
          scrollTo(materias1Container);
        });
      });
    });
  }
}

function step1() {
  materias1();
  materias2();
  materias3();
  materias4();
  materias5();
  materias6();
  materias7();
  materias8();
}

function step2() {
  bairros1();
  bairros2();
  bairros3();
  bairros4();
  bairros5();
}

function step3() {
  pessoal1();
  pessoal2();
  pessoal3();
  pessoal4();
  pessoal5();
  pessoal6();
  pessoal7();
  pessoal8();
  pessoal9();
}

function materias1() {
  //Tipo de ensino
  var container = materias1Container;
  var nextContainer;

  container.find('.orange-button').on('click', function () {
    var tipoSelecionado = container.find('input[type="radio"]:checked').length > 0;

    if (!tipoSelecionado) {
      alert('Selecione uma opção para continuar.');
      return;
    }

    var reforco = $('input[name="radio-tipo-materia"]:checked').val() === 'reforco' ? true : false;

    if (reforco) {
      nextContainer = materias2Container;
    } else {
      nextContainer = materias4Container;
      inglesSelected = true;
    }

    $('.step-right').attr('src', '/img/step-1-side.png');
    $('.step-right').fadeIn('fast');
    moveOn(container, nextContainer);
  });
}

function materias2() {
  //Materias reforco
  var container = materias2Container;
  var nextContainer = materias3Container;

  container.find('.orange-button').on('click', function () {
    var disciplinaSelecionada = container.find('input[type="checkbox"]:checked').length > 0;
    var hasOther = container.find('input[name="outro-reforco"]').val() != '';

    if (!disciplinaSelecionada && !hasOther) {
      alert('Selecione uma disciplina para continuar.');
      return;
    }

    moveOn(container, nextContainer);
  });
}

function materias3() {
  // Nivel reforco
  var container = materias3Container;
  var nextContainer = materias6Container;

  container.find('.orange-button').on('click', function () {
    var nivelSelecionado = container.find('input[type="checkbox"]:checked').length > 0;

    if (!nivelSelecionado) {
      alert('Selecione um nível para continuar.');
      return;
    }

    moveOn(container, nextContainer);
  });
}

function materias4() {
  //Materias idiomas
  var container = materias4Container;
  var nextContainer = materias5Container;

  container.find('.orange-button').on('click', function () {
    var idiomaSelecionado = container.find('input[type="checkbox"]:checked').length > 0;
    var hasOther = container.find('input[name="outro-idioma"]').val() != '';

    if (!idiomaSelecionado && !hasOther) {
      alert('Selecione um idioma para continuar.');
      return;
    }

    moveOn(container, nextContainer);
  });
}

function materias5() {
  //nivel idiomas
  var container = materias5Container;
  var nextContainer = materias7Container;

  container.find('.orange-button').on('click', function () {
    var nivelSelecionado = container.find('input[type="checkbox"]:checked').length > 0;

    if (!nivelSelecionado) {
      alert('Selecione um nível para continuar.');
      return;
    }

    moveOn(container, nextContainer);
  });
}

function materias6() {
  // ja deu aula reforco
  var container = materias6Container;

  container.find('.orange-button').on('click', function () {
    var selecionado = container.find('input[type="radio"]:checked').length > 0;

    if (!selecionado) {
      alert('Selecione uma opção para continuar.');
      return;
    }

    sendStep1();
  });
}

function materias7() {
  // ja deu aula idiomas
  var container = materias7Container;
  var nextContainer = materias8Container;

  container.find('.orange-button').on('click', function () {
    var selecionado = container.find('input[type="radio"]:checked').length > 0;

    if (!selecionado) {
      alert('Selecione uma opção para continuar.');
      return;
    }

    moveOn(container, nextContainer);
  });
}

function materias8() {
  // possui material idiomas
  var container = materias8Container;

  container.find('.orange-button').on('click', function () {
    var selecionado = container.find('input[type="radio"]:checked').length > 0;

    if (!selecionado) {
      alert('Selecione uma opção para continuar.');
      return;
    }

    sendStep1();
  });
}

function bairros1() {
  //Cidade

  var container = bairros1Container;
  var nextContainer;

  container.find('.orange-button').on('click', function () {
    var cidadeSelecionada = $('input[name="radio-cidade"]:checked').length > 0;

    if (!cidadeSelecionada) {
      alert('Selecione uma cidade para continuar.');
      return;
    }

    var selecionouReforco =
      $('input[name="radio-tipo-materia"]:checked').val() === 'reforco' ||
      (response.materiasIdiomas && response.materiasIdiomas[0] == '');
    var cidade = $('input[name="radio-cidade"]:checked').val();
    if (cidade == 'Brasília' && selecionouReforco) {
      nextContainer = bairros2Container;
    } else if (cidade == 'Brasília') {
      nextContainer = bairros4Container;
    } else {
      nextContainer = bairros5Container;
    }

    $('.step-right').attr('src', '/img/step-2-side.png');
    $('.step-right').fadeIn('fast');
    moveOn(container, nextContainer);
  });
}

function bairros2() {
  // disclaimer
  var container = bairros2Container;
  var nextContainer = bairros3Container;

  $('.bsb-reforco-accept').on('click', function () {
    ga('send', 'event', {
      eventCategory: 'Deslocamento cadastro professor',
      eventAction: 'aceita',
      eventLabel: 'Sim'
    });
  });

  $('.bsb-reforco-reject').on('click', function () {
    ga('send', 'event', {
      eventCategory: 'Deslocamento cadastro professor',
      eventAction: 'aceita',
      eventLabel: 'Não'
    });

    window.location.href = '/';
  });

  container.find('.orange-button').on('click', function () {
    moveOn(container, nextContainer);
  });
}

function bairros3() {
  // zonas bsb
  var container = bairros3Container;

  container.find('.orange-button').on('click', function () {
    var selecionado = container.find('input[type="radio"]:checked').length > 0;

    if (!selecionado) {
      alert('Selecione uma opção para continuar.');
      return;
    }

    sendStep2();
  });
}

function bairros4() {
  //Bairros bsb
  var container = bairros4Container;

  container.find('.orange-button').on('click', function () {
    var bairroSelecionado = container.find('input[type="checkbox"]:checked').length > 0;
    var hasOther = container.find('input[name="outro-bairro"]').val() != '';

    if (!bairroSelecionado && !hasOther) {
      alert('Selecione um bairro para continuar.');
      return;
    }

    sendStep2();
  });
}

function bairros5() {
  // Bairros SP
  var container = bairros5Container;

  container.find('.orange-button').on('click', function () {
    var bairroSelecionado = container.find('input[type="checkbox"]:checked').length > 0;
    var hasOther = container.find('input[name="outro-bairro"]').val() != '';

    if (!bairroSelecionado && !hasOther) {
      alert('Selecione um bairro para continuar.');
      return;
    }

    sendStep2();
  });
}

function pessoal1() {
  //Nome

  $('form#step1-form').on('submit', function (e) {
    e.preventDefault();
  });

  var container = pessoal1Container;
  var nextContainer = pessoal2Container;

  container.find('.orange-button').on('click', function () {
    var nome = $('input[name="nome"]').val();

    if (!nome) {
      alert('Digite seu nome para continuar.');
      return;
    }

    $('.step-right').attr('src', '/img/step-3-side.png');
    $('.step-right').fadeIn('fast');
    moveOn(container, nextContainer);
  });
}

function pessoal2() {
  //Nascimento

  var container = pessoal2Container;
  var nextContainer = pessoal3Container;

  container.find('.orange-button').on('click', function () {
    var nascimento = $('input[name="nascimento"]').val();

    if (!nascimento) {
      alert('Digite sua data de nascimento para continuar.');
      return;
    }

    if ($('input[name="nascimento"]').cleanVal().length < 8) {
      alert('Digite uma data de nascimento válida para continuar.');
      return;
    }

    moveOn(container, nextContainer);
  });
}

function pessoal3() {
  //CPF

  var container = pessoal3Container;
  var nextContainer = pessoal4Container;

  container.find('.orange-button').on('click', function () {
    var cpf = $('input[name="cpf"]').val();

    if (!cpf) {
      alert('Digite o número de seu CPF para continuar.');
      return;
    }
    if ($('input[name="cpf"]').cleanVal().length < 11) {
      alert('Digite um número de CPF válido para continuar.');
      return;
    }

    moveOn(container, nextContainer);
  });
}
function pessoal4() {
  //RG

  var container = pessoal4Container;
  var nextContainer = pessoal5Container;

  container.find('.orange-button').on('click', function () {
    var rgNumero = $('input[name="rg_numero"]').val();
    var rgOrgao = $('input[name="rg_orgao"]').val();
    var rgData = $('input[name="rg_data"]').cleanVal();

    if (!rgNumero || !rgOrgao || !rgData) {
      alert('Digite os dados do seu RG para continuar.');
      return;
    }

    if (rgData.length < 8) {
      alert('Insira uma data de emissão válida para continuar');
      return;
    }

    moveOn(container, nextContainer);
  });
}
function pessoal5() {
  //Endereco

  var container = pessoal5Container;
  var nextContainer = pessoal6Container;

  container.find('.orange-button').on('click', function () {
    var logradouro = $('input[name="logradouro"]').val();
    var numero = $('input[name="numero"]').val();
    var cidade = $('input[name="cidade"]').val();
    var bairro = $('input[name="bairro"]').val();
    var estado = $('select[name="estado"]').val();
    var cep = $('input[name="cep"]').cleanVal();

    if (!logradouro || !numero || !cidade || !bairro || !estado || !cep) {
      alert('Digite o dados do seu endereço para continuar.');
      return;
    }

    if (cep.length < 8) {
      alert('Digite um CEP válido para continuar');
      return;
    }

    moveOn(container, nextContainer);
  });
}

function pessoal6() {
  //Possui carro
  var container = pessoal6Container;
  var nextContainer = pessoal7Container;

  container.find('.orange-button').on('click', function () {
    var selecionado = container.find('input[type="radio"]:checked').length > 0;

    if (!selecionado) {
      alert('Selecione uma opção para continuar.');
      return;
    }

    moveOn(container, nextContainer);
  });
}

function pessoal7() {
  //Mae

  var container = pessoal7Container;
  var nextContainer = pessoal8Container;

  container.find('.orange-button').on('click', function () {
    var nomeMae = $('input[name="nome_mae"]').val();

    if (!nomeMae) {
      alert('Digite o nome completo da sua mãe para continuar.');
      return;
    }

    moveOn(container, nextContainer);
  });
}

function pessoal8() {
  //Como conheceu

  var container = pessoal8Container;
  var nextContainer = pessoal9Container;

  container.find('.orange-button').on('click', function () {
    var comoConheceu = $('input[name="como_conheceu"]').val();

    if (!comoConheceu) {
      alert('Digite como nos conheceu para continuar.');
      return;
    }

    if (inglesSelected || (response.materiasIdiomas && response.materiasIdiomas.length > 1)) {
      changeCurriculoTitle();
    }
    moveOn(container, nextContainer);
  });
}
function pessoal9() {
  //Historico

  $('input[name="historico"]').on('change', function () {
    var fileName = $('input[name="historico"]')[0].files[0].name;

    if (fileName !== '') {
      $('.file-button').hide();
      $('.file-button-success')
        .find('.file-button-name')
        .html(fileName);
      $('.file-button-success').show();
    } else {
      $('.file-button-success')
        .find('.file-button-name')
        .html('');
      $('.file-button-success').hide();
      $('.file-button').show();
    }
  });

  var container = pessoal9Container;

  container.find('.orange-button').on('click', function () {
    var historico = $('input[name="historico"]');

    if (!historico) {
      alert('Insira seu documento para continuar.');
      return;
    }

    if (!checkExtention(historico.val(), ['pdf', 'doc', 'docx', 'png', 'jpg', 'jpeg'])) {
      alert('Insira seu documento no formato pdf, doc, docx, png, jpg ou jpeg!');
      return;
    }

    if (historico[0].files[0].size > 10 * 1024 * 1024) {
      alert('Insira um arquivo de até 10Mb!');
      return;
    }

    sendStep3();
  });
}

function sendStep1() {
  var aulaData = {
    reforco: {
      materias: [],
      nivel: []
    },
    idiomas: {
      materias: [],
      nivel: []
    }
  };

  $('.materias-2 input[type="checkbox"]:checked').each(function (el) {
    aulaData.reforco.materias.push($(this).val());
  });

  if ($('input[name="outro-reforco"]').val() != '') {
    aulaData.reforco.materias.push($('input[name="outro-reforco"]').val());
  }

  $('.materias-3 input[type="checkbox"]:checked').each(function (el) {
    aulaData.reforco.nivel.push($(this).val());
  });

  $('.materias-4 input[type="checkbox"]:checked').each(function (el) {
    aulaData.idiomas.materias.push($(this).val());
  });

  if ($('input[name="outro-idioma"]').val() != '') {
    aulaData.idiomas.materias.push($('input[name="outro-idioma"]').val());
  }

  $('.materias-5 input[type="checkbox"]:checked').each(function (el) {
    aulaData.idiomas.nivel.push($(this).val());
  });

  var inf = {
    materiasReforco: aulaData.reforco.materias.length > 0 ? aulaData.reforco.materias : [''],
    materiasIdiomas: aulaData.idiomas.materias.length > 0 ? aulaData.idiomas.materias : [''],
    nivelReforco: aulaData.reforco.nivel.length > 0 ? aulaData.reforco.nivel : [''],
    nivelIdiomas: aulaData.idiomas.nivel.length > 0 ? aulaData.idiomas.nivel : [''],
    deuAulaReforco: $('input[name="radio-deu-aula-reforco"]:checked').val(),
    deuAulaIdioma: $('input[name="radio-deu-aula-idioma"]:checked').val(),
    possuiMaterialIdioma: $('input[name="radio-possui-material-idioma"]:checked').val()
  };

  var requestData = {
    step: 2,
    data: inf
  };

  $.ajax({
    url: '/professor/cadastro/update',
    type: 'post',
    data: requestData
  })
    .done(function (response) {
      if (response.success) {
        materiasContainer.fadeOut('fast', function () {
          bairros1Container.fadeIn('fast', function () {
            $('.step-right').hide();
            scrollTo(bairros1Container);
          });
        });
      } else if (response.message) {
        alert(response.message);
      }
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
      alert('Houve um erro ao atualizar suas informações, tente novamente mais tarde.');
    });
}

function sendStep2() {
  var data = {
    cidade: '',
    bairros: []
  };

  data.cidade = $('input[name="radio-cidade"]:checked').val();

  $('.bairros-2 input[type="checkbox"]:checked').each(function (el) {
    data.bairros.push($(this).val());
  });

  $('.bairros-3 input[type="checkbox"]:checked').each(function (el) {
    data.bairros.push($(this).val());
  });

  if ($('input[name="outro-bairro-bsb"]').val() != '') {
    data.bairros.push($('input[name="outro-bairro-bsb"]').val());
  }
  if ($('input[name="outro-bairro-sp"]').val() != '') {
    data.bairros.push($('input[name="outro-bairro-sp"]').val());
  }

  var inf = {
    cidade: data.cidade,
    bairros: data.bairros
  };

  var selecionouReforco =
    $('input[name="radio-tipo-materia"]:checked').val() === 'reforco' ||
    (response.materiasIdiomas && response.materiasIdiomas[0] == '');

  if (data.cidade === 'Brasília' && selecionouReforco) {
    inf = {
      cidade: data.cidade,
      zona: $('input[name="radio-zona-bsb"]:checked').val()
    };
  }

  var dados = {
    step: 4,
    data: inf
  };

  $.ajax({
    url: '/professor/cadastro/update',
    type: 'post',
    data: dados
  })
    .done(function (response) {
      if (response.success) {
        bairrosContainer.fadeOut('fast', function () {
          pessoal1Container.fadeIn('fast', function () {
            $('.step-right').hide();
            scrollTo(pessoal1Container);
          });
        });
      } else if (response.message) {
        alert(response.message);
      }
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
      alert('Houve um erro ao atualizar suas informações, tente novamente mais tarde.');
    });
}

function sendStep3() {
  var formData = new FormData($('form#step1-form')[0]);
  //select and file are already in form
  // formData.append('estado', $('select[name="estado"] option:selected').val());

  formData.append('nome', $('input[name="nome"]').val());
  formData.append('nascimento', $('input[name="nascimento"]').val());
  formData.append('cpf', $('input[name="cpf"]').val());
  formData.append('rg_numero', $('input[name="rg_numero"]').val());
  formData.append('rg_orgao', $('input[name="rg_orgao"]').val());
  formData.append('rg_data', $('input[name="rg_data"]').val());
  formData.append('logradouro', $('input[name="logradouro"]').val());
  formData.append('numero', $('input[name="numero"]').val());
  formData.append('cidade', $('input[name="cidade"]').val());
  formData.append('bairro', $('input[name="bairro"]').val());
  formData.append('cep', $('input[name="cep"]').cleanVal());
  formData.append('nome_mae', $('input[name="nome_mae"]').val());
  formData.append('como_conheceu', $('input[name="como_conheceu"]').val());
  formData.append(
    'possui_carro',
    $('.pessoal-6')
      .find('input[name="radio-carro"]:checked')
      .val()
  );

  formData.append('historico', $('input[name="historico"]').val());

  $.ajax({
    url: '/professor/cadastro/update/final',
    enctype: 'multipart/form-data',
    processData: false, // Important!
    contentType: false,
    cache: false,
    type: 'post',
    data: formData
  })
    .done(function (response) {
      if (response.success) {
        window.location = '/professor/cadastro/';
      } else if (response.message) {
        alert(response.message);
      }
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
      alert(
        'Houve um erro ao finalizar seu cadastro. Por favor, entre em contato conosco pelo WhatsApp (61) 9 9805-2073.'
      );
    });
}

function hideAll() {
  $('.step-right').hide();
  $('.introduction-container').hide();
  $('.welcomeback-container').hide();

  // materiasContainer.hide();
  materias1Container.hide();
  materias2Container.hide();
  materias3Container.hide();
  materias4Container.hide();
  materias5Container.hide();
  materias6Container.hide();
  materias7Container.hide();
  materias8Container.hide();

  // $('.bairros-container').hide();
  bairros1Container.hide();
  bairros2Container.hide();
  bairros3Container.hide();
  bairros4Container.hide();
  bairros5Container.hide();

  // $('.pessoal-container').hide();
  pessoal1Container.hide();
  pessoal2Container.hide();
  pessoal3Container.hide();
  pessoal4Container.hide();
  pessoal5Container.hide();
  pessoal6Container.hide();
  pessoal7Container.hide();
  pessoal8Container.hide();
  pessoal9Container.hide();
}

function handleBackButton() {
  $('.back-arrow').on('click', function () {
    if ($(this).attr('disabled') != true) {
      var target = $(this).attr('data-steptarget');
      var currentContainer = $(this).parent();
      moveBack(currentContainer, $(target));
      clearFields(currentContainer);
    }
  });
}

function moveOn(currentContainer, nextContainer) {
  if ($(nextContainer).css('opacity') != '1') {
    nextContainer.fadeTo('fast', 1, function () {
      currentContainer.fadeTo('fast', 0.3);
      disableElements(currentContainer);
      enableElements(nextContainer);
      scrollTo(nextContainer);
    });
  } else {
    nextContainer.fadeIn('fast', function () {
      currentContainer.fadeTo('fast', 0.3);
      disableElements(currentContainer);
      enableElements(nextContainer);
      scrollTo(nextContainer);
    });
  }
}

function moveBack(currentContainer, nextContainer) {
  //enable all previous elements
  scrollTo(nextContainer, function () {
    nextContainer.fadeTo('fast', 1, function () {
      enableElements(nextContainer);
      currentContainer.fadeTo('fast', 0);
    });
  });
}

function scrollTo(el, callback) {
  var elOffset = el.offset().top;
  var elHeight = el.height();
  var windowHeight = $(window).height();
  var offset;

  if (elHeight < windowHeight) {
    offset = elOffset - (windowHeight / 2 - elHeight / 2);
  } else {
    offset = elOffset;
  }

  $('html, body')
    .stop()
    .animate(
      {
        scrollTop: offset
      },
      500,
      function () {
        if (callback) callback();
      }
    );

  // window.scroll({
  //     top: Math.floor($(el).offset().top),
  //     left: 0,
  //     behavior: 'smooth'
  // });
}

function disableElements(parent) {
  $(parent)
    .find('input')
    .prop('disabled', 'true');
  $(parent)
    .find('button')
    .prop('disabled', 'true');
  $(parent)
    .find('.back-arrow')
    .prop('disabled', 'true');
}

function enableElements(parent) {
  $(parent)
    .find('input')
    .removeAttr('disabled');
  $(parent)
    .find('button')
    .removeAttr('disabled');
  $(parent)
    .find('.back-arrow')
    .removeAttr('disabled');
}

function clearFields(container) {
  $(container)
    .find('input[type="text"]')
    .val('');
  $(container)
    .find('input[type="checkbox"]')
    .prop('checked', false);
  $(container)
    .find('input[type="radio"]')
    .prop('checked', false);
  $(container)
    .find('.select-bairros')
    .val(null)
    .trigger('change');
}

function fixSelect() {
  var scrollTop;
  $('select').on('select2:selecting', function (event) {
    var $pr = $('#' + event.params.args.data._resultId).parent();
    scrollTop = $pr.prop('scrollTop');
  });
  $('select').on('select2:select', function (event) {
    var $pr = $('#' + event.params.data._resultId).parent();
    $pr.prop('scrollTop', scrollTop);
  });
}

function checkExtention(filename, extentionArray) {
  var extention;

  for (var i = filename.length - 1; i >= 0; i--) {
    if (filename[i] == '.') {
      extention = filename.slice(i + 1, filename.length).toLowerCase();
      break;
    }
  }

  if (extentionArray.indexOf(extention) != -1) return true;
  else return false;
}

function changeCurriculoTitle() {
  $('.pessoal-9 .warning-message.red-warning').hide();
  $('.pessoal-9 .title-heading').html(
    'Por último, precisamos que nos envie seu currículo em formato PDF, JPEG, JPG, PNG, etc.'
  );
}
