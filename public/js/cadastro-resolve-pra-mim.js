var autenticado = false;
var _fb_response;
var _fb_authResponse;

$(function() {
  init();

  $('.register-button').on('click', function() {
    if (validateRegister() != false) {
      register();
    }
  });

  var maskBehavior = function(val) {
      return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    },
    options = {
      onKeyPress: function(val, e, field, options) {
        field.mask(maskBehavior.apply({}, arguments), options);
      }
    };

  $('#register-telefone').mask(maskBehavior, options);
});

function register() {
  var data = {
    nome: $('#register-nome').val(),
    email: $('#register-email').val(),
    telefone: $('#register-telefone').val()
  };

  $.ajax({
    url: '/como-resolve/registrar',
    type: 'POST',
    data: data
  })
    .done(function(data) {
      console.log(data);
      if (data.success) {
        // Checa se o Parse retornou algum erro
        console.log(data);
        try {
          woopra.identify({
            email: data.email,
            name: data.nome,
            phone: data.phone
          });
        } catch (e) {
          console.log('DEU MERDA');
        }
        woopra.track('resolvePraMim-signup', {
          resolvePraMim: true
        });
        console.log('HERE 2');
        window.location.href = '/como-resolve/finalizado';
      } else {
        alert('Ocorreu um erro ao realizar o cadastro.');
        return;
        if (data.code) {
          var message = getErrorMessage(data.code);
          $('.error-container-register').html(message);
        } else if (data.message) {
          $('.error-container-register').html(data.message);
        }
      }
    })
    .fail(function(jqXHR, textStatus, errorThrown) {
      console.log('erro');
      console.log(textStatus);
      console.log(errorThrown);
      $('.error-container-register').html('Algo deu errado, tente novamente mais tarde.');
    });
}

function validateRegister() {
  $('.error-container-register').html('');

  var name = $('#register-nome').val();
  if (name === undefined || name === null || /\S/.test(name) === false) {
    // Checa se a string é vazia
    $('.error-container-register').html('Digite um nome para se cadastrar');
    return false;
  }

  var email = $('#register-email').val();
  if (email === undefined || email === null || /.+@.+/.test(email) === false) {
    // Checa se a string é vazia
    $('.error-container-register').html('O email inserido não é válido.');
    return false;
  }

  var telefone = $('#register-telefone').val();
  if (telefone === undefined || telefone === null || telefone == '') {
    $('.error-container-register').html('Insira um número de telefone!');
    return false;
  }
  return true;
}

function init() {
  //PARSEEEEE

  //production
  Parse.initialize(
    'ICT2Kq9bUvJwDEHFADoJehfyEZZtVh5MhPgtueTB',
    '6rqk0gh891YfNY91DptzDrPKj2tnU9CPE7VYMDcU'
  );

  // Teste
  // Parse.initialize("H5h9AeZtIyrDlVYX9hI1ODxM9npPa3qohJnH5H7a", "OoL5Hd8ddSeq0hxPXOkYp0gSOHSThGZ49ZXFADMU");

  Parse.serverURL = 'https://parseapi.back4app.com/';
}

function saveUser(userData, phone, id) {
  var user = new Parse.User();

  var phoneNumber = $('#fb-telefone').val();
  var digitsId = $('#fb-digits-id').val();

  var date =
    moment()
      .add('s', userData.expiresIn)
      .utcOffset(0)
      .format('YYYY-MM-DDTHH:mm:ss.SSS') + 'Z';

  var fbAuthData = {
    facebook: {
      id: userData.authData.userID,
      access_token: userData.authData.accessToken,
      expiration_date: date
    }
  };

  user.set('email', userData.email);
  var rnd = Math.random()
    .toString(36)
    .slice(-9);
  user.set('password', rnd);
  user.set('username', userData.authData.userID);
  user.set('telefone', phoneNumber);
  user.set('nome', userData.name);
  user.set('digits_id', digitsId);
  user.set('fbAuthData', fbAuthData);
  user.set('image', userData.photo);

  console.log('setou tudo');
  user.signUp(null, {
    success: function(user) {
      try {
        woopra.identify({
          email: user.get('email'),
          name: user.get('nome'),
          phone: uset.get('telefone'),
          idparse: user.objectId
        });

        woopra.track('signup', {
          facebook: true
        });
      } catch (e) {}
      loginUser();
    },
    error: function(user, error) {
      console.log(user);
      console.log(error);
    }
  });
}
