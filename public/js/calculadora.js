var test = '';

$(function() {
  setupTest();

  $('.header__button, .header-mobile__button, .phone__button').on('click', function() {
    if (mobilecheck()) {
      try {
        ga(function(tracker) {
          ga('send', 'event', {
            eventCategory: 'Landing Calculadora',
            eventAction: 'click baixar',
            eventLabel: tracker.get('clientId')
          });

          var url = new URL(window.location.href);

          var utm_source = url.searchParams.get('utm_source');
          var utm_medium = url.searchParams.get('utm_medium');
          var utm_campaign = url.searchParams.get('utm_campaign');
          var utm_content = url.searchParams.get('utm_content');
          var utm_term = url.searchParams.get('utm_term');

          var data = {
            utm_source: utm_source,
            utm_medium: utm_medium,
            utm_campaign: utm_campaign,
            utm_content: utm_content,
            utm_term: utm_term
              ? utm_term
              : window.location.pathname
                  .split('/')
                  .slice(1)
                  .join(','),
            ga_id: tracker.get('clientId'),
            '~campaign': utm_campaign,
            '~channel': utm_medium,
            '~feature': 'Landing mobile banner'
          };

          branch.link({ data: data }, function(e, a) {
            if (e) {
              console.log(e);
            } else {
              window.location.href = a;
            }
          });
        });
      } catch (error) {}
    } else {
      window.location.href =
        'https://play.google.com/store/apps/details?id=br.com.aulascolmeia.colmeia_android';
    }
  });

  $('.form__input--pacote').keypress(function() {
    $(this).val('');
  });

  $('form').submit(function(e) {
    e.preventDefault();

    if ($('.form__input--nome').val() == '' || $('.form__input--telefone').val() == '') {
      alert('Por favor, preencha todos os campos!');
      return;
    }

    ga(function(tracker) {
      $.ajax({
        url: '/calculadora-pas-lead',
        type: 'POST',
        data: {
          nome: $('.form__input--nome').val(),
          telefone: $('.form__input--telefone').val(),
          info: $('.form__input--pacote').val(),
          ga_id: tracker.get('clientId')
        }
      })
        .done(function(result) {
          if (result.success) {
            try {
              woopra.identify({
                name: $('.form__input--nome').val(),
                phone: $('.form__input--telefone').val()
              });

              woopra.track('inscreveuCalculadora', {});
            } catch (e) {}

            alert('Recebemos seus dados, a Colmeia agradece!');
            $('form')
              .find('.form__input')
              .val('');
          } else if (result.message) {
            alert(result.message);
          }
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
          alert(
            'Desculpe, houve um erro ao receber suas informações. Por favor, entre em contato conosco pelo WhatsApp (61) 9 9805-2073.'
          );
        });
    });
  });

  $('.pas-block__input').on('change keyup paste', function() {
    checkAllFields(function() {
      var pas1El = {
        idioma: $('#pas1')
          .find('.pas-block__input--idioma')
          .val(),
        redacao: Number(
          $('#pas1')
            .find('.pas-block__input--redacao')
            .val()
            .replace(/,/g, '.')
        ),
        escore: Number(
          $('#pas1')
            .find('.pas-block__input--escore')
            .val()
            .replace(/,/g, '.')
        )
      };

      var pas2El = {
        idioma: $('#pas2')
          .find('.pas-block__input--idioma')
          .val(),
        redacao: Number(
          $('#pas2')
            .find('.pas-block__input--redacao')
            .val()
            .replace(/,/g, '.')
        ),
        escore: Number(
          $('#pas2')
            .find('.pas-block__input--escore')
            .val()
            .replace(/,/g, '.')
        )
      };

      var pas3El = {
        idioma: $('#pas3')
          .find('.pas-block__input--idioma')
          .val(),
        redacao: Number(
          $('#pas3')
            .find('.pas-block__input--redacao')
            .val()
            .replace(/,/g, '.')
        ),
        escore: Number(
          $('#pas3')
            .find('.pas-block__input--escore')
            .val()
            .replace(/,/g, '.')
        )
      };

      if (pas1El.escore > 90 || pas2El.escore > 90 || pas3El.escore > 90) {
        $('.grade__error').html('O valor máximo do escore é 90!');
        $('.grade__value').html('--');
      } else if (pas1El.redacao > 10 || pas2El.redacao > 10 || pas3El.redacao > 10) {
        $('.grade__error').html('O valor máximo da redação é 10!');
        $('.grade__value').html('--');
      } else {
        var end = false;

        var res = $('.grade__value');
        res.html('-');

        var timerId = setInterval(function() {
          if (res.html().length > 4 && !end) {
            res.html('-');
          } else if (!end) {
            res.html(res.html() + '-');
          }
        }, 500);

        $.post('/calculadora-pas', { pas1: pas1El, pas2: pas2El, pas3: pas3El }).done(function(
          response
        ) {
          end = true;
          clearInterval(timerId);

          if (response.success) {
            $('#modal').modal();
            $('.grade__value').html(response.argumento.toFixed(2));
          } else {
            $('.grade__value').html('Você deve inserir dados válidos!');
            $('.grade__value').html('--');
          }
        });
      }
    });
  });
});

function checkAllFields(callback) {
  var pas1El = {
    idioma: $('#pas1')
      .find('.pas-block__input--idioma')
      .val(),
    redacao: $('#pas1')
      .find('.pas-block__input--redacao')
      .val()
      .replace(/,/g, '.'),
    escore: $('#pas1')
      .find('.pas-block__input--escore')
      .val()
      .replace(/,/g, '.')
  };
  var pas2El = {
    idioma: $('#pas2')
      .find('.pas-block__input--idioma')
      .val(),
    redacao: $('#pas2')
      .find('.pas-block__input--redacao')
      .val()
      .replace(/,/g, '.'),
    escore: $('#pas2')
      .find('.pas-block__input--escore')
      .val()
      .replace(/,/g, '.')
  };
  var pas3El = {
    idioma: $('#pas3')
      .find('.pas-block__input--idioma')
      .val(),
    redacao: $('#pas3')
      .find('.pas-block__input--redacao')
      .val()
      .replace(/,/g, '.'),
    escore: $('#pas3')
      .find('.pas-block__input--escore')
      .val()
      .replace(/,/g, '.')
  };

  if (
    validateField(pas1El.redacao) &&
    validateField(pas1El.escore) &&
    validateField(pas2El.redacao) &&
    validateField(pas2El.escore) &&
    validateField(pas3El.redacao) &&
    validateField(pas3El.escore)
  ) {
    $('.grade__error').html('');
    callback();
  } else {
    $('.grade__value').html('--');
  }
}

function validateField(fieldValue) {
  return fieldValue != '' && fieldValue != undefined && fieldValue != ' ';
}

function setupTest() {
  ga(function(tracker) {
    var id = tracker.get('clientId');
    var flag = Boolean(Math.floor(Number(id) % 2));

    if (flag) {
      $('.grade').insertAfter('.calculadora');
      test = 'A';
    } else {
      test = 'B';
    }
  });
}
