var BAIRROS = {
  brasilia: {
    'todos-os-bairros': 'Todos os bairros',
    'aguas-claras': 'Águas Claras',
    'asa-norte': 'Asa Norte',
    'asa-sul': 'Asa Sul',
    brazlandia: 'Brazlândia',
    candangolandia: 'Candangolândia',
    ceilandia: 'Ceilândia',
    'cruzeiro-novo': 'Cruzeiro Novo',
    'cruzeiro-velho': 'Cruzeiro Velho',
    gama: 'Gama',
    'grande-colorado': 'Grande Colorado',
    'granja-do-torto': 'Granja do Torto',
    'guara-i': 'Guará I',
    'guara-ii': 'Guará II',
    itapoa: 'Itapoã',
    'jardim-botanico': 'Jardim Botânico',
    'lago-norte': 'Lago Norte',
    'lago-sul': 'Lago Sul',
    noroeste: 'Noroeste',
    'nucleo-bandeirante': 'Núcleo Bandeirante',
    octogonal: 'Octogonal',
    paranoa: 'Paranoá',
    'park-way': 'Park Way',
    planaltina: 'Planaltina',
    'recanto-das-emas': 'Recanto das Emas',
    'riacho-fundo-i': 'Riacho Fundo I',
    'riacho-fundo-ii': 'Riacho Fundo II',
    samambaia: 'Samambaia',
    'santa-maria': 'Santa Maria',
    'setor-de-mansoes-lago-norte': 'Setor de Mansões Lago Norte',
    'setor-militar': 'Setor Militar',
    sia: 'SIA',
    sig: 'SIG',
    'sobradinho-i': 'Sobradinho I',
    'sobradinho-ii': 'Sobradinho II',
    'sof-sul': 'SOF Sul',
    sudoeste: 'Sudoeste',
    'sao-sebastiao': 'São Sebastião',
    taguatinga: 'Taguatinga',
    taquari: 'Taquari',
    varjao: 'Varjão',
    'vicente-pires': 'Vicente Pires',
    'vila-planalto': 'Vila Planalto'
  },
  'sao-paulo': {
    'todos-os-bairros': 'Todos os bairros',
    'agua-branca': 'Água Branca',
    'alto-de-pinheiros': 'Alto de Pinheiros',
    'bela-vista': 'Bela Vista',
    butanta: 'Butantã',
    'campo-belo': 'Campo Belo',
    'cidade-ademar': 'Cidade Ademar',
    consolacao: 'Consolação',
    'itaim-bibi': 'Itaim Bibi',
    jabaquara: 'Jabaquara',
    'jardim-paulista': 'Jardim Paulista',
    moema: 'Moema',
    mooca: 'Mooca',
    morumbi: 'Morumbi',
    perdizes: 'Perdizes',
    pinheiros: 'Pinheiros',
    samambaia: 'Samambaia',
    'santo-amaro': 'Santo Amaro',
    saude: 'Saúde',
    'vila-mariana': 'Vila Mariana'
  }
};

var CIDADES = {
  brasilia: 'Brasília',
  'sao-paulo': 'São Paulo'
};

var TIPOS = {
  linguas: 'Línguas',
  escolar: 'Escolar'
};

var MATERIAS = {
  linguas: {
    ingles: 'Inglês'
  },
  escolar: {
    'todas-as-materias': 'Todas as matérias',
    matematica: 'Matemática',
    portugues: 'Português',
    fisica: 'Física',
    quimica: 'Química',
    biologia: 'Biologia',
    historia: 'História',
    geografia: 'Geografia'
  }
};

var MATERIAS_ALL = {
  'todas-as-materias': 'Todas as matérias',
  matematica: 'Matemática',
  portugues: 'Português',
  fisica: 'Física',
  quimica: 'Química',
  biologia: 'Biologia',
  historia: 'História',
  geografia: 'Geografia'
  // ingles: 'Inglês'
};

// var NIVEIS = {
//   linguas: {
//     basico: 'Básico',
//     intermediario: 'Intermediário',
//     avancado: 'Avançado'
//   },
//   escolar: {
//     fundamental: 'Fundamental',
//     medio: 'Médio'
//   }
// };

// var NIVEIS_ALL = {
//   fundamental: 'Fundamental',
//   medio: 'Médio',
//   basico: 'Básico',
//   intermediario: 'Intermediário',
//   avancado: 'Avançado'
// };

var ANOS = {
  '1o-ano-fundamental': '1º ano Fund.',
  '2o-ano-fundamental': '2º ano Fund.',
  '3o-ano-fundamental': '3º ano Fund.',
  '4o-ano-fundamental': '4º ano Fund.',
  '5o-ano-fundamental': '5º ano Fund.',
  '6o-ano-fundamental': '6º ano Fund.',
  '7o-ano-fundamental': '7º ano Fund.',
  '8o-ano-fundamental': '8º ano Fund.',
  '9o-ano-fundamental': '9º ano Fund.',
  '1o-ano-medio': '1º ano Médio',
  '2o-ano-medio': '2º ano Médio',
  '3o-ano-medio': '3º ano Médio'
};

var ANOS_NIVEIS = {
  '1o-ano-fundamental': 'Fundamental 1',
  '2o-ano-fundamental': 'Fundamental 1',
  '3o-ano-fundamental': 'Fundamental 1',
  '4o-ano-fundamental': 'Fundamental 1',
  '5o-ano-fundamental': 'Fundamental 1',
  '6o-ano-fundamental': 'Fundamental 2',
  '7o-ano-fundamental': 'Fundamental 2',
  '8o-ano-fundamental': 'Fundamental 2',
  '9o-ano-fundamental': 'Fundamental 2',
  '1o-ano-medio': 'Médio',
  '2o-ano-medio': 'Médio',
  '3o-ano-medio': 'Médio'
};

var NIVEIS = {
  'fundamental-1': 'Fundamental 1',
  'fundamental-2': 'Fundamental 2',
  medio: 'Médio'
};

var CHAMADAS = {
  'reforco-escolar': 'Reforço escolar',
  reforco: 'Reforço escolar',
  'aula-particular': 'Aula particular',
  'professor-particular': 'Professor particular'
};

var EBOOKS = {
  'ebook-1': 'rotina-de-estudos',
  'ebook-2': 'educacao-em-dia'
};

var ESTADOS = {
  'acre': 'Acre',
  'alagoas': 'Alagoas',
  'amapa': 'Amapá',
  'amazonas': 'Amazonas',
  'bahia': 'Bahia',
  'ceara': 'Ceará',
  'distrito federal': 'Distrito Federal',
  'espirito santo': 'Espírito Santo',
  'goias': 'Goiás',
  'maranhao': 'Maranhão',
  'mato grosso': 'Mato Grosso',
  'mato grosso do sul': 'Mato Grosso do Sul',
  'minas gerais': 'Minas Gerais',
  'para': 'Pará',
  'paraiba': 'Paraíba',
  'parana': 'Paraná',
  'pernambuco': 'Pernambuco',
  'Piauí': 'Piauí',
  'rio de janeiro': 'Rio de Janeiro',
  'rio grande do norte': 'Rio Grande do Norte',
  'rio grande do sul': 'Rio Grande do Sul',
  'rondonia': 'Rondônia',
  'roraima': 'Roraima',
  'santa catarina': 'Santa Catarina',
  'sao paulo': 'São Paulo',
  'sergipe': 'Sergipe',
  'tocantins': 'Tocantins',
};

(function(exports) {
  exports.CONST = {
    CIDADES: CIDADES,
    BAIRROS: BAIRROS,
    TIPOS: TIPOS,
    MATERIAS: MATERIAS,
    MATERIAS_ALL: MATERIAS_ALL,
    ANOS: ANOS,
    ANOS_NIVEIS: ANOS_NIVEIS,
    NIVEIS: NIVEIS,
    CHAMADAS: CHAMADAS,
    EBOOKS: EBOOKS,
    ESTADOS: ESTADOS
  };
})(typeof exports === 'undefined' ? this : exports);
