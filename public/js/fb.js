var fb          = false;

function fbLogin() {

  fb = true;

  // FB.logout();
  FB.login(function(response) {
    if(response.authResponse) {
      // Usuario logado
      checkUserExists(response.authResponse);
    }
  }, {scope: 'email, public_profile'});
}

function checkUserExists(authResponse) {
  var token = authResponse.accessToken;
  var uid = authResponse.userID;

  if (token) {

    //APENAS BUSCA USER E VE SE ENCONTRA
    Parse.User.logOut();
    var User = Parse.Object.extend('_User');
    var query = new Parse.Query(User);
    query.equalTo('username', uid);
    query.find({
      success: function(response) {

          if(response.length == 0) {
          // usuário não encontrado

          if(autenticado) {
            callGraph(response, authResponse);
          } 
          else {
            handleTelefoneForFacebook();
          }
        } else {
          loginUser();
        }
        
      },
      error: function(error) {
        console.log(error);
      }
    });
  }
}

function callGraph(user, authResponse) {

  $.LoadingOverlay("show");

  var token = authResponse.accessToken;
  var uid = authResponse.userID;

  if(token) {
    FB.api('/me', {fields:'id,email,name'}, function(response) {
      var email = response.email;
      var name = response.name;
      var imageUrl = 'https://graph.facebook.com/' + response.id + '/picture?type=large';

      toDataUrl(imageUrl, function(base64Img) {
        var photo = new Parse.File('photo.jpg', { base64: base64Img });
        photo.save().then(function() {
          // file saved
          var userData = {
            email: email,
            name: name,
            photo: photo,
            authData: authResponse
          };

          saveUser(userData);

        }, function(error) {
          console.log(error);
          alert('Erro ao entrar com Facebook!');
        });

      });
    });
  }
}

function loginUser() {

  $.LoadingOverlay("show");

  Parse.FacebookUtils.logIn(null, {
    success: function(user) {
      var u = {
        id: user.id,
        email: user.get('email'),
        nome: user.get('nome')
      };
      var token = user.getSessionToken();

      $.ajax({
        url: '/aluno/entrar-facebook',
        type: 'POST',
        data: {user: u, token: token}
      })
      .done(function() {
        window.location = next;

      })
      .fail(function() {
        console.log("error");
      });
    },
    error: function(user, error) {
      console.log(user);
      console.log(error);
    }
  });
}

function saveUser(userData, phone, id) {

  var user = new Parse.User();

  var phoneNumber = $('#fb-telefone').val();
  var digitsId    = $('#fb-digits-id').val();

  var date = moment().add('s', userData.expiresIn)
                     .utcOffset(0)
                     .format('YYYY-MM-DDTHH:mm:ss.SSS') + 'Z';

  var fbAuthData = { facebook: {
      id: userData.authData.userID,
      access_token: userData.authData.accessToken,
      expiration_date: date
    }
  }

  user.set('email', userData.email);
  var rnd = Math.random().toString(36).slice(-9);
  user.set('password', rnd);
  user.set('username', userData.authData.userID);
  user.set('telefone', phoneNumber);
  user.set('nome', userData.name);
  user.set('digits_id', digitsId);
  user.set('fbAuthData', fbAuthData);
  user.set('image', userData.photo);

  console.log('setou tudo');
  user.signUp(null, {
    success: function(user) {
      loginUser();
    },
    error: function(user, error) {
      console.log(user);
      console.log(error);
    }
  });
}

function toDataUrl(src, callback, outputFormat) {
  var img = new Image();
  img.crossOrigin = 'Anonymous';
  img.src = src;
  img.onload = function() {
    var canvas = document.createElement('CANVAS');
    var ctx = canvas.getContext('2d');
    var dataURL;
    canvas.height = this.height;
    canvas.width = this.width;
    ctx.drawImage(this, 0, 0);
    dataURL = canvas.toDataURL(outputFormat);
    callback(dataURL);
  };
  if (img.complete || img.complete === undefined) {
    img.src = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==";
    img.src = src;
  }
}