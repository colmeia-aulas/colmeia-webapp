$(window).load(function() {
  //fix todas as imagens apos elas serem carregadas
  $('.aula-card .icon-container .img-container img').each(function() {
    fixImagem($(this));
    $(this).fadeIn();
  });
});

$(function() {
  moment()
    .tz('America/Sao_Paulo')
    .format();

  $('.mode-toggle').on('click', function() {
    $('.mode-selector').toggle();
  });

  showHistorico(function() {
    $('.aula-card .icon-container .img-container img').each(function() {
      $(this).on('load', function() {
        fixImagem($(this));
        $(this).fadeIn();
      });
    });
  });
});

function getHistorico(callback) {
  $.ajax({
    url: '/aluno/historico/buscar',
    type: 'GET'
  })
    .done(function(result) {
      if (result.success) {
        callback(result.aulas);
      } else if (result.message) {
        alert(result.message);
      }
    })
    .fail(function() {
      alert('Houve um erro ao obter seu histórico de aulas, tente novamente mais tarde.');
    });
}

function showHistorico(callback) {
  getHistorico(function(aulas) {
    if (aulas.length == 0) {
      $('.sem-aulas-container').show();
      return;
    }

    for (var i = 0; i < aulas.length; i++) {
      var $container = $('.aulas-container');
      var aula = aulas[i];
      var id = aula.id;
      var fotoProf = aula.prof.foto ? aula.prof.foto : '/img/profile_placeholder.png';
      var nomeProf = aula.prof.nome;
      var notaProf = aula.prof.avaliacao;
      var duracao = aula.duracao;
      var dia = moment(aula.dia);
      var hora =
        dia.clone().format('HH:mm') +
        ' - ' +
        dia
          .clone()
          .add(duracao, 'hours')
          .format('HH:mm');

      var diaString = dia.date() + ' de ' + moment.months()[dia.month()];
      var duracaoAula = '';
      if (duracao == 0.5) {
        duracaoAula = '30 minutos de aula';
      } else if (duracao == 1) {
        duracaoAula = '1 hora de aula';
      } else if (duracao == 1.5) {
        duracaoAula = '1 hora e 30 minutos de aula';
      } else if (duracao == 2) {
        duracaoAula = '2 horas de aula';
      } else if (duracao == 2.5) {
        duracaoAula = '2 horas e 30 minutos de aula';
      } else if (duracao == 3) {
        duracaoAula = '3 horas de aula';
      }

      var canceladaText =
        aula.status == 'Cancelada' ? "<p class='cancelada-text'>Aula cancelada</p>" : '';

      var materia = aula.topico;
      var nivel = aula.ensino;
      var bairro = aula.endereco.bairro;
      var endereco =
        aula.endereco.endereco + ' ' + aula.endereco.numero + ' ' + aula.endereco.complemento;
      var preco = 'R$' + Number(aula.preco).toFixed(2);
      var card;

      if (aula.card) {
        var cartao = '<span class="card-number-gray">xxxx-xxxx-xxxx-</span>' + aula.card.digits;
        var bandeiraFoto = '/img/' + aula.card.brand.toLowerCase() + '-logo.png';

        card =
          "<div class='aula-cartao'>\
                <div class='card-numero'>\
                <img class='card-icon' src='/img/gray-card.png' />\
                    <div class='card-final'>" +
          cartao +
          "</div>\
                </div>\
                <img src='" +
          bandeiraFoto +
          "' alt='Cartao bandeira' class='card-bandeira'>\
                </div>";
      } else {
        card = '';
      }

      $container.append(
        "\
            <div class='aula-card' id=" +
          id +
          " display: hidden>\
                <div class='aula-content'>\
                    <div class='aula-data'>\
                        <div class='aula-dia'>" +
          diaString +
          "</div>\
                        <div class='aula-horario'>" +
          hora +
          "</div>\
                        <div class='aula-duracao'>" +
          duracaoAula +
          "</div>\
                    </div>\
                    <hr class='card-separator'>\
                    <div class='aula-professor'>\
                        <div class='professor-left'>\
                                <img src='" +
          fotoProf +
          "' />\
                        </div>\
                        <div class='professor-right'>\
                            <div class='professor-nome'>" +
          nomeProf +
          "</div>\
                            <div class='professor-rating'>\
                                <div class='professor-rating-value'>" +
          notaProf +
          "</div>\
                                <img src='/img/rating-star.png' class='professor-rating-img' />\
                            </div>\
                            <div class='aula-materia'>" +
          materia +
          "</div><br>\
                            <div class='aula-nivel'>" +
          nivel +
          "</div>\
                        </div>\
                    </div>\
                    <hr class='card-separator'>\
                    <div class='aula-endereco'>\
                        <div class='aula-bairro'> " +
          bairro +
          "</div>\
                        <div class='aula-endereco-completo'>" +
          endereco +
          "</div>\
                    </div>\
                    <hr class='card-separator'>" +
          card +
          "<div class='aula-preco'>\
                        Total:\
                        <div class='aula-preco-value'>" +
          preco +
          '</div>\
                    </div>\
                    </div>' +
          canceladaText +
          '</div>\
                </div>\
            '
      );

      $('#' + id).fadeIn('slow');
    }
    callback();
  });
}

function fixImagem(foto) {
  foto = $(foto);

  var width = foto[0].width;
  var height = foto[0].height;

  if (width < height) {
    foto.css('width', '100%');
    foto.css('height', 'auto');
    // $('.intro-container img').css('margin-right', (height - width) + 'px');
  } else if (width > height) {
    foto.css('width', 'auto');
    foto.css('height', '100%');
    // $('.intro-container img').css('margin-left', (height - width) + 'px');
  } else if (width == height) {
    foto.css('height', '100%');
    foto.css('width', '100%');
  }
}
