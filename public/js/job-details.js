$(function() {
  setupButton();

  function setupButton() {
    var offset = 325;
    var initialTop = $('.job__button').offset().top;
    var initialLeft = $('.job__button').offset().left;

    $(window).scroll(function() {
      $('.job__button').toggleClass('job__button--active', $(this).scrollTop() > initialTop - 90);

      if ($(this).scrollTop() > initialTop - 90) {
        var top = initialTop + $(this).scrollTop();
        var left = initialLeft;

        $('.job__button').css('top', '115px');
        $('.job__button').css('left', left);
      } else {
        $('.job__button').css('top', 'unset');
        $('.job__button').css('left', 'unset');
      }
    });
  }
});
