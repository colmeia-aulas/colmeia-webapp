$(function() {
    setupModal();
    setupCounter();
    $('#modal').modal();
});

function setupModal() {
    $('.modal').on('show.bs.modal', function() {
        $(this).show();
        setModalMaxHeight(this);
    });

    $('#modal').on('hidden.bs.modal', function() {
        window.location.href = window.location.pathname.split('obrigado')[0] + window.location.search;
    });

    $('#modal .aula-marcada-button').on('click', function() {
        window.location.href = window.location.pathname.split('obrigado')[0] + window.location.search;
    });

    $(window).resize(function() {
        if ($('.modal.in').length != 0) {
            setModalMaxHeight($('.modal.in'));
        }
    });
}

function setModalMaxHeight(element) {
    this.$element = $(element);
    this.$content = this.$element.find('.modal-content');
    var borderWidth = this.$content.outerHeight() - this.$content.innerHeight();
    var dialogMargin = $(window).width() < 768 ? 20 : 60;
    var contentHeight = $(window).height() - (dialogMargin + borderWidth);
    var headerHeight = this.$element.find('.modal-header').outerHeight() || 0;
    var footerHeight = this.$element.find('.modal-footer').outerHeight() || 0;
    var maxHeight = contentHeight - (headerHeight + footerHeight);

    this.$content.css({
        overflow: 'hidden'
    });

    this.$element.find('.modal-body').css({
        'max-height': maxHeight,
        'overflow-y': 'auto'
    });
}

function setupCounter() {
    var target = $('.countdown');
    var countDownDate = new Date('Nov 24, 2018 23:00:00').getTime();

    var x = setInterval(function() {
        var now = new Date().getTime();

        var distance = countDownDate - now;

        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        target.html(days + 'd ' + hours + 'h ' + minutes + 'm ' + seconds + 's ');

        if (distance < 0) {
            clearInterval(x);
            target.html('Promoção encerrada');
        }
    }, 1000);
}
