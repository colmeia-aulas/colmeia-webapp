$(function() {
    setupCounter();

    var maskBehavior = function(val) {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
        },
        options = {
            onKeyPress: function(val, e, field, options) {
                field.mask(maskBehavior.apply({}, arguments), options);
            }
        };

    $('.form__input--telefone').mask(maskBehavior, options);

    $('.preco__button, .porque__link, .closing__link').on('click', function() {
        $('html, body').animate(
            {
                scrollTop: $('header').offset().top
            },
            1000
        );
        try {
            ga(function(tracker) {
                ga('send', 'event', {
                    eventCategory: 'Landing campanha',
                    eventAction: 'cta',
                    eventLabel: $(this).attr('class'),
                    eventId: tracker.get('clientId')
                });
            });
        } catch (error) {}
    });

    $('form').submit(function(e) {
        e.preventDefault();

        console.log('test');

        if ($('.form__input--nome').val() == '' || $('.form__input--telefone').val() == '') {
            alert('Por favor, preencha todos os campos!');
            return;
        }

        ga(function(tracker) {
            $.ajax({
                url: '/landing-campanha',
                type: 'POST',
                data: {
                    nome: $('.form__input--nome').val(),
                    telefone: '+55' + $('.form__input--telefone').cleanVal(),
                    ga_id: tracker.get('clientId'),
                    origin: 'BLACK_FRIDAY',
                    info: ''
                }
            })
                .done(function(result) {
                    if (result.success) {
                        $('form')
                            .find('.form__input')
                            .val('');

                        var link = window.location.pathname;
                        if (link[link.length - 1] != '/') {
                            link += '/';
                        }

                        link += 'obrigado/' + window.location.search;

                        window.location.href = link;
                    } else {
                        alert('Desculpe, houve um erro ao receber seus dados.');
                    }
                })
                .fail(function(jqXHR, textStatus, errorThrown) {
                    alert('Desculpe, houve um erro ao receber seus dados.');
                });
        });
    });

    function setupCounter() {
        var target = $('.countdown');
        var countDownDate = new Date('Nov 24, 2018 23:00:00').getTime();

        var x = setInterval(function() {
            var now = new Date().getTime();

            var distance = countDownDate - now;

            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            target.html(days + 'd ' + hours + 'h ' + minutes + 'm ' + seconds + 's ');

            if (distance < 0) {
                clearInterval(x);
                target.html('Promoção encerrada');
            }
        }, 1000);
    }
});
