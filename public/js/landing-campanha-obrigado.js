$(function() {
  setupModal();
  setupWhatsapp();
  $('#modal').modal();

  function setupModal() {
    $('.modal').on('show.bs.modal', function() {
      $(this).show();
      setModalMaxHeight(this);
    });

    $('#modal').on('hidden.bs.modal', function() {
      window.location.href = window.location.pathname.split('obrigado')[0] + window.location.search;
    });

    $('#modal .continuar-button').on('click', function() {
      window.location.href = window.location.pathname.split('obrigado')[0] + window.location.search;
    });
    $('#modal .whatsapp-button').on('click', function() {
      window.location.href =
        'https://api.whatsapp.com/send?phone=+5561998052073&text=Olá, eu gostaria de informações sobre as aulas de reforço' +
        pageIdTitle +
        '.';
    });

    $(window).resize(function() {
      if ($('.modal.in').length != 0) {
        setModalMaxHeight($('.modal.in'));
      }
    });
  }

  function setModalMaxHeight(element) {
    this.$element = $(element);
    this.$content = this.$element.find('.modal-content');
    var borderWidth = this.$content.outerHeight() - this.$content.innerHeight();
    var dialogMargin = $(window).width() < 768 ? 20 : 60;
    var contentHeight = $(window).height() - (dialogMargin + borderWidth);
    var headerHeight = this.$element.find('.modal-header').outerHeight() || 0;
    var footerHeight = this.$element.find('.modal-footer').outerHeight() || 0;
    var maxHeight = contentHeight - (headerHeight + footerHeight);

    this.$content.css({
      overflow: 'hidden'
    });

    this.$element.find('.modal-body').css({
      'max-height': maxHeight,
      'overflow-y': 'auto'
    });
  }

  function setupWhatsapp() {}
});
