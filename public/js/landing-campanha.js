$(function() {
  var selectedTest = '';
  // setupTest(function(flag) {
  // selectedTest = flag ? 'A' : 'B';
  // if (flag) {
  $('.form__button').addClass('form__button--test');
  $('.cta__button').addClass('cta__button--test');
  // }
  // });

  // $('#overlay').css('display', 'block');
  // $('body').css('overflow', 'hidden');

  setupWhatsappForm();

  $('.overlay__button--orange').on('click', function() {
    $('#overlay').css('display', 'none');
    $('body').css('overflow', 'auto');

    try {
      try {
        ga('send', 'event', {
          eventCategory: 'Landing campanha',
          eventAction: 'overlay',
          eventLabel: 'aluno'
        });
      } catch (error) {}
    } catch (error) {}
  });

  $('.overlay__button--black').on('click', function() {
    try {
      ga('send', 'event', {
        eventCategory: 'Landing campanha',
        eventAction: 'overlay',
        eventLabel: 'professor'
      });
      window.location.href = '/professor';
    } catch (error) {}
  });

  setupMask();
  //setupButtonTest();

  $('.preco__button, .porque__link, .closing__link, .cta__button').on('click', function() {
    $('html, body').animate(
      {
        scrollTop: $('header').offset().top
      },
      1000
    );
    try {
      ga(function(tracker) {
        ga('send', 'event', {
          eventCategory: 'Landing campanha',
          eventAction: 'cta',
          eventLabel: $(this).attr('class'),
          eventId: tracker.get('clientId')
        });
      });
    } catch (error) {}
  });
  $('.pacote-block__button').on('click', function() {
    $('html, body').animate(
      {
        scrollTop: $('header').offset().top
      },
      1000
    );

    var pacote = String($(this).data('pacote'));

    $('.form__input--pacote').val(pacote);

    try {
      ga(function(tracker) {
        ga('send', 'event', {
          eventCategory: 'Landing campanha',
          eventAction: pacote,
          eventLabel: pageId,
          eventId: tracker.get('clientId')
        });
      });
    } catch (error) {}
  });

  $('form.campanha-form').submit(function(e) {
    e.preventDefault();

    if (
      $('.form__input--nome').val() == '' ||
      $('.form__input--telefone').val() == '' ||
      $('.form__input--email').val() == ''
    ) {
      alert('Por favor, preencha todos os campos!');
      return;
    }

    // var _navigator = {};
    // for (var i in navigator) _navigator[i] = navigator[i];
    // _navigator = JSON.stringify(_navigator);

    // ga(function(tracker) {
    $.ajax({
      // url: 'https://aulascolmeia.com.br/landing-campanha',
      url: '/landing-campanha',
      type: 'POST',
      data: {
        nome: $('.form__input--nome').val(),
        telefone: '+55' + $('.form__input--telefone').cleanVal(),
        email: $('.form__input--email').val(),
        info: $('.form__input--pacote').val() || '',
        ga_id: '',
        origin: ('landing-campanha-' + pageId).toUpperCase(),
        navigator: '',
        url: window.location.href
      }
    })
      .done(function(result) {
        if (result.success) {
          try {
            woopra.identify({
              name: $('.form__input--nome').val(),
              phone: '+55' + $('.form__input--telefone').val()
            });

            woopra.track('inscreveuLandingCampanha', {});
          } catch (e) {}

          $('form')
            .find('.form__input')
            .val('');

          var link = window.location.pathname;
          if (link[link.length - 1] != '/') {
            link += '/';
          }

          link += 'obrigado/' + window.location.search;

          window.location.href = link;
        } else if (result.message) {
          alert(result.message);
        }
      })
      .fail(function(jqXHR, textStatus, errorThrown) {
        alert(
          'Desculpe, houve um erro ao receber suas informações. Por favor, entre em contato conosco pelo WhatsApp (61) 9 9805-2073.'
        );
      });
    // });
  });

  // function setupTest(cb) {
  //   ga(function(tracker) {
  //     var id = tracker.get('clientId');
  //     var flag = Boolean(Math.floor(Number(id) % 2));

  //     cb(flag);
  //   });
  // }

  function setupMask() {
    var maskBehavior = function(val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
      },
      options = {
        onKeyPress: function(val, e, field, options) {
          field.mask(maskBehavior.apply({}, arguments), options);
        }
      };

    $('.form__input--telefone').mask(maskBehavior, options);
    $('.whatsapp__input--phone').mask(maskBehavior, options);
  }

  function setupButtonTest() {
    $('.cta__button').on('click', function() {
      try {
        ga(function(tracker) {
          ga('send', 'event', {
            eventCategory: 'Landing Campanha AB Test',
            eventAction: 'cta bottom',
            eventLabel: selectedTest,
            eventId: tracker.get('clientId')
          });
        });
      } catch (error) {}
    });

    $('.form__button').on('click', function() {
      try {
        ga(function(tracker) {
          ga('send', 'event', {
            eventCategory: 'Landing Campanha AB Test',
            eventAction: 'cta form',
            eventLabel: selectedTest,
            eventId: tracker.get('clientId')
          });
        });
      } catch (error) {}
    });
  }

  function setupWhatsappForm() {
    $('.whatsapp__form').on('submit', function(e) {
      e.preventDefault();

      if (
        $('.whatsapp__input--name').val() == '' ||
        $('.whatsapp__input--phone').val() == '' ||
        $('.whatsapp__input--email').val() == ''
      ) {
        return alert('Por favor, preencha todos os campos!');
      }

      var emailRegex = new RegExp(
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      );
      if (!emailRegex.test($('.whatsapp__input--email').val())) {
        return alert('O email que você inseriu não é válido!');
      }

      $.ajax({
        url: '/landing-campanha',
        type: 'POST',
        data: {
          nome: $('.whatsapp__input--name').val(),
          telefone: '+55' + $('.whatsapp__input--phone').cleanVal(),
          email: $('.whatsapp__input--email').val(),
          origin: ('landing-campanha-' + pageId + '-WHATSAPP').toUpperCase(),
          url: window.location.href
        }
      })
        .done(function(result) {
          if (result.success) {
            window.location.href =
              'https://api.whatsapp.com/send?phone=5561998052073&text=Olá, meu nome é ' +
              $('.whatsapp__input--name').val() +
              ' e eu gostaria de informações sobre as aulas de reforço' +
              pageIdTitle +
              '.';
          } else if (result.message) {
            alert(result.message);
          }
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
          alert(
            'Desculpe, houve um erro ao receber suas informações. Por favor, entre em contato conosco pelo WhatsApp (61) 9 9805-2073.'
          );
        });
    });
  }
});
