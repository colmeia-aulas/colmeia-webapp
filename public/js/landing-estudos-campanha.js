$(function() {
  var bannerShown = false;
  var bannerClicked = false;

  handleFlickity();
  handleScroll();
  handleWhatsapp();
  handleOverlay();
  // handleBanner();

  setupMask();
  submitTopForm();
  submitBottomForm();

  function handleWhatsapp() {
    $('.whatsapp__form').on('submit', function(e) {
      e.preventDefault();

      if (
        $('.whatsapp__input--name').val() == '' ||
        $('.whatsapp__input--phone').val() == '' ||
        $('.whatsapp__input--email').val() == ''
      ) {
        return alert('Por favor, preencha todos os campos!');
      }

      var emailRegex = new RegExp(
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      );
      if (!emailRegex.test($('.whatsapp__input--email').val())) {
        return alert('O email que você inseriu não é válido!');
      }

      if ($('.whatsapp__input--phone').cleanVal().length < 10) {
        return alert('O telefone que você inseriu não é válido!');
      }

      $.ajax({
        url: '/landing-campanha',
        type: 'POST',
        data: {
          nome: $('.whatsapp__input--name').val(),
          telefone: '+55' + $('.whatsapp__input--phone').cleanVal(),
          email: $('.whatsapp__input--email')
            .val()
            .toLowerCase()
            .trim(),
          origin: ('landing-campanha-' + pageId + '-WHATSAPP').toUpperCase(),
          url: window.location.href
        }
      })
        .done(function(result) {
          if (result.success) {
            window.location.href =
              'https://api.whatsapp.com/send?phone=5561998052073&text=Olá, meu nome é ' +
              $('.whatsapp__input--name').val() +
              ' e eu gostaria de informações sobre as aulas de reforço' +
              pageIdTitle +
              '.';
          } else if (result.message) {
            alert(result.message);
          }
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
          alert(
            'Desculpe, houve um erro ao receber suas informações. Por favor, entre em contato conosco pelo WhatsApp (61) 9 9805-2073.'
          );
        });
    });
  }

  function handleFlickity() {
    $('.clients__carousel').flickity({
      cellAlign: 'center',
      contain: true,
      autoPlay: true
    });
  }

  function handleScroll() {
    $('.js-cta').on('click', function() {
      $('html, body').animate(
        {
          scrollTop: $('body').offset().top
        },
        750
      );
    });
  }

  function handleOverlay() {}

  function handleBanner() {
    if (!mobilecheck()) {
      $(document).mouseleave(function() {
        if (!bannerShown) {
          bannerShown = true;

          ga('send', 'event', {
            eventCategory: 'New Landing Campanha',
            eventAction: 'Banner',
            eventLabel: 'Left page'
          });

          $('.landing-campanha__wrapper').css('transform', 'translateX(-42rem)');
          $('.landing-campanha__banner').css('transform', 'translateX(-42rem)');
          $('.landing-campanha__overlay').css('background-color', 'rgba(0, 0, 0, 0.75)');
        }
      });

      $('.landing-campanha__banner').on('click', function() {
        bannerClicked = true;

        ga('send', 'event', {
          eventCategory: 'New Landing Campanha',
          eventAction: 'Banner',
          eventLabel: 'Clicked banner'
        });

        $('html, body').animate(
          {
            scrollTop: $('body').offset().top
          },
          750
        );

        $('.landing-campanha__wrapper').css('transform', 'translateX(0)');
        $('.landing-campanha__banner').css('transform', 'translateX(0)');
        $('.landing-campanha__overlay').css('background-color', 'rgba(0, 0, 0, 0)');

        $('.form-campaign').addClass('form-campaign--highlight');
      });
    }
  }

  function setupMask() {
    var maskBehavior = function(val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
      },
      options = {
        onKeyPress: function(val, e, field, options) {
          field.mask(maskBehavior.apply({}, arguments), options);
        }
      };

    $('.form__input--phone').mask(maskBehavior, options);
    $('.form-campaign__input--phone').mask(maskBehavior, options);
    $('.whatsapp__input--phone').mask(maskBehavior, options);
  }

  function submitBottomForm() {
    $('form#form-contact').on('submit', function(e) {
      e.preventDefault();

      if (
        $('.form__input--name').val() == '' ||
        $('.form__input--phone').cleanVal() == '' ||
        $('.form__input--email').val() == ''
      ) {
        return alert('Por favor, preencha todos os campos para solicitar nosso contato.');
      }

      var emailRegex = new RegExp(
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      );
      if (!emailRegex.test($('.form__input--email').val())) {
        return alert('O email que você inseriu não é válido.');
      }

      if ($('.form__input--phone').cleanVal().length < 10) {
        return alert('O telefone que você inseriu não é válido!');
      }

      $.ajax({
        url: '/landing-campanha',
        type: 'POST',
        data: {
          nome: $('.form__input--name').val(),
          telefone: '+55' + $('.form__input--phone').cleanVal(),
          email: $('.form__input--email')
            .val()
            .toLowerCase()
            .trim(),
          info: bannerClicked ? 'Clicou no banner' : '',
          ga_id: '',
          origin: ('landing-campanha-contact-' + pageId).toUpperCase(),
          navigator: '',
          url: window.location.href
        }
      })
        .done(function(result) {
          dataLayer.push({
            event: 'cadastroEnviado'
          });

          if (result.success) {
            alert(
              'Recebemos sua solicitação com sucesso. Em breve entraremos em contato, a Colmeia agradece!'
            );
          } else if (result.message) {
            alert(result.message);
          }

          $('#form-contact')
            .find('input')
            .val('');
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
          alert(
            'Desculpe, houve um erro ao receber suas informações. Por favor, entre em contato conosco pelo WhatsApp (61) 9 9805-2073.'
          );
        });
    });
  }

  function submitTopForm() {
    $('form#form-top').on('submit', function(e) {
      e.preventDefault();

      if (
        $('.form-campaign__input--name').val() == '' ||
        $('.form-campaign__input--phone').cleanVal() == '' ||
        $('.form-campaign__input--email').val() == '' ||
        $('.form-campaign__input--como-conheceu').val() == ''
      ) {
        return alert('Por favor, preencha todos os campos para solicitar nosso contato.');
      }

      var emailRegex = new RegExp(
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      );
      if (!emailRegex.test($('.form-campaign__input--email').val())) {
        return alert('O email que você inseriu não é válido.');
      }

      var _navigator = {};
      for (var i in window.navigator) _navigator[i] = window.navigator[i];

      delete _navigator.plugins;
      delete _navigator.mimeTypes;

      var abTestString;

      if (window.location.href.includes('/acompanhamento-escolar/')) {
        abTestString = '';
      } else {
        abTestString = abTest ? '-SEM-AULA-GRATIS' : '';
      }

      if ($('.form-campaign__input--phone').cleanVal().length < 10) {
        return alert('O telefone que você inseriu não é válido!');
      }

      var values = {
        nome: $('.form-campaign__input--name').val(),
        telefone: '+55' + $('.form-campaign__input--phone').cleanVal(),
        email: $('.form-campaign__input--email')
          .val()
          .toLowerCase()
          .trim(),
        info: bannerClicked ? 'Clicou no banner' : '',
        ga_id: '',
        origin: ('landing-campanha-' + pageId + abTestString).toUpperCase(),
        navigator: JSON.stringify(_navigator),
        url: window.location.href,
        comoConheceu: $('.form-campaign__input--como-conheceu').val()
      };

      console.log(values);

      $.ajax({
        url: '/landing-campanha',
        type: 'POST',
        data: values
      })
        .done(function(result) {
          if (result.success) {
            dataLayer.push({
              event: 'cadastroEnviado'
            });

            alert(
              'Recebemos sua solicitação com sucesso. Em breve entraremos em contato, a Colmeia agradece!'
            );
          } else if (result.message) {
            alert(result.message);
          }

          $('.form-campaign__form')
            .find('input')
            .val('');
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
          alert(
            'Desculpe, houve um erro ao receber suas informações. Por favor, entre em contato conosco pelo WhatsApp (61) 9 9805-2073.'
          );
        });
    });
  }
});
