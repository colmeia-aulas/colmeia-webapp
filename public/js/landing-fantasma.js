$(function() {
  $('.form__button').addClass('form__button--test');
  $('.cta__button').addClass('cta__button--test');

  // $('#overlay').css('display', 'block');
  // $('body').css('overflow', 'hidden');

  setupTest();

  $('.black__button, .promo__button').on('click', function() {
    $('html, body').animate(
      {
        scrollTop: 0
      },
      1000
    );
  });

  if (localStorage.getItem('abLabel') === 'B') {
    $('.header--a').hide();
  } else {
    $('.header--b').hide();
  }

  setupWhatsappForm();

  $('.overlay__button--orange').on('click', function() {
    $('#overlay').css('display', 'none');
    $('body').css('overflow', 'auto');

    try {
      try {
        ga('send', 'event', {
          eventCategory: 'Landing campanha',
          eventAction: 'overlay',
          eventLabel: 'aluno'
        });
      } catch (error) {}
    } catch (error) {}
  });

  $('.overlay__button--black').on('click', function() {
    try {
      ga('send', 'event', {
        eventCategory: 'Landing campanha',
        eventAction: 'overlay',
        eventLabel: 'professor'
      });
      window.location.href = '/professor';
    } catch (error) {}
  });

  setupMask();

  $('.preco__button, .porque__link, .closing__link, .cta__button').on('click', function() {
    $('html, body').animate(
      {
        scrollTop: 0
      },
      1000
    );
    try {
      ga(function(tracker) {
        ga('send', 'event', {
          eventCategory: 'Landing campanha',
          eventAction: 'cta',
          eventLabel: $(this).attr('class'),
          eventId: tracker.get('clientId')
        });
      });
    } catch (error) {}
  });
  $('.pacote-block__button').on('click', function() {
    $('html, body').animate(
      {
        scrollTop: $('header').offset().top
      },
      1000
    );

    var pacote = String($(this).data('pacote'));

    $('.form__input--pacote').val(pacote);

    try {
      ga(function(tracker) {
        ga('send', 'event', {
          eventCategory: 'Landing campanha',
          eventAction: pacote,
          eventLabel: pageId,
          eventId: tracker.get('clientId')
        });
      });
    } catch (error) {}
  });

  $('form.campanha-form').submit(function(e) {
    e.preventDefault();

    var test = localStorage.getItem('abLabel');

    if (
      $('.header--' + test.toLowerCase())
        .find('.form__input--nome')
        .val() == '' ||
      $('.header--' + test.toLowerCase())
        .find('.form__input--telefone')
        .val() == '' ||
      $('.header--' + test.toLowerCase())
        .find('.form__input--email')
        .val() == ''
    ) {
      alert('Por favor, preencha todos os campos!');
      return;
    }

    // var _navigator = {};
    // for (var i in navigator) _navigator[i] = navigator[i];
    // _navigator = JSON.stringify(_navigator);

    // ga(function(tracker) {
    $.ajax({
      // url: 'https://aulascolmeia.com.br/landing-campanha',
      url: '/landing-campanha',
      type: 'POST',
      data: {
        nome: $('.header--' + test.toLowerCase())
          .find('.form__input--nome')
          .val(),
        telefone:
          '+55' +
          $('.header--' + test.toLowerCase())
            .find('.form__input--telefone')
            .cleanVal(),
        email: $('.header--' + test.toLowerCase())
          .find('.form__input--email')
          .val(),
        info:
          $('.header--' + test.toLowerCase())
            .find('.form__input--pacote')
            .val() || '',
        ga_id: '',
        origin: ('landing-campanha-' + pageId).toUpperCase(),
        navigator: '',
        url: window.location.href
      }
    })
      .done(function(result) {
        if (result.success) {
          $('form')
            .find('.form__input')
            .val('');

          // var link = window.location.pathname;
          // if (link[link.length - 1] != '/') {
          //   link += '/';
          // }

          // link += 'obrigado/' + window.location.search;

          // window.location.href = link;

          alert('Recebemos seus dados, em breve entraremos em contato. A Colmeia agradece!');

          ga('send', 'event', {
            eventCategory: 'Fantasma AB Test',
            eventAction: 'lead',
            eventLabel: localStorage.getItem('abLabel')
          });
        } else {
          alert('Desculpe, houve um erro ao receber seus dados.');
        }
      })
      .fail(function(jqXHR, textStatus, errorThrown) {
        alert('Desculpe, houve um erro ao receber seus dados.');
      });
    // });
  });

  function setupMask() {
    var maskBehavior = function(val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
      },
      options = {
        onKeyPress: function(val, e, field, options) {
          field.mask(maskBehavior.apply({}, arguments), options);
        }
      };

    $('.form__input--telefone').mask(maskBehavior, options);
    $('.whatsapp__input--phone').mask(maskBehavior, options);
  }

  function setupWhatsappForm() {
    $('.whatsapp__form').on('submit', function(e) {
      e.preventDefault();

      if (
        $('.whatsapp__input--name').val() == '' ||
        $('.whatsapp__input--phone').val() == '' ||
        $('.whatsapp__input--email').val() == ''
      ) {
        return alert('Por favor, preencha todos os campos!');
      }

      var emailRegex = new RegExp(
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      );
      if (!emailRegex.test($('.whatsapp__input--email').val())) {
        return alert('O email que você inseriu não é válido!');
      }

      $.ajax({
        url: '/landing-campanha',
        type: 'POST',
        data: {
          nome: $('.whatsapp__input--name').val(),
          telefone: '+55' + $('.whatsapp__input--phone').cleanVal(),
          email: $('.whatsapp__input--email').val(),
          origin: ('landing-campanha-' + pageId + '-WHATSAPP').toUpperCase(),
          url: window.location.href
        }
      })
        .done(function(result) {
          if (result.success) {
            window.location.href =
              'https://api.whatsapp.com/send?phone=5561998052073&text=Olá, meu nome é ' +
              $('.whatsapp__input--name').val() +
              ' e eu gostaria de informações sobre as aulas de reforço' +
              pageIdTitle +
              '.';
          } else if (result.message) {
            alert(result.message);
          }
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
          alert(
            'Desculpe, houve um erro ao receber suas informações. Por favor, entre em contato conosco pelo WhatsApp (61) 9 9805-2073.'
          );
        });
    });
  }

  function setupTest(cb) {
    if (!localStorage.getItem('abLabel')) {
      localStorage.setItem('abLabel', Math.random() >= 0.5 ? 'A' : 'B');
    }

    ga('send', 'event', {
      eventCategory: 'Fantasma AB Test',
      eventAction: 'pageview',
      eventLabel: localStorage.getItem('abLabel')
    });
  }
});
