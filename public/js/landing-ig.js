$(function() {
    $('#cta').on('click', function() {
        $(this).hide(400, function() {
            $('.form-container').slideDown(400);
        });
    });

    var maskBehavior = function(val) {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
        },
        options = {
            onKeyPress: function(val, e, field, options) {
                field.mask(maskBehavior.apply({}, arguments), options);
            }
        };

    $('#telefone').mask(maskBehavior, options);

    $('form').submit(function(e) {
        e.preventDefault();

        if ($('#nome').val() == '') {
            alert('Por favor, insira seu nome!');
            return;
        }

        if ($('#telefone').val() == '') {
            alert('Por favor, insira seu telefone!');
            return;
        }

        ga(function(tracker) {
            $.ajax({
                url: '/conheca-a-colmeia',
                type: 'POST',
                data: { phone: $('#telefone').val(), name: $('#nome').val(), ga_id: tracker.get('clientId') }
            })
                .done(function(result) {
                    if (result.success) {
                        try {
                            woopra.identify({
                                phone: $('#telefone').val(),
                                name: $('#name').val()
                            });

                            woopra.track('inscreveuLandingSp', {});
                        } catch (e) {}

                        alert('Recebemos seus dados e iremos entrar em contato para agendar sua aula!');
                        $('form')
                            .find('input')
                            .val('');
                    } else {
                        alert('Desculpe, houve um erro ao receber seus dados.');
                    }
                })
                .fail(function(jqXHR, textStatus, errorThrown) {
                    alert('Desculpe, houve um erro ao receber seus dados.');
                });
        });
    });
});
