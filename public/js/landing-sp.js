$(function() {
    var maskBehavior = function(val) {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
        },
        options = {
            onKeyPress: function(val, e, field, options) {
                field.mask(maskBehavior.apply({}, arguments), options);
            }
        };

    $('#sp-telefone').mask(maskBehavior, options);

    $('form').submit(function(e) {
        e.preventDefault();

        if ($('#sp-nome').val() == '') {
            alert('Por favor, insira seu nome!');
            return;
        }

        if ($('#sp-telefone').val() == '') {
            alert('Por favor, insira seu telefone!');
            return;
        }

        ga(function(tracker) {
            $.ajax({
                url: '/aula-particular-sp',
                type: 'POST',
                data: { phone: $('#sp-telefone').val(), name: $('#sp-nome').val(), ga_id: tracker.get('clientId') }
            })
                .done(function(result) {
                    if (result.success) {
                        try {
                            woopra.identify({
                                phone: $('#sp-telefone').val(),
                                name: $('#sp-name').val()
                            });

                            woopra.track('inscreveuLandingSp', {});
                        } catch (e) {}

                        alert('Recebemos seus dados e iremos entrar em contato para agendar sua aula!');
                        $('form')
                            .find('input')
                            .val('');
                    } else {
                        alert('Desculpe, houve um erro ao receber seus dados.');
                    }
                })
                .fail(function(jqXHR, textStatus, errorThrown) {
                    alert('Desculpe, houve um erro ao receber seus dados.');
                });
        });
    });
});
