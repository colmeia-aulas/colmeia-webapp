//configuracao para o loadingOverlay

$.LoadingOverlaySetup({
    fade: true,
    color: 'rgba(255, 255, 255, 0.7)',
    imagePosition: 'center center',
    image: '/img/loader.gif',
    // fontawesome     : "fa fa-spinner fa-spin",
    maxSize: '80px',
    minSize: '150px',
    resizeInterval: 0,
    size: '50%',
    zIndex: 9999
});
