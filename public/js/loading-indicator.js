//configuracao para o loadingOverlay

$.LoadingOverlaySetup({
    fade: true,
    color: 'rgba(255, 255, 255, 0.7)',
    imagePosition: 'center center',
    image: 'https://aulascolmeia.com.br/img/loader.gif',
    // fontawesome     : "fa fa-spinner fa-spin",
    maxSize: '80px',
    minSize: '150px',
    resizeInterval: 0,
    size: '50%',
    zIndex: 9999
});

$(document).ajaxStart(function() {
    $.LoadingOverlay('show');
});
$(document).ajaxStop(function() {
    $.LoadingOverlay('hide');
});
