var autenticado = false;
var shouldAskTipo = false;
var _fb_response;
var _fb_authResponse;
var emailEnviado = false;

$(function() {
  handleButtons();
  captureEmail();

  function captureEmail() {
    $('#register-email').on('focusout', function() {
      if (
        $(this).val() !== undefined &&
        $(this).val() !== null &&
        /.+@.+/.test($(this).val()) &&
        !emailEnviado
      ) {
        var data = {
          email: $(this).val()
        };

        $.LoadingOverlay('hide');
        $.ajax({
          url: '/aluno/pre-cadastro/email',
          type: 'POST',
          data: data
        })
          .done(function(data) {
            if (data.success) {
              emailEnviado = true;
            }
          })
          .fail(function(jqXHR, textStatus, errorThrown) {
            console.log('Erro! ', textStatus);
          });

        return true;
      }
    });
  }

  function handleButtons() {
    $('.login-button').on('click', function() {
      if (validateLogin()) {
        login();
      }
    });

    $('#login-email, #login-senha').keyup(function(e) {
      if (e.keyCode === 13) {
        $('.login-button').click();
      }
    });

    $('.register-button').on('click', function() {
      if (validateRegister()) {
        register();
      }
    });

    $('#register-email, #register-senha').keyup(function(e) {
      if (e.keyCode === 13) {
        $('.register-button').click();
      }
    });

    $('#register-telefone').on('click focus', function() {
      if (!autenticado) {
        $('#register-telefone').slideUp(500, function() {
          $('#firebaseui-auth-container').slideDown(500);
          $('.firebaseui-title').html('Insira o número do seu celular com DDD');
        });
      }
    });

    // tipo
    $('input[name="radio-tipo"]').on('change', function() {
      if ($(this).val() == 'PROFESSOR') {
        $('.tipo-container-professor').show();
        $('.tipo-container-aluno-pai').hide();
      } else {
        $('.tipo-container-aluno-pai').show();
        $('.tipo-container-professor').hide();
      }
    });

    $('#tipo-container .colmeia-button').on('click', function() {
      var tag = $('input[name="radio-tipo"]:checked').val();
      if (!tag) {
        alert('Selecione uma opção para prosseguir!');
        return;
      }
      var value = {
        data: {
          tipoCliente: tag
        }
      };

      $.ajax({ url: '/aluno/update', type: 'POST', data: value })
        .done(function(data) {
          if (data.success) {
            if (tag == 'PROFESSOR') {
              window.location = '/professor' + window.location.search;
            } else {
              window.location = next;
            }
          } else if (data.message) {
            alert(data.message);
          }
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
          alert('Houve um erro ao atualizar suas informações, tente novamente mais tarde.');
        });
    });
  }

  function login() {
    var values = {
      email: $('#login-email').val(),
      password: $('#login-senha').val()
    };

    $.ajax({ url: '/aluno/entrar', type: 'POST', data: values })
      .done(function(data) {
        if (data.success) {
          if (data.user.fav) {
          } else {
          }
          try {
            woopra.identify({
              email: data.user.email,
              name: data.user.nome,
              phone: data.user.phone,
              idparse: data.user.id
            });

            woopra.track('login', {
              facebook: false
            });
          } catch (e) {}
          window.location = next;
        } else if (data.message) {
          alert(data.message);
        } else {
          // var message = getErrorMessage(data.data.code);
          $('.error-container-login').html('Email ou senha inválidos.');
        }
      })
      .fail(function(jqXHR, textStatus, errorThrown) {
        alert('Houve um erro ao realizar seu login, tente novamente mais tarde.');
      });
  }

  function register() {
    ga(function(tracker) {
      var values = {
        nome: $('#register-nome').val(),
        password: $('#register-senha').val(),
        email: $('#register-email')
          .val()
          .toLowerCase()
          .trim(),
        telefone: $('#register-telefone').val(),
        digits_id: $('#register-digits-id').val(),
        ga_id: tracker.get('clientId')
      };

      $.ajax({
        url: '/aluno/registrar',
        type: 'POST',
        data: values
      })
        .done(function(data) {
          if (data.success) {
            // Checa se o Parse retornou algum erro

            try {
              woopra.identify({
                email: data.user.email,
                name: data.user.nome,
                phone: data.user.phone,
                idparse: data.user.id
              });

              woopra.track('signup', {
                facebook: false
              });
            } catch (e) {}

            showTipoContainer();
          } else {
            alert(
              data.message ||
                'Houve um erro ao realizar seu cadastro. Por favor, entre em contato conosco pelo WhatsApp (61) 9 9805-2073.'
            );
          }
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
          alert(
            'Houve um erro ao realizar seu cadastro. Por favor, entre em contato conosco pelo WhatsApp (61) 9 9805-2073.'
          );
        });
    });
  }

  function validateRegister() {
    $('.error-container-register').html('');

    var name = $('#register-nome').val();
    if (name === undefined || name === null || /\S/.test(name) === false) {
      // Checa se a string é vazia
      $('.error-container-register').html('Digite um nome para se cadastrar.');
      return false;
    }

    var email = $('#register-email').val();
    if (
      email === '' ||
      email === undefined ||
      email === null ||
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
        email
      ) === false
    ) {
      // Checa se a string é vazia
      $('.error-container-register').html('O email inserido não é válido.');
      return false;
    }

    var password = $('#register-senha').val();
    if (password === undefined || password === null || password.length < 6) {
      $('.error-container-register').html('A senha deve ter no mínimo 6 caracteres.');
      return false;
    }

    var telefone = $('#register-telefone').val();
    if (telefone === undefined || telefone === null || telefone == '') {
      $('.error-container-register').html('Insira um número de telefone.');
      return false;
    }

    var termosAceitos = $("input[type='checkbox']").prop('checked');
    if (!termosAceitos) {
      $('.error-container-register').html('Você deve aceitar os termos para prosseguir.');
      return false;
    }

    return true;
  }

  function validateLogin() {
    $('.error-container-login').html('');

    var email = $('#login-email').val();
    var password = $('#login-senha').val();

    if (email == '' || password == '') {
      // Checa se a string é vazia
      $('.error-container-login').html('Preencha os campos para prosseguir!');
      return false;
    }

    return true;
  }

  function showTipoContainer() {
    $.LoadingOverlay('hide');
    $.LoadingOverlay('hide');
    $.LoadingOverlay('hide');
    $.LoadingOverlay('hide');
    $('.login-container').hide();
    $('#tipo-container').show();
  }
});

function afterFirebaseAuth(phone, id) {
  $('#firebaseui-auth-container').slideUp(500, function() {
    $('#register-telefone').slideDown(500);
    $('#register-telefone').val(phone);
    $('#register-digits-id').val(id);

    autenticado = true;
  });
}
