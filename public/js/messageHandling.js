/*
Para usar, adicione no html:
    <script src="/js/lib/bootbox.min.js"></script>
    <script src="/js/messageHandling.js"></script>
*/


bootbox.setDefaults({closeButton: false});

function showLoading(message)
{
    var box = bootbox.dialog({ message: message, header: message, size: 'small' });
    box.find('.bootbox-body')
       .append('<div class="progress" style="margin-top: 10px; margin-bottom: 5px"><div class="progress-bar progress-bar-info progress-bar-striped active" ' +
                'style="width: 100%; background-color: #fca845"></div></div>');
}

function showMessage(message)
{
  var box = bootbox.alert({
      message: message,
      size: 'small'
  });
  box.find('.modal-body').css({'border': 'none'});
  box.find('.modal-footer').css({'border': 'none'});
  box.find('.btn-primary').css({'background-color': '#fca845', 'border': 'none'});
}

function showConfirmMessage(title, message, callback)
{
  var box = bootbox.confirm({
    title: title,
    message: message,
    backdrop: false,
    buttons: {
        confirm: {
            label: 'Sim',
            className: 'btn btn-default colmeia-button'
        },
        cancel: {
            label: 'Não',
            className: 'btn btn-default colmeia-button-cancel'
        }
    },
    callback: function (result) {
        callback(result);
    }
  });

  box.find('.bootbox-body').css({'font-size': '15px', 'font-weight': 'normal', 'text-align': 'center', 'padding-bottom': '5px'});
  box.find('.modal-header').css({'padding-bottom': '5px'});
  box.find('.modal-footer').css({'border': '0', 'margin': '0 auto'});
}

function showErrorMessage(message)
{
    var box = bootbox.alert({
        message: message,
        size: 'small'
    });
    box.find('.bootbox-body').css({'font-size': '15px', 'font-weight': 'bold'})
    box.find('.modal-footer').css({'border': 0});
    box.find('.btn-primary').css({'background-color': '#FF8008'});
}

function getErrorMessage(code)
{
  switch(code){
      case 101:
          return "E-mail ou senha incorretos"
      case 100:
          return "Sem conexão"
      case 202:
      case 203:
          return "E-mail já existente"
      default:
          return "Erro. Tente novamente"
  }
}

function closeMessages()
{
    bootbox.hideAll();
}
