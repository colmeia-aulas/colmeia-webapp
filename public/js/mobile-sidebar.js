$(function() {
  $('.navbar-toggle').on('click', function() {
    if ($('.mobile-sidebar').css('right') == '-250px') {
      $('.mobile-sidebar').css('right', '0px');
      $('.navbar-toggle').addClass('toggle-active');
    }
    else {
      $('.mobile-sidebar').css('right', '-250px');
      $('.navbar-toggle').removeClass('toggle-active');
    }
  });

  $('.mobile-sidebar-link').on('click', function() {
    $('.mobile-sidebar').css('right', '-250px');
  });

  $('.mobile-dropdown-toggle').on('click', function() {
    $('.mobile-sidebar-dropdown-menu').slideToggle('fast');
  });

});
