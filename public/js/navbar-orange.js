$(function() {
    $('a.dropdown-toggle').on('click', function() {
      if ( $('.dropdown-menu').css('display') == 'none' ) {
        $('.dropdown-menu').css('display', 'block');
      }
      else {
        $('.dropdown-menu').css('display', 'none');
      }
    });

    if (mobilecheck()) {
      $("#navbar-logo").attr("src","/img/logo.png");
      $("#mainNav").addClass("navbar-fixed-top");
    }
  });
  