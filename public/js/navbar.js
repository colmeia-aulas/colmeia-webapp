$(function() {
    $('a.dropdown-toggle').on('click', function() {
        if ($('.dropdown-menu').css('display') == 'none') {
            $('.dropdown-menu').css('display', 'block');
        } else {
            $('.dropdown-menu').css('display', 'none');
        }
    });

    if (mobilecheck()) {
        $('#navbar-logo').attr('src', '/img/logo-orange.png');
        $('#mainNav').addClass('navbar-fixed-top');
    }

    // link hack
    $('a').each(function(i, el) {
        var href = $(this).attr('href');

        if (href[0] == '/' || href.includes('aulascolmeia.com.br')) {
            $(this).attr({
                href: href + window.location.search
            });
        }
    });
});
