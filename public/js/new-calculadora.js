$(function() {
  handleWhatsapp();
  setupFlickity();
  setupMask();
  submitBottomForm();
  submitModalForm();
  handleModalButton();

  setupEscolas();

  function setupEscolas() {
    var escolas = [
      'Ced Leonardo Da Vinci Unidade Taguatinga',
      'Ced Catolica De Brasilia',
      'La Salle (Escola)',
      'Ced Leonardo Da Vinci Unid Asa Norte',
      'Marista Champagnat (Colegio)',
      'Ced Sigma',
      'Fund Bradesco (Escola De Educacao Basica E Profissional)',
      'Presbiteriano Mackenzie Brasilia (Colegio)',
      'Ideal (Colegio)',
      'Marista Joao Paulo Ii (Colegio)',
      'Do Servico Social Do Comercio Edusesc (Educ)',
      'Adventista Do Gama (Escola)',
      'Rogacionista (Colegio)',
      'Sao Jose (Instituto)',
      'Ciman (Colegio)',
      'Santo Elias (Instituto Educacional)',
      'Col Wgs',
      'Madre Carmen Salles (Colegio)',
      'Ced Juscelino Kubitschek Gama',
      'Do Sesi Df Taguatinga (Centro Educacional)',
      'Ced Sigma Aguas Claras',
      'Claretiano Ced Stella Maris',
      'Ebenezer (Centro Educacional)',
      'Ced Juscelino Kubitschek Asa Norte I',
      'La Salle (Colegio)',
      'Adventista De Planaltina (Escola)',
      'Ced Adventista De Taguatinga',
      'Ced Adventista Milton Afonso',
      'Batista De Brasilia (Colegio)',
      'Sagrado Coracao De Maria (Colegio)',
      'La Salle Brasilia (Colegio)',
      'Jesus Maria Jose (Colegio)',
      'Col Alub Sede V',
      'Santa Rita De Cassia (Centro Educacional)',
      'Col Serios',
      'Vila Das Criancas (Escola)',
      'Salesiana Sao Domingos Savio (Escola)',
      'Paroquial Santo Antonio (Escola)',
      'Candanguinho Cecan (Centro Educacional)',
      'Das Nacoes (Escola)',
      'Galois (Colegio)',
      'Ced Leonardo Da Vinci',
      'Notre Dame (Colegio)',
      'Ced Sigma Asa Norte',
      'Olimpo (Colegio)',
      'Sagarana (Instituto)',
      'Dinamico Ced',
      'Alub (Colegio)',
      'Maxwell (Colegio)',
      'Reacao Ii (Colegio)',
      'Col Alub Sede Vii',
      'Cor Jesu (Colegio)',
      'Mapa (Colegio)',
      'Nossa Senhora Do Perpetuo Socorro (Colegio)',
      'Do Sesi Df Gama (Centro Educacional)',
      'Col Alub Sede Viii',
      'Impacto (Colegio)',
      'Americana De Brasilia (Escola)',
      'Vitoria (Colegio)',
      'Tiradentes (Colegio)',
      'Passionista Mae Da Santa Esperanca (Centro Educacional)',
      'La Salle Sobradinho (Colegio)',
      'Esc Jardim Do Eden',
      'Do Sol (Colegio)',
      'Soma (Colegio)',
      'Ced Projecao Taguatinga Norte',
      'Brasil Central (Centro Educacional)',
      'Barao Do Rio Branco Paranoa (Colegio)',
      'Olimpico De Ensino (Centro)',
      'Ced Maria Auxiliadora',
      'Lyccee Francais Francois Mitterrand',
      'Santa Terezinha (Colegio)',
      'De Maria (Educandario)',
      'Ced Sagrada Familia',
      'De Fatima (Educandario)',
      'Franciscana Nossa Sra De Fatima (Escola)',
      'Santa Maria (Colegio)',
      'Esplanada (Colegio)',
      'Coc Brasilia',
      'Col Dj',
      'Paloma (Colegio)',
      'Vital Brazil (Colegio)',
      'Ced Juscelino Kubitschek',
      'Col Alub Sede Iii',
      'Isaac Newton (Centro Educacional)',
      'Athos (Colegio)',
      'Objetivo Gama (Colegio)',
      'Instei (Centro Educacional)',
      'Isaac Newton (Colegio)',
      'Dom Cesar (Colegio)',
      'Inst Global De Educ',
      'Mont Blanc Ced',
      'Cenel C De Educ Nery Lacerda',
      'Dom Bosco (Colegio)',
      'Espu (Colegio)',
      'Santa Doroteia (Colegio)',
      'Ced Certo',
      'Master Ii (Escola)',
      'Ced Horacina Catta Preta',
      'Ced Sete Estrelas',
      'Dinamico Ced Taguatinga',
      'Dromos (Instituto Educacional)',
      'Col Olimpo De Aguas Claras',
      'Col Madre Teresa',
      'Triangulo Recanto (Colegio)',
      'Educacional Compact Gama',
      'Ced Origem',
      'Ced Expoente',
      'Ced Juscelino Kubitschek Taguatinga',
      'Cip Col Integrado Polivalente',
      'Ced Objetivo Taguatinga',
      'Instei Ce Vicente Pires',
      'Mariano (Colegio)',
      'Col Marista De Brasilia Ens Medio',
      'Alub (Colegio)',
      'Podion (Colegio)',
      'Ced Cci Senior',
      'Unico Educacional',
      'Ced Projecao Guara Ii',
      'Col Alub Sede Vi Ens Medio',
      'Delta (Cem)',
      'Ce Do Sesi Df - Sobradinho',
      'Maua (Instituto)'
    ].sort();

    $('.calculadora-modal__input--escola').append(
      $('<option>', {
        value: 'Outra',
        text: 'Outra'
      })
    );

    $.each(escolas, function(i, item) {
      $('.calculadora-modal__input--escola').append(
        $('<option>', {
          value: item,
          text: item
        })
      );
    });
  }

  function submitModalForm() {
    $('.calculadora-modal__form').on('submit', function(e) {
      e.preventDefault();

      if (
        $('.calculadora-modal__input--nome').val() == '' ||
        $('.calculadora-modal__input--celular').cleanVal() == '' ||
        $('.calculadora-modal__input--email').val() == '' ||
        $('.calculadora-modal__input--ano').val() == ''
      ) {
        return alert('Por favor, preencha todos os campos para solicitar nosso email!');
      }

      if (!$('.calculadora-modal__termos input').prop('checked')) {
        return alert('Você precisa concordar com nossos termos de uso para continuar!');
      }

      $.ajax({
        url: '/calculadora-pas-lead',
        type: 'POST',
        data: {
          nome: $('.calculadora-modal__input--nome').val(),
          telefone: '+55' + $('.calculadora-modal__input--celular').cleanVal(),
          email: $('.calculadora-modal__input--email').val(),
          ano: $('.calculadora-modal__input--ano').val(),
          escola: $('.calculadora-modal__input--escola').val(),
          url: window.location.href,
          argumento: $('.grade__value').html()
        }
      })
        .done(function(result) {
          if (result.success) {
            alert('Recebemos seus dados. Em breve te enviaremos um email, a Colmeia agradece!');

            $('#modal-toggle').prop('checked', false);
            $('.grade__value').html('--');
            $('.container.container--s').find('input').val('');
            $('.container.container--s select').val('Inglês');
          } else if (result.message) {
            alert(result.message);
          }

          $('.calculadora-modal__form')
            .find('input')
            .val('');
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
          alert(
            'Desculpe, houve um erro ao receber suas informações. Por favor, entre em contato conosco pelo WhatsApp (61) 9 9805-2073.'
          );
        });
    });
  }

  function handleModalButton() {
    $('.calculadora-modal__button').on('click', function() {
      if ($('.grade__value').html() == '--') {
        $('#modal-toggle').prop('checked', false);
        alert('Por favor, preencha todos os campos para calcular sua nota ou verifique os valores informados!');
      } else {
        $('#modal-toggle').prop('checked', true);
      }
    });
  }

  function setupFlickity() {
    $('.clients__carousel').flickity({
      cellAlign: 'center',
      contain: true,
      autoPlay: true
    });
  }

  function handleWhatsapp() {
    $('.whatsapp__form').on('submit', function(e) {
      e.preventDefault();

      if (
        $('.whatsapp__input--name').val() == '' ||
        $('.whatsapp__input--phone').val() == '' ||
        $('.whatsapp__input--email').val() == ''
      ) {
        return alert('Por favor, preencha todos os campos!');
      }

      var emailRegex = new RegExp(
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      );
      if (!emailRegex.test($('.whatsapp__input--email').val())) {
        return alert('O email que você inseriu não é válido!');
      }

      $.ajax({
        url: '/landing-campanha',
        type: 'POST',
        data: {
          nome: $('.whatsapp__input--name').val(),
          telefone: '+55' + $('.whatsapp__input--phone').cleanVal(),
          email: $('.whatsapp__input--email').val(),
          origin: ('landing-campanha-' + 'CALCULADORA' + '-WHATSAPP').toUpperCase(),
          url: window.location.href
        }
      })
        .done(function(result) {
          if (result.success) {
            window.location.href =
              'https://api.whatsapp.com/send?phone=5561998052073&text=Olá, meu nome é ' +
              $('.whatsapp__input--name').val() +
              ' e eu gostaria de informações sobre as aulas de reforço.';
          } else if (result.message) {
            alert(result.message);
          }
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
          alert(
            'Desculpe, houve um erro ao receber suas informações. Por favor, entre em contato conosco pelo WhatsApp (61) 9 9805-2073.'
          );
        });
    });
  }

  function setupMask() {
    var maskBehavior = function(val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
      },
      options = {
        onKeyPress: function(val, e, field, options) {
          field.mask(maskBehavior.apply({}, arguments), options);
        }
      };

    $('.form__input--phone').mask(maskBehavior, options);
    $('.form-campaign__input--phone').mask(maskBehavior, options);
    $('.whatsapp__input--phone').mask(maskBehavior, options);
    $('.calculadora-modal__input--celular').mask(maskBehavior, options);
  }

  function submitBottomForm() {
    $('form#form-contact').on('submit', function(e) {
      e.preventDefault();

      if (
        $('.form__input--name').val() == '' ||
        $('.form__input--phone').cleanVal() == '' ||
        $('.form__input--email').val() == ''
      ) {
        return alert('Por favor, preencha todos os campos para solicitar nosso contato.');
      }

      $.ajax({
        url: '/landing-campanha',
        type: 'POST',
        data: {
          nome: $('.form__input--name').val(),
          telefone: '+55' + $('.form__input--phone').cleanVal(),
          email: $('.form__input--email').val(),
          info: '',
          ga_id: '',
          origin: 'landing-calculadora'.toUpperCase(),
          navigator: '',
          url: window.location.href
        }
      })
        .done(function(result) {
          if (result.success) {
            alert(
              'Recebemos sua solicitação com sucesso. Em breve entraremos em contato, a Colmeia agradece!'
            );
          } else if (result.message) {
            alert(result.message);
          }

          $('#form-contact')
            .find('input')
            .val('');
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
          alert(
            'Desculpe, houve um erro ao receber suas informações. Por favor, entre em contato conosco pelo WhatsApp (61) 9 9805-2073.'
          );
        });
    });
  }

  $('.pas-block__input').on('change keyup paste', function() {
    checkAllFields(function() {
      var pas1El = {
        idioma: $('#pas1')
          .find('.pas-block__input--idioma')
          .val(),
        redacao: Number(
          $('#pas1')
            .find('.pas-block__input--redacao')
            .val()
            .replace(/,/g, '.')
        ),
        escore: Number(
          $('#pas1')
            .find('.pas-block__input--escore')
            .val()
            .replace(/,/g, '.')
        )
      };

      var pas2El = {
        idioma: $('#pas2')
          .find('.pas-block__input--idioma')
          .val(),
        redacao: Number(
          $('#pas2')
            .find('.pas-block__input--redacao')
            .val()
            .replace(/,/g, '.')
        ),
        escore: Number(
          $('#pas2')
            .find('.pas-block__input--escore')
            .val()
            .replace(/,/g, '.')
        )
      };

      var pas3El = {
        idioma: $('#pas3')
          .find('.pas-block__input--idioma')
          .val(),
        redacao: Number(
          $('#pas3')
            .find('.pas-block__input--redacao')
            .val()
            .replace(/,/g, '.')
        ),
        escore: Number(
          $('#pas3')
            .find('.pas-block__input--escore')
            .val()
            .replace(/,/g, '.')
        )
      };

      if (pas1El.escore > 90 || pas2El.escore > 90 || pas3El.escore > 90) {
        $('.grade__error').html('O valor máximo do escore é 90!');
        $('.grade__value').html('--');
      } else if (pas1El.redacao > 10 || pas2El.redacao > 10 || pas3El.redacao > 10) {
        $('.grade__error').html('O valor máximo da redação é 10!');
        $('.grade__value').html('--');
      } else {
        var end = false;

        var res = $('.grade__value');
        res.html('-');

        var timerId = setInterval(function() {
          if (res.html().length > 4 && !end) {
            res.html('-');
          } else if (!end) {
            res.html(res.html() + '-');
          }
        }, 500);

        $.post('/calculadora-pas', { pas1: pas1El, pas2: pas2El, pas3: pas3El }).done(function(
          response
        ) {
          end = true;
          clearInterval(timerId);

          if (response.success) {
            $('.grade__value').html(response.argumento.toFixed(2));
          } else {
            $('.grade__value').html('Você deve inserir dados válidos!');
            $('.grade__value').html('--');
            $('#modal-toggle').prop('checked', false);
          }
        });
      }
    });
  });

  function checkAllFields(callback) {
    var pas1El = {
      idioma: $('#pas1')
        .find('.pas-block__input--idioma')
        .val(),
      redacao: $('#pas1')
        .find('.pas-block__input--redacao')
        .val()
        .replace(/,/g, '.'),
      escore: $('#pas1')
        .find('.pas-block__input--escore')
        .val()
        .replace(/,/g, '.')
    };
    var pas2El = {
      idioma: $('#pas2')
        .find('.pas-block__input--idioma')
        .val(),
      redacao: $('#pas2')
        .find('.pas-block__input--redacao')
        .val()
        .replace(/,/g, '.'),
      escore: $('#pas2')
        .find('.pas-block__input--escore')
        .val()
        .replace(/,/g, '.')
    };
    var pas3El = {
      idioma: $('#pas3')
        .find('.pas-block__input--idioma')
        .val(),
      redacao: $('#pas3')
        .find('.pas-block__input--redacao')
        .val()
        .replace(/,/g, '.'),
      escore: $('#pas3')
        .find('.pas-block__input--escore')
        .val()
        .replace(/,/g, '.')
    };

    if (
      validateField(pas1El.redacao) &&
      validateField(pas1El.escore) &&
      validateField(pas2El.redacao) &&
      validateField(pas2El.escore) &&
      validateField(pas3El.redacao) &&
      validateField(pas3El.escore)
    ) {
      $('.grade__error').html('');
      callback();
    } else {
      $('.grade__value').html('--');
    }
  }

  function validateField(fieldValue) {
    return fieldValue != '' && fieldValue != undefined && fieldValue != ' ';
  }
});
