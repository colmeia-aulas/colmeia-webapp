$(function() {
  handleFlickity();
  handleScroll();
  handleWhatsapp();

  setupMask();
  submitBottomForm();

  function handleFlickity() {
    $('.clients__carousel').flickity({
      cellAlign: 'center',
      contain: true,
      autoPlay: true
    });
  }

  function handleScroll() {
    $('.js-cta').on('click', function() {
      $('html, body').animate(
        {
          scrollTop: $('body').offset().top
        },
        750
      );
    });
  }

  function handleWhatsapp() {
    $('.whatsapp__form').on('submit', function(e) {
      e.preventDefault();

      if (
        $('.whatsapp__input--name').val() == '' ||
        $('.whatsapp__input--phone').val() == '' ||
        $('.whatsapp__input--email').val() == ''
      ) {
        return alert('Por favor, preencha todos os campos!');
      }

      var emailRegex = new RegExp(
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      );
      if (!emailRegex.test($('.whatsapp__input--email').val())) {
        return alert('O email que você inseriu não é válido!');
      }

      $.ajax({
        url: '/landing-campanha',
        type: 'POST',
        data: {
          nome: $('.whatsapp__input--name').val(),
          telefone: '+55' + $('.whatsapp__input--phone').cleanVal(),
          email: $('.whatsapp__input--email').val(),
          origin: ('landing-campanha-' + pageId + '-WHATSAPP').toUpperCase(),
          url: window.location.href
        }
      })
        .done(function(result) {
          if (result.success) {
            window.location.href =
              'https://api.whatsapp.com/send?phone=5561998052073&text=Olá, meu nome é ' +
              $('.whatsapp__input--name').val() +
              ' e eu gostaria de informações sobre as aulas de reforço' +
              pageIdTitle +
              '.';
          } else if (result.message) {
            alert(result.message);
          }
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
          alert(
            'Desculpe, houve um erro ao receber suas informações. Por favor, entre em contato conosco pelo WhatsApp (61) 9 9805-2073.'
          );
        });
    });
  }

  function setupMask() {
    var maskBehavior = function(val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
      },
      options = {
        onKeyPress: function(val, e, field, options) {
          field.mask(maskBehavior.apply({}, arguments), options);
        }
      };

    $('.form__input--phone').mask(maskBehavior, options);
    $('.form-campaign__input--phone').mask(maskBehavior, options);
    $('.whatsapp__input--phone').mask(maskBehavior, options);
  }

  function submitBottomForm() {
    $('form#form-contact').on('submit', function(e) {
      e.preventDefault();

      if (
        $('.form__input--name').val() == '' ||
        $('.form__input--phone').cleanVal() == '' ||
        $('.form__input--email').val() == ''
      ) {
        return alert('Por favor, preencha todos os campos para solicitar nosso contato.');
      }

      $.ajax({
        url: '/landing-campanha',
        type: 'POST',
        data: {
          nome: $('.form__input--name').val(),
          telefone: '+55' + $('.form__input--phone').cleanVal(),
          email: $('.form__input--email').val(),
          info: bannerClicked ? 'Clicou no banner' : '',
          ga_id: '',
          origin: 'landing-empresas'.toUpperCase(),
          navigator: '',
          url: window.location.href
        }
      })
        .done(function(result) {
          if (result.success) {
            alert(
              'Recebemos sua solicitação com sucesso. Em breve entraremos em contato, a Colmeia agradece!'
            );
          } else if (result.message) {
            alert(result.message);
          }

          $('#form-contact')
            .find('input')
            .val('');
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
          alert(
            'Desculpe, houve um erro ao receber suas informações. Por favor, entre em contato conosco pelo WhatsApp (61) 9 9805-2073.'
          );
        });
    });
  }
});
