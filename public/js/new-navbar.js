$(function() {
  var offset = 55;

  $(window).scroll(function() {
    $('.navbar').toggleClass('navbar--active', $(this).scrollTop() > offset);
  });

  if ($(window).scrollTop() > offset) {
    $('.navbar').addClass('.navbar--active');
  }
});
