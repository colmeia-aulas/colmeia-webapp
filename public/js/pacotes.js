$(function() {
  $('form').submit(function(e) {
    e.preventDefault();

    if ($('#nome').val() == '' || $('#telefone').val() == '') {
      alert('Por favor, preencha todos os campos!');
      return;
    }

    ga(function(tracker) {
      $.ajax({
        url: '/acompanhamento-escolar',
        type: 'POST',
        data: {
          nome: $('#nome').val(),
          telefone: $('#telefone').val(),
          ga_id: tracker.get('clientId')
        }
      })
        .done(function(result) {
          if (result.success) {
            try {
              woopra.identify({
                name: $('#nome').val(),
                phone: $('#telefone').val()
              });

              woopra.track('inscreveuPacote', {});
            } catch (e) {}

            alert('Recebemos seus dados, a Colmeia agradece!');
            $('form')
              .find('input')
              .val('');
          } else if (result.message) {
            alert(result.message);
          }
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
          alert(
            'Desculpe, houve um erro ao receber suas informações. Por favor, entre em contato conosco pelo WhatsApp (61) 9 9805-2073.'
          );
        });
    });
  });

  $('#call-to-action').on('click', function() {
    $('html, body').animate(
      {
        scrollTop: $('section#contato').offset().top
      },
      800
    );
  });
});
