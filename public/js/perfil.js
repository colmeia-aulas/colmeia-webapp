$(function() {
  $('.perfil-img-box').on('click', function() {
    $("input[name='foto']").click();
  });

  $("input[name='foto']").change(function(e) {
    if (!checkExtention($(this).val(), ['png', 'jpg', 'jpeg'])) {
      alert('Por favor, insira sua foto no formato png, jpeg ou jpg.');
      return;
    } else if ($(this)[0].files[0].size > 10 * 1024 * 1024) {
      alert('Por favor, insira uma foto de até 10mb.');
    }

    if (confirm('Você deseja alterar sua foto de perfil?')) {
      //upload image
      var formData = new FormData($('#form-foto')[0]);
      formData.append('foto', $("input[type='file']")[0].files[0]);

      $.ajax({
        url: '/aluno/perfil/foto',
        enctype: 'multipart/form-data',
        processData: false, // Important!
        contentType: false,
        cache: false,
        type: 'POST',
        data: formData
      })
        .done(function(response) {
          if (response.success) {
            window.location = '/aluno/perfil';
          } else if (response.message) {
            alert(response.message);
          }
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
          alert('Houve um erro ao alterar sua imagem de perfil, tente novamente mais tarde.');
        });
    }
  });

  getPerfil(function(result) {
    var user = result.perfil;
    var foto = user.image ? user.image.url : '/img/profile_placeholder.png';
    var enderecos = result.enderecos.ends;
    var cartao = result.cartaoFavorito.favorite;
    var tel = user.telefone.split('+55')[1];
    var ddd = '' + tel[0] + tel[1];
    var telefoneFormatado = '(' + ddd + ') ' + tel.slice(2);

    var $container = $('.perfil-container');

    $container.find('.perfil-img').attr('src', foto);
    $container.find('.aluno-nome').html(user.nome);
    $container.find('.aluno-telefone').html(telefoneFormatado);

    if (user.email) {
      $container.find('.aluno-email').html(user.email);
    }

    $container.find('.perfil-img').on('load', function() {
      fixImagem($(this));
    });

    showEnderecosCartoes(enderecos, cartao, function() {
      $container.fadeIn('slow');
    });
  });
});

function showEnderecosCartoes(enderecos, cartao, callback) {
  var $enderecosContainer = $('.enderecos-container');
  var $cartoesContainer = $('.cartoes-container');

  if (enderecos.length > 0) {
    for (var i = 0; i < enderecos.length; i++) {
      $enderecosContainer.append(
        '<div class="row" id=' +
          enderecos[i].id +
          '>\
                    <div class="col-lg-12 col-md-12">\
                    <div class="info-card">' +
          '<b>' +
          enderecos[i].bairro +
          '</b> - ' +
          enderecos[i].endereco +
          ' ' +
          enderecos[i].complemento +
          ' ' +
          enderecos[i].numero +
          '</div>\
                    <div class="delete-icon">\
                        <img src="/img/remove-white.png" alt="">\
                    </div>\
                    </div>\
              </div>'
      );
    }

    $('.enderecos-container .delete-icon').on('click', function() {
      var id = $(this)
        .parent()
        .parent()
        .attr('id');

      deleteAddress(id);
    });
  } else {
    $enderecosContainer.append(
      '<div class="row">\
                <div class="col-lg-8 col-md-8">\
                    <p class="empty-error">Você ainda não possui nenhum endereço cadastrado.</p>\
                </div>\
          </div>'
    );
  }

  if (cartao) {
    $cartoesContainer.append(
      '\
        <div class="row" id=' +
        cartao.objectId +
        '>\
            <div class="col-lg-12 col-md-12">\
                <div class="info-card">' +
        '<b> Final: </b>' +
        cartao.digits +
        '<img src=/img/' +
        cartao.brand.toLowerCase() +
        '-logo.png' +
        '>' +
        '</div>\
                <div class="delete-icon">\
                    <img src="/img/remove-white.png" alt="">\
                </div>\
            </div>\
        </div>'
    );

    $('.cartoes-container .delete-icon').on('click', function() {
      var id = $(this)
        .parent()
        .parent()
        .attr('id');

      deleteCartao(id);
    });
  } else {
    $cartoesContainer.append(
      '<div class="row">\
                <div class="col-lg-8 col-md-8">\
                    <p class="empty-error">Você ainda não possui nenhum cartão cadastrado.</p>\
                </div>\
          </div>'
    );
  }

  var addCardButton =
    '<div class="row">\
    <div class="col-lg-8 col-md-8">\
        <div class="colmeia-button button-orange">\
        Alterar forma de pagamento\
        </div>\
    </div>\
    </div>';

  $('.cartoes-container').append(addCardButton);

  var addAddressButton =
    '<div class="row">\
           <div class="col-lg-8 col-md-8">\
             <div class="colmeia-button button-orange">\
               Adicionar novo endereço\
             </div>\
           </div>\
         </div>';

  $('.enderecos-container').append(addAddressButton);

  handleNovoCartao();
  handleNovoEndereco();

  callback();
}

function getPerfil(callback) {
  $.ajax({
    url: '/aluno/perfil/buscar',
    type: 'GET'
  })
    .done(function(result) {
      if (result.success) {
        callback(result);
      } else if (result.message) {
        alert(result.message);
      }
    })
    .fail(function() {
      alert('Houve um erro ao obter seu perfil, tente novamente mais tarde.');
    });
}

function fixImagem(foto) {
  foto = $(foto);

  var width = foto[0].width;
  var height = foto[0].height;

  if (width < height) {
    foto.css('width', '100%');
    foto.css('height', 'auto');
    // $('.intro-container img').css('margin-right', (height - width) + 'px');
  } else if (width > height) {
    foto.css('width', 'auto');
    foto.css('height', '100%');
    // $('.intro-container img').css('margin-left', (height - width) + 'px');
  } else if (width == height) {
    foto.css('height', '100%');
    foto.css('width', '100%');
  }
}

function deleteAddress(addressId) {
  $.ajax({
    url: '/reforco-escolar/endereco/remover',
    type: 'POST',
    data: { id: addressId }
  })
    .done(function(result) {
      if (result.success) {
        $('#' + addressId)
          .fadeOut('slow')
          .remove();
      } else {
        alert(result.error);
      }
    })
    .fail(function(jqXHR, textStatus, errorThrown) {
      alert('Houve um erro ao excluir seu endereço, tente novamente mais tarde.');
    });
}

function deleteCartao(cartaoId) {
  $.ajax({
    url: '/reforco-escolar/pagamento/remover',
    type: 'POST',
    data: { id: cartaoId }
  })
    .done(function(result) {
      if (result.success) {
        window.location = '/aluno/perfil';
      } else {
        alert(result.error);
      }
    })
    .fail(function(jqXHR, textStatus, errorThrown) {
      alert('Houve um erro ao excluir seu cartão, tente novamente mais tarde.');
    });
}

function handleNovoEndereco() {
  $('.enderecos-container .colmeia-button').on('click', function() {
    $('.enderecos-container').fadeOut('fast', function() {
      $('#enderecos-header').html('Novo endereço');
      $('#novo-endereco').fadeIn('fast');
    });
  });

  $('#select-cidade').empty();
  $('#select-bairro').empty();

  Object.keys(CIDADES).forEach(function(key, index) {
    $('#select-cidade').append("<option value='" + key + "'>" + CIDADES[key] + '</option>');
  });

  var bairros = BAIRROS['brasilia'];

  Object.keys(bairros).forEach(function(key, index) {
    if (key != 'todos-os-bairros') {
      $('#select-bairro').append("<option value='" + key + "'>" + bairros[key] + '</option>');
    }
  });

  $('#select-cidade').on('change', function() {
    var cidade = $(this).val();
    bairros = BAIRROS[cidade];

    $('#select-bairro').empty();

    Object.keys(bairros).forEach(function(key, index) {
      if (key != 'todos-os-bairros') {
        $('#select-bairro').append("<option value='" + key + "'>" + bairros[key] + '</option>');
      }
    });
  });

  //voltar
  $('#novo-endereco .colmeia-button.button-orange').on('click', function() {
    $('#novo-endereco').fadeOut('fast', function() {
      $('#enderecos-header').html('Meus endereços');
      $('.enderecos-container').fadeIn('fast');
      $('#novo-endereco')
        .find('input:text')
        .val('');
    });
  });

  //confirmar
  $('#novo-endereco .colmeia-button.button-filled').on('click', function() {
    if ($('#input-endereco').val() == '' || $('#input-numero').val() == '') {
      alert('Por favor, preencha todos os campos.');
    } else {
      var bairroVal = $('#select-bairro').val();
      var cidade = $('#select-cidade').val();
      var bairro = BAIRROS[cidade][bairroVal];

      var novoEnd = {
        endereco: $('#input-endereco').val(),
        numero: $('#input-numero').val(),
        complemento: $('#input-complemento').val(),
        bairro: bairro
      };

      $.ajax({
        url: '/reforco-escolar/endereco/adicionar',
        type: 'POST',
        data: novoEnd
      })
        .done(function(results) {
          if (results.success) {
            try {
              woopra.identify({
                email: user.email,
                name: user.nome,
                phone: user.phone,
                idparse: user.id
              });

              woopra.track('add_address', {
                bairro: results.end.bairro,
                cidade: results.end.cidade
              });
            } catch (e) {}
            // carregarEnderecos();
            window.location = '/aluno/perfil';
          } else {
            alert(results.error);
          }
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
          alert('Houve um erro ao adicionar seu endereço, tente novamente mais tarde.');
        });
    }
  });
}

function handleNovoCartao() {
  $('.cartoes-container .colmeia-button').on('click', function() {
    $('.cartoes-container').fadeOut('fast', function() {
      $('#cartoes-header').html('Forma de pagamento');
      $('#novo-cartao').fadeIn('fast');
    });
  });

  $('#input-mes').mask('00');
  $('#input-ano').mask('00');
  $('#input-seguranca').mask('0000');
  $('#input-nascimento').mask('00/00/0000');

  //voltar
  $('#novo-cartao .colmeia-button.button-orange').on('click', function() {
    $('#novo-cartao').fadeOut('fast', function() {
      $('#cartoes-header').html('Meus cartões');
      $('.cartoes-container').fadeIn('fast');
      $('#novo-cartao')
        .find('input:text')
        .val('');
    });
  });

  //confirmar
  $('#novo-cartao .colmeia-button.button-filled').on('click', function() {
    if (
      $('#input-nome').val() == '' ||
      $('#input-cpf').val() == '' ||
      $('#input-mes').val() == '' ||
      $('#input-ano').val() == '' ||
      $('#input-cartaonu').val() == '' ||
      $('#input-seguranca').val() == '' ||
      $('#input-nascimento').val() == ''
    ) {
      alert('Por favor, preencha todos os campos.');
      return;
    }

    if ($('#input-nascimento').val().length < 10) {
      alert('Insira uma data de nascimento válida.');
      return;
    }

    if ($('#input-cpf').val().length < 11) {
      alert('Insira um número de cpf válido.');
      return;
    }

    var nascimentoArray = $('#input-nascimento')
      .val()
      .split('/');
    var nascimento = nascimentoArray[2] + '-' + nascimentoArray[1] + '-' + nascimentoArray[0];

    var novoCard = {
      nome: $('#input-nome').val(),
      numero: $('#input-cartaonu').val(),
      mes: $('#input-mes').val(),
      ano: '20' + $('#input-ano').val(),
      cpf: $('#input-cpf').val(),
      seguranca: $('#input-seguranca').val(),
      nascimento: nascimento
    };

    hashCard(novoCard);
  });
}

function hashCard(novoCard) {
  var isNumberValid = Moip.Validator.isValid(novoCard.numero);
  var isExpiryValid = Moip.Validator.isExpiryDateValid(novoCard.mes, novoCard.ano);
  var isCvcValid = Moip.Validator.isSecurityCodeValid(novoCard.numero, novoCard.seguranca);

  if (!isNumberValid) {
    alert(
      'O número do cartão que você inseriu não é válido. Verifique os campos e tente novamente.'
    );
    return false;
  }
  if (!isExpiryValid) {
    alert(
      'A data de expiração do cartão que você inseriu não é válido. Verifique os campos e tente novamente.'
    );
    return false;
  }
  if (!isCvcValid) {
    alert(
      'O código de segurança do cartão que você inseriu não é válido. Verifique os campos e tente novamente.'
    );
    return false;
  }

  var objectToEncryp = {
    expirationMonth: novoCard.mes,
    expirationYear: novoCard.ano,
    number: novoCard.numero,
    cvc: novoCard.seguranca,
    birthdate: novoCard.nascimento,
    fullname: novoCard.nome,
    cpf: novoCard.cpf
  };

  var encryptedCard = NodeRsaEncryptString('card', JSON.stringify(objectToEncryp));
  addCard(encryptedCard);
}

function addCard(hash) {
  var cpf = $('#input-cpf').val();

  $.ajax({
    url: '/reforco-escolar/pagamento/adicionar',
    type: 'POST',
    data: { card_hash: hash, cpf: cpf }
  })
    .done(function(data) {
      if (data.success) {
        try {
          fbq('track', 'AddPaymentInfo', {
            value: precoTotal,
            currency: 'BRL'
          });
          woopra.identify({
            email: user.email,
            name: user.nome,
            phone: user.phone,
            idparse: user.id
          });

          woopra.track('add_card', {});
        } catch (e) {}

        window.location = '/aluno/perfil';
      } else if (data.path) {
        window.localtion = data.path;
      } else if (data.message) {
        alert(data.message);
      }
    })
    .fail(function() {
      console.log('error');
    });
}

function checkExtention(filename, extentionArray) {
  var extention;

  for (var i = filename.length - 1; i >= 0; i--) {
    if (filename[i] == '.') {
      extention = filename.slice(i + 1, filename.length);
      break;
    }
  }

  if (extentionArray.indexOf(extention) != -1) return true;
  else return false;
}
