var selectedProf;
var isProfSelected = false;

// var PRECO_BSB = precos['Brasília'];
// var PRECO_SP = precos['São Paulo'];

var daysHash = {
  0: 'SU',
  1: 'MO',
  2: 'TU',
  3: 'WE',
  4: 'TH',
  5: 'FR',
  6: 'SA'
};

var professor;
var schedule;
var rules;
var busy;
var busy_class;
var free_class;
var busyClassInterval;

var _scheduleDuration = 3; // $('#select-duracao').val() * 2
var _initialDate = moment();
var _currentDate = moment();
var _chosenHour = '';

var _currentDayOfYear = _initialDate.dayOfYear();
var _currenYear = _initialDate.year();
var _currentHour = _initialDate.format('HH:mm');

$(function() {
  fill();
});

function fill() {
  var id = prof.id;
  var curriculo = prof.curriculo
    ? prof.curriculo.replace(/"/g, '&quot;').replace(/(?:\r\n|\r|\n)/g, '<br />')
    : '';
  var img = prof.image && prof.image.url ? prof.image.url : '/img/profile_placeholder.png';

  var anos = prof.anos;

  var anosLinguas = prof.nivelLinguas;

  var topicos = prof.topicos;
  var bairros = prof.bairros;

  var horarios = prof.horarios;
  var profSchedule = prof.schedule;

  var profBusyClassInterval = prof.busyClassInterval;

  var imgAlt = prof.nome.split(' ')[0] + ' professor particular.';
  var nome = prof.nome;

  var avaliacao = prof.avaliacao.toFixed(2);
  var profId = prof.id;
  var profUserId = prof.userId;

  var profData = {
    bairros: bairros,
    anos: anos,
    nivelLinguas: anosLinguas,
    topicos: topicos
  };

  var container = $('.professor-selecionado');

  var nums = [];
  var days = [];

  for (var i = 0; i < horarios.length; i++) {
    nums.push(horarios[i][0]);
  }

  $('.professor-dias .professor-dia').each(function() {
    $(this).removeClass('professor-dia-active');
  });

  schedule = profSchedule;
  rules = reduceRuleByDay(schedule.rules === undefined ? [] : schedule.rules);
  busy = schedule.busy;
  busy_class = schedule.busy_class;
  free_class = schedule.free;
  busyClassInterval = profBusyClassInterval;

  if (rules['SU'].length > 0) $('.professor-dias #domingo').addClass('professor-dia-active');
  if (rules['MO'].length > 0) $('.professor-dias #segunda').addClass('professor-dia-active');
  if (rules['TU'].length > 0) $('.professor-dias #terca').addClass('professor-dia-active');
  if (rules['WE'].length > 0) $('.professor-dias #quarta').addClass('professor-dia-active');
  if (rules['TH'].length > 0) $('.professor-dias #quinta').addClass('professor-dia-active');
  if (rules['FR'].length > 0) $('.professor-dias #sexta').addClass('professor-dia-active');
  if (rules['SA'].length > 0) $('.professor-dias #sabado').addClass('professor-dia-active');

  container.find('.psh-image img').attr('src', img);
  container.find('.pscm-img img').attr('src', img);

  container.find('.psh-image img').attr('alt', imgAlt);
  container.find('.pscm-img img').attr('alt', imgAlt);

  container.find('.psh-name').html(nome);
  container.find('.pscm-name').html(nome);

  container.find('.professor-selecionado-info .professor-curriculo').html(curriculo);

  container.find('.psh-disciplinas').html('');

  topicos.forEach(function(e) {
    container.find('.psh-disciplinas').append('<div class="psh-disciplina">' + e + '</div>');
  });

  container.find('.professor-niveis').html('');

  anos.forEach(function(e) {
    container.find('.professor-niveis').append('<div class="professor-nivel">' + e + '</div>');
  });

  container.find('.professor-bairros').html(bairros.join(', '));

  container.find('.psh-rating-value').html(avaliacao);
  container.find('.pscm-rating-value').html(avaliacao);

  $('.professor-selecionado-avaliacao').html();
  (prof.avaliacoes || []).forEach(function(e) {
    $('.professor-selecionado-avaliacao').append(
      '\
                <div class="professor-avaliacao">\
                    <div class="psa-left">\
                            <div class="psa-image"><img src="' +
        (e.img ? e.img : '/img/profile_placeholder.png') +
        '" alt="Aulas Colmeia reforço escolar professor foto"></div>\
                    </div>\
                    <div class="psa-middle">\
                        <div class="psa-author">\
                            ' +
        e.nome +
        ' \
                        </div>\
                        <div class="psa-avaliacao">\
                        ' +
        e.conteudo +
        ' \
                        </div>\
                        <div class="psa-info">\
                            Teve aulas de ' +
        e.materia +
        ' com ' +
        prof.nome.split(' ')[0] +
        '\
                        </div>\
                    </div>\
                    <div class="psa-right">\
                        <div class="psa-rating">\
                            <div class="psa-rating-value">' +
        e.nota.toFixed(2) +
        '</div>\
                            <img class="psa-rating-img" src="/img/rating-star.png" alt="Avaliação professor Aulas Colmeia">\
                        </div>\
                        <div class="psa-date">\
                            ' +
        e.data +
        '\
                        </div>\
                    </div>\
                </div>'
    );
  });

  updateView();
}

function fixImagem(foto) {
  if (!(foto instanceof jQuery)) {
    foto = $(foto);
  }

  var width = foto[0].width;
  var height = foto[0].height;

  if (width < height) {
    foto.css('width', '100%');
    foto.css('height', 'auto');
    // $('.intro-container img').css('margin-right', (height - width) + 'px');
  } else if (width > height) {
    foto.css('width', 'auto');
    foto.css('height', '100%');
    // $('.intro-container img').css('margin-left', (height - width) + 'px');
  } else if (width == height) {
    foto.css('height', '100%');
    foto.css('width', '100%');
  }
}

function handleMobileDetaches() {
  if (mobilecheck()) {
    var first = $('#data-e-horario');

    first
      .next('.row')
      .clone(true)
      .prependTo('.horario-mobile'); //duracao
    first.detach().prependTo('.horario-mobile'); //data e horario

    $('.horario-mobile').prepend('<div class="horario-title">Escolher horário</div>');

    $('.horario-mobile').append(
      "<div class='row'><div class='col-lg-12 col-md-12'><div class='colmeia-button button-filled' id='mobile-action-button'>Agendar aula</div></div></div>"
    ); // botao
  }
}

////////////////////////////==HORARIOS==////////////////////////////////////////

function updateView() {
  $('[id=horario-erro]').hide();
  // _chosenHour = "";

  var horarios = generatePossibleSlots(_scheduleDuration, _currentDate);

  var splitedHorarios = { Manhã: [], Tarde: [], Noite: [] };

  for (var i = 0; i < horarios.length; i++) {
    var horario = horarios[i];
    splitedHorarios[getPeriod(horario)].push(horario);
  }

  if (horarios.length === 0) {
    $('[id=input-horario]')
      .empty()
      .append('<option value="" disabled selected>Sem horários</option>');
    $('[id=input-horario]').prop('disabled', true);

    $('[id=horario-erro]').show();
  } else {
    $('[id=input-horario]')
      .empty()
      .append('<option value="" disabled selected>Selecione horário</option>');
    $('[id=input-horario]').prop('disabled', false);

    if (splitedHorarios['Manhã'].length > 0) {
      generatePeriodList('Manhã', splitedHorarios['Manhã']);
    }
    if (splitedHorarios['Tarde'].length > 0) {
      generatePeriodList('Tarde', splitedHorarios['Tarde']);
    }
    if (splitedHorarios['Noite'].length > 0) {
      generatePeriodList('Noite', splitedHorarios['Noite']);
    }
  }
}

function reduceRuleByDay(rules) {
  var result = { SU: [], MO: [], TU: [], WE: [], TH: [], FR: [], SA: [] };
  for (var i = 0; i < rules.length; i++) {
    const rule = rules[i],
      rrule = rule.RRULE,
      days = rrule.BYDAY,
      duration = rrule.DURATION,
      date =
        rule.DTSTART.iso === undefined
          ? rule.DTSTART + '/' + duration
          : rule.DTSTART.iso + '/' + duration;
    for (var j = 0; j < days.length; j++) {
      const day = days[j];
      result[day].push(date);
    }
  }
  return result;
}

function generateRuleSlots(duration, date) {
  const selectedDayOfYear = moment(date).dayOfYear(),
    selectedYear = moment(date).year(),
    day = daysHash[moment(date).day()],
    rulesArray = rules[day];
  var result = [];
  if (_currentDayOfYear <= selectedDayOfYear && _currenYear <= selectedYear) {
    for (var i = 0; i < rulesArray.length; i++) {
      const rule = rulesArray[i],
        initialTime = moment(rule.split('/')[0]),
        ruleDuration = moment.duration(rule.split('/')[1]);
      while (ruleDuration.asMinutes() >= duration * 30) {
        const selectedHour = initialTime.tz('America/Sao_Paulo').format('HH:mm');
        if (selectedHour >= _currentHour || selectedDayOfYear != _currentDayOfYear) {
          result.push(initialTime.tz('America/Sao_Paulo').format('HH:mm'));
        }
        initialTime.add(30, 'm');
        ruleDuration.subtract(30, 'm');
      }
    }
  }
  return result.sort();
}

function generateBusySlots(slotArray, duracao, date) {
  var result = [];

  if (slotArray !== undefined) {
    const dayOfYear = moment(date).dayOfYear();

    for (var i = 0; i < slotArray.length; i++) {
      const currentSlot = slotArray[i],
        splitString = currentSlot.split('/'),
        currentDate = moment(splitString[0]),
        currentDuration = moment.duration(splitString[1]);

      if (currentDate.dayOfYear() === dayOfYear) {
        // currentDate.subtract((30 * duracao) - 30, 'm');
        // currentDuration.add(30 * duracao, 'm');

        while (currentDuration.asMinutes() > 0) {
          result.push(currentDate.tz('America/Sao_Paulo').format('HH:mm'));
          currentDate.add(30, 'm');
          currentDuration.subtract(30, 'm');
        }
      }
    }
  }
  return result.sort();
}

function generateBusyClassSlots(slotArray, duracao, date) {
  var interval = busyClassInterval || 1;
  var result = [];

  if (slotArray !== undefined) {
    const dayOfYear = moment(date).dayOfYear();

    for (var i = 0; i < slotArray.length; i++) {
      const currentSlot = slotArray[i],
        splitString = currentSlot.split('/'),
        currentDate = moment(splitString[0]),
        currentDuration = moment.duration(splitString[1]);

      if (currentDate.dayOfYear() === dayOfYear) {
        currentDate.subtract(30 * interval * duracao, 'm');
        currentDuration.add(30 * interval * duracao, 'm');

        while (currentDuration.asMinutes() > 0) {
          result.push(currentDate.tz('America/Sao_Paulo').format('HH:mm'));
          currentDate.add(30, 'm');
          currentDuration.subtract(30, 'm');
        }
      }
    }
  }
  return result.sort();
}

function generateFreeSlots(slotArray, duracao, date) {
  var result = [];

  if (slotArray !== undefined) {
    const dayOfYear = moment(date).dayOfYear();

    for (var i = 0; i < slotArray.length; i++) {
      const currentSlot = slotArray[i],
        splitString = currentSlot.split('/'),
        currentDate = moment(splitString[0]),
        currentDuration = moment.duration(splitString[1]);

      if (currentDate.dayOfYear() === dayOfYear) {
        while (currentDuration.asMinutes() > 0) {
          result.push(currentDate.tz('America/Sao_Paulo').format('HH:mm'));
          currentDate.add(30, 'm');
          currentDuration.subtract(30, 'm');
        }
      }
    }
  }
  return result.sort();
}

function generatePossibleSlots(duration, date) {
  var slots = generateRuleSlots(duration, date),
    freeSlots = generateFreeSlots(free_class, duration, date),
    busySlots = generateBusySlots(busy, duration, date),
    busyClassSlots = generateBusyClassSlots(busy_class, duration, date);

  var allowedSlots = concatenateArrays(slots, freeSlots);
  var notAllowedSlots = concatenateArrays(busySlots, busyClassSlots);

  return allowedSlots.filter(function(item, pos) {
    return $.inArray(item, notAllowedSlots) === -1;
  });
}

function getPeriod(hour) {
  if (hour < '12:00') {
    return 'Manhã';
  } else if (hour > '18:00') {
    return 'Noite';
  }
  return 'Tarde';
}

function generatePeriodList(period, horarios) {
  $('[id=input-horario]').append('<optgroup label="' + period + '"></optgroup>');

  for (var i = 0; i < horarios.length; i++) {
    const horario = horarios[i];

    $('[id=input-horario]')
      .find('optgroup[label=' + period + ']')
      .append('<option value="' + horario + '">' + horario + '</option>');
  }
}

function concatenateArrays(a, b) {
  //junta arrays e remove repeticoes
  var c = a.concat(
    b.filter(function(item) {
      return a.indexOf(item) < 0;
    })
  );

  return c.sort();
}
