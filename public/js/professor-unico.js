var DIAS = {
  'MO': 'Segunda',
  'TU': 'Terça',
  'WE': 'Quarta',
  'TH': 'Quinta', 
  'FR': 'Sexta',
  'SA': 'Sábado',
  'SU': 'Domingo'
};

$(function() {

  var prof = professor[0];

  loadProf(prof, $('.professor-selecionado'));
});

function loadProf(prof, container) {

  var days = [];

  // troca \n por <br/>
  var curriculoProcessado = (prof.curriculo) ? (prof.curriculo).replace(/(?:\r\n|\r|\n)/g, '<br />') : "";
  var imgSrc = (prof.image) ? (prof.image.url) : ('/img/profile_placeholder.png');

  for (var i = 0; i < prof.horarios.length; i++) {
    nums.push(prof.horarios[i][0]);
  }

  if(prof.schedule.rules) {
    for(var i = 0 ; i < prof.schedule.rules.length; i++) {
      var byday = prof.schedule.rules[i]["RRULE"]["BYDAY"];
      for (var j = 0; j < byday.length; j ++) {
        days.push(DIAS[byday[j]]);
      }
    }
  }

  var daysToShow = [];

  $.each(days, function (i, el) {
    if ($.inArray(el, daysToShow) === -1) daysToShow.push(el);
  });

  container.find('.prof-foto img').attr('src', imgSrc);
  container.find('.prof-nome').html(prof.nome);
  container.find('.curriculo-text').html(curriculoProcessado);
  container.find('.disciplina-text').html(prof.topicos.join(', '));

  container.find('.nivel-text').html(prof.anos.join(', '));
  container.find('.bairros-text').html(prof.bairros.join(', '));
  container.find('.full-stars').css('width', professor.avaliacao * 20 + '%');

  container.find('.dias-text').html(daysToShow.join(', '));

  fixImagem(container.find('.prof-foto img'));

  container.show();
}

function fixImagem(foto) {

  if (!(foto instanceof jQuery)) {
    foto = $(foto);
  }

  var width = foto[0].width;
  var height = foto[0].height;

  if (width < height) {
    foto.css('width', '100%');
    foto.css('height', 'auto');
    // $('.intro-container img').css('margin-right', (height - width) + 'px');
  }

  else if (width > height) {
    foto.css('width', 'auto');
    foto.css('height', '100%');
    // $('.intro-container img').css('margin-left', (height - width) + 'px');
  }

  else if (width == height) {
    foto.css('height', '100%');
    foto.css('width', '100%');
  }
}