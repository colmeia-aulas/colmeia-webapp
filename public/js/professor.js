$(function() {
  setup();
  setupForms();
  setupCta();
  setupFaq();
  setupSimulator();

  function setupCta() {
    $('.cta__button').on('click', function() {
      $('html, body').animate(
        {
          scrollTop: $('nav').offset().top
        },
        800
      );
    });
  }

  function setupFaq() {
    $('.faq__button')
      .first()
      .addClass('faq__button--active');

    $('.faq__block')
      .not(':first-child')
      .hide();

    $('.faq__button').on('click', function() {
      $('.faq__block').hide();
      $('.faq__button').removeClass('faq__button--active');
      $(this).addClass('faq__button--active');
      $('.' + $(this).data('target')).show();
    });
  }

  function setupSimulator() {
    calculateReceita();
    fillPreviousRadio();
    $('.simulador__checkbox').on('change', function() {
      calculateReceita();
    });

    $('input[name="hours"]').on('change', function() {
      calculateReceita();

      $('input[name="hours"]').each(function() {
        $(this)
          .parent()
          .removeClass('simulador__radio--active');
      });

      $(this)
        .parent()
        .addClass('simulador__radio--active');

      fillPreviousRadio();
    });
  }

  function calculateReceita() {
    var horas = $('input[name="hours"]:checked').val();
    var tipo = $('.simulador__checkbox').is(':checked') ? 30 : 45;

    var receita = Number(horas) * tipo * 4;
    var offset =
      $('input[name="hours"]:checked').offset().left -
      $('input[name="hours"]:checked')
        .parent()
        .parent()
        .offset().left;

    $('.simulador__value').html('R$' + receita.toFixed(2));
    $('.simulador__radio-container-bar').css('width', offset);
  }

  function fillPreviousRadio() {
    $('input[name="hours"]').each(function() {
      if (
        $(this)
          .parent()
          .hasClass('simulador__radio--active')
      ) {
        return false;
      } else {
        if (
          !$(this)
            .parent()
            .hasClass('simulador__radio--active')
        ) {
          $(this)
            .parent()
            .addClass('simulador__radio--active');
        }
      }
    });
  }

  function setup() {
    var maskBehavior = function(val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
      },
      options = {
        onKeyPress: function(val, e, field, options) {
          field.mask(maskBehavior.apply({}, arguments), options);
        }
      };

    $('#telefone').mask(maskBehavior, options);
  }

  function setupForms() {
    $('a.orange-link, .form__login-button').on('click', function(e) {
      e.preventDefault();

      if (!user || !user.id) {
        openLoginModal();
      }
    });

    $('#formLogin').on('submit', function(e) {
      e.preventDefault();

      var values = {
        email: $('#inputEmail')
          .val()
          .toLowerCase(),
        password: $('#inputPassword').val()
      };

      $('#loginMessage').html('');

      // ajax
      $.ajax({ url: '/aluno/entrar', type: 'POST', data: values })
        .done(function(response) {
          if (response.success) {
            $('#loginModal').modal('toggle');
            window.location = '/professor/cadastro';
          } else if (response.message) {
            alert(response.message);
          } else {
            $('#loginMessage').html('Login ou senha inválidos!');
          }
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
          alert('Houve um erro ao realizar seu login, tente novamente mais tarde.');
        });
    });

    $('#formSignin').on('submit', function(e) {
      e.preventDefault();
      if (
        $('#nome').val() == '' ||
        $('#email').val() == '' ||
        $('#telefone').cleanVal() == '' ||
        $('#senha').val() == '' ||
        $('#nome').val() == ''
      ) {
        alert('Preencha todos os campos para prosseguir!');
      } else if ($('#senha').val().length < 6) {
        alert('Sua senha deve ter no mínimo 6 caracteres!');
      } else if ($('#telefone').cleanVal().length < 10) {
        alert('Insira um número de telefone válido!');
      } else if (!$('#check-termos').is(':checked')) {
        alert('Você deve concordar com os Termos de Uso!');
      } else if (!isValidEmail($('#email').val())) {
        alert('Insira um email válido!');
      } else {
        var values = {
          nome: $('#nome').val(),
          email: $('#email')
            .val()
            .toLowerCase(),
          password: $('#senha').val(),
          telefone: '+55' + $('#telefone').cleanVal(),
          url: window.location.href,
          tipoCliente: 'PROFESSOR'
        };

        $.ajax({
          url: '/aluno/registrar',
          type: 'POST',
          data: values
        })
          .done(function(data) {
            if (data.success) {
              window.location = '/professor/cadastro';

              try {
                goog_report_conversion();

                woopra.identify({
                  email: values.email,
                  phone: values.telefone,
                  idParse: data.user.objectId
                });

                woopra.track('registroCadastroProfessor');
              } catch (e) {}
            } else if (data.message) {
              alert(data.message);
            }
          })
          .fail(function(jqXHR, textStatus, errorThrown) {
            alert(
              'Houve um erro ao realizar seu cadastro. Por favor, entre em contato conosco pelo WhatsApp (61) 9 9805-2073.'
            );
          });
      }
    });
  }

  function showLoginForm() {
    $('#loginModal .registerBox').fadeOut('fast', function() {
      $('.loginBox').fadeIn('fast');
      $('.register-footer').fadeOut('fast', function() {
        $('.login-footer').fadeIn('fast');
      });
      $('.modal-title').html('Login');
    });
    $('.error')
      .removeClass('alert alert-danger')
      .html('');
  }

  function openLoginModal() {
    showLoginForm();
    setTimeout(function() {
      $('#loginModal').modal('show');
    }, 230);
  }

  function isValidEmail(email) {
    for (var i = 0; i < email.length; i++) {
      if (email[i] == '@') return true;
    }
    return false;
  }
});
