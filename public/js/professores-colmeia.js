//@deprecated
var materiasMap = new Map();
materiasMap.set('Qualquer', 'Qualquer');
materiasMap.set('matematica', 'Matemática');
materiasMap.set('portugues', 'Português');
materiasMap.set('fisica', 'Física');
materiasMap.set('quimica', 'Química');
materiasMap.set('biologia', 'Biologia');
materiasMap.set('historia', 'História');
materiasMap.set('geografia', 'Geografia');
materiasMap.set('ingles', 'Inglês');

bairrosMap = new Map();
bairrosMap.set('Qualquer', 'Qualquer');
bairrosMap.set('asa-sul', 'Asa Sul');
bairrosMap.set('aguas-claras', 'Águas Claras');
bairrosMap.set('asa-norte', 'Asa Norte');
bairrosMap.set('candangolandia', 'Candangolândia');
bairrosMap.set('cruzeiro-novo', 'Cruzeiro Novo');
bairrosMap.set('granja-do-torto', 'Granja do Torto');
bairrosMap.set('guara', 'Guará');
bairrosMap.set('jardim-botanico', 'Jardim Botânico');
bairrosMap.set('lago-norte', 'Lago Norte');
bairrosMap.set('lago-sul', 'Lago Sul');
bairrosMap.set('noroeste', 'Noroeste');
bairrosMap.set('nucleo-bandeirante', 'Núcleo Bandeirante');
bairrosMap.set('octogonal', 'Octogonal');
bairrosMap.set('park-way', 'Park Way');
bairrosMap.set('setor-militar', 'Setor Militar');
bairrosMap.set('sobradinho', 'Sobradinho');
bairrosMap.set('sudoeste', 'Sudoeste');
bairrosMap.set('taguatinga', 'Taguatinga');
bairrosMap.set('taquari', 'Taquari');
bairrosMap.set('vicente-pires', 'Vicente Pires');
bairrosMap.set('grande-colorado', 'Grande Colorado');

var urlParams = decodeURI(window.location.href).split('/');
var materia_selected = '';
var bairro_selected = '';

$(function() {
    //se existem parametros
    if (urlParams.length > 4) {
        materia_selected = materiasMap.get(urlParams[4]);
        bairro_selected = bairrosMap.get(urlParams[6]);
        var title = 'Professores particulares';

        if (materia_selected == 'Qualquer' && bairro_selected == 'Qualquer') {
            title = title + ' em Brasília';
            $('.resultados-container h2').html('Professores de qualquer disciplina e em qualquer bairro');
        } else if (materia_selected == 'Qualquer') {
            title = title + ' no bairro ' + bairro_selected;
            $('.resultados-container h2').html('Professores de qualquer disciplina no bairro ' + bairro_selected);
        } else if (bairro_selected == 'Qualquer') {
            title = title + ' de ' + materia_selected;
            $('.resultados-container h2').html('Professores de ' + materia_selected + ' em qualquer bairro');
        } else {
            title = title + ' de ' + materia_selected;
            title = title + ' no bairro ' + bairro_selected;
            $('.resultados-container h2').html('Professores de ' + materia_selected + ' no bairro ' + bairro_selected);
        }
        title = title;
        document.title = title;
        $('meta[name=description]').remove();
        var description =
            title + '. Agende pelo aplicativo ou pelo site e tenha aulas com segurança no conforto da sua casa.';
        description = '<meta name="description" content="' + String(description) + '">';
        $('head').append(description);
    }

    setMateriasSelect();
    setBairrosSelect();

    showProfessores();

    $('.busca-container .colmeia-button').on('click', function() {
        $(this).html(
            "<img style='width: 30px; display: block; margin: 0px auto; margin-top: -4px;' src='/img/rolling.gif' />"
        );

        var materia = $('select#selectMateria')
            .val()
            .split(' ')
            .join('-');
        var bairro = $('select#selectBairro')
            .val()
            .split(' ')
            .join('-');

        var url = '/professores/' + materia + '/brasilia/' + bairro;
        window.location = url;
    });

    $('.professor-container').on('click', function() {
        window.open('/professores/' + $(this).attr('id'), '_blank');
    });
});

function setMateriasSelect() {
    for (var key in materiasMap) {
        const value = materiasMap[key];
        if (value == materia_selected) {
            $('select#selectMateria').append('<option selected value=' + key + '>' + value + '</option>');
        } else {
            $('select#selectMateria').append('<option value=' + key + '>' + value + '</option>');
        }
    }
}

function setBairrosSelect() {
    for (var key in bairrosMap) {
        const value = bairrosMap[key];
        if (value == bairro_selected) {
            $('select#selectBairro').append('<option selected value=' + key + '>' + value + '</option>');
        } else {
            $('select#selectBairro').append('<option value=' + key + '>' + value + '</option>');
        }
    }
}

function showProfessores() {
    console.log(professores);

    if (professores.length == 0) {
        $('.professores-encontrados').append(
            "\
    <div class='nao-encontrado'>\
      <img src='/img/Sad-100.png' alt=''>\
      <p>Desculpe, mas não encontramos professores para os parâmetros escolhidos.</p>\
    </div>\
    "
        );
    }

    for (var i = 0; i < professores.length; i++) {
        var name = professores[i].nome;
        var nota = professores[i].avaliacao;
        var materia;
        var bairros;

        if (professores[i].bairros.length > 6) {
            bairros = professores[i].bairros.splice(0, 6).join(', ') + ', ...';
        } else {
            bairros = professores[i].bairros.join(', ');
        }

        if (professores[i].topicos.length > 6) {
            materias = professores[i].topicos.splice(0, 6).join(', ') + ', ...';
        } else {
            materias = professores[i].topicos.join(', ');
        }

        var img = professores[i].image ? professores[i].image.url : '/img/profile_placeholder.png';

        $('.professores-encontrados').append(
            "\
    <div class='professor-container' id=" +
                professores[i].name_id +
                ">\
      <div class='professor-foto'>\
        <img src='" +
                img +
                "' alt='Foto professor'>\
      </div>\
      <div class='professor-info'>\
        <div class='professor-nome'>\
        " +
                name +
                "\
        </div>\
        <div class='professor-nota'>\
          <div class='empty-stars'></div>\
          <div class='full-stars' style='width:" +
                nota * 10 * 2 +
                "%'></div>\
        </div>\
        <div class='professor-materias'>\
          <i style='margin-right: 10px' class='fa fa-book'></i>" +
                materias +
                "\
        </div>\
        <div class='professor-bairros'>\
        <i style='margin-right: 10px' class='fa fa-map-marker'></i>" +
                bairros +
                '\
        </div>\
      </div>\
    </div>\
    <hr>'
        );
    }
}
