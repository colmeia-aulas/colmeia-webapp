var selectedProf;
var isProfSelected = false;

var PRECO_BSB = precos['Brasília'];
var PRECO_SP = precos['São Paulo'];

var daysHash = {
  0: 'SU',
  1: 'MO',
  2: 'TU',
  3: 'WE',
  4: 'TH',
  5: 'FR',
  6: 'SA'
};

var professor;
var schedule;
var rules;
var busy;
var busy_class;
var free_class;
var busyClassInterval;

var _scheduleDuration = 3; // $('#select-duracao').val() * 2
var _initialDate = moment();
var _currentDate = moment();
var _chosenHour = '';

var _currentDayOfYear = _initialDate.dayOfYear();
var _currenYear = _initialDate.year();
var _currentHour = _initialDate.format('HH:mm');

$(window).load(function() {
  $('.professor-container .professor-image img').each(function() {
    const img = $(this);
    const url = img.data('src');

    if (url && url.indexOf('placeholder') === -1) {
      $.ajax({
        url: '/resize-image',
        type: 'POST',
        data: { url: url }
      })
        .done(function(res) {
          var src;
          if (res.buffer) {
            src = 'data:image/png;base64,' + res.buffer;
          }
          img.attr('src', src);
          fixImagem(img);
          img.fadeIn();
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
          console.log(textStatus, errorThrown);
        });
    } else {
      src = '/img/profile_placeholder.png';
      img.attr('src', src);
      fixImagem(img);
      img.fadeIn();
    }
  });
});

$(function() {
  var url = window.location.pathname;
  var split_url = url.split('/');

  var chamada = split_url[1];
  var materia = split_url[2];
  var cidade = split_url[3];
  var bairro = split_url[4];
  var ano = split_url[5];

  var params = {
    materia_selected: MATERIAS_ALL[materia],
    bairro_selected: BAIRROS[cidade][bairro],
    cidade_selected: CIDADES[cidade],
    ano_selected: ANOS[ano]
  };

  setupProfCards(params);

  handleCaixaAgendamento();
  carregarSelects();
  updateButton();
  setupCalendar();
  handleMobileDetaches();
  handleFilter(cidade);
  defaultSelectValues();

  //BUTTONS
  $('#action-button').on('click', function() {
    if (isProfSelected) {
      enviarDados();
    }
  });

  //BUTTONS
  $('#mobile-action-button').on('click', function() {
    enviarDados();
  });

  //toggle mobile
  $('.perfil-toggle').on('click', function() {
    $('.perfil-container').toggle();
  });

  $('.professor-voltar').click(function() {
    $('.professor-selecionado').fadeOut(300);
    $('.professores').fadeIn(300);
    $('#data-e-horario').fadeOut(200);
    $('.caixa-busca').fadeOut(200);
    isProfSelected = false;
    selectedProf = null;
    updateButton();
    carregarSelects();
  });

  moment()
    .tz('America/Sao_Paulo')
    .format();
  // generatePossibleSlots(3, moment()); //today, 1h30
});

//cria um card para cada proffessor
function setupProfCards(params) {
  $('.professor-card').each(function(i) {
    var prof = $(this);
    var id = prof.attr('id');
    var curriculo = prof.data('curriculo');
    var img = prof.data('original-image');

    var anos = prof.data('anos');

    var anosLinguas = prof.data('anos-linguas');

    var topicos = prof.data('topicos');
    var bairros = prof.data('bairros');

    var horarios = prof.data('horarios');
    var profSchedule = prof.data('schedule');

    var profBusyClassInterval = prof.data('bc-interval');

    var imgAlt = prof.find('.professor-image img').attr('alt');
    var nome = prof.find('.professor-name').html();
    var starStyle = prof.find('.professor-nota .full-stars').attr('style');

    var avaliacao = prof.data('avaliacao');
    var profId = prof.data('id');
    var profUserId = prof.data('user-id');

    var profData = {
      bairros: bairros,
      anos: anos,
      nivelLinguas: anosLinguas,
      topicos: topicos
    };

    // $('#' + id)

    prof.find('.ver-mais').on('click', function() {
      prof.find('.professor-escolher-button').click();
    });

    // $('#' + id)
    $(this)
      .find('.professor-escolher-button')
      .click(function() {
        var container = mobilecheck()
          ? $('.professor-selecionado-mobile')
          : $('.professor-selecionado');

        if (mobilecheck()) {
          $('.professor-selecionado-mobile')
            .find('.perfil-container')
            .html('');
          $('.professor-selecionado-mobile')
            .find('.perfil-container')
            .html($('.professor-selecionado-info').clone());
        }

        var nums = [];
        var days = [];

        for (var i = 0; i < horarios.length; i++) {
          nums.push(horarios[i][0]);
        }

        $('.professor-dias .professor-dia').each(function() {
          $(this).removeClass('professor-dia-active');
        });

        schedule = profSchedule;
        rules = reduceRuleByDay(schedule.rules === undefined ? [] : schedule.rules);
        busy = schedule.busy;
        busy_class = schedule.busy_class;
        free_class = schedule.free;
        busyClassInterval = profBusyClassInterval;

        console.log(schedule)

        if (rules['SU'].length > 0) $('.professor-dias #domingo').addClass('professor-dia-active');
        if (rules['MO'].length > 0) $('.professor-dias #segunda').addClass('professor-dia-active');
        if (rules['TU'].length > 0) $('.professor-dias #terca').addClass('professor-dia-active');
        if (rules['WE'].length > 0) $('.professor-dias #quarta').addClass('professor-dia-active');
        if (rules['TH'].length > 0) $('.professor-dias #quinta').addClass('professor-dia-active');
        if (rules['FR'].length > 0) $('.professor-dias #sexta').addClass('professor-dia-active');
        if (rules['SA'].length > 0) $('.professor-dias #sabado').addClass('professor-dia-active');

        container.find('.psh-image img').attr('src', img);
        container.find('.pscm-img img').attr('src', img);

        container.find('.psh-image img').attr('alt', imgAlt);
        container.find('.pscm-img img').attr('alt', imgAlt);

        container.find('.psh-name').html(nome);
        container.find('.pscm-name').html(nome);

        container.find('.professor-selecionado-info .professor-curriculo').html(curriculo);

        container.find('.psh-disciplinas').html('');

        topicos.forEach(function(e) {
          container.find('.psh-disciplinas').append('<div class="psh-disciplina">' + e + '</div>');
        });

        container.find('.professor-niveis').html('');

        anos.forEach(function(e) {
          container
            .find('.professor-niveis')
            .append('<div class="professor-nivel">' + e + '</div>');
        });

        container.find('.professor-bairros').html(bairros.join(', '));

        container.find('.psh-rating-value').html(avaliacao.toFixed(2));
        container.find('.pscm-rating-value').html(avaliacao.toFixed(2));

        $('.professor-selecionado-avaliacao').html();
        (prof.avaliacoes || []).forEach(function(e) {
          $('.professor-selecionado-avaliacao').append(
            '\
                    <div class="professor-avaliacao">\
                        <div class="psa-left">\
                                <div class="psa-image"><img src="' +
              (e.img ? e.img : '/img/profile_placeholder.png') +
              '" alt="Aulas Colmeia reforço escolar professor foto"></div>\
                        </div>\
                        <div class="psa-middle">\
                            <div class="psa-author">\
                                ' +
              e.nome +
              ' \
                            </div>\
                            <div class="psa-avaliacao">\
                            ' +
              e.conteudo +
              ' \
                            </div>\
                            <div class="psa-info">\
                                Teve aulas de ' +
              e.materia +
              ' com ' +
              prof.nome.split(' ')[0] +
              '\
                            </div>\
                        </div>\
                        <div class="psa-right">\
                            <div class="psa-rating">\
                                <div class="psa-rating-value">' +
              e.nota.toFixed(2) +
              '</div>\
                                <img class="psa-rating-img" src="/img/rating-star.png" alt="Avaliação professor Aulas Colmeia">\
                            </div>\
                            <div class="psa-date">\
                                ' +
              e.data +
              '\
                            </div>\
                        </div>\
                    </div>'
          );
        });

        isProfSelected = true;
        selectedProf = {
          nome: nome,
          img: img,
          avaliacao: avaliacao,
          bairros: bairros,
          id: profId,
          userId: profUserId
        };
        updateButton();
        updateSelects(profData);

        $('#input-horario')
          .empty()
          .append('<option value="" disabled selected>Selecione horário</option>');
        $('#horario-erro').hide();

        atualizarPreco();
        updateView();

        if (mobilecheck()) {
          //mobile perfil
          $('.professor-voltar')
            .detach()
            .prependTo('.professor-selecionado-mobile');

          $('.mobile-controller').hide();
          $('.caixa-busca').hide();
          $('.professores').hide();

          $('.professor-selecionado-mobile').fadeIn(300, function() {
            $('html, body').animate(
              {
                scrollTop: $('.professor-voltar').offset().top - 80
              },
              300
            );
          });

          $('.professor-voltar').unbind();

          $('.professor-voltar').click(function() {
            $('.professor-selecionado-mobile').hide();
            $('#data-e-horario').hide();
            $('.horario-mobile').hide();
            $('.perfil-container').hide();

            $('.mobile-controller').show();
            $('.professores').show();

            isProfSelected = false;
            selectedProf = null;

            updateButton();
            updateSelects(profData);
          });

          $('.horario-mobile')
            .show()
            .children()
            .show();
        } else {
          $('.professores').fadeOut(300);
          $('#data-e-horario').fadeIn(200);
          $('.caixa-busca').fadeIn(200);
          $('.professor-selecionado').fadeIn(300, function() {
            $('html, body').animate(
              {
                scrollTop: $('.professor-voltar').offset().top
              },
              400
            );
          });
        }

        try {
          woopra.identify({
            email: user.email,
            name: user.nome,
            phone: user.phone,
            idparse: user.id
          });

          woopra.track('select_teacher', {
            professor: profId,
            avaliacao: String(avaliacao),
            bairro: $('#select-bairro option:selected').html(),
            materia: $('#select-materia option:selected').html(),
            cidade: $('#select-cidade option:selected').html()
          });
        } catch (e) {}
      });
  });
}

function handleCaixaAgendamento() {
  if (mobilecheck()) {
    $('.caixa-busca').hide();
    $('.mobile-controller').show();

    $('.mobile-toggle').on('click', function() {
      $('.caixa-busca').toggle();
    });
    $('#action-button').html('Buscar professores');
  }
}

function fixImagem(foto) {
  if (!(foto instanceof jQuery)) {
    foto = $(foto);
  }

  var width = foto[0].width;
  var height = foto[0].height;

  if (width < height) {
    foto.css('width', '100%');
    foto.css('height', 'auto');
    // $('.intro-container img').css('margin-right', (height - width) + 'px');
  } else if (width > height) {
    foto.css('width', 'auto');
    foto.css('height', '100%');
    // $('.intro-container img').css('margin-left', (height - width) + 'px');
  } else if (width == height) {
    foto.css('height', '100%');
    foto.css('width', '100%');
  }
}

function carregarSelects() {
  // carregar cidades
  $('#select-cidade').empty();

  Object.keys(CIDADES).forEach(function(key, index) {
    $('#select-cidade').append("<option value='" + key + "'>" + CIDADES[key] + '</option>');
  });

  $('#select-cidade').prop('disabled', false);

  $('#select-cidade').unbind();
  $('#select-materia').unbind();

  defaultSelectValues();

  //carregar bairros
  var cid = $('#select-cidade').val();
  carregarBairros(BAIRROS[cid]);

  carregarMaterias(MATERIAS_ALL);

  carregarNiveis(NIVEIS);

  carregarDuracao();

  $('#select-cidade').change(function() {
    var cid = $(this).val();
    carregarBairros(BAIRROS[cid]);
  });

  $('body').on('change', '#select-duracao', function() {
    _scheduleDuration = $(this).val() * 2;
    atualizarPreco();

    if (isProfSelected) {
      updateView();
    }
  });

  $('#datetimepicker').on('dp.change', function(e) {
    _currentDate = e.date;
    updateView();
  });

  $('#input-horario').change(function() {
    _chosenHour = $(this).val();
  });

  defaultSelectValues();
  atualizarPreco();
}

function carregarBairros(bairros) {
  $('#select-bairro').empty();

  Object.keys(bairros).forEach(function(key, index) {
    $('#select-bairro').append('<option value=' + key + '>' + bairros[key] + '</option>');
  });
}

function carregarMaterias(materias) {
  $('#select-materia').empty();

  Object.keys(materias).forEach(function(key, index) {
    $('#select-materia').append('<option value=' + key + '>' + materias[key] + '</option>');
  });
}

function carregarNiveis(niveis) {
  $('#select-nivel').prop('disabled', true);
  $('#select-nivel').empty();

  Object.values(niveis).forEach(function(val, index) {
    $('#select-nivel').append('<option value="' + val + '">' + val + '</option>');
  });
}

function carregarDuracao() {
  $('#select-duracao').empty();

  // $('#select-duracao').append("<option value='1'>1h</option>");
  $('#select-duracao').append("<option value='1.5'>1h30m</option>");
  $('#select-duracao').append("<option value='2'>2h</option>");
  $('#select-duracao').append("<option value='3'>3h</option>");

  // //comeca com 1h30
  $('#select-duracao')
    .val('1.5')
    .trigger('change');
  atualizarPreco();
}

function defaultSelectValues() {
  var url = window.location.pathname;
  var split_url = url.split('/');

  var materia = split_url[2];
  var cidade = split_url[3];
  var bairro = split_url[4];
  var ano = split_url[5];

  $('#select-materia option[value=' + materia + ']').attr('selected', 'selected');
  $('#inputMateria option[value=' + materia + ']').attr('selected', 'selected');

  $('#select-cidade option[value=' + cidade + ']').attr('selected', 'selected');
  $('#inputCidade option[value=' + cidade + ']').attr('selected', 'selected');

  $('#select-bairro option[value=' + bairro + ']').attr('selected', 'selected');
  $('#inputBairro')
    .val(bairro)
    .trigger('change');

  $('#select-nivel option[value="' + ANOS_NIVEIS[ano] + '"]').attr('selected', 'selected');
  $('#inputAno option[value=' + ano + ']').attr('selected', 'selected');
}

function atualizarPreco() {
  var precoHora;
  var dur = _scheduleDuration / 2;

  if ($('#select-cidade').val() === 'brasilia') {
    precoHora = PRECO_BSB;
  } else if ($('#select-cidade').val() === 'sao-paulo') {
    precoHora = PRECO_SP;
  } else {
    return alert('A cidade selecionada é inválida.');
  }

  var novoPreco = 'R$' + (precoHora * parseFloat(dur)).toFixed(2);
  $('.preco-aula').html(novoPreco);
}

function handleMobileDetaches() {
  if (mobilecheck()) {
    var first = $('#data-e-horario');

    first
      .next('.row')
      .clone(true)
      .prependTo('.horario-mobile'); //duracao
    first.detach().prependTo('.horario-mobile'); //data e horario

    $('.horario-mobile').prepend('<div class="horario-title">Escolher horário</div>');

    $('.horario-mobile').append(
      "<div class='row'><div class='col-lg-12 col-md-12'><div class='colmeia-button button-filled' id='mobile-action-button'>Agendar aula</div></div></div>"
    ); // botao
  }
}

function updateButton() {
  // var button = $('#action-button');
  // if (isProfSelected) {
  //     button.html('Agendar aula');
  //     button.addClass('button-filled');
  // } else {
  //     button.html('Buscar professores');
  //     button.removeClass('button-filled');
  // }
}

function updateSelects(prof) {
  if (isProfSelected) {
    var cidade = $('#select-cidade').val();

    //copy objects
    var bairros = BAIRROS[cidade];
    var materias = MATERIAS_ALL;
    // var niveisEscolar = NIVEIS['escolar'];
    // var niveisLinguas = NIVEIS['linguas'];

    var bairrosPossiveis = {};
    var materiasPossiveis = {};
    // var niveisEscolarPossiveis = {};
    // var niveisLinguasPossiveis = {};

    //remove bairros que o professor nao atende
    Object.keys(bairros).forEach(function(key, index) {
      if ($.inArray(bairros[key], prof.bairros) != -1) {
        bairrosPossiveis[key] = bairros[key];
      }
    });

    //remove materias que o professor nao atende
    Object.keys(materias).forEach(function(key, index) {
      if ($.inArray(materias[key], prof.topicos) != -1) {
        materiasPossiveis[key] = materias[key];
      }
    });

    // //remove niveis que o professor nao atende
    // Object.keys(niveisEscolar).forEach(function(key, index) {
    //   if ($.inArray(niveisEscolar[key], prof.anos) != -1) {
    //     niveisEscolarPossiveis[key] = niveisEscolar[key];
    //   }
    // });

    // Object.keys(niveisLinguas).forEach(function(key, index) {
    //   if ($.inArray(niveisLinguas[key], prof.nivelLinguas) != -1) {
    //     niveisLinguasPossiveis[key] = niveisLinguas[key];
    //   }
    // });

    // var niveisPossiveis =
    //   $('#select-materia').val() == 'ingles' ? niveisLinguasPossiveis : niveisEscolarPossiveis;

    $('#select-cidade').prop('disabled', true);

    $('#select-cidade').unbind();
    $('#select-materia').unbind();

    carregarBairros(bairrosPossiveis);
    carregarMaterias(materiasPossiveis);
    // carregarNiveis(niveisPossiveis);

    // $('#select-materia').change(function() {
    //   var niveisPossiveis =
    //     $('#select-materia').val() == 'ingles' ? niveisLinguasPossiveis : niveisEscolarPossiveis;
    //   carregarNiveis(niveisPossiveis);
    //   atualizarPreco();
    // });
    defaultSelectValues();
  } else {
    carregarSelects();
  }
}

function setupCalendar() {
  $('#datetimepicker').datetimepicker({
    inline: false,
    locale: 'pt-br',
    format: 'DD/MM/YYYY',
    minDate: new Date(),
    ignoreReadonly: true
  });

  // $('#input-data').val($('#datetimepicker').data("DateTimePicker").date().format('DD/MM/YYYY'));

  //data inicial para o campo nao ficar em branco
  $('#input-data').val(moment().format('DD/MM/YYYY'));

  $('#input-data').click(function() {
    $('#datetimepicker')
      .data('DateTimePicker')
      .toggle();
  });
}

////////////////////////////==HORARIOS==////////////////////////////////////////

function enviarDados() {
  //falta validar dados
  var correctDuration = _scheduleDuration / 2,
    minimumDate = _initialDate.clone().add(4, 'h'),
    minimumHour = minimumDate.format('HH:mm'),
    selectedDayOfYear = _currentDate.dayOfYear(),
    selectedYear = _currentDate.year();

  // var cidade = $('#select-cidade').val();
  var cidade = $('#select-cidade option:selected').html();
  var bairro = $('#select-bairro').val();
  var topico = $('#select-materia').val();
  var ano = window.location.pathname.split('/')[5];

  var tipo = topico == 'ingles' ? 'Línguas' : 'Escolar';

  if ($('#input-data').val() == '' || $('#input-data').val() == undefined) {
    alert('Você deve escolher um dia!');
    return;
  }
  if ($('#input-horario').val() == '' || $('#input-horario').val() == undefined) {
    alert('Você deve escolher um horário!');
    return;
  }
  if (
    minimumHour > _chosenHour &&
    _currentDayOfYear === selectedDayOfYear &&
    _currenYear === selectedYear
  ) {
    alert('A aula não pode ser agendada pois faltam menos de 4 horas para ela acontecer.');
    return;
  }
  var dados = {
    dia: _currentDate.format('DD/MM/YYYY'),
    hora: _chosenHour,
    duration: correctDuration,
    topico: topico,
    ano: ano,
    bairro: bairro,
    cidade: cidade,
    tipo: tipo,
    professor: selectedProf,
    url: window.location.pathname
  };

  $.ajax({
    url: '/reforco-escolar/horarios/escolher',
    type: 'POST',
    data: dados
  })
    .done(function() {
      try {
        woopra.identify({
          email: user.email,
          name: user.nome,
          phone: user.phone,
          idparse: user.id
        });

        woopra.track('progresso', {
          stepName: 'PROFESSORES',
          step: 2
        });

        woopra.track('progresso', {
          stepName: 'HORÁRIOS',
          step: 3
        });
      } catch (e) {}

      window.location = '/reforco-escolar/resumo' + window.location.search;
    })
    .fail(function(jqXHR, textStatus, errorThrown) {
      console.log('Erro!');
      console.log(textStatus, errorThrown);
    });
}

function handleFilter(c) {
  $('#click-date').on('click', function() {
    $('.input-group-addon').css('background-color', 'rgba(255, 128, 8, 0.8)');
  });

  $('.input-group-addon').on('click', function() {
    $('.input-group-addon').css('background-color', 'rgba(245, 245, 245, 0.85)');
  });

  $('#inputBairro').select2({
    minimumResultsForSearch: -1
  });

  $('b[role="presentation"]').hide();

  createCidadesOptions();
  createBairrosOptions(c);
  createMateriasOptions();
  createAnosOptions();

  ///////INDEX LOGIC////////
  $('#inputCidade').change(function() {
    createBairrosOptions($(this).val());
  });

  $('#send').on('click', function() {
    if ($('#inputBairro').val() == null) {
      $('#modal-agendamento-erro .modal-body').html('Escolha um bairro para prosseguir!');
      $('#modal-agendamento-erro').modal('show');
      $('#modal-agendamento-erro .colmeia-button').on('click', function() {
        $('#modal-agendamento-erro').modal('hide');
      });
      return false;
    }

    if ($('#inputMateria').val() == null) {
      $('#modal-agendamento-erro .modal-body').html('Escolha uma matéria para prosseguir!');
      $('#modal-agendamento-erro').modal('show');
      $('#modal-agendamento-erro .colmeia-button').on('click', function() {
        $('#modal-agendamento-erro').modal('hide');
      });
      return false;
    }

    if ($('#inputAno').val() == null) {
      $('#modal-agendamento-erro .modal-body').html('Escolha o ano para prosseguir!');
      $('#modal-agendamento-erro').modal('show');
      $('#modal-agendamento-erro .colmeia-button').on('click', function() {
        $('#modal-agendamento-erro').modal('hide');
      });
      return false;
    }

    var cidade = $('#inputCidade').val(),
      materia = $('#inputMateria').val(),
      bairro = $('#inputBairro').val(),
      ano = $('#inputAno').val();

    $.LoadingOverlay('show');

    try {
      woopra.identify({
        email: user.email,
        name: user.nome,
        phone: user.phone,
        idparse: user.id
      });

      woopra.track('progresso', {
        stepName: 'AGENDAMENTO',
        step: 1
      });
    } catch (e) {}

    window.location =
      '/reforco-escolar/' +
      materia +
      '/' +
      cidade +
      '/' +
      bairro +
      '/' +
      ano +
      window.location.search;
  });
}

function createCidadesOptions() {
  var cidades = CONST.CIDADES;
  var html = '';

  Object.keys(cidades).forEach(function(key, index) {
    html += "<option value='" + key + "'>" + cidades[key] + '</option>';
  });

  $('#inputCidade').html(html);
}

function createBairrosOptions(cidade) {
  // Array de matérias
  var bairros = CONST.BAIRROS[cidade];

  var html = '<option disabled selected value> Bairro </option>';

  Object.keys(bairros).forEach(function(key, index) {
    html += "<option value='" + key + "'>" + bairros[key] + '</option>';
  });

  // Seta o comportamento inicial
  $('#inputBairro').html(html);
}

// Cria o codigo html para as opções de materias no select
function createMateriasOptions() {
  var materiasHtml = '<option disabled selected value> Matéria </option>';
  var materias = CONST.MATERIAS_ALL;

  Object.keys(materias).forEach(function(key, index) {
    materiasHtml += "<option value='" + key + "'>" + materias[key] + '</option>';
  });

  $('#inputMateria').html(materiasHtml);
}

function createAnosOptions() {
  var anosHtml = '<option disabled value> Ano </option>';

  Object.keys(CONST.ANOS).forEach(function(key, index) {
    anosHtml += "<option value='" + key + "'>" + CONST.ANOS[key] + '</option>';
  });

  $('#inputAno').html(anosHtml);
}

function updateView() {
  $('[id=horario-erro]').hide();
  // _chosenHour = "";

  var horarios = generatePossibleSlots(_scheduleDuration, _currentDate);

  var splitedHorarios = { Manhã: [], Tarde: [], Noite: [] };

  for (var i = 0; i < horarios.length; i++) {
    var horario = horarios[i];
    splitedHorarios[getPeriod(horario)].push(horario);
  }

  if (horarios.length === 0) {
    $('[id=input-horario]')
      .empty()
      .append('<option value="" disabled selected>Sem horários</option>');
    $('[id=input-horario]').prop('disabled', true);

    $('[id=horario-erro]').show();
  } else {
    $('[id=input-horario]')
      .empty()
      .append('<option value="" disabled selected>Selecione horário</option>');
    $('[id=input-horario]').prop('disabled', false);

    if (splitedHorarios['Manhã'].length > 0) {
      generatePeriodList('Manhã', splitedHorarios['Manhã']);
    }
    if (splitedHorarios['Tarde'].length > 0) {
      generatePeriodList('Tarde', splitedHorarios['Tarde']);
    }
    if (splitedHorarios['Noite'].length > 0) {
      generatePeriodList('Noite', splitedHorarios['Noite']);
    }
  }
}

function reduceRuleByDay(rules) {
  var result = { SU: [], MO: [], TU: [], WE: [], TH: [], FR: [], SA: [] };
  for (var i = 0; i < rules.length; i++) {
    const rule = rules[i],
      rrule = rule.RRULE,
      days = rrule.BYDAY,
      duration = rrule.DURATION,
      date =
        rule.DTSTART.iso === undefined
          ? rule.DTSTART + '/' + duration
          : rule.DTSTART.iso + '/' + duration;
    for (var j = 0; j < days.length; j++) {
      const day = days[j];
      result[day].push(date);
    }
  }
  return result;
}

function generateRuleSlots(duration, date) {
  const selectedDayOfYear = moment(date).dayOfYear(),
    selectedYear = moment(date).year(),
    day = daysHash[moment(date).day()],
    rulesArray = rules[day];
  var result = [];
  if (_currentDayOfYear <= selectedDayOfYear && _currenYear <= selectedYear) {
    for (var i = 0; i < rulesArray.length; i++) {
      const rule = rulesArray[i],
        initialTime = moment(rule.split('/')[0]),
        ruleDuration = moment.duration(rule.split('/')[1]);
      while (ruleDuration.asMinutes() >= duration * 30) {
        const selectedHour = initialTime.tz('America/Sao_Paulo').format('HH:mm');
        if (selectedHour >= _currentHour || selectedDayOfYear != _currentDayOfYear) {
          result.push(initialTime.tz('America/Sao_Paulo').format('HH:mm'));
        }
        initialTime.add(30, 'm');
        ruleDuration.subtract(30, 'm');
      }
    }
  }
  return result.sort();
}

function generateBusySlots(slotArray, duracao, date) {
  var result = [];

  if (slotArray !== undefined) {
    const dayOfYear = moment(date).dayOfYear();

    for (var i = 0; i < slotArray.length; i++) {
      const currentSlot = slotArray[i],
        splitString = currentSlot.split('/'),
        currentDate = moment(splitString[0]),
        currentDuration = moment.duration(splitString[1]);

      if (currentDate.dayOfYear() === dayOfYear) {
        // currentDate.subtract((30 * duracao) - 30, 'm');
        // currentDuration.add(30 * duracao, 'm');

        while (currentDuration.asMinutes() > 0) {
          result.push(currentDate.tz('America/Sao_Paulo').format('HH:mm'));
          currentDate.add(30, 'm');
          currentDuration.subtract(30, 'm');
        }
      }
    }
  }
  return result.sort();
}

function generateBusyClassSlots(slotArray, duracao, date) {
  var interval = busyClassInterval || 1;
  var result = [];

  if (slotArray !== undefined) {
    const dayOfYear = moment(date).dayOfYear();

    for (var i = 0; i < slotArray.length; i++) {
      const currentSlot = slotArray[i],
        splitString = currentSlot.split('/'),
        currentDate = moment(splitString[0]),
        currentDuration = moment.duration(splitString[1]);

      if (currentDate.dayOfYear() === dayOfYear) {
        currentDate.subtract(30 * interval * duracao, 'm');
        currentDuration.add(30 * interval * duracao, 'm');

        while (currentDuration.asMinutes() > 0) {
          result.push(currentDate.tz('America/Sao_Paulo').format('HH:mm'));
          currentDate.add(30, 'm');
          currentDuration.subtract(30, 'm');
        }
      }
    }
  }
  return result.sort();
}

function generateFreeSlots(slotArray, duracao, date) {
  var result = [];

  if (slotArray !== undefined) {
    const dayOfYear = moment(date).dayOfYear();

    for (var i = 0; i < slotArray.length; i++) {
      const currentSlot = slotArray[i],
        splitString = currentSlot.split('/'),
        currentDate = moment(splitString[0]),
        currentDuration = moment.duration(splitString[1]);

      if (currentDate.dayOfYear() === dayOfYear) {
        while (currentDuration.asMinutes() > 0) {
          result.push(currentDate.tz('America/Sao_Paulo').format('HH:mm'));
          currentDate.add(30, 'm');
          currentDuration.subtract(30, 'm');
        }
      }
    }
  }
  return result.sort();
}

function generatePossibleSlots(duration, date) {
  var slots = generateRuleSlots(duration, date),
    freeSlots = generateFreeSlots(free_class, duration, date),
    busySlots = generateBusySlots(busy, duration, date),
    busyClassSlots = generateBusyClassSlots(busy_class, duration, date);

  var allowedSlots = concatenateArrays(slots, freeSlots);
  var notAllowedSlots = concatenateArrays(busySlots, busyClassSlots);

  return allowedSlots.filter(function(item, pos) {
    return $.inArray(item, notAllowedSlots) === -1;
  });
}

function getPeriod(hour) {
  if (hour < '12:00') {
    return 'Manhã';
  } else if (hour > '18:00') {
    return 'Noite';
  }
  return 'Tarde';
}

function generatePeriodList(period, horarios) {
  $('[id=input-horario]').append('<optgroup label="' + period + '"></optgroup>');

  for (var i = 0; i < horarios.length; i++) {
    const horario = horarios[i];

    $('[id=input-horario]')
      .find('optgroup[label=' + period + ']')
      .append('<option value="' + horario + '">' + horario + '</option>');
  }
}

function concatenateArrays(a, b) {
  //junta arrays e remove repeticoes
  var c = a.concat(
    b.filter(function(item) {
      return a.indexOf(item) < 0;
    })
  );

  return c.sort();
}
