$(function() {
  $('#success-container a').on('click', function() {
    $('#login-email').val('');
    $('#success-container').slideUp(function() {
      $('#email-container').slideDown();
    });
  });

  $('.login-button').on('click', function() {
    var email = $('#login-email').val();
    if (email) {
      $.ajax({
        url: '/aluno/recuperar-senha',
        type: 'POST',
        data: { email: email }
      })
        .done(function(data) {
          if (data.success) {
            $('#email-container').slideUp(function() {
              $('#success-container').slideDown();
            });
          } else if (data.message) {
            alert(data.message);
          }
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
          alert(
            'Houve um erro ao solicitar a redefinição da sua senha, tente novamente mais tarde.'
          );
        });
    }
  });
});
