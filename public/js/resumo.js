$(function() {
  var allCards;
  var precoTotal = 0;
  var oldPrice = dados.discountCode ? '' : dados.preco;

  $(window).load(function() {
    //fix todas as imagens apos elas serem carregadas
    $('.prof-foto img').each(function() {
      fixImagem($(this));
      $(this).fadeIn();
    });
  });

  try {
    woopra.identify({
      email: user.email,
      name: user.nome,
      phone: user.phone,
      idparse: user.id
    });

    woopra.track('progresso', {
      stepName: 'RESUMO',
      step: 4
    });
  } catch (e) {}

  moment()
    .tz('America/Sao_Paulo')
    .format();

  $('#file-obs').filestyle();

  $('.professor-voltar').on('click', function() {
    var url = dados.agendamentoUrl !== '' ? dados.agendamentoUrl : '/';
    window.location.href = url + window.location.search;
  });

  $('.codigo-container-toggle').on('click', function() {
    $(this).slideUp(300, function() {
      $('.codigo-content').slideDown(300);
    });
  });

  setupModal();

  moverCaixaAgendamento();
  setupCaixaAgendamento();

  carregarEnderecos();
  carregarCartoes();

  handleNovoEndereco();
  handleNovoCartao();

  validarCodigo();

  handleEscolaAno();

  marcarAula();

  function setupCaixaAgendamento() {
    var professor = dados.professor;
    var dia = moment(dados.dia, 'DD/MM/YYYY HH:mm');
    var duracao = dados.duracao; //minutos
    var diaString =
      '' + moment.weekdays(dia.day()) + ', ' + dia.date() + ' de ' + moment.months()[dia.month()];
    var duracaoString =
      '' +
      dia.format('HH:mm') +
      ' - ' +
      dia
        .clone()
        .add(duracao, 'hours')
        .format('HH:mm');

    $('.prof-nome, .mcr-name').html(professor.nome);

    $('.prof-materia').html(dados.topico);

    $('.prof-foto img, .mcr-img img').attr('src', dados.professor.img);

    $('.prof-rating .prof-rating-value').html(professor.avaliacao);

    $('.data-aula, .mcr-day').html(diaString);
    $('.horario-aula, .mcr-hour').html(duracaoString);
    $('.preco-aula').html('R$' + (dados.preco * dados.duracao).toFixed(2));

    var duracaoAula = '';
    if (dados.duracao == 0.5) {
      duracaoAula = '30 minutos de aula';
    } else if (dados.duracao == 1) {
      duracaoAula = '1 hora de aula';
    } else if (dados.duracao == 1.5) {
      duracaoAula = '1 hora e 30 minutos de aula';
    } else if (dados.duracao == 2) {
      duracaoAula = '2 horas de aula';
    } else if (dados.duracao == 2.5) {
      duracaoAula = '2 horas e 30 minutos de aula';
    } else if (dados.duracao == 3) {
      duracaoAula = '3 horas de aula';
    }

    $('.duracao-aula, .mcr-duration').html(duracaoAula);
  }

  function carregarEnderecos() {
    $('#enderecos-list').empty();

    var id = user.id;

    $.ajax({
      url: '/reforco-escolar/endereco/buscar',
      type: 'POST',
      data: { id: id }
    })
      .done(function(results) {
        if (results.success) {
          for (var i = 0; i < results.ends.length; i++) {
            if ($.inArray(results.ends[i].bairro, dados.professor.bairros) !== -1) {
              var favorite = false;
              if (results.ends[i].id == results.favorite) {
                favorite = true;
              }

              var content =
                '<b>' +
                results.ends[i].bairro +
                '</b> - ' +
                results.ends[i].endereco +
                ' ' +
                results.ends[i].complemento +
                ' ' +
                results.ends[i].numero;

              var novoCard = makeCard(results.ends[i].id, content, favorite);

              $('#enderecos-list').append(novoCard);
            }
          }

          //clickers
          $('#enderecos-list .info-card-content').on('click', function() {
            $('#enderecos-list')
              .find('.info-card-content')
              .removeClass('card-checked');
            $(this).addClass('card-checked');
          });

          $('#enderecos-list .info-card-delete').on('click', function() {
            var id = $(this)
              .parent()
              .parent()
              .parent()
              .attr('id');

            deleteAddress(id);
          });

          var button =
            '<div class="row">\
         <div class="col-lg-6 col-md-6">\
           <div class="colmeia-button button-orange">\
             Adicionar novo endereço\
           </div>\
         </div>\
       </div>';

          $('#enderecos-list').append(button);

          //novo endereco
          $('#enderecos-list .colmeia-button').on('click', function() {
            $('#enderecos-list').hide();
            $('#novo-endereco').show();
          });
        } else {
          alert(results.error);
        }
      })
      .fail(function(jqXHR, textStatus, errorThrown) {
        console.log('Erro!');
        console.log(textStatus);
        console.log(errorThrown);
      });
  }

  function carregarCartoes() {
    $('#cartoes-list').empty();

    var id = user.id;

    $.ajax({
      url: '/reforco-escolar/pagamento/buscar',
      type: 'POST',
      data: { id: id }
    })
      .done(function(results) {
        if (results.success) {
          allCards = results.cards;

          for (var i = 0; i < results.cards.length; i++) {
            var favorite = false;
            if (results.cards[i].objectId == results.favorite) {
              favorite = true;
            }

            var content =
              '<b> Final: </b>' +
              results.cards[i].digits +
              '<img src=/img/' +
              results.cards[i].brand.toLowerCase() +
              '-logo.png' +
              '>';

            var novoCard = makeCard(results.cards[i].objectId, content, favorite);

            $('#cartoes-list').append(novoCard);
          }

          //clickers
          $('#cartoes-list .info-card-content').on('click', function() {
            $('#cartoes-list')
              .find('.info-card-content')
              .removeClass('card-checked');
            $(this).addClass('card-checked');
          });

          $('#cartoes-list .info-card-delete').on('click', function() {
            var id = $(this)
              .parent()
              .parent()
              .parent()
              .attr('id');

            deleteCartao(id);
          });

          var button =
            '<div class="row">\
        <div class="col-lg-6 col-md-6">\
          <div class="colmeia-button button-orange">\
            Adicionar novo cartão\
          </div>\
        </div>\
      </div>';

          $('#cartoes-list').append(button);

          //novo card
          $('#cartoes-list .colmeia-button').on('click', function() {
            $('#cartoes-list').hide();
            $('#novo-cartao').show();
          });

          handleUnselectedCards();
        } else {
          alert(results.error);
        }
      })
      .fail(function(jqXHR, textStatus, errorThrown) {
        alert('Houve um erro ao obter seus cartões, tente novamente mais tarde.');
      });
  }

  function makeCard(id, content, selected) {
    return (
      '<div class="row" id=' +
      id +
      '>\
<div class="col-lg-12 col-md-12">\
    <div class="info-card">\
        <div class="info-card-content ' +
      (selected ? 'card-checked' : '') +
      '">' +
      content +
      '</div>\
    <div class="info-card-delete">&#x2716;</div>\
    </div>\
</div>\
</div>'
    );
  }

  function pushSingleEndereco(end) {
    $('#enderecos-list')
      .find('.info-card-content')
      .removeClass('card-checked');

    var content =
      '<b>' + end.bairro + '</b> - ' + end.endereco + ' ' + end.complemento + ' ' + end.numero;
    var novoCard = makeCard(end.id, content, true);

    $('#enderecos-list').prepend(novoCard);

    var currentEnd = $('#' + end.id).find('.info-card-content');
    currentEnd.on('click', function() {
      $('#enderecos-list')
        .find('.info-card-content')
        .removeClass('card-checked');
      currentEnd.addClass('card-checked');
    });

    $('#' + end.id)
      .find('.info-card-delete')
      .on('click', function() {
        deleteAddress(end.id);
      });
  }

  function pushSingleCartao(card) {
    $('#cartoes-list')
      .find('.info-card-content')
      .removeClass('card-checked');

    var content = '<b> Final: </b>' + card.digits + '<img src="/img/' + card.brand + '-logo.png">';
    var novoCard = makeCard(card.objectId, content, true);

    $('#cartoes-list').prepend(novoCard);

    var currentCard = $('#' + card.objectId).find('.info-card-content');
    currentCard.on('click', function() {
      $('#cartoes-list')
        .find('.info-card-content')
        .removeClass('card-checked');
      currentCard.addClass('card-checked');
    });

    $('#' + card.objectId)
      .find('.info-card-delete')
      .on('click', function() {
        deleteCartao(card.objectId);
      });
  }

  function moverCaixaAgendamento() {
    if (mobilecheck()) {
      $('.caixa-resumo').hide();
      $('.mobile-controller').show();

      $('.mobile-caixa-resumo-toggle').on('click', function() {
        $('.caixa-resumo').toggle();
      });
    } else {
      $('.caixa-resumo').show();
    }
  }

  function deleteAddress(addressId) {
    $.ajax({
      url: '/reforco-escolar/endereco/remover',
      type: 'POST',
      data: { id: addressId }
    })
      .done(function(result) {
        if (result.success) {
          $('#' + addressId).remove();
        } else {
          alert(result.error);
        }
      })
      .fail(function(jqXHR, textStatus, errorThrown) {
        alert('Houve um erro ao excluir seu endereço, tente novamente mais tarde.');
      });
  }

  function deleteCartao(cartaoId) {
    $.ajax({
      url: '/reforco-escolar/pagamento/remover',
      type: 'POST',
      data: { id: cartaoId }
    })
      .done(function(result) {
        if (result.success) {
          $('#' + cartaoId).remove();
        } else {
          alert(result.error);
        }
      })
      .fail(function(jqXHR, textStatus, errorThrown) {
        alert('Houve um erro ao excluir seu cartão, tente novamente mais tarde.');
      });
  }

  function validarCodigo() {
    var button = mobilecheck() ? $('#code-button-mobile') : $('#code-button');
    var feedback = mobilecheck() ? $('#code-feedback-mobile') : $('#code-feedback');
    var input = mobilecheck() ? $('#input-codigo-mobile') : $('#input-codigo');

    button.on('click', function() {
      if (dados.discountCode) {
        feedback.html('Voce já possui um código ativo: ' + dados.discountCode);
      } else {
        checkCode(feedback, input);
      }
    });
  }

  function handleUnselectedCards() {
    var endSelecionado = $('#enderecos-list').find('.card-checked').length;
    var cardSelecionado = $('#cartoes-list').find('.card-checked').length;

    if (endSelecionado == 0) {
      $('#enderecos-list .info-card')
        .first()
        .click();
    } else {
      $('#enderecos-list')
        .find('.card-checked')
        .click();
    }

    if (cardSelecionado == 0) {
      var cartao = $('#cartoes-list .info-card').first();

      var id = cartao
        .parent()
        .parent()
        .attr('id');

      cartao.click();
    } else {
      $('#cartoes-list')
        .find('.card-checked')
        .click();
    }
  }

  function handleNovoEndereco() {
    $('#select-cidade').empty();
    $('#select-bairro').empty();

    var _bairros = BAIRROS[toSimpleString(dados.cidade)];

    $('#select-cidade').append(
      "<option value='" + toSimpleString(dados.cidade) + "'>" + dados.cidade + '</option>'
    );

    Object.keys(_bairros).forEach(function(key, index) {
      if ($.inArray(_bairros[key], dados.professor.bairros) !== -1) {
        $('#select-bairro').append('<option value=' + key + '>' + _bairros[key] + '</option>');
      }
    });

    //pre seleciona o mesmo bairro que ele escolheu no inicio
    $('#select-bairro').val(dados.bairro);

    //voltar
    $('#novo-endereco .colmeia-button.button-orange').on('click', function() {
      $('#novo-endereco').hide();
      $('#enderecos-list').show();
      $('#novo-endereco')
        .find('input:text')
        .val('');
    });

    //confirmar
    $('#novo-endereco .colmeia-button.button-filled').on('click', function() {
      if ($('#input-endereco').val() == '' || $('#input-numero').val() == '') {
        alert('Por favor, preencha todos os campos.');
      } else {
        var bairroVal = $('#select-bairro').val();
        var cidade = $('#select-cidade').val();
        var bairro = BAIRROS[cidade][bairroVal];

        var novoEnd = {
          endereco: $('#input-endereco').val(),
          numero: $('#input-numero').val(),
          complemento: $('#input-complemento').val(),
          bairro: bairro
        };

        $.ajax({
          url: '/reforco-escolar/endereco/adicionar',
          type: 'POST',
          data: novoEnd
        })
          .done(function(results) {
            if (results.success) {
              try {
                woopra.identify({
                  email: user.email,
                  name: user.nome,
                  phone: user.phone,
                  idparse: user.id
                });

                woopra.track('add_address', {
                  bairro: results.end.bairro,
                  cidade: results.end.cidade
                });
              } catch (e) {}
              // carregarEnderecos();
              pushSingleEndereco(results.end);

              $('#novo-endereco .colmeia-button.button-orange').click();
            } else {
              alert(results.error);
            }
          })
          .fail(function(jqXHR, textStatus, errorThrown) {
            alert('Houve um erro ao adicionar seu endereço, tente novamente mais tarde.');
          });
      }
    });
  }

  function handleNovoCartao() {
    $('#input-mes').mask('00');
    $('#input-ano').mask('00');
    $('#input-seguranca').mask('0000');
    $('#input-nascimento').mask('00/00/0000');

    //voltar
    $('#novo-cartao .colmeia-button.button-orange').on('click', function() {
      $('#novo-cartao').hide();
      $('#cartoes-list').show();
      $('#novo-cartao')
        .find('input:text')
        .val('');
    });

    //confirmar
    $('#novo-cartao .colmeia-button.button-filled').on('click', function() {
      if (
        $('#input-nome').val() == '' ||
        $('#input-cpf').val() == '' ||
        $('#input-mes').val() == '' ||
        $('#input-ano').val() == '' ||
        $('#input-cartaonu').val() == '' ||
        $('#input-seguranca').val() == '' ||
        $('#input-nascimento').val() == ''
      ) {
        alert('Por favor, preencha todos os campos.');
        return;
      }

      if ($('#input-nascimento').val().length < 10) {
        alert('Insira uma data de nascimento válida.');
        return;
      }

      if ($('#input-cpf').val().length < 11) {
        alert('Insira um número de cpf válido.');
        return;
      }

      var nascimentoArray = $('#input-nascimento')
        .val()
        .split('/');
      var nascimento = nascimentoArray[2] + '-' + nascimentoArray[1] + '-' + nascimentoArray[0];

      var novoCard = {
        nome: $('#input-nome').val(),
        numero: $('#input-cartaonu').val(),
        mes: $('#input-mes').val(),
        ano: '20' + $('#input-ano').val(),
        cpf: $('#input-cpf').val(),
        seguranca: $('#input-seguranca').val(),
        nascimento: nascimento
      };

      hashCard(novoCard);
    });
  }

  function checkCode(feedback, input) {
    input.removeClass('code-valid');
    input.removeClass('code-invalid');
    feedback.removeClass('feedback-valid');
    feedback.removeClass('feedback-invalid');

    feedback.html('');

    var code = input.val();
    if (/\S/.test(code) === false) return;

    //get selected card
    var cardId = $('#cartoes-list')
      .find('.card-checked')
      .parent()
      .parent()
      .parent()
      .attr('id');
    var currentCard;

    for (var i = 0; i < allCards.length; i++) {
      if (allCards[i].objectId == cardId) {
        currentCard = allCards[i];
      }
    }

    if (currentCard) {
      var issuer = currentCard.issuer ? currentCard.issuer : '';
    } else {
      var issuer = '';
    }

    $.ajax({
      url: '/reforco-escolar/verificar-codigo',
      type: 'POST',
      data: { code: code, type: dados.tipo, issuer: issuer }
    })
      .done(function(data) {
        console.log(data);
        if (data.price) {
          var price = data.price;

          input.addClass('code-valid');
          feedback.addClass('feedback-valid');
          feedback.html(
            'Código válido! O novo preço é: ' + (price * dados.duracao).toFixed(2) + '.'
          );

          $('.preco-aula').html(
            'R$<del>' +
              (oldPrice * dados.duracao).toFixed(2) +
              '</del> ' +
              (price * dados.duracao).toFixed(2)
          );

          precoTotal = price * dados.duracao;
        } else if (data.error) {
          input.addClass('code-invalid');
          feedback.addClass('feedback-invalid');
          feedback.html(data.error);
        }
      })
      .fail(function(jqXHR, textStatus, errorThrown) {
        console.log('erro:', textStatus, errorThrown);
        alert('Houve um erro ao verificar seu código. Tente novamente mais tarde.');
      });
  }

  function handleEscolaAno() {
    if (dados.tipo === 'Escolar' && !dados.hasEscolaAno) {
      //se a materia é escolar e user nao tem escola e ano setados,
      //mostra campos para pedir.

      if (dados.nivel == 'Fundamental') {
        $('#select-ano').append(
          '\
        <option value="1º ANO ENS FUNDAMENTAL">1º ano</option>\
        <option value="2º ANO ENS FUNDAMENTAL">2º ano</option>\
        <option value="3º ANO ENS FUNDAMENTAL">3º ano</option>\
        <option value="4º ANO ENS FUNDAMENTAL">4º ano</option>\
        <option value="5º ANO ENS FUNDAMENTAL">5º ano</option>\
        <option value="6º ANO ENS FUNDAMENTAL">6º ano</option>\
        <option value="7º ANO ENS FUNDAMENTAL">7º ano</option>\
        <option value="8º ANO ENS FUNDAMENTAL">8º ano</option>\
        <option value="9º ANO ENS FUNDAMENTAL">9º ano</option>\
      '
        );
      } else if (dados.nivel == 'Médio') {
        $('#select-ano').append(
          '\
        <option value="1º ANO END MEDIO">1º ano</option>\
        <option value="2º ANO END MEDIO">2º ano</option>\
        <option value="3º ANO END MEDIO">3º ano</option>\
        '
        );
      } else {
        alert('Houve um erro no agendamento!');
      }

      for (var i = 0; i < escolas.length; i++) {
        $('#select-escola').append(
          '\
        <option value=' + escolas[i] + '>' + escolas[i] + '</option>\
      '
        );
      }

      $('#select-escola').on('change', function() {
        if ($(this).val() == 'Outra') {
          $('#nova-escola-card').show();
        } else {
          $('#nova-escola-card').hide();
        }
      });

      $('.info-escola').show();
    }
  }

  function marcarAula() {
    $('#final-button').on('click', function() {
      if (
        $('#enderecos-list')
          .find('.card-checked')
          .val() == undefined
      ) {
        alert('Você deve selecionar um endereço para prosseguir!');
        return;
      }

      if (
        $('#cartoes-list')
          .find('.card-checked')
          .val() == undefined
      ) {
        alert('Você deve selecionar um cartão para prosseguir!');
        return;
      }

      if ($('#foto-obs').val() != '' && $('#foto-obs').val() != undefined) {
        if (!checkExtention($('#foto-obs').val(), ['png', 'jpg', 'jpeg'])) {
          alert('A imagem deve possuir extensão png, jpeg ou jpg.');
          return;
        }

        if ($('#foto-obs')[0].files[0].size > 10 * 1024 * 1024) {
          alert('A imagem não deve exceder 10Mb em tamanho.');
          return;
        }
      }

      if (dados.tipo === 'Escolar' && !dados.hasEscolaAno) {
        if ($('#select-ano').val() == '' || $('#select-ano').val() == null) {
          alert('Por favor, insira o ano escolar.');
          return;
        }

        if ($('#select-escola').val() == '' || $('#select-escola').val() == null) {
          alert('Por favor, insira o nome da escola.');
          return;
        }

        if ($('#select-escola').val() == 'Outra' && $('#nova-escola').val() == '') {
          alert('Por favor, insira o nome da escola.');
          return;
        }
      }

      var dadosAgendamento = {};

      dadosAgendamento.cardId = $('#cartoes-list')
        .find('.card-checked')
        .parent()
        .parent()
        .parent()
        .attr('id');
      dadosAgendamento.endId = $('#enderecos-list')
        .find('.card-checked')
        .parent()
        .parent()
        .parent()
        .attr('id');
      dadosAgendamento.observacao = $('#input-observacao').val();
      dadosAgendamento.cidade = dados.cidade;

      if (!dados.hasEscolaAno) {
        if ($('#select-escola').val() == 'Outra') {
          dadosAgendamento.nomeEscola = $('#nova-escola')
            .val()
            .toUpperCase();
        } else {
          dadosAgendamento.nomeEscola = $('#select-escola')
            .val()
            .toUpperCase();
        }

        dadosAgendamento.anoEscola = $('#select-ano')
          .val()
          .toUpperCase();
      }

      $("#resumo-form input[name='cardId']").val(dadosAgendamento.cardId);
      $("#resumo-form input[name='endId']").val(dadosAgendamento.endId);
      $("#resumo-form input[name='observacao']").val(dadosAgendamento.observacao);

      $("#resumo-form input[name='nomeEscola']").val(dadosAgendamento.nomeEscola);
      $("#resumo-form input[name='anoEscola']").val(dadosAgendamento.anoEscola);
      $("#resumo-form input[name='cidade']").val(dadosAgendamento.cidade);

      var form = $('#resumo-form')[0];
      var formData = new FormData(form);

      $.ajax({
        url: '/reforco-escolar/marcar-aula',
        enctype: 'multipart/form-data',
        processData: false, // Important!
        contentType: false,
        cache: false,
        type: 'POST',
        data: formData
      })
        .done(function(response) {
          if (response.success == false) {
            alert(response.message);
            return;
          }

          try {
            woopra.identify({
              email: user.email,
              name: user.nome,
              phone: user.phone,
              idparse: user.id
            });

            woopra.track('agendamento', {
              preco: response.aula.preco,
              isCodigoPromocional: response.aula.codigoPromocional != undefined,
              materia: response.aula.topico,
              enderecoId: response.aula.endereco.objectId,
              duracao: response.aula.duracao,
              professorId: response.aula.professor.objectId
            });

            goog_report_conversion();
          } catch (e) {}

          $('#aula-modal').modal();
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
          console.log('Erro!');
          console.log(textStatus);
          console.log(errorThrown);
        });
    });
  }

  function hashCard(novoCard) {
    var isNumberValid = Moip.Validator.isValid(novoCard.numero);
    var isExpiryValid = Moip.Validator.isExpiryDateValid(novoCard.mes, novoCard.ano);
    var isCvcValid = Moip.Validator.isSecurityCodeValid(novoCard.numero, novoCard.seguranca);

    if (!isNumberValid) {
      alert(
        'O número do cartão que você inseriu não é válido. Verifique os campos e tente novamente.'
      );
      return false;
    }
    if (!isExpiryValid) {
      alert(
        'A data de expiração do cartão que você inseriu não é válido. Verifique os campos e tente novamente.'
      );
      return false;
    }
    if (!isCvcValid) {
      alert(
        'O código de segurança do cartão que você inseriu não é válido. Verifique os campos e tente novamente.'
      );
      return false;
    }

    var objectToEncryp = {
      expirationMonth: novoCard.mes,
      expirationYear: novoCard.ano,
      number: novoCard.numero,
      cvc: novoCard.seguranca,
      birthdate: novoCard.nascimento,
      fullname: novoCard.nome,
      cpf: novoCard.cpf
    };

    disabled = false;

    var encryptedCard = NodeRsaEncryptString('card', JSON.stringify(objectToEncryp));
    addCard(encryptedCard);
  }

  function addCard(hash) {
    var cpf = $('#input-cpf').val();

    $.ajax({
      url: '/reforco-escolar/pagamento/adicionar',
      type: 'POST',
      data: { card_hash: hash, cpf: cpf }
    })
      .done(function(data) {
        if (data.success) {
          try {
            fbq('track', 'AddPaymentInfo', {
              value: precoTotal,
              currency: 'BRL'
            });
            woopra.identify({
              email: user.email,
              name: user.nome,
              phone: user.phone,
              idparse: user.id
            });

            woopra.track('add_card', {});
          } catch (e) {}

          $('#novo-cartao .colmeia-button.button-orange').click();
          carregarCartoes();
        } else if (data.path) {
          window.localtion = data.path;
        } else if (data.message) {
          alert(data.message);
        }
      })
      .fail(function() {
        console.log('error');
      });
  }

  function fixImagem(foto) {
    var width = foto[0].width;
    var height = foto[0].height;

    if (width < height) {
      foto.css('width', '100%');
      foto.css('height', 'auto');
      // $('.intro-container img').css('margin-right', (height - width) + 'px');
    } else if (width > height) {
      foto.css('width', 'auto');
      foto.css('height', '100%');
      // $('.intro-container img').css('margin-left', (height - width) + 'px');
    } else if (width == height) {
      foto.css('height', '100%');
      foto.css('width', '100%');
    }
  }

  function checkExtention(filename, extentionArray) {
    var extention;

    for (var i = filename.length - 1; i >= 0; i--) {
      if (filename[i] == '.') {
        extention = filename.slice(i + 1, filename.length);
        break;
      }
    }

    if (extentionArray.indexOf(extention) != -1) return true;
    else return false;
  }

  function setupModal() {
    $('.modal').on('show.bs.modal', function() {
      $(this).show();
      setModalMaxHeight(this);
    });

    $('#aula-modal').on('hidden.bs.modal', function() {
      window.location.href = '/' + window.location.search;
    });

    $('#aula-modal .aula-marcada-button').on('click', function() {
      window.location.href = '/' + window.location.search;
    });

    $(window).resize(function() {
      if ($('.modal.in').length != 0) {
        setModalMaxHeight($('.modal.in'));
      }
    });
  }

  function setModalMaxHeight(element) {
    this.$element = $(element);
    this.$content = this.$element.find('.modal-content');
    var borderWidth = this.$content.outerHeight() - this.$content.innerHeight();
    var dialogMargin = $(window).width() < 768 ? 20 : 60;
    var contentHeight = $(window).height() - (dialogMargin + borderWidth);
    var headerHeight = this.$element.find('.modal-header').outerHeight() || 0;
    var footerHeight = this.$element.find('.modal-footer').outerHeight() || 0;
    var maxHeight = contentHeight - (headerHeight + footerHeight);

    this.$content.css({
      overflow: 'hidden'
    });

    this.$element.find('.modal-body').css({
      'max-height': maxHeight,
      'overflow-y': 'auto'
    });
  }
});
