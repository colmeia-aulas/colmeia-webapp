var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows();
    }
};

if (isMobile.any()) {
    $('#navbar-logo').attr('src', '/img/logo-orange.png');
}

$(function() {
    var maskBehavior = function(val) {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
        },
        options = {
            onKeyPress: function(val, e, field, options) {
                field.mask(maskBehavior.apply({}, arguments), options);
            }
        };

    $('#telefone').mask(maskBehavior, options);

    $('#sendButton').on('click', function() {
        $('#registerMessage').html('');

        if (
            $('#nome').val() == '' ||
            $('#email').val() == '' ||
            $('#telefone').cleanVal() == '' ||
            $('#bairro').val() == '' ||
            $('#endereco').val() == ''
        ) {
            $('#registerMessage').html('Preencha todos os campos para prosseguir!');
        } else if ($('#telefone').cleanVal().length < 10) {
            $('#registerMessage').html('Insira um número de telefone válido!');
        } else if (!$('#check-termos').is(':checked')) {
            $('#registerMessage').html('Você deve concordar com os Termos de Uso!');
        } else if (!isValidEmail($('#email').val())) {
            $('#registerMessage').html('Insira um email válido!');
        } else {
            var values = {
                email: $('#email')
                    .val()
                    .toLowerCase(),
                nome: $('#nome').val(),
                telefone: $('#telefone').val(),
                bairro: $('#bairro').val(),
                endereco: $('#endereco').val()
            };

            $('#registerMessage').html('');
            // ajax
            $.ajax({
                url: '/professor/simples',
                type: 'POST',
                data: values
            })
                .done(function(data) {
                    if (data.success) {
                        window.location.href = '/professor/simples/obrigado';
                    } else {
                        if (data == 202) {
                            alert('Email já cadastrado!');
                        } else {
                            alert('Algo deu errado, tente novamente mais tarde!');
                        }
                    }
                })
                .fail(function(jqXHR, textStatus, errorThrown) {
                    alert('Algo deu errado, tente novamente mais tarde!');
                });
        }
    });
});

function isValidEmail(email) {
    for (var i = 0; i < email.length; i++) {
        if (email[i] == '@') return true;
    }
    return false;
}
