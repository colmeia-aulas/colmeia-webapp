var isMobile = {
  Android: function() {
    return navigator.userAgent.match(/Android/i);
  },
  BlackBerry: function() {
    return navigator.userAgent.match(/BlackBerry/i);
  },
  iOS: function() {
    return navigator.userAgent.match(/iPhone|iPad|iPod/i);
  },
  Opera: function() {
    return navigator.userAgent.match(/Opera Mini/i);
  },
  Windows: function() {
    return navigator.userAgent.match(/IEMobile/i);
  },
  any: function() {
    return (
      isMobile.Android() ||
      isMobile.BlackBerry() ||
      isMobile.iOS() ||
      isMobile.Opera() ||
      isMobile.Windows()
    );
  }
};

if (isMobile.any()) {
  $('#navbar-logo').attr('src', '/img/logo-orange.png');
}

$(function() {
  var maskBehavior = function(val) {
      return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    },
    options = {
      onKeyPress: function(val, e, field, options) {
        field.mask(maskBehavior.apply({}, arguments), options);
      }
    };

  $('#telefone').mask(maskBehavior, options);

  $('#formLogin').on('submit', function(e) {
    e.preventDefault();

    var values = {
      email: $('#inputEmail')
        .val()
        .toLowerCase(),
      password: $('#inputPassword').val()
    };

    $('#loginMessage').html('');

    // ajax
    $.ajax({ url: '/aluno/entrar', type: 'POST', data: values })
      .done(function(response) {
        if (response.success == true) {
          $('#loginModal').modal('toggle');
          window.location = '/professor/cadastro';
        } else {
          $('#loginMessage').html('Login ou senha inválidos!');
        }
      })
      .fail(function(jqXHR, textStatus, errorThrown) {
        alert('Houve um erro ao realizar seu login!');
      });
  });

  $('#sendButton').on('click', function() {
    $('#registerMessage').html('');

    if (
      $('#nome').val() == '' ||
      $('#email').val() == '' ||
      $('#telefone').cleanVal() == '' ||
      $('#senha').val() == '' ||
      $('#senha_conf').val() == ''
    ) {
      $('#registerMessage').html('Preencha todos os campos para prosseguir!');
    } else if ($('#senha').val() != $('#senha_conf').val()) {
      $('#registerMessage').html('Senhas não são as mesmas!');
    } else if ($('#senha').val().length < 6) {
      $('#registerMessage').html('Sua senha deve ter no mínimo 6 caracteres!');
    } else if ($('#telefone').cleanVal().length < 10) {
      $('#registerMessage').html('Insira um número de telefone válido!');
    } else if (!$('#check-termos').is(':checked')) {
      $('#registerMessage').html('Você deve concordar com os Termos de Uso!');
    } else if (!isValidEmail($('#email').val())) {
      $('#registerMessage').html('Insira um email válido!');
    } else {
      var values = {
        email: $('#email')
          .val()
          .toLowerCase(),
        senha: $('#senha').val(),
        telefone: $('#telefone').val()
      };

      $('#registerMessage').html('');
      // ajax
      $.ajax({
        url: '/aluno/registrar',
        type: 'POST',
        data: values
      })
        .done(function(data) {
          if (data.success) {
            // Checa se o Parse retornou algum erro
            try {
              goog_report_conversion();

              window.location = '/professor/cadastro';
            } catch (e) {}
          } else {
            if (data == 202) {
              $('#registerMessage').html('Email já cadastrado!');
            } else {
              $('#registerMessage').html('Algo deu errado, tente novamente mais tarde!');
            }
          }
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
          alert('Houve um erro ao realizar seu cadastro.');
        });
    }
  });
});

function showLoginForm() {
  $('#loginModal .registerBox').fadeOut('fast', function() {
    $('.loginBox').fadeIn('fast');
    $('.register-footer').fadeOut('fast', function() {
      $('.login-footer').fadeIn('fast');
    });
    $('.modal-title').html('Login');
  });
  $('.error')
    .removeClass('alert alert-danger')
    .html('');
}

function openLoginModal() {
  showLoginForm();
  setTimeout(function() {
    $('#loginModal').modal('show');
  }, 230);
}

function isValidEmail(email) {
  for (var i = 0; i < email.length; i++) {
    if (email[i] == '@') return true;
  }
  return false;
}
