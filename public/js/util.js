$(function() {
  $('.js-cta').on('click', function() {
    var target = $('.js-cta-target').length > 0 ? $('.js-cta-target').offset().top : 0;

    $('html, body').animate(
      {
        scrollTop: target
      },
      800
    );
  });
});
