// (function (exports) {
//     exports.toSimpleString = function (strAccents) {

//         var strAccents = strAccents.split('');
//         var strAccentsOut = new Array();
//         var strAccentsLen = strAccents.length;
//         var accents     = 'ÀÁÂÃÄÅàáâãäåÒÓÔÕÕÖØòóôõöøÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž';
//         var accentsOut  = 'AAAAAAaaaaaaOOOOOOOooooooEEEEeeeeeCcDIIIIiiiiUUUUuuuuNnSsYyyZz';
//         for (var y = 0; y < strAccentsLen; y++) {
//             if (accents.indexOf(strAccents[y]) != -1) {
//                 strAccentsOut[y] = accentsOut.substr(accents.indexOf(strAccents[y]), 1);
//             } else
//                 strAccentsOut[y] = strAccents[y];
//         }
//         strAccentsOut = strAccentsOut.join('');
//         strAccentsOut = strAccentsOut.replace(/\s+/g, '-').toLowerCase();

//         return strAccentsOut;
//     };
// })(typeof exports === 'undefined' ? this['debug'] = {} : exports);
function toSimpleString(strAccents) {

    var strAccents = strAccents.split('');
    var strAccentsOut = new Array();
    var strAccentsLen = strAccents.length;
    var accents = 'ÀÁÂÃÄÅàáâãäåÒÓÔÕÕÖØòóôõöøÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž';
    var accentsOut = 'AAAAAAaaaaaaOOOOOOOooooooEEEEeeeeeCcDIIIIiiiiUUUUuuuuNnSsYyyZz';
    for (var y = 0; y < strAccentsLen; y++) {
        if (accents.indexOf(strAccents[y]) != -1) {
            strAccentsOut[y] = accentsOut.substr(accents.indexOf(strAccents[y]), 1);
        } else
            strAccentsOut[y] = strAccents[y];
    }
    strAccentsOut = strAccentsOut.join('');
    strAccentsOut = strAccentsOut.replace(/\s+/g, '-').toLowerCase();

    return strAccentsOut;
}