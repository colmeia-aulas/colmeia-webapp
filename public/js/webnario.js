$(function(){

  $("input[type='text']").keypress(function(e) {
    var key = e.which;
    if(key == 13) //enter
    {
      $("button[type='submit']").click();
      return false;
    }
  });

  $('.send-result').on('click', function() {
    $('.send-result').slideUp();
  });

  $("button[type='submit']").on('click', function() {
    isEmailValid(function(){

          $.post('/addWebnarioGuest', {email: $("input[type='text']").val() }, function(data) {
              if (data.success) {
                $('.send-result').html('Agradecemos pela inscrição.<br>Você receberá o link para o seminário no seu email!');
              }
              else {
                $('.send-result').html('Desculpe!<br>Algo deu errado, tente novamente mais tarde!');
              }

              $('.send-result').slideDown();
              $("input[type='text']").val("");

              // setTimeout(function(){
              //   $('.send-result').slideUp();
              // }, 2500);
          });
    });

  });

  function isEmailValid(callback) {
    var email = $("input[type='text']").val();

    if (  email == null ||
          email == ''   ||
        ( email.indexOf('@') == -1 || email.indexOf('.') == -1 ) ){

      $('.email-error').html('Insira um email válido.');
      $('.email-error').fadeIn();

      setTimeout(function(){
        $('.email-error').fadeOut();
      }, 2000);

    }
    else {
      $('.email-error').html('');
      callback();
    }

  }
});
