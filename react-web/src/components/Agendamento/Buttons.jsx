import React, { useState, useEffect } from 'react';

export default ({ nextStep, previousStep, totalSteps, step }) => {
  const [loading, setloading] = useState(false);

  useEffect(() => {
    if (loading) {
      nextStep();
      // setloading(false);
    }

    return () => loading && setloading(false);
  }, [loading]);

  const renderButtons = () => {
    if (loading) {
      return (
        <img src="/img/logo-laranja.png" alt="Logo Colmeia " className="agendamento__spinner" />
      );
    }

    return (
      <>
        {step > 1 && (
          <button
            className="agendamento__button agendamento__button--previous"
            onClick={previousStep}
          >
            Voltar
          </button>
        )}

        {step < totalSteps - 1 ? (
          <button className="agendamento__button agendamento__button--next" onClick={nextStep}>
            Continuar
          </button>
        ) : (
          <button
            className="agendamento__button agendamento__button--next"
            onClick={() => setloading(true)}
          >
            Finalizar
          </button>
        )}
      </>
    );
  };

  return <div className="agendamento__bottom-button-container">{renderButtons()}</div>;
};
