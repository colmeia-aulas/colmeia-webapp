import React from 'react';

export default props => (
  <div className="agendamento__nav">
    {Array.from({ length: props.totalSteps }).map((e, i) => (
      <div
        // onClick={() => props.goToStep(i + 1)}
        className={`agendamento__nav-step ${
          props.currentStep > i ? 'agendamento__nav-step--active' : ''
        }`}
      />
    ))}
  </div>
);
