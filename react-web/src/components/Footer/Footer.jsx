import React from 'react';

export default () => (
  <section className="footer u-padding-vertical-m">
    <div className="container">
      <div className="grid grid--pad-s">
        <div className="col">
          <img src="/img/logo.png" alt="Aulas Colmeia Logo" className="footer__logo" />
          <div className="footer__p">Ligações: (61) 9 9805-2073</div>
          <div className="footer__p">
            Whatsapp:
            <img
              src="/img/whatsapp-icon.png"
              alt="Aulas Colmeia Whatsapp"
              className="footer__whatsapp"
            />
            - (61) 9 9805-2073
          </div>
          <div className="footer__p">contato@aulascolmeia.com.br</div>
        </div>
        <div className="col">
          <a href="https://aulascolmeia.com.br/#como-funciona" className="footer__link">
            Como funciona
          </a>
          <a href="https://aulascolmeia.com.br/perguntas-frequentes" className="footer__link">
            Perguntas frequentes
          </a>
          <a href="https://aulascolmeia.com.br/quem-somos" className="footer__link">
            Quem somos
          </a>
          <a href="https://aulascolmeia.com.br/blog" className="footer__link">
            Nosso blog
          </a> 
        </div>
        <div className="col">
          <a href="https://aulascolmeia.com.br/aluno/registrar" className="footer__link">
            Cadastre-se
          </a>
          <a href="https://aulascolmeia.com.br/calculadora-pas" className="footer__link">
            Calculadora do Pas
          </a>
          <a
            href="https://aulascolmeia.com.br/colmeia_ebook_como_ajudar_meu_filho_a_estudar.pdf"
            className="footer__link"
            target="_blank"
          >
            Ajude seu filho a estudar
          </a>
        </div>
        <div className="col">
          <a href="https://aulascolmeia.com.br/professor" className="footer__link">
            Seja um professor
          </a>
          <a href="https://aulascolmeia.com.br/Termos-de-Uso-Colmeia.pdf" className="footer__link">
            Termos de Uso
          </a>
          <a href="https://aulascolmeia.com.br/politica-de-privacidade" className="footer__link">
            Política de Privacidade
          </a>
        </div>
      </div>

      <div className="footer__hr" />

      <div className="footer__bottom">
        <div className="footer__p">Copyright Colmeia 2020</div>
        <div className="footer__p">
          Colmeia Tecnologia LTDA ME, 2017 - CNPJ 26.263.713/0001-31 <br />
          COTI - Edifício Victoria Office Tower - SAUS Q. 4, Sala 701 - Asa Sul, Brasília - DF,
          70070-938
        </div>
      </div>
    </div>
  </section>
);
