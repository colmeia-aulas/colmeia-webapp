import React from 'react';

export default () => (
  <footer className="footer-simple">
    <div className="container">
      <div className="footer-simple__content">
        <img src="/img/logo.png" alt="Aulas Colmeia Logo" className="footer-simple__logo" />
        <p className="footer-simple__text">Whatsapp (61) 9 9805-2073</p>
      </div>
    </div>
  </footer>
);
