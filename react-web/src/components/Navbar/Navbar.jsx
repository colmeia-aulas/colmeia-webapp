import React from 'react';

export default () => (
  <nav className="navbar">
    <div className="navbar__content">
      <a className="navbar__logo-link" href="/">
        <img src="/img/logo-cinza.png" alt="Aulas Colmeia Logo" className="navbar__logo-img" />
      </a>
      <input type="checkbox" className="navbar__checkbox" id="navbar-toggle" />

      <label for="navbar-toggle" className="navbar__toggle">
        <div />
        <div />
        <div />
      </label>
      <div className="navbar__links">
        <a href="/acompanhamento-escolar" className="navbar__link">
          Acompanhamento
        </a>
        <a href="/calculadora-pas" className="navbar__link">
          Calculadora do PAS
        </a>
        <a href="/blog" className="navbar__link">
          Nosso Blog
        </a>
        <a href="/professor" className="navbar__link">
          Seja um professor
        </a>

        <a href="/aluno/entrar" className="navbar__link navbar__link-login">
          Criar conta/entrar
        </a>
      </div>
    </div>
  </nav>
);
