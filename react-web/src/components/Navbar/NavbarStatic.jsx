import React from 'react';

export default ({ text }) => {
  const fullNumber = `+55${text.replace(/\D/g, '')}`;
  const whatsappUrl = `https://api.whatsapp.com/send?phone=${fullNumber}&text=${encodeURIComponent(
    'Olá! Tudo bem? Eu gostaria de agendar aulas particulares.'
  )}`;

  return (
    <nav className="navbar-static">
      <div className="container">
        <div className="navbar-static__content">
          <div className="navbar-static__left">
            <a className="navbar-static__logo-link" href="/">
              <img
                src="/img/colmeia_logo.png"
                alt="Aulas Colmeia Logo"
                className="navbar-static__logo-img"
              />
            </a>
          </div>
          <a className="navbar-static__right" href={whatsappUrl}>
            {text}
          </a>
        </div>
      </div>
    </nav>
  );
};
