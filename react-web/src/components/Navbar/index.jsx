import Navbar from './Navbar';
import NavbarStatic from './NavbarStatic';

export { Navbar, NavbarStatic };
