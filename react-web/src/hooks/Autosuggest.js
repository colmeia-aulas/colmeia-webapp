import React, { useState, useEffect } from 'react';
import { FiX } from 'react-icons/fi';

export default ({ suggestionOptions, placeholder, LeftIcon }) => {
  const [value, setValue] = useState('');

  const [alwaysRenderSuggestions, setAlwaysRenderSuggestions] = useState(true);
  const [suggestions, setSuggestions] = useState([]);

  // https://developer.mozilla.org/en/docs/Web/JavaScript/Guide/Regular_Expressions#Using_Special_Characters
  const escapeRegexCharacters = str => str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');

  const getSuggestions = value => {
    const escapedValue = escapeRegexCharacters(value.trim().toLowerCase());

    if (escapedValue === '') return suggestionOptions;

    const array = [];

    suggestionOptions.forEach(e => {
      if (e.name.toLowerCase().includes(escapedValue)) array.push(e);
    });

    return array;
  };

  const getSuggestionValue = suggestion => suggestion.name;

  const renderSuggestion = suggestion => (
    <span className="agendamento__suggestion">{suggestion.name}</span>
  );

  const renderInputComponent = inputProps => {
    const props = {
      ...inputProps,
      className: 'agendamento__input',
      value
    };

    return (
      <div className="agendamento__input-wrapper">
        <input {...props} />
        <LeftIcon className="agendamento__input-icon" />
        {inputProps.value && (
          <FiX
            className="agendamento__input-icon agendamento__input-icon--right"
            onClick={() => {
              setValue('');
            }}
          />
        )}
      </div>
    );
  };

  const onSuggestionsFetchRequested = ({ value }) => setSuggestions(getSuggestions(value));

  const onSuggestionsClearRequested = () => setSuggestions([]);

  const onChangeSuggestion = (event, { newValue, method }) => {
    setValue(newValue);
  };

  const inputProps = {
    placeholder,
    value,
    onChange: onChangeSuggestion
  };

  const autosuggestProps = {
    suggestions,
    onSuggestionsClearRequested,
    onSuggestionsFetchRequested,
    getSuggestionValue,
    inputProps,
    renderSuggestion,
    renderInputComponent,
    highlightFirstSuggestion: true,
    alwaysRenderSuggestions: true
  };

  return { autosuggestProps, value, setValue };
};
