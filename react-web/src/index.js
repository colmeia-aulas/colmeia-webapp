import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';

import Agendamento from './pages/Agendamento';
import Idiomas from './pages/Idiomas';
import Videochat2 from './pages/Videochat2';
import Draw from './pages/Draw';

import * as Sentry from '@sentry/react';

import './sass/main.scss';

function Main() {
  return (
    <BrowserRouter basename="/app">
      <Switch>
        <Route exact path="/agendamento" component={Agendamento} />
        <Route exact path="/idiomas" component={Idiomas} />
        <Route exact path="/aula/:id" component={Videochat2} />
        <Route exact path="/draw/:id" component={Draw} />
        <Redirect from="*" to="/agendamento" />
      </Switch>
    </BrowserRouter>
  );
}

Sentry.init({dsn: "https://8d1dca968ca241dcb0ae8c6ff2c7233a@o415837.ingest.sentry.io/5307526"});
ReactDOM.render(<Main />, document.getElementById('root'));
