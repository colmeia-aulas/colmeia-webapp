import React, { useState, useEffect } from 'react';
import Autosuggest from 'react-autosuggest';
import { useAutosuggest } from '../../hooks';
import { FiSearch } from 'react-icons/fi';
import Buttons from '../../components/Agendamento/Buttons';
import Const from '../../utils/Const';

export default props => {
  const materias = Object.values(Const.MATERIAS)
    .slice(1)
    .map(e => ({ name: e }));

  const [conteudo, setConteudo] = useState('');
  const { autosuggestProps, value, setValue } = useAutosuggest({
    placeholder: 'Procurar por matéria',
    LeftIcon: FiSearch,
    suggestionOptions: materias,
    defaultValueKey: 'materia'
  });

  useEffect(() => {
    const materia = new URL(window.location.href).searchParams.get('materia');

    if (materia) {
      setValue(materia);
    }

    document.querySelector('input.agendamento__input').click();
  }, []);

  const isInputValid = () =>
    Object.values(Const.MATERIAS)
      .slice(1)
      .includes(value);

  const validate = () => {
    if (!isInputValid()) {
      alert('Por favor, selecione uma das opções para continuar.');
    } else if (!conteudo) {
      alert('Por favor, preencha todos os campos para continuar.');
    } else {
      props.update({ conteudo, materia: value });
      props.nextStep();
    }
  };

  const renderTextArea = () =>
    isInputValid() && (
      <>
        <label htmlFor="inputConteudo" className="agendamento__label">
          Quais conteúdos serão abordados?
        </label>
        <textarea
          id="inputConteudo"
          className="agendamento__input agendamento__input--textarea"
          onChange={e => setConteudo(e.target.value)}
        />
      </>
    );

  return (
    <div className="container container--xs agendamento__screen">
      <h2 className="agendamento__title">
        Vamos encontrar o melhor professor particular para você. Mas antes, precisamos de algumas
        informações!
      </h2>

      <Autosuggest {...autosuggestProps} />

      {renderTextArea()}

      <Buttons step={1} {...props} nextStep={validate} />
    </div>
  );
};
