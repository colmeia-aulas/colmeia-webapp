import React, { useState } from 'react';
import Buttons from '../../components/Agendamento/Buttons';
import { FiBook, FiBarChart } from 'react-icons/fi';
import Const from '../../utils/Const';

export default props => {
  const ANOS = Object.values(Const.ANOS);

  const [escola, setEscola] = useState('');
  const [ano, setAno] = useState('');

  const validate = () => {
    if (!escola || !ano) {
      alert('Por favor, preencha os campos para continuar.');
    } else {
      props.update({ escola, ano });
      props.nextStep();
    }
  };

  return (
    <div className="container container--xs agendamento__screen">
      <h2 className="agendamento__title">Qual a escola e o ano do(a) aluno(a)?</h2>

      <label htmlFor="inputEscola" className="agendamento__label">
        Escola
      </label>

      <div className="agendamento__input-wrapper">
        <input
          type="text"
          id="inputEscola"
          className="agendamento__input"
          placeholder="Escola"
          onChange={e => setEscola(e.target.value.trim())}
        />

        <FiBook className="agendamento__input-icon" />
      </div>

      <label htmlFor="inputAno" className="agendamento__label">
        Ano Escolar
      </label>

      <div className="agendamento__input-wrapper">
        <select
          id="inputAno"
          defaultValue=""
          className="agendamento__select agendamento__select--ano"
          onChange={e => setAno(e.target.value)}
        >
          <option disabled value="">
            Escolha um ano
          </option>
          {ANOS.map(ano => (
            <option key={ano} value={ano}>
              {ano}
            </option>
          ))}
        </select>

        <FiBarChart className="agendamento__input-icon" />
      </div>

      <Buttons step={2} {...props} nextStep={validate} />
    </div>
  );
};
