import React, { useState } from 'react';
import Buttons from '../../components/Agendamento/Buttons';
import { FiUsers, FiEdit } from 'react-icons/fi';

export default props => {
  const [showOther, setShowOther] = useState(false);
  const [showForm, setShowForm] = useState(false);
  const [transtorno, setTranstorno] = useState('');

  const onRadioClick = e => setShowForm(JSON.parse(e.target.value));

  const validate = () => {
    if (showForm) {
      if (!transtorno) {
        return alert('Por favor, selecione uma opção para continuar.');
      }

      if (showOther && transtorno === 'Outro') {
        return alert('Por favor, digite o diagnóstico do aluno para continuar.');
      }

      props.update({ transtorno });
    } else {
      props.update({ transtorno: 'Não' });
    }

    props.nextStep();
  };

  const onTranstornoChange = e => {
    if (e.target.value === 'Outro') {
      setShowOther(true);
    } else {
      setShowOther(false);
    }

    setTranstorno(e.target.value);
  };

  return (
    <div className="container container--xs agendamento__screen">
      <h2 className="agendamento__title agendamento__title--s">
        {`Legal! Temos professores que dão aulas para alunos do ${props.form.escola}.`}
      </h2>

      <h4 className="agendamento__title agendamento__title--secondary">
        O aluno(a) que receberá essa aula necessita de alguma atenção especial?
      </h4>

      <div className="agendamento__radio-container">
        <label className="agendamento__radio" onChange={onRadioClick}>
          Sim
          <input type="radio" name="radio-especial" value="true" />
          <span className="agendamento__radio-check" />
        </label>

        <label className="agendamento__radio" onChange={onRadioClick}>
          Não
          <input type="radio" name="radio-especial" value="false" />
          <span className="agendamento__radio-check" />
        </label>
      </div>

      {showForm && (
        <>
          <label htmlFor="inputTranstorno" className="agendamento__label">
            O aluno(a) foi diagnosticado com:
          </label>

          <div className="agendamento__input-wrapper">
            <select
              id="inputTranstorno"
              defaultValue=""
              className="agendamento__select agendamento__select--ano"
              onChange={onTranstornoChange}
            >
              <option disabled value="">
                Escolha uma opção
              </option>
              {['Outro', 'TDAH', 'Hiperatividade', 'Autismo', 'Dislexia'].map(e => (
                <option key={e} value={e}>
                  {e}
                </option>
              ))}
            </select>

            <FiUsers className="agendamento__input-icon" />
          </div>

          {showOther && (
            <>
              <label htmlFor="inputOutroTranstorno" className="agendamento__label">
                Qual?
              </label>

              <div className="agendamento__input-wrapper">
                <input
                  type="text"
                  id="inputOutroTranstorno"
                  className="agendamento__input"
                  placeholder="Digite o diagnóstico"
                  onChange={e => setTranstorno(e.target.value)}
                />

                <FiEdit className="agendamento__input-icon" />
              </div>
            </>
          )}
        </>
      )}

      <Buttons step={3} {...props} nextStep={validate} />
    </div>
  );
};
