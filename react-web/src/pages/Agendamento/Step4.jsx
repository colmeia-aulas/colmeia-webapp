import React, { useState } from 'react';
import Buttons from '../../components/Agendamento/Buttons';

const DIAS = ['Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado', 'Domingo'];
const HORARIOS = ['Manhã', 'Tarde', 'Noite'];

const ToggleButton = props => {
  const { children, ...otherProps } = props;

  return (
    <label className="agendamento__toggle-button">
      <input {...otherProps} type="checkbox" />
      <button className="agendamento__toggle-button-content">{children}</button>
    </label>
  );
};

export default props => {
  const validate = () => {
    if (dias.length === 0 || horarios.length === 0) {
      alert('Por favor, selecione dias e horários para continuar.');
    } else {
      props.update({ horarios, dias });
      props.nextStep();
    }
  };

  const [dias, setDias] = useState([]);
  const [horarios, setHorarios] = useState([]);

  const filterUnique = (x, i, a) => a.indexOf(x) === i;

  const handleDiasClick = e => setDias([...dias, e.target.value].filter(filterUnique));
  const handleHorariosClick = e => setHorarios([...horarios, e.target.value].filter(filterUnique));

  return (
    <div className="container container--xs agendamento__screen">
      <h2 className="agendamento__title">
        Quais são os melhores dias e horários para a aula acontecer?
      </h2>

      <label htmlFor="" className="agendamento__label">
        Quais os dias? (Você pode escolher mais de um)
      </label>
      <div className="agendamento__toggle-button-container">
        {DIAS.map(dia => (
          <ToggleButton key={dia} value={dia} name="radio-dias" onChange={handleDiasClick}>
            {dia}
          </ToggleButton>
        ))}
      </div>

      <label htmlFor="" className="agendamento__label">
        Quais os turnos? (Você pode escolher mais de um)
      </label>
      <div className="agendamento__toggle-button-container">
        {HORARIOS.map(horario => (
          <ToggleButton
            key={horario}
            value={horario}
            name="radio-horarios"
            onChange={handleHorariosClick}
          >
            {horario}
          </ToggleButton>
        ))}
      </div>

      <Buttons step={4} {...props} nextStep={validate} />
    </div>
  );
};
