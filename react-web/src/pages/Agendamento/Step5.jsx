import React, { useState } from 'react';
import Buttons from '../../components/Agendamento/Buttons';
import { FiMapPin, FiMap } from 'react-icons/fi';
import Const from '../../utils/Const';

export default props => {
  const [bairro, setBairro] = useState('');
  const [cidade, setCidade] = useState('brasilia');

  const BAIRROS = Object.values(Const.BAIRROS[cidade]).slice(1);
  const CIDADES = Const.CIDADES;

  const validate = () => {
    if (!cidade || !bairro) {
      alert('Por favor, selecione uma cidade e um bairro para continuar.');
    } else {
      props.update({ bairro, cidade: CIDADES[cidade] });
      props.nextStep();
    }
  };

  return (
    <div className="container container--xs agendamento__screen">
      <h2 className="agendamento__title">Onde você quer receber suas aulas particulares?</h2>

      <label htmlFor="inputCidade" className="agendamento__label">
        Cidade
      </label>

      <div className="agendamento__input-wrapper">
        <select
          id="inputCidade"
          defaultValue=""
          className="agendamento__select"
          onChange={e => setCidade(e.target.value)}
        >
          {Object.keys(Const.CIDADES).map(e => (
            <option key={e} value={e}>
              {Const.CIDADES[e]}
            </option>
          ))}
        </select>

        <FiMap className="agendamento__input-icon" />
      </div>

      <label htmlFor="inputAno" className="agendamento__label">
        Bairro
      </label>

      <div className="agendamento__input-wrapper">
        <select
          id="inputBairro"
          defaultValue=""
          className="agendamento__select"
          onChange={e => setBairro(e.target.value)}
        >
          <option disabled value="">
            Escolha um bairro
          </option>
          {BAIRROS.map(e => (
            <option key={e} value={e}>
              {e}
            </option>
          ))}
        </select>

        <FiMapPin className="agendamento__input-icon" />
      </div>

      <Buttons step={5} {...props} nextStep={validate} />
    </div>
  );
};
