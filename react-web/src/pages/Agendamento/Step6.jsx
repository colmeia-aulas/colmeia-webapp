import React, { useState } from 'react';
import InputMask from 'react-input-mask';
import Buttons from '../../components/Agendamento/Buttons';
import { FiUser, FiMail, FiPhone, FiMessageCircle } from 'react-icons/fi';

export default props => {
  const [nome, setNome] = useState('');
  const [email, setEmail] = useState('');
  const [telefone, setTelefone] = useState('');
  const [origem, setOrigem] = useState('');

  const validate = async () => {
    if (!nome || !email || !telefone || !origem) {
      alert('Por favor, preencha todos os campos para continuar.');
    } else {
      props.update({ nome, email, telefone: `+55${telefone}`, origem });
      try {
        const { data } = await props.submit();

        if (data.success) {
          props.nextStep();
        } else {
          alert(data.message);
        }
      } catch (error) {
        alert('Houve um erro ao realizar seu agendamento.');
      }
    }
  };

  return (
    <div className="container container--xs agendamento__screen">
      <h2 className="agendamento__title">
        Pronto! Já temos todos os dados que precisamos para encontrar o melhor professor particular
        para seu filho(a)!
      </h2>
      <h3 className="agendamento__title agendamento__title--secondary">
        Preencha os campos abaixo para que nossa equipe pedagógica possa entrar em contato.
      </h3>

      <label htmlFor="inputNome" className="agendamento__label">
        Nome
      </label>

      <div className="agendamento__input-wrapper">
        <input
          type="text"
          id="inputNome"
          className="agendamento__input"
          placeholder="Digite seu nome"
          onChange={e => setNome(e.target.value)}
        />

        <FiUser className="agendamento__input-icon" />
      </div>

      <label htmlFor="inputEmail" className="agendamento__label">
        Email
      </label>

      <div className="agendamento__input-wrapper">
        <input
          type="email"
          id="inputEmail"
          className="agendamento__input"
          placeholder="Digite seu email"
          onChange={e => setEmail(e.target.value)}
        />

        <FiMail className="agendamento__input-icon" />
      </div>

      <label htmlFor="inputCelular" className="agendamento__label">
        Celular
      </label>

      <div className="agendamento__input-wrapper">
        <InputMask
          type="text"
          id="inputCelular"
          className="agendamento__input"
          placeholder="Digite seu telefone celular"
          maskChar=" "
          mask={telefone.length <= 10 ? '(99) 9999-9999?' : '(99) 99999-9999'}
          formatChars={{ 9: '[0-9]', '?': '[0-9]' }}
          onChange={ev => {
            const brazilianPhone = ev.target.value.replace(/[^0-9]+/g, '');
            setTelefone(brazilianPhone);
          }}
          value={telefone}
        />

        <FiPhone className="agendamento__input-icon" />
      </div>

      <label htmlFor="inputCelular" className="agendamento__label">
        Como conheceu a Colmeia?
      </label>

      <div className="agendamento__input-wrapper">
        <input
          type="text"
          id="inputOrigem"
          className="agendamento__input"
          placeholder="Digite"
          onChange={e => setOrigem(e.target.value)}
        />

        <FiMessageCircle className="agendamento__input-icon" />
      </div>

      <Buttons step={6} {...props} nextStep={validate} />
    </div>
  );
};
