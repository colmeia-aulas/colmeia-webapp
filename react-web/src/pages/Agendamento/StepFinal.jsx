import React, { useState, useEffect } from 'react';
import { FiLock, FiMail } from 'react-icons/fi';

export default props => {
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState(false);
  const submit = async () => {
    if (!password) return alert('Insira uma senha para continuar.');

    try {
      const { email, telefone, nome } = props.form;
      await props.createAccount({ email, telefone, nome, password });

      window.location.href = '/acompanhamento-escolar';
    } catch (error) {
      alert(error.message);
    }
  };

  useEffect(() => {
    if (loading) submit();
  }, [loading]);

  return (
    <div className="container container--xs agendamento__screen">
      <h2 className="agendamento__title">
        Agradecemos por escolher a Colmeia! <br />
      </h2>

      <h5 className="agendamento__title-sub">
        Observação: Note que a sua aula ainda <strong>não</strong> está agendada. Assim que
        selecionarmos o melhor professor para você, entraremos em contato.
      </h5>

      <h4 className="agendamento__title agendamento__title--secondary">
        Nosso sistema de agendamento está escolhendo o melhor professor para você. Em instantes
        entraremos em contato. <br />
        <span className="agendamento__title--bold">
          Que tal agilizar ainda mais o seu agendamento?
        </span>
      </h4>

      <div className="agendamento__form">
        <h3 className="agendamento__form-title">Criar conta na Colmeia</h3>

        <label htmlFor="inputEmailFinal" className="agendamento__label">
          Email
        </label>

        <div className="agendamento__input-wrapper">
          <input
            disabled
            type="email"
            id="inputEmailFinal"
            className="agendamento__input"
            value={props.form.email}
          />

          <FiMail className="agendamento__input-icon" />
        </div>

        <label htmlFor="inputSenha" className="agendamento__label">
          Senha
        </label>

        <div className="agendamento__input-wrapper">
          <input
            type="password"
            id="inputSenha"
            className="agendamento__input"
            placeholder="Senha"
            onChange={e => setPassword(e.target.value)}
          />

          <FiLock className="agendamento__input-icon" />
        </div>

        <div className="agendamento__bottom-button-container">
          {loading ? (
            <img src="/img/logo-laranja.png" alt="Logo Colmeia " className="agendamento__spinner" />
          ) : (
            <button
              onClick={() => setLoading(true)}
              className="agendamento__button agendamento__button--next"
            >
              Quero criar minha conta!
            </button>
          )}
        </div>
        <a href="/" className="agendamento__form-button-final">
          Voltar para página inicial
        </a>
      </div>
    </div>
  );
};
