import React, { useState, useEffect } from 'react';
import StepWizard from 'react-step-wizard';
import axios from 'axios';
import { NavbarStatic } from '../../components/Navbar';
import { FooterSimple } from '../../components/Footer';
import transitions from '../../components/Agendamento/Transitions.css';
import Step1 from './Step1';
import Step2 from './Step2';
import Step3 from './Step3';
import Step4 from './Step4';
import Step5 from './Step5';
import Step6 from './Step6';
import Navigation from '../../components/Agendamento/Navigation';
import StepFinal from './StepFinal';

export default props => {
  const transitionValues = {
    enterRight: `${transitions.animated} ${transitions.enterRight}`,
    enterLeft: `${transitions.animated} ${transitions.enterLeft}`,
    exitRight: `${transitions.animated} ${transitions.exitRight}`,
    exitLeft: `${transitions.animated} ${transitions.exitLeft}`,
    intro: `${transitions.animated} ${transitions.intro}`
  };

  const [form, setForm] = useState({});
  const [SW, setSW] = useState(null);

  const onStepChange = stats => {
    // console.log(stats);
    // console.log(form);
  };

  const updateForm = vals => {
    let newForm = form;

    Object.keys(vals).forEach(v => (form[v] = vals[v]));

    setForm(newForm);
  };

  const setInstance = SW => setSW(SW);

  const submitForm = () => axios.post('/novo-agendamento', { data: form });

  const createAccount = data => axios.post('/aluno/registrar', data);

  return (
    <div className="agendamento">
      <NavbarStatic text="(61) 9 9805-2073" />
      <div className="agendamento__content">
        <StepWizard
          initialStep={1}
          onStepChange={onStepChange}
          isHashEnabled={false}
          // transitions={transitionValues}
          transitions={{}}
          nav={<Navigation />}
          instance={setInstance}
        >
          <Step1 hashKey="passo1" update={updateForm} />
          <Step2 hashKey="passo2" update={updateForm} />
          <Step3 hashKey="passo3" update={updateForm} form={form} />
          <Step4 hashKey="passo4" update={updateForm} />
          <Step5 hashKey="passo5" update={updateForm} />
          <Step6 hashKey="passo6" update={updateForm} form={form} submit={submitForm} />
          <StepFinal hashKey="obrigado" form={form} createAccount={createAccount} />
        </StepWizard>
      </div>
      <FooterSimple />
    </div>
  );
};
