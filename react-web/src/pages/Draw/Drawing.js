export default class Drawing {
  
  constructor(mouseDown = false, canvas, color = 'blue', drawSize = 2) {
    this._mouseDown = mouseDown;
    this._canvas = canvas;
    this._color = color;
    this._drawSize = drawSize;

    this._lastX = 0;
    this._lastY = 0;
    this._x = 0;
    this._y = 0;
    this._dataTrack = null;
    this._text = '';
  }

  set mouseDown(newBoolValue) {
    this._mouseDown = newBoolValue;
  }

  get action() {
    return this._action;
  }

  currAction() {
    return this.action;
  }

  set action(newAction) {
    this._action = newAction;
  }

  get mouseDown() {
    return this._mouseDown;
  }

  get canvas() {
    return this._canvas;
  }

  get canvas2dContext() {
    this._canvas2dContext = this._canvas.getContext('2d');
    this._canvas2dContext.fillStyle = this._color;
    this._canvas2dContext.strokeStyle = this._color;
    this._canvas2dContext.lineWidth = 2 * this._drawSize;
    this._canvas2dContext.globalAlpha = 1;
    return this._canvas2dContext;
  }

  set lastX(newX) {
    this._lastX = newX;
  }

  get lastX() {
    return this._lastX;
  }

  set lastY(newY) {
    this._lastY = newY;
  }

  get lastY() {
    return this._lastY;
  }

  set x(currX) {
    this._x = currX; 
  }

  get x() {
    return this._x;
  }

  set y(currY) {
    this._y = currY; 
  }

  get y() {
    return this._y;
  }

  set dataTrack(localDataTrack) {
    this._dataTrack = localDataTrack;
  }

  get dataTrack() {
    return this._dataTrack;
  }

  clear() {
    const ctx = this.canvas.getContext('2d');
    const { width, height } = ctx.canvas;    
    ctx.clearRect(0, 0, width, height);
  }

  resetCoords() {
    this.lastX = 0;
    this.lastY = 0;
  }

  drawLine() {
    const ctx = this.canvas2dContext;
    const hasOldCoords = this._lastX && this._lastY;
    const isCurrCoordsNew = this._x !== this._lastX || this._y !== this._lastY;
    
    if (hasOldCoords && isCurrCoordsNew) {
      ctx.beginPath();
      ctx.moveTo(this._lastX, this._lastY);
      ctx.lineTo(this._x, this._y);
      ctx.stroke();
    }
    ctx.beginPath();
    ctx.arc(this._x, this._y, this._drawSize, 0, Math.PI * 2, true);
    ctx.closePath();
    ctx.fill();

    this._lastX = this._x;
    this._lastY = this._y;
  }

  highlight() {
    const ctx = this.canvas2dContext;
    ctx.fillStyle = '#fbe25a';
    ctx.globalAlpha = 0.1;
    ctx.beginPath();
    ctx.fillRect(this._x, this._y, 20, 20);
  }

  drawDots() {
    const ctx = this.canvas2dContext;
    ctx.beginPath();
    ctx.arc(this._x, this._y, 2, 0, Math.PI * 2, false);
    ctx.fill();
    ctx.stroke();
  }

  draw() {
    this[this._action]();
  }

  eraserSetup() {
    this.canvas.onmousedown = () => {
      this.mouseDown = true;
    };
    this.canvas.onmouseup = () => {
      this.mouseDown = false;
    };
    this.canvas.onmousemove = (event) => {
      this.erase(event.clientX, event.clientY);
    }
  }

  erase(clientX, clientY) {
    if (!this.mouseDown) return;
    const canvasRect = this.canvas.getBoundingClientRect();
    const x = clientX - canvasRect.left;
    const y = clientY - canvasRect.top;
    const ctx = this.canvas2dContext;

      ctx.clearRect(x, y, 40, 40);
      this.dataTrack.send(JSON.stringify({
        action: 'erase',
          x: x / this.canvas.width,
          y: y / this.canvas.height
      }))
  }

  writeSetup(callback) {
    this.canvas.onclick = (event) => {
      const canvasRect = this.canvas.getBoundingClientRect();
      const x = (event.clientX - canvasRect.left);
      const y = (event.clientY - canvasRect.top);
      this._text = '';

      callback(x, y);
    }
  }

  write(key, x, y) {
    this._text += key;
    const ctx = this.canvas2dContext;
    ctx.font = '20px Arial';
    ctx.fillText(this._text, x, y);

    this.dataTrack.send(JSON.stringify({
      action: 'write',
      string: this._text,
      x: x / this.canvas.width,
      y: y / this.canvas.height
    }));
  }
  
}