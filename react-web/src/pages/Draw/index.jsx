import React, { useEffect, useState, useCallback, useRef } from 'react';
import { useParams } from 'react-router-dom';
import Video from 'twilio-video';
import Drawing from './Drawing';

import './index.css';

function Draw() {
  const [token, setToken] = useState('');
  const [room, setRoom] = useState('');
  const [localDataTrack, setLocalDataTrack] = useState(null);
  const [remoteDataTrack, setRemoteDataTrack] = useState(null);
  const [participants, setParticipants] = useState([]);
  const [action, setAction] = useState('');

  const { id: username } = useParams();
  const roomName = 'soso';

  const canvasRef = useRef();
  const remoteVideoRef = useRef();

  const localDrawing = new Drawing(false, canvasRef.current, 'blue', 2);
  const remoteDrawing = new Drawing(false, canvasRef.current, 'red', 2);

  const getToken = useCallback(() => {
    fetch('/video/token', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        identity: username,
        room: roomName
      })
    })
      .then((data) => data.json())
      .then((result) => setToken(result.token));
  }, []);

  function downloadCanvas(){
    /* const ctx = localDrawing.canvas2dContext;
    ctx.globalCompositeOperation = 'destination-over';
    ctx.fillStyle = 'white';
    ctx.fillRect(0, 0, localDrawing.canvas.width, localDrawing.canvas.height); */

    const link = document.createElement('a');
    link.download = 'canvas.jpg';
    link.href = localDrawing.canvas.toDataURL('image/jpg');
    link.click();
    link.remove();
  }

  function clearCanvas() {
    localDrawing.clear();
    localDataTrack.send(JSON.stringify({
      action: 'clear'
    }));
  }

  function selectAction(event) {
    canvasRef.current.onclick = null;
    window.onkeypress = null;
    canvasRef.current.onmousemove = null;
    localDrawing.canvas.classList.remove('eraser-on');

    const button = event.target;
    const action = button.getAttribute('data-action');
    setAction(action);
  }

  function handleWriting() {
    setAction('');
    localDrawing.canvas.classList.remove('eraser-on');
    canvasRef.current.onmousemove = null;
    canvasRef.current.onmousedown = null;
    canvasRef.current.onmouseup = null;

    localDrawing.dataTrack = localDataTrack;
    localDrawing.writeSetup((x, y) => {
      window.onkeypress = (event) => {
        localDrawing.write(event.key, x, y);
      }
    });
  }

  function handleErasing() {
    if (localDrawing.canvas.classList.contains('eraser-on')) {
      localDrawing.canvas.classList.remove('eraser-on');
      canvasRef.current.onmousemove = null;
      canvasRef.current.onmousedown = null;
      canvasRef.current.onmouseup = null;
      window.onkeypress = null;
      return;
    }

    setAction('');
    window.onkeypress = null;
    canvasRef.current.onclick = null;
    localDrawing.dataTrack = localDataTrack;
    localDrawing.canvas.classList.add('eraser-on');
    localDrawing.eraserSetup();
  }

  useEffect(() => {
    if (!token) return getToken();

    const dataTrack = new Video.LocalDataTrack();
    Video.connect(token, {
      name: roomName,
      video: false,
      audio: false,
      tracks: [dataTrack]
    })
      .then((room) => {
        setRoom(room);
        setLocalDataTrack(dataTrack);
      });
  }, [token]);

  useEffect(() => {
    if (!room) return;
    function registerParticipant(participant) {
      setParticipants((participants) => [...participants, participant]);
    }
    function unregisterParticipant(participant) {
      setParticipants((participants) => participants.filter(p => p !== participant));
    }

    registerParticipant(room.localParticipant);
    room.participants.forEach(registerParticipant);
    room.on('participantConnected', registerParticipant);
    room.on('participantDisconnected', unregisterParticipant);

    /* Video.createLocalVideoTrack().then((stream) => {
      room.localParticipant.publishTrack(stream);
      console.log('Videotrack', stream);
    }).catch((error) => console.log(error)); */
  }, [room]);

  useEffect(() => {
    if (!localDataTrack) return;

    const drawing = localDrawing;

    canvasRef.current.onmousedown = () => {
      drawing.mouseDown = true;
    };
  
    canvasRef.current.onmouseup = () => {
      drawing.mouseDown = false;
      drawing.resetCoords();

      localDataTrack.send(JSON.stringify({
        action: 'reset',
      }));
    };
  
    canvasRef.current.onmousemove = (event) => {
      const canvasRect = drawing.canvas.getBoundingClientRect();
      
      drawing.x = (event.clientX - canvasRect.left);
      drawing.y = (event.clientY - canvasRect.top);
  
      if (drawing.mouseDown && action) {
        drawing.action = action;
        drawing.draw();
  
        localDataTrack.send(JSON.stringify({
          action: 'draw',
          fn: drawing.action,
          mouseCoords: {
            x: drawing.x / drawing.canvas.width,
            y: drawing.y / drawing.canvas.height
          }
        }));
      }
    };
  }, [localDataTrack, action]);

  useEffect(() => {
    if (!remoteDataTrack) return;

    remoteDataTrack.on('message', (data) => {
      const message = JSON.parse(data);

      switch(message.action) {
        case 'clear':
          remoteDrawing.clear();
          break;
        case 'reset':
          remoteDrawing.resetCoords();
          break;
        case 'draw':
          remoteDrawing.x = message.mouseCoords.x * remoteDrawing.canvas.width;
          remoteDrawing.y = message.mouseCoords.y * remoteDrawing.canvas.height;
          remoteDrawing.action = message.fn;
          remoteDrawing.draw();
          break;
        case 'write':
          const remoteX1 = message.x * remoteDrawing.canvas.width;
          const remoteY1 = message.y * remoteDrawing.canvas.height;

          const ctx1 = remoteDrawing.canvas2dContext;
          ctx1.font = '20px Arial';
          ctx1.fillText(message.string, remoteX1, remoteY1);
          break;
        case 'erase':
          const remoteX2 = message.x * remoteDrawing.canvas.width;
          const remoteY2 = message.y * remoteDrawing.canvas.height;

          const ctx2 = remoteDrawing.canvas2dContext;
          ctx2.clearRect(remoteX2, remoteY2, 40, 40);
          break;
      }
    });
  }, [remoteDataTrack]);

  useEffect(() => {
    if (!participants.length) return;
    
    function registerTrack(track) {
      if (track.kind === 'data') return setRemoteDataTrack(track);
      if (track.kind === 'video') {
        track.attach(remoteVideoRef.current);
      }
    }

    participants.forEach((participant) => {
      participant.on('trackSubscribed', registerTrack);
    });

  }, [participants]);

  return (
    <>
      <div className="header">
        <div>USUÁRIO: {username}</div>
        <button onClick={clearCanvas}>Limpar canvas</button>
        <button data-action="drawDots" onClick={selectAction}>Linha pontinhada</button>
        <button data-action="drawLine" onClick={selectAction}>Linha contínua</button>
        <button data-action="highlight" onClick={selectAction}>Destacar</button>
        <button data-action="text" onClick={handleWriting}>Escrever</button>
        <button data-action="erase" onClick={handleErasing}>Apagar</button>
        <button data-action="download" onClick={downloadCanvas}>Baixar canvas</button>

      </div>
      <img
        className="test-background"
        src="https://www.oficinadanet.com.br/imagens/post/25773/windowsxp-hack_1400x875_5cdec2c884345.jpg"
        width={window.innerWidth * 0.6}
        height={window.innerHeight * 0.9}
      />
      <canvas
        ref={canvasRef}
        height={window.innerHeight * 0.9}
        width={window.innerWidth * 0.6}
      ></canvas>
      {/* <video
        ref={remoteVideoRef}
        height={window.innerHeight * 0.9}
        width={window.innerWidth * 0.6}
      >
      </video> */}
    </>
  )
}

export default Draw;