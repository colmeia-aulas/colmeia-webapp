import React, { useState } from 'react';

const IDIOMAS = ['Inglês', 'Espanhol'];
const NIVEIS = ['Básico', 'Intermediário', 'Avançado'];

const ToggleButton = props => {
  const { children, ...otherProps } = props;

  return (
    <label className="agendamento__toggle-button">
      <input {...otherProps} type="checkbox" />
      <button className="agendamento__toggle-button-content">{children}</button>
    </label>
  );
};

export default props => {
  const validate = () => {
    if (niveis.length === 0) {
      alert('Por favor, selecione idiomas e níveis para continuar.');
    } else {
      props.update({ idiomas: niveis });
      props.nextStep();
    }
  };

  const [idiomas, setIdiomas] = useState([]);
  const [niveis, setNiveis] = useState([]);

  const handleIdiomasClick = e => {
    setIdiomas(
      Array.from(document.querySelectorAll('[name="check-idiomas"]:checked')).map(e => e.value)
    );
  };

  const handleNivelClick = e => {
    const newNiveis = idiomas.map(e => ({
      idioma: e,
      niveis: Array.from(document.querySelectorAll(`[name="check-niveis-${e}"]:checked`)).map(
        e => e.value
      )
    }));

    setNiveis(newNiveis);
  };

  const renderNiveis = () =>
    IDIOMAS.filter(e => idiomas.includes(e)).map(e => (
      <>
        <label htmlFor="" className="agendamento__label agendamento__label--center">
          Como você avalia seu nível de {e}?
        </label>
        <div className="agendamento__toggle-button-container agendamento__toggle-button-container--center">
          {NIVEIS.map(nivel => (
            <ToggleButton
              key={nivel}
              value={nivel}
              name={`check-niveis-${e}`}
              onChange={e => handleNivelClick(e.target.value)}
            >
              {nivel}
            </ToggleButton>
          ))}
        </div>
      </>
    ));

  const renderButtons = () => (
    <div className="container container--s">
      <div className="agendamento__bottom-button-container">
        <button className="agendamento__button agendamento__button--next" onClick={validate}>
          Continuar
        </button>
      </div>
    </div>
  );

  return (
    <div className="container container--xs agendamento__screen">
      <h2 className="agendamento__title">
        Bem-vindo(a) ao agendamento <br />
        de idiomas da Colmeia.
      </h2>
      <h2 className="agendamento__title">
        Você gostaria de ter aulas de qual idioma? <br />
        <p className="agendamento__info">(Você pode escolher mais de um)</p>
      </h2>
      <div className="agendamento__toggle-button-container agendamento__toggle-button-container--center">
        {IDIOMAS.map(dia => (
          <ToggleButton key={dia} value={dia} name="check-idiomas" onChange={handleIdiomasClick}>
            {dia}
          </ToggleButton>
        ))}
      </div>

      {renderNiveis()}

      {niveis.length > 0 && renderButtons()}
    </div>
  );
};
