import React, { useState } from 'react';

const HORARIOS = ['Manhã', 'Almoço', 'Pós expediente'];

const ToggleButton = props => {
  const { children, ...otherProps } = props;

  return (
    <label className="agendamento__toggle-button">
      <input {...otherProps} type="checkbox" />
      <button className="agendamento__toggle-button-content">{children}</button>
    </label>
  );
};

export default props => {
  const validate = () => {
    if (horarios.length === 0) {
      alert('Por favor, selecione os horários para continuar.');
    } else {
      props.update({ horarios });
      props.nextStep();
    }
  };

  const [horarios, setHorarios] = useState([]);

  const handleHorariosClick = e => {
    setHorarios(
      Array.from(document.querySelectorAll('[name="check-horarios"]:checked')).map(e => e.value)
    );
  };

  const renderButtons = () => (
    <div className="container container--s">
      <div className="agendamento__bottom-button-container">
        <button
          className="agendamento__button agendamento__button--previous"
          onClick={props.previousStep}
        >
          Voltar
        </button>
        <button className="agendamento__button agendamento__button--next" onClick={validate}>
          Continuar
        </button>
      </div>
    </div>
  );

  return (
    <div className="container container--xs agendamento__screen">
      <h2 className="agendamento__title">E qual a sua disponibilidade para as aulas?</h2>
      <label htmlFor="" className="agendamento__label agendamento__label--center">
        Quais os turnos? (Você pode escolher mais de um)
      </label>
      <div className="agendamento__toggle-button-container agendamento__toggle-button-container--center">
        {HORARIOS.map(horario => (
          <ToggleButton
            key={horario}
            value={horario}
            name="check-horarios"
            onChange={handleHorariosClick}
          >
            {horario}
          </ToggleButton>
        ))}
      </div>
      {renderButtons()}
    </div>
  );
};
