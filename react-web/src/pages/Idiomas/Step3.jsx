import React, { useState, useEffect } from 'react';
import InputMask from 'react-input-mask';
import { FiUser, FiMail, FiPhone, FiBriefcase } from 'react-icons/fi';

export default props => {
  const [nome, setNome] = useState('');
  const [email, setEmail] = useState('');
  const [telefone, setTelefone] = useState('');
  const [empresa, setEmpresa] = useState('');

  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (loading) validate();
  }, [loading]);

  const validate = async () => {
    if (!nome || !email || !telefone || !empresa) {
      alert('Por favor, preencha todos os campos para continuar.');
    } else {
      props.update({ nome, email, telefone: `+55${telefone}`, empresa });
      try {
        const { data } = await props.submit();

        if (data.success) {
          props.nextStep();
        } else {
          alert(data.message);
        }
      } catch (error) {
        alert('Houve um erro ao realizar seu agendamento.');
      }
    }
  };

  const renderButtons = () => (
    <div className="container container--s">
      <div className="agendamento__bottom-button-container">
        {loading ? (
          <img src="/img/logo-laranja.png" alt="Logo Colmeia " className="agendamento__spinner" />
        ) : (
          <>
            <button
              className="agendamento__button agendamento__button--previous"
              onClick={props.previousStep}
            >
              Voltar
            </button>
            <button
              className="agendamento__button agendamento__button--next"
              onClick={() => setLoading(true)}
            >
              Finalizar
            </button>
          </>
        )}
      </div>
    </div>
  );

  return (
    <div className="container container--xs agendamento__screen">
      <h2 className="agendamento__title">
        Pronto! Agora, faça seu cadastro para que encontremos o melhor professor particular para
        você!
      </h2>
      <label htmlFor="inputNome" className="agendamento__label">
        Nome
      </label>

      <div className="agendamento__input-wrapper">
        <input
          type="text"
          id="inputNome"
          className="agendamento__input"
          placeholder="Digite seu nome"
          onChange={e => setNome(e.target.value)}
        />

        <FiUser className="agendamento__input-icon" />
      </div>

      <label htmlFor="inputEmail" className="agendamento__label">
        Email
      </label>

      <div className="agendamento__input-wrapper">
        <input
          type="email"
          id="inputEmail"
          className="agendamento__input"
          placeholder="Digite seu email"
          onChange={e => setEmail(e.target.value)}
        />

        <FiMail className="agendamento__input-icon" />
      </div>

      <label htmlFor="inputCelular" className="agendamento__label">
        Celular
      </label>

      <div className="agendamento__input-wrapper">
        <InputMask
          type="text"
          id="inputCelular"
          className="agendamento__input"
          placeholder="Digite seu telefone celular"
          maskChar=" "
          mask={telefone.length <= 10 ? '(99) 9999-9999?' : '(99) 99999-9999'}
          formatChars={{ 9: '[0-9]', '?': '[0-9]' }}
          onChange={ev => {
            const brazilianPhone = ev.target.value.replace(/[^0-9]+/g, '');
            setTelefone(brazilianPhone);
          }}
          value={telefone}
        />

        <FiPhone className="agendamento__input-icon" />
      </div>

      <label htmlFor="inputCelular" className="agendamento__label">
        Empresa
      </label>

      <div className="agendamento__input-wrapper">
        <input
          type="text"
          id="inputEmpresa"
          className="agendamento__input"
          placeholder="Digite o nome da sua empresa"
          onChange={e => setEmpresa(e.target.value)}
        />

        <FiBriefcase className="agendamento__input-icon" />
      </div>

      {renderButtons()}
    </div>
  );
};
