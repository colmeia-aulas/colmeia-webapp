import React from 'react';

export default ({ form }) => (
  <div className="container container--xs agendamento__screen">
    <h2 className="agendamento__title">Agradecemos por escolher a Colmeia, {form.nome}!</h2>

    <h4 className="agendamento__title agendamento__title--secondary">
      Nossa equipe entrará em contato com você em breve com os detalhes das suas aulas de{' '}
      {form.idiomas && form.idiomas.map(e => e.idioma).join(' e ')}.
    </h4>

    <div className="container container--s">
      <div className="agendamento__bottom-button-container">
        <a
          href="https://aulascolmeia.com.br"
          className="agendamento__button agendamento__button--next agendamento__button--full"
        >
          Página inicial
        </a>
      </div>
    </div>
  </div>
);
