import React, { useState } from 'react';
import StepWizard from 'react-step-wizard';
import axios from 'axios';
import { NavbarStatic } from '../../components/Navbar';
import { FooterSimple } from '../../components/Footer';
import Navigation from '../../components/Agendamento/Navigation';
import Step1 from './Step1';
import Step2 from './Step2';
import Step3 from './Step3';
import StepFinal from './StepFinal';

export default () => {
  const [form, setForm] = useState({});

  const onStepChange = stats => {
    // console.log(stats);
    // console.log(form);
  };

  const updateForm = vals => {
    let newForm = form;

    Object.keys(vals).forEach(v => (form[v] = vals[v]));

    setForm(newForm);
  };

  const submitForm = () => axios.post('/novo-agendamento-idiomas', { data: form });

  return (
    <div className="agendamento">
      <NavbarStatic text="(61) 9 9805-2073" />
      <div className="agendamento__content">
        <StepWizard
          initialStep={1}
          onStepChange={onStepChange}
          isHashEnabled={false}
          // transitions={transitionValues}
          transitions={{}}
          nav={<Navigation />}
        >
          <Step1 hashKey="passo1" update={updateForm} form={form} />
          <Step2 hashKey="passo2" update={updateForm} form={form} />
          <Step3 hashKey="passo3" update={updateForm} form={form} submit={submitForm} />
          <StepFinal form={form} />
        </StepWizard>
      </div>
      <FooterSimple />
    </div>
  );
};
