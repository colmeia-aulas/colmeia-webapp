import React from 'react';
import PropTypes from 'prop-types';

function Lobby({ username, handleUsernameChange, handleSubmit }) {
  return (
    <form onSubmit={handleSubmit} className="twilio-login">
      <h3>Acessar sala de aula</h3>
      <div className="user">
        <label htmlFor="name">Digite seu nome abaixo</label>
        <input
          type="text"
          id="name"
          placeholder="Escreva seu nome"
          value={username}
          onChange={handleUsernameChange}
          required
        />
      </div>
      <button type="submit"><span>Entrar na sala</span><img src="/img/aulaonline-login-icon.png" alt="ícone para logar" /></button>
    </form>
  );
}

export default Lobby;

Lobby.propTypes = {
  username: PropTypes.string.isRequired,
  handleUsernameChange: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
}
