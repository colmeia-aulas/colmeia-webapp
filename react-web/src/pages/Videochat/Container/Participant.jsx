import React, { useState, useEffect } from 'react';

function Participant({ participant, videoRef, audioRef, setLocalVideoTrack, setLocalAudioTrack, localContainerRef, roomRef }) {
  const [videoTracks, setVideoTracks] = useState([]);
  const [audioTracks, setAudioTracks] = useState([]);

  function exitFullscreen() {
    document.exitFullscreen();
  }

  function enterFullscreen() {
    if (roomRef.current.requestFullscreen) {
      roomRef.current.requestFullscreen();
    } else if (roomRef.current.mozRequestFullScreen) { /* Firefox */
      roomRef.current.mozRequestFullScreen();
    } else if (roomRef.current.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
      roomRef.current.webkitRequestFullscreen();
    } else if (roomRef.current.msRequestFullscreen) { /* IE/Edge */
      roomRef.current.msRequestFullscreen();
    }
  }

  function handleFullscreen() {
    if (document.fullscreen) {
      return exitFullscreen();
    }

    enterFullscreen();
  }

  const trackpubsToTracks = trackMap => Array.from(trackMap.values())
    .map(publication => publication.track)
    .filter(track => track !== null);

  useEffect(() => {
    const trackSubscribed = track => {
      if (track.kind === 'data') {
        track.on('message', (data) => {
          console.dir(data);
        });
      }

      if (track.kind === 'video') {

        if (track.name === 'screen') {
          videoRef.current.classList.add('is-screen-share');
        }

        setVideoTracks(videoTracks => [...videoTracks, track]);
      } else if (track.kind === 'audio') {
        setAudioTracks(audioTracks => [...audioTracks, track]);
      }
    };

    const trackUnsubscribed = track => {
      if (track.kind === 'video') {
        console.log(track);

        if (track.name === 'screen') {
          videoRef.current.classList.remove('is-screen-share');
        }

        setVideoTracks(videoTracks => videoTracks.filter(v => v !== track));
      } else if (track.kind === 'audio') {
        setAudioTracks(audioTracks => audioTracks.filter(a => a !== track));
      }
    };

    setVideoTracks(trackpubsToTracks(participant.videoTracks));
    setAudioTracks(trackpubsToTracks(participant.audioTracks));

    participant.on('trackSubscribed', trackSubscribed);
    participant.on('trackUnsubscribed', trackUnsubscribed);

    return () => {
      setVideoTracks([]);
      setAudioTracks([]);
      participant.removeAllListeners();
    };
  }, [participant]);

  useEffect(() => {
    const videoTrack = !!videoTracks && !!videoTracks.length && videoTracks[videoTracks.length - 1];
    setLocalVideoTrack && setLocalVideoTrack(videoTrack);
    if (videoTrack) {
      videoTrack.attach(videoRef.current);
      return () => {
        videoTrack.detach();
      };
    }
  }, [videoTracks]);

  useEffect(() => {
    const audioTrack = audioTracks[0];
    setLocalAudioTrack && setLocalAudioTrack(audioTrack);
    if (audioTrack) {
      audioTrack.attach(audioRef.current);
      return () => {
        audioTrack.detach();
      };
    }
  }, [audioTracks]);

  return (
    <div className="participant-container" ref={localContainerRef}>
      <button className="fullscreen-btn" onClick={handleFullscreen}>
        <img src="/img/aulaonline-fullscreen-icon.png" alt="ícone para deixar a vídeo em tela cheia" />
      </button>
      <video ref={videoRef} autoPlay muted/>
      <audio ref={audioRef} autoPlay />
    </div>
  );
};

export default Participant;