import React, { memo } from 'react';

function Message({ username, chatIconRef, msg, showChat }) {
  const { author, body, url } = msg;
  const isLocalMsg = author === username;
  const infoMsg = typeof msg === 'string';

  if (!showChat) {
    chatIconRef.current.src = '/img/aulaonline-chatpush-icon.png';
  }

  if (url) {
    return (
      <p className="msg">
        <span>
          <a href={url} target="_blank" className="infomsg">{author} enviou um arquivo!</a>
        </span>
      </p>
    );
  }

  if (body) {
    return (
      <p className="msg">
        <span className={`author-${isLocalMsg ? 'me' : 'other'}`}>
          {author + ': '}
        </span>
        {body}
      </p>
    );
  }

  if (infoMsg) {
    return (
      <p className="msg">
        <span className="infomsg">
          {msg}
        </span>
      </p>
    );
  }

}

export default memo(Message);
