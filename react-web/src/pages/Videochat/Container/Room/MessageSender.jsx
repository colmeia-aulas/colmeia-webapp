import React, { memo } from 'react';

// chatMsg, setChatMsg, handleMsg, handleFile

function MessageSender({ chatMsg, setChatMsg, handleFile, handleMsg }) {

  return (
    <div className="messages__sender">
      <input
        type="text"
        value={chatMsg}
        placeholder="Escreva sua mensagem aqui."
        onChange={(event) => setChatMsg(event.target.value)}
        onKeyPress={(event) => {
          if (event.key === 'Enter') {
            handleMsg();
          }
        }}
      />
      <label
        htmlFor="file-upload"
      >
        <img
          className="file-upload-img"
          src="https://www.nicepng.com/png/detail/188-1882728_send-file-send-file-icon-png.png"
          alt="enviar arquivo"
        />
      </label>
      <input
        id="file-upload"
        type="file"
        style={{ 'display': 'none' }}
        onChange={handleFile}
      >
      </input>
      <button type="button" onClick={handleMsg}></button>
    </div>
  );
}

export default memo(MessageSender);