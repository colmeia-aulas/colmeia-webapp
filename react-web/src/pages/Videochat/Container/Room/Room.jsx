import React, { useState, useEffect, useRef, Fragment } from 'react';
import Video, { LocalVideoTrack, LocalDataTrack } from 'twilio-video';
import * as Chat from 'twilio-chat';

import Participant from '../Participant';
import Message from './Message';
import MessageSender from './MessageSender';

function Room({ roomName, token, handleLogout, username }) {
  const [room, setRoom] = useState(null);
  const [participants, setParticipants] = useState([]);
  const [popupMessage, setPopupMessage] = useState('');
  const [chatAlert, setChatAlert] = useState('');
  const [localVideoTrack, setLocalVideoTrack] = useState(null);
  const [localAudioTrack, setLocalAudioTrack] = useState(null);
  const [chatPermission, setChatPermission] = useState(null);
  const [chatMsg, setChatMsg] = useState('');
  const [chatMsgs, setChatMsgs] = useState([]);
  const [showChat, setShowChat] = useState(false);
  const [screenShare, setScreenShare] = useState(null);
  const [isScreenShareOn, toggleScreenShare] = useState(false);
  const [isRoomFullscreen, toggleRoomFullscreen] = useState(document.fullscreen);
  const [fsPopup, showFsPopup] = useState(false);

  const portrait = window
    .matchMedia('(max-width: 630px)');
  const landscape = window
    .matchMedia('only screen and (orientation: landscape) and (max-width: 850px)');

  const videoRef = useRef();
  const audioRef = useRef();
  const remoteVideoRef = useRef();
  const remoteAudioRef = useRef();
  const msgRef = useRef();
  const chatIconRef = useRef();
  const cameraIconRef = useRef();
  const micIconRef = useRef();
  const screenShareIconRef = useRef();
  const localContainerRef = useRef();
  const roomRef = useRef();
  const fsPopupRef = useRef();

  let isVideoOn = true;
  let isAudioOn = true;
  
  let chatReadyMsg = 'Chat pronto para uso';

  const remoteParticipants = participants.length
    ? participants.map(participant => {
        return <Participant
          key={participant.sid}
          participant={participant}
          videoRef={remoteVideoRef}
          audioRef={remoteAudioRef}
          roomRef={roomRef}
        />
      })
    : <div className="remote-participant-background">
        <img src="/img/aulaonline-remoteparticipant-icon.png" alt="ícone provisório do usuário" />
      </div>; 

  async function handleScreenshare() {
    if (isScreenShareOn) {
      screenShare.stop();
      room.localParticipant.unpublishTrack(screenShare);
      playVideo();
      screenShareIconRef.current.src = '/img/aulaonline-screenshare-icon.png';
      toggleScreenShare(false);
    } else {
      const stream = await navigator.mediaDevices.getDisplayMedia();
      const screenTrack = new LocalVideoTrack(stream.getTracks()[0], { name: 'screen' });
      console.log(stream.getTracks()[0])
      setScreenShare(screenTrack);
      room.localParticipant.publishTrack(screenTrack);
      screenTrack.attach(videoRef.current);
      screenShareIconRef.current.src = '/img/aulaonline-screenshareon-icon.png';
      toggleScreenShare(true);
    }

  }

  function enterFullscreen() {
    if (roomRef && roomRef.current && roomRef.current.requestFullscreen) {
      roomRef.current.requestFullscreen();
    } else if (roomRef.current.mozRequestFullScreen) { /* Firefox */
      roomRef.current.mozRequestFullScreen();
    } else if (roomRef.current.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
      roomRef.current.webkitRequestFullscreen();
    } else if (roomRef.current.msRequestFullscreen) { /* IE/Edge */
      roomRef.current.msRequestFullscreen();
    }

    showFsPopup(false)
  }

  function handlePopup(msg) {
    setPopupMessage(msg);
    setTimeout(() => { setPopupMessage(); }, 2000);
  }

  function handleChatSupportPopup(msg) {
    setChatAlert(msg);
  }

  function pauseVideo() {
    videoRef.current.style.display = 'none';
    isVideoOn = false;
    console.log(localVideoTrack)
    localVideoTrack && localVideoTrack.disable();
    localVideoTrack.detach();
    cameraIconRef.current.src = '/img/aulaonline-camerablocked-icon.png';
    localContainerRef.current.style.minWidth = '200px';
  }

  function playVideo() {
    videoRef.current.style.display = 'unset';
    isVideoOn = true;
    console.log(localVideoTrack)
    localVideoTrack && localVideoTrack.enable();
    room.localParticipant.publishTrack(localVideoTrack);
    localVideoTrack.attach(videoRef.current);
    cameraIconRef.current.src = '/img/aulaonline-camera-icon.png';
    localContainerRef.current.style.minWidth = 'unset';
  }

  function handleVideoToggle() {
    if (isVideoOn) {
      return pauseVideo();
    }
    playVideo();
  }

  function handleAudioToggle() {
    if (isAudioOn) {
      isAudioOn = false;
      micIconRef.current.src = '/img/aulaonline-micblocked-icon.png';
      localAudioTrack && localAudioTrack.stop();
      console.dir(localAudioTrack);
      localAudioTrack && room.localParticipant.unpublishTrack(localAudioTrack);
      setLocalAudioTrack(null);
    } else {
      Video.createLocalAudioTrack().then((stream) => {
        isAudioOn = true;
        micIconRef.current.src = '/img/aulaonline-mic-icon.png';
        setLocalAudioTrack(stream);
        room.localParticipant.publishTrack(stream);
        stream.attach(audioRef.current);
      })
    }
  }

  async function handleChatRequest() {

    if (landscape.matches) {
      handleChatSupportPopup('Por favor, posicione seu celular na posição horizontal para acessar o chat');
    } else {
      chatIconRef.current.src = '/img/aulaonline-chat-icon.png'
      setShowChat(!showChat);
    }
  }

  function handleMsg() {
    console.log('entrou');
    console.log(chatPermission);
    if (chatPermission && chatMsg.trim()) {
      chatPermission.sendMessage(chatMsg);
    }
  }

  function handleFile(event) {
    if (chatPermission && event.target.files[0]) {
      const formData = new FormData();
      formData.append('file', event.target.files[0]);
      chatPermission.sendMessage(formData);
      event.target.value = '';
    }
  }

  function toggleVideo(event) {
    const img = event.target;
    const currentVideo = img.nextSibling;
    let currentVideoStyle = currentVideo.style;

    if (currentVideoStyle.display === 'none') {
      currentVideoStyle.display = 'block';
      img.src="/img/aulaonline-minimizeparticipant-icon.png";
      img.className="minimize-icon";
    } else {
      currentVideoStyle.display = 'none';
      img.src = '/img/arrow-down-dark.png';
      img.className="maximize-icon"
    }
  }

  useEffect(() => {
    document.querySelector('.conversations').scrollTo({ top: document.querySelector('.conversations').scrollHeight, behavior: 'smooth' });
    console.log('scroll')
  }, [chatMsgs])
  
  useEffect(() => {
    const participantConnected = participant => {
      setParticipants(prevParticipants => [...prevParticipants, participant]);
    };
    const participantDisconnected = participant => {
      handlePopup(`${participant.identity} saiu na sala`);
      setParticipants(prevParticipants =>
        prevParticipants.filter(p => p !== participant)
      );
    };

    document.documentElement.style.overflow = 'hidden';
    document.body.style.overflow = 'auto';

    function scrollTopWhenPortrait(event = undefined) {
      if (window.matchMedia('(orientation: portrait)').matches) {
        window.scrollTo({ top: 0 });
      }
    }

    scrollTopWhenPortrait();

    function setUpVideo() {
      const localDataTrack = new LocalDataTrack();
      /* window.addEventListener('mousemove', function(event) {
        localDataTrack.send('Olá');
      }); */

      Video.connect(token, {
        name: roomName,
        audio: false,
        video: false,
        tracks: [localDataTrack]
      }).then(room => {
        setRoom(room);
        handlePopup(`Seja bem vindo (a)`);
        room.on('participantConnected', (participant) => {
          handlePopup(`${participant.identity} entrou na sala`);
          participantConnected(participant);
        });
        room.on('participantDisconnected', participantDisconnected);
        room.participants.forEach(participantConnected);
  
        roomRef.current.onfullscreenchange = (event) => {
          toggleRoomFullscreen(document.fullscreen);
        }
  
        const mediaQuery = window.matchMedia("(max-width: 630px)");
        function handleMobile(mediaQuery) {

        }
        mediaQuery.addListener(handleMobile);

        navigator.mediaDevices.enumerateDevices()
          .then((devices) => {
            const isCameraAvailable = devices.some((device) => device.kind === 'videoinput');
            const isAudioAvailable = devices.some((device) => device.kind === 'audioinput');

            if (isCameraAvailable) {
              Video.createLocalVideoTrack().then((stream) => {
                setLocalVideoTrack(stream);
                room.localParticipant.publishTrack(stream);
                stream.attach(videoRef.current);
              });
            }

            if (isAudioAvailable) {
              Video.createLocalAudioTrack().then((stream) => {
                setLocalAudioTrack(stream);
                room.localParticipant.publishTrack(stream);
                stream.attach(audioRef.current);
              })
            }

            if (!navigator.mediaDevices.ondevicechange) {
              navigator.mediaDevices.ondevicechange = (event) => {
                console.log('DEVICE CHANGE', event);
                navigator.mediaDevices.enumerateDevices()
                  .then((devices) => {
                    const isCameraAvailable = devices.some((device) => device.kind === 'videoinput');
                    const isAudioAvailable = devices.some((device) => device.kind === 'audioinput');

                    if (isCameraAvailable) {
                      Video.createLocalVideoTrack().then((stream) => {
                        setLocalVideoTrack(stream);
                        room.localParticipant.publishTrack(stream);
                        stream.attach(videoRef.current);
                      });
                    } else {
                      localVideoTrack && localVideoTrack.stop();
                      room && room.localParticipant.unpublishTrack(localVideoTrack);
                    }

                    if (isAudioAvailable) {
                      Video.createLocalAudioTrack().then((stream) => {
                        setLocalAudioTrack(stream);
                        room.localParticipant.publishTrack(stream);
                        stream.attach(audioRef.current);
                      })
                    } else {
                      localAudioTrack && localAudioTrack.stop();
                      room && room.localParticipant.unpublishTrack(localAudioTrack);
                    }
                  })
              }
            }
          })
          .catch((error) => {
            console.error(error);
          })
      })
        .catch((err) => {
          console.error(err);
          if (err.message.includes('Permission denied')) {
            alert('Câmera ou microfone se encontram bloqueados!\nPor favor, habilite esses recursos e atualize a página');
          }
        })
    }

    setUpVideo();

    // Requisitando acesso a chat
    Chat.Client.create(token).then((client) => {
      function chatEvents(chat, event) {
        const events = {
          memberJoined: () => {
            chat.on('memberJoined', (member) => {
              console.log(member);
            });
          },

          messageAdded: () => {
            chat.on('messageAdded', (message) => {
              console.log(message);
              const { author, body, sid } = message;
              
              if (message.type !== 'media') {
                setChatMsgs((previousMsgs) => [...previousMsgs, { author, body, sid }]);
                author === username && setChatMsg('');
              } else {
                message.media.getContentUrl().then((url) => {
                  console.log(url);
                  setChatMsgs((previousMsgs) => [...previousMsgs, { author, url, sid }]);
                  author === username && setChatMsg('');
                })
              }
  
            });
          },
        };

        events[event]();
      }

      function join(client) {
        if (client.status != 'joined') {
          client.join().then(() => {
            console.log(client);
            chatEvents(client, 'memberJoined');
            chatEvents(client, 'messageAdded');

            setChatPermission(client);
            setChatMsgs([chatReadyMsg]);
          }).catch((error) => console.log(error));
        } else {
          chatEvents(client, 'messageAdded');

          setChatPermission(client);
          setChatMsgs([chatReadyMsg]);
        }
      }

      client.getChannelByUniqueName(roomName).then(join).catch(() => {
        client.createChannel({ uniqueName: roomName, friendlyName: 'Chat Colmeia'})
          .then(join)
          .catch((error) => console.log(error));
      });
    });

    function endSession() {
      document.documentElement.style.overflow = '';
      document.body.style.overflow = '';

      setRoom(currentRoom => {
        const isConnected = currentRoom && currentRoom.localParticipant.state === 'connected';

        if (isConnected) {
          currentRoom.localParticipant.tracks.forEach(function(trackPublication) {
            trackPublication.track.stop();
            currentRoom.localParticipant.unpublishTrack(trackPublication.track);
          });
          currentRoom.disconnect();
          return null;
        } else {
          return currentRoom;
        }
      });

      setChatPermission((chatPermission) => {
        chatPermission.leave();
        return null;
      });
    }

    window.addEventListener('unload', (event) => {
      endSession();
      delete event['returnValue'];
    });

    window.addEventListener('pagehide', (event) => {
      endSession();
      delete event['returnValue'];
    });

    return () => {
      endSession();
    };
  }, [roomName, token]);

  return (
    <div
      className="twilio-room"
      ref={roomRef}
      style={
        isRoomFullscreen
        ? { 'gridTemplateRows': '100vh' }
        : { 'gridTemplateRows': 'calc(100vh - 104px)' }
      }
    >
      <div
        className="popup-message"
        onClick={() => setPopupMessage()}
        style={{ 'visibility': popupMessage ? 'visible' : 'hidden' }}
      >
        {popupMessage}
      </div>
      <div
        className="popup-message"
        onClick={() => setChatAlert()}
        style={{ 'visibility': chatAlert ? 'visible' : 'hidden' }}
      >
        {chatAlert}
      </div>
      <aside className="twilio-options">
        <div className="features">
          <button onClick={handleVideoToggle} className="video-btn">
            <img
              ref={cameraIconRef}
              src="/img/aulaonline-camera-icon.png"
              alt="ícone para permitir uso da câmera"
            />
            <span>Vídeo</span>
          </button>
          <button onClick={handleAudioToggle} className="mic-btn">
            <img
              ref={micIconRef}
              src="/img/aulaonline-mic-icon.png"
              alt="ícone para permitir uso do microfone"
            />
            <span>Microfone</span>
          </button>
          <button onClick={handleScreenshare} className="screen-btn">
            <img
              ref={screenShareIconRef}
              src="/img/aulaonline-screenshare-icon.png"
              alt="ícone para compartilhar a tela com quem te assite"
            />
            <span>Compartilhar a tela</span>
          </button>
          <button onClick={handleChatRequest} className="chat-btn">
            <img
              ref={chatIconRef}
              src="/img/aulaonline-chat-icon.png"
              alt="ícone para chat de mensagens e compartilhamento de arquivos"
            />
            <span>Chat</span>
          </button>
        </div>
        <div className="exit">
          <button onClick={handleLogout}>
            <div className="exit-img"></div>
            <span>Sair da sala</span>
          </button>
        </div>
      </aside>
      <aside className={`twilio-screen ${ showChat ? '' : 'twilio-screen-no-chat'}`}>
        <div className="remote-participants">{remoteParticipants}</div>
        <div className="local-participant"
          style={ showChat ? { 'right': '24%' } : {} }
        >
          {room &&
            <Fragment>
              <img
                src="/img/aulaonline-minimizeparticipant-icon.png"
                alt="ícone para minimizar seu vídeo"
                className="minimize-icon"
                onClick={toggleVideo}
              />
              <Participant
                key={room.localParticipant.sid}
                participant={room.localParticipant}
                videoRef={videoRef}
                audioRef={audioRef}
                setLocalVideoTrack={setLocalVideoTrack}
                setLocalAudioTrack={setLocalAudioTrack}
                isVideoOn={isVideoOn}
                localContainerRef={localContainerRef}
              />
            </Fragment>
          }
        </div>
      </aside>
      <aside className="messages" ref={msgRef} style={{ 'display': showChat ? 'flex' : 'none' }}>
        <div className="messages__header">
          <h3 className="messages__title">Chat</h3>
          <div className="close-chat-btn" onClick={handleChatRequest}></div>
        </div>
        <div className="messages__box">
          <div className="conversations">
            {
              chatMsgs.length
              ? chatMsgs.map((msg) => 
                  <Message
                    key={msg.sid || 'chatreadymsg'}
                    username={username}
                    chatIconRef={chatIconRef}
                    msg={msg}
                    showChat={showChat}
                  />
                )
              : <div className="msg">Se conectando ao chat...</div>
            }
          </div>
          <MessageSender
            chatMsg={chatMsg}
            setChatMsg={setChatMsg}
            handleMsg={handleMsg}
            handleFile={handleFile}
          />
        </div>
      </aside>
    </div>
  );
}

export default Room;