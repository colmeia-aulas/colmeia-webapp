import React, { Fragment, useState, useCallback } from 'react';

import Lobby from './Lobby';
import Room from './Room/Room.jsx';
import axios from 'axios';

import LogoutModal from '../../../stories/components/LogoutModal';
import StreamAvaliation from '../../../stories/components/StreamAvaliation';

function Container({ token, setToken }) {
  const [component, setComponent] = useState('main');
  const [username, setUsername] = useState('');
  const [lessonTips, showLessonTips] = useState(false);
  
  const [modal, showModal] = useState(false);
  const [feedback, setFeedback] = useState('');
  const [streamAvaliation, setStreamAvaliation] = useState('');

  const roomName = window.location.search.split('=').pop();

  const handleUsernameChange = useCallback((event) => {
    setUsername(event.target.value);
  }, []);

  const handleSubmit = useCallback(async (event) => {
    event.preventDefault();

    const { data } = await axios.post('/video/token', 
      {
        identity: username,
        room: roomName
      },
      {
        headers: {
          'Content-Type': 'application/json'
        }
      }
    );

    if (localStorage.getItem('colmeia-permissao-aulas') === 'true') {
      setToken(data.token);
    } else {
      localStorage.setItem('colmeia-temp-token', data.token);
      showLessonTips(true);
    }

  }, [username, roomName]);

  function dismissTips() {
    localStorage.setItem('colmeia-permissao-aula', true);
    showLessonTips(false);
    const token = localStorage.getItem('colmeia-temp-token');
    setToken(token);
  }

  const handleLogout = useCallback(event => {
    if (document.fullscreen) {
      document.exitFullscreen();
    }

    showModal(true);
  }, []);

  const confirmLogout = useCallback(event => {
    showModal(false);
    setToken(null);
    setComponent('sub');
  }, []);

  const cancelLogout = useCallback(event => {
    showModal(false);
  }, []);

  function clearUser() {
    setStreamAvaliation('');
    setFeedback('');
    setUsername('');
  }

  function finishSession(msg) {
    alert(msg);
    clearUser();
    setComponent('main');
  }

  function handleStreamAvaliation() {
    if (!streamAvaliation) return alert('Por favor, nos ajude a melhorar sua experiência availando a transmissão');

    const errorMsg = 'Erro na gravação da avaliação. Por favor, contate-nos pelo whatsapp (61) 9 9805-2073';

    axios.post('/streamfeedback', {
      streamAvaliation,
      feedback,
      username
    })
      .then(({data}) => {
        if (!data.success) return alert (errorMsg);
        finishSession('Obrigado pela avaliação!')

      })
      .catch((error) => {
        console.log(error);
        finishSession(errorMsg)
      });
  }

  const handleVote = useCallback(event => {
    let btn;
    let btnType;
    
    if (event.target.className.includes('thumbs')) {
      btnType = event.target.className;
      btn = event.target;
    } else {
      btnType = event.target.parentElement.className;
      btn = event.target.parentElement;
    }
    
    let isBtnSelected;
    const selectedEffect = 'brightness(0.5)';
    const notSelectedEffect = '';
  
    if (!!btn.style.filter) {
      btn.style.filter = notSelectedEffect;
      isBtnSelected = false;
    } else {
      btn.style.filter = selectedEffect;
      isBtnSelected = true;
    }
    
    const siblingBtn = btn.nextSibling ? btn.nextSibling : btn.previousSibling;
    siblingBtn.style.filter = notSelectedEffect;
  
  
    let avaliation;
  
    if (isBtnSelected) {
      avaliation = btnType === 'thumbs-up' ? 'boa' : 'ruim';
      return setStreamAvaliation(avaliation);
    }
  
    setStreamAvaliation('');

  }, [streamAvaliation]);

  if (component == 'main') {
    return (
      <Fragment>
        <div className={ lessonTips ? 'lesson-tips lesson-tips-shown' : 'lesson-tips' } style={{ 'display': lessonTips ? 'flex' : 'none' }}>
          <div className="title">
            <h3>Para garantir uma boa aula</h3>
          </div>
          <div className="info">
            <div className="info-line">
              <img src="/img/aulaonline-abertura-camera.png" />
              <p>1. Para que o seu video e audio seja transmitido <b>é preciso que você dê permissão de acesso ao microfone e câmera do seu computador</b> (na próxima página será solicitado esse acesso pelo seu navegador)</p>
            </div>
            <div className="info-line">
              <img src="/img/aulaonline-abertura-sound.png" />
              <p>2. Procure um lugar silencioso, bem iluminado e reservado para ter a aula</p>
            </div>
            <div className="info-line">
              <img src="/img/aulaonline-abertura-wifi.png" /> 
              <p>3. Verifique se o lugar escolhido possui uma boa conexão com a internet</p>
            </div>
            
          </div>
          <div className="btn">
            <button onClick={dismissTips}>Continuar</button>
          </div>
        </div>
        <LogoutModal isModalOpen={modal} confirmLogout={confirmLogout} cancelLogout={cancelLogout} />
        {token && !lessonTips
          ? <Room
              roomName={roomName}
              token={token}
              handleLogout={handleLogout}
              username={username}
            />
          : <Lobby username={username} handleUsernameChange={handleUsernameChange} handleSubmit={handleSubmit}/>
        }
      </Fragment>
    );
  }

  if (component == 'sub') {
    return (
      <StreamAvaliation
        handleVote={handleVote}
        handleStreamAvaliation={handleStreamAvaliation}
        feedbackState={{ feedback, setFeedback }}
      />
    );
  }
}

export default Container;