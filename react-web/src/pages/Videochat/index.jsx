import React, { useState } from 'react';

import Container from './Container';

//import './index.css';

function Videochat() {
  const [token, setToken] = useState(null);

  return (
    <div className="twilio-videochat"> 
      <header className={ token ? 'twilio-header twilio-header-on-class' : 'twilio-header' }>
        <a className="navbar-static__logo-link" href="/">
          <img src="/img/colmeia_logo.png" alt="Aulas Colmeia Logo" className="navbar-static__logo-img" />
        </a>
        <h3>Sala de aula online</h3>
      </header>
      <main className={token ? '' : 'twilio-box'}>
        <Container token={token} setToken={setToken} />
      </main>
    </div>
  )
} 

export default Videochat;