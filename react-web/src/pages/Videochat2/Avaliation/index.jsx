import React, { useState, useCallback, useRef } from 'react';
import axios from 'axios';

import TopBar from '../Components/TopBar';

import './avaliation.css';

function Avaliation({ username, roomName }) {
  const [streamAvaliation, setStreamAvaliation] = useState('');

  const textareaRef = useRef();

  const handleVote = useCallback(event => {
    const btn = event.target;
    const btnType = event.target.className;

    console.log(btnType);
    
    let isBtnSelected;
    const selectedEffect = 'brightness(0.5)';
    const notSelectedEffect = '';
  
    if (!!btn.style.filter) {
      btn.style.filter = notSelectedEffect;
      isBtnSelected = false;
    } else {
      btn.style.filter = selectedEffect;
      isBtnSelected = true;
    }
    
    const siblingBtn = btn.nextSibling ? btn.nextSibling : btn.previousSibling;
    siblingBtn.style.filter = notSelectedEffect;
  
  
    let avaliation;
  
    if (isBtnSelected) {
      avaliation = btnType.includes('thumbs-up') ? 'boa' : 'ruim';
      return setStreamAvaliation(avaliation);
    }
  
    setStreamAvaliation('');

    return () => {

    }

  }, [streamAvaliation]);

  const handleStreamAvaliation = useCallback(() => {
    if (!streamAvaliation) return alert('Por favor, nos ajude a melhorar sua experiência availando a transmissão');

    const errorMsg = 'Erro na gravação da avaliação. Por favor, contate-nos pelo whatsapp (61) 9 9805-2073';
    const feedback = textareaRef.current.value;

    function finishSession(msg) {
      alert(msg);
      window.location.reload();
    }

    axios.post('/streamfeedback', {
      streamAvaliation,
      feedback,
      username,
      roomName
    })
      .then(({data}) => {
        if (!data.success) return alert (errorMsg);
        finishSession('Obrigado pela avaliação!');
      })
      .catch((error) => {
        console.log(error);
        finishSession(errorMsg);
      });
  }, [streamAvaliation]);


  return (
    <div className="avaliation-component">
      <TopBar />
      <div className="avaliation-container">
        <div className="avaliation-box">
          <div className="section heading">
            <h3>Aula finalizada</h3>
          </div>
          <div className="section vote">
            <h5>Avalie a qualidade da transmissão</h5>
            <div className="buttons">
              <button onClick={handleVote} className="good btn thumbs-up"></button>
              <button onClick={handleVote} className="bad btn thumbs-down"></button>
            </div>
          </div>
          <div className="section feedback">
            <h5>Deixe seu comentário sobre a aula</h5>
            <textarea
              ref={textareaRef}
              rows="6"
              placeholder="Escreva seu comentário aqui"
            >
            </textarea>
          </div>
          <div className="section submit">
            <button onClick={handleStreamAvaliation}>Enviar</button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Avaliation;
