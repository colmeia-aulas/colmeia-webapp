import React, { useRef, useCallback } from 'react';

import './login-form.css';

function LoginForm({ generateToken, setUserType }) {
  const usernameRef = useRef();
  const studentCheckboxRef = useRef();
  const teacherCheckboxRef = useRef();

  const handleSubmit = useCallback(() => {
    if (!usernameRef.current.value.trim()) return alert('Por favor insira um nome');
    const username = usernameRef.current.value;
    const userType = studentCheckboxRef.current.checked ? 
      studentCheckboxRef.current.value : teacherCheckboxRef.current.value;
    setUserType(userType);
    generateToken(username);
  }, []);

  return (
    <div className="login-form">
      <div className="login-box">
        <div className="section">
          <h3 className="heading">Acessar sala de aula</h3>
        </div>
        <div className="section section__input">
          <label htmlFor="username">Digite seu nome abaixo</label>
          <input
            ref={usernameRef}
            id="username"
            placeholder="Escreva seu nome"
          />
        </div>
        <div className="section section__user-type">
          <div>
            <input
              ref={studentCheckboxRef}
              id="aluno"
              value="aluno"
              type="checkbox"
              checked
              onClick={() => {
                teacherCheckboxRef.current.checked = false;
              }}
            />
            <label htmlFor="aluno">Aluno</label>
          </div>
          <div>
            <input
              ref={teacherCheckboxRef}
              id="professor"
              value="professor"
              type="checkbox"
              onClick={() => {
                studentCheckboxRef.current.checked = false;
              }}
            />
            <label htmlFor="professor">Professor</label>
          </div>
        </div>
        <div className="section section__button">
          <button onClick={handleSubmit}>
            <span className="text">Entrar na sala</span>
            <span className="img"></span>
          </button>
        </div>
      </div>
    </div>
  );
}

export default LoginForm;
