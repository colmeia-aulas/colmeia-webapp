import React from 'react';

import './tips.css';

function Tips({ enterRoom }) {
  return (
    <div className="tips">
      <div className="tips-box">
        <div className="section">
          <h3 className="heading">Para garantir uma boa aula</h3>
        </div>
        <div className="section">
          <div className="info-line">
            <img src="/img/aulaonline-abertura-camera.png" />
            <p>1. Para que o seu video e audio seja transmitido <b>é preciso que você dê permissão de acesso ao microfone e câmera do seu computador</b> (na próxima página será solicitado esse acesso pelo seu navegador)</p>
          </div>
          <div className="info-line">
            <img src="/img/aulaonline-abertura-sound.png" />
            <p>2. Procure um lugar silencioso, bem iluminado e reservado para ter a aula</p>
          </div>
          <div className="info-line">
            <img src="/img/aulaonline-abertura-wifi.png" /> 
            <p>3. Verifique se o lugar escolhido possui uma boa conexão com a internet</p>
          </div>
          
        </div>
        <div className="section">
          <button onClick={enterRoom}>
            <span className="text">Continuar</span>
            <span className="img"></span>
          </button>
        </div>
      </div>
    </div>
  );
}

export default Tips;
