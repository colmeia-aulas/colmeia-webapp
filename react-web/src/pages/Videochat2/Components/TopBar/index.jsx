import React from 'react';

import './top-bar.css';

function TopBar() {
  return (
    <header className="top-bar">
      <a className="logo-colmeia">
        <img src="/img/colmeia_logo.png" alt="Aulas Colmeia Logo"/>
      </a>
      <h3>Sala de aula online</h3>
    </header>
  );
}

export default TopBar;