import React from 'react';

function MessageHeader({ handleChangeChatVisibility }) {
  return (
    <div className="section header">
      <h3>Chat</h3>
      <span
        className="close-img"
        onClick={handleChangeChatVisibility}
      >
      </span>
    </div>
  );
}

export default MessageHeader;
