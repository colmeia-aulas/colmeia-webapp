import React, { useState, useEffect, useContext, useRef, memo } from 'react';

import { RoomContext } from '../../../../index';

import messageSound from './message.mp3';

function MessageList({ messageData }) {
  const initialMessage = { sid: new Date().getTime(), alert: 'Inicializando o chat...' };

  const [messages, setMessages] = useState([initialMessage]);

  const { username } = useContext(RoomContext);

  const newMessageSound = new Audio(messageSound);

  const listRef = useRef();

  useEffect(() => {
    if (!messageData.sid) return;

    newMessageSound.play();
    setMessages((previousMsgs) => [...previousMsgs, messageData]);
  }, [messageData.sid]);

  // TODO: Estilizar mensagens
  // TODO: Separar componente de mensagem
  // TODO: Colocar scroll

  return (
    <div className="section list" ref={listRef}>
      {messages.map(({ author, body, url, alert, sid }) => {
        if (body) {
          return <div
            key={sid}
            className={ author === username ? 'local-msg' : 'remote-msg' }
          >
            <span className="chat-author">{author}: </span><span className="chat-msg">{body}</span>
          </div>
        }

        if (url) {
          return <div
            key={sid}
            className={ author === username ? 'local-msg' : 'remote-msg' }
          >
            <span className="chat-author"><a href={url} target="_blank">{author === username ? 'Você' : author} enviou um arquivo! <img className="new-tab-icon" alt="Abelha apontando para outra aba" src="/img/aulaonline-file-sent-icon.png" /></a></span>
          </div>
        }

        if (alert) {
          return <div
            key={sid}
            className={ author === username ? 'local-msg' : 'remote-msg' }
          >
            <span className="chat-author">{alert}</span>
          </div>
        }
      }
      )}
    </div>
  );
}

export default memo(MessageList);
