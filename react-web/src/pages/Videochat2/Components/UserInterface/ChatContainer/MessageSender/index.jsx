import React, { useState, useRef, memo } from 'react';

function MessageSender({ handleMessageSubmit }) {
  const textInputRef = useRef();

  function sendMessage(event) {
    const isTargettedEvent = event.key === 'Enter' || event.type === 'click';
    if (!isTargettedEvent) return;

    const textInputValue = textInputRef.current.value;
    textInputRef.current.value = '';
    handleMessageSubmit(textInputValue)
  }

  function sendFile(event) {
    const file = event.target.files[0];
    const formData = new FormData();
    formData.append('file', file);
    event.target.value = '';
    handleMessageSubmit(formData);
  }

  return (
    <div className="section sender">
      <input
        ref={textInputRef}
        id="text-msg"
        type="text"
        placeholder="Escreva sua mensagem aqui"
        onKeyPress={sendMessage}
      />
      <input
        id="file-msg"
        type="file"
        onChange={sendFile}
      />
      <label htmlFor="file-msg"></label>
      <button
        className="text-msg-btn"
        onClick={sendMessage}
      >
      </button>
    </div>
  );
}

export default memo(MessageSender);
