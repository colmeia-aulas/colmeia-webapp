import React, { useContext, useEffect, useCallback, useState, memo } from 'react';
import * as Sentry from '@sentry/react';
import * as Chat from 'twilio-chat';

import MessageHeader from './MessageHeader';
import MessageList from './MessageList';
import MessageSender from './MessageSender';

import { RoomContext } from '../../../index';

import './chat-container.css';

function ChatContainer({ handleChangeChatVisibility, chatRef }) {
  const [chatClient, setChatClient] = useState(null);
  const [messageData, setMessageData] = useState('');

  const { token, roomName, username } = useContext(RoomContext);

  const handleMessageSubmit = useCallback((message) => {
    if (!chatClient) return;
    chatClient.sendMessage(message);
  }, [chatClient]);

  const initializeChat = useCallback(() => {
    if (!token && !roomName) return;

    function registerEvent(client, eventName) {
      const events = {
        memberJoined: () => {
          client.on('memberJoined', (member) => {
            console.log(member);
          });
        },

        messageAdded: () => {
          client.on('messageAdded', (message) => {
            const { author, body, sid } = message;

            if (message.type === 'media') {
              message.media.getContentUrl().then((url) => {
                setMessageData({ author, url, sid });
              });
            } else {
              setMessageData({ author, body, sid });
            }
          });
        },
      };

      events[eventName]();
    }

    function join(client) {
      function defaultSetup() {
        registerEvent(client, 'messageAdded');
        setChatClient(client);
      }

      if (client.status !== 'joined') {
        client.join().then(() => {
          registerEvent(client, 'memberJoined');
          return defaultSetup();
        }).catch((error) => console.log(error));
      }

      defaultSetup();
    }

    Chat.Client.create(token).then((client) => {
      client.getChannelByUniqueName(roomName).then(join).catch((error) => {
        console.log(error);
        
        client.createChannel({ uniqueName: roomName, friendlyName: 'Chat Colmeia' })
          .then(join).catch((error) => {
            console.log(error);
            Sentry.captureEvent(error);
            alert('Não foi possível se conectar ao chat');
          });
      });
    }).catch((error) => {
      Sentry.captureEvent(error);
    });
  }, [token, roomName]);

  useEffect(() => {
    initializeChat();

    return () => {
      setChatClient((chatClient) => {
        chatClient.leave();
        return null;
      });
    }
  }, [token, roomName]);

  useEffect(() => {
    if(!chatClient) return;
    setMessageData({ sid: new Date().getTime(), alert: 'Chat pronto para uso!' });
    chatRef.current.children['chat-img']
      .classList.add('chat-img__ready');
  }, [chatClient]);

  return (
    <div className="chat-container">
      <MessageHeader handleChangeChatVisibility={handleChangeChatVisibility} />
      <MessageList messageData={messageData} />
      <MessageSender handleMessageSubmit={handleMessageSubmit} username={username} />
    </div>
  );
}

export default memo(ChatContainer);
