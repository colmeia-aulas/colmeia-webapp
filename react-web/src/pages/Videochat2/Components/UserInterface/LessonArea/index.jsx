import React from 'react';

import VideoContainer from '../VideoContainer';
import ChatContainer from '../ChatContainer';

import './lesson-area.css';

function LessonArea({ handleChangeChatVisibility, chatRef, popupFinishLessonRef, setLocalDataTrack }) {
  return (
    <div className="lesson-area">
      <VideoContainer popupFinishLessonRef={popupFinishLessonRef} setLocalDataTrack={setLocalDataTrack} />
      <ChatContainer handleChangeChatVisibility={handleChangeChatVisibility} chatRef={chatRef} />
    </div>
  );
}

export default LessonArea;
