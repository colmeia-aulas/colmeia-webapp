import React, { useRef, useEffect, memo } from 'react';

function Button({ onclickFn, effectFn, type, text, state }) {
  const btnRef = useRef();

  useEffect(() => {
    effectFn(btnRef, type, state);
  }, [state]);

  return (
    <button onClick={onclickFn} ref={btnRef} className={type + '-btn'}>
      <span className="navbar-img" id={type + '-img'}></span>
      <span className="navbar-text" id={type + '-text'}>{text}</span>
    </button>
  );
}

export default memo(Button);
