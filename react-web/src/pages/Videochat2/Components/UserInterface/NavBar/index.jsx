import React, { useContext, useCallback } from 'react';
import * as Sentry from '@sentry/react';

import { RoomContext }  from '../../../index';

import Button from './Button';

import './navbar.css';

function NavBar({ handleChangeChatVisibility, chatRef, popupFinishLessonRef, localDataTrack }) {
  const { videoState, audioState, screenState } = useContext(RoomContext);

  const { setVideoStop, videoStop } = videoState;
  const { setAudioStop, audioStop } = audioState;
  const { setScreenShare, screenShare } = screenState;

  const toggleVideo = useCallback(() => {
    setVideoStop(!videoStop);
  }, [videoState]);

  const toggleAudio = useCallback(() => {
    setAudioStop(!audioStop);
  }, [audioState]);

  const toggleScreen = useCallback(() => {
    setScreenShare(!screenShare);
  }, [screenState]);

  const finishLessonConfirmation = useCallback(() => {
    popupFinishLessonRef.current.style.display = 'flex';
  }, [popupFinishLessonRef]);

  const effectFn = useCallback((ref, type, state) => {
    const imgChild = `${type}-img`;
    const altImg = `${type}-img__alt`;

    if (state) {
      Sentry.captureMessage(`Event: click - ${ref.current.innerText}`);

      ref.current.children[imgChild]
        .classList.add(altImg); 
    } else {
      ref.current.children[imgChild]
        .classList.remove(altImg);  
    }
  }, []);

  const showChrono = useCallback(() => {
    const component = document.querySelector('.chrono-popup');
    if (component.style.visibility == 'visible') {
      component.style.visibility = 'hidden';
    } else {
      component.style.visibility = 'visible';
    }
  }, []);

  const showWhiteBoard = useCallback(() => {
    if (!localDataTrack) return;
    const canvas = document.querySelector('canvas');
    const ctx = canvas.getContext('2d');

    if (canvas.width === 0) {
      const videoContainer = document.querySelector('.video-container');
      document.querySelector('.local-participant').style.zIndex = '2';
      canvas.width = videoContainer.offsetWidth - 2;
      canvas.height = videoContainer.offsetHeight - 2;
      ctx.globalCompositeOperation = 'destination-over';
      ctx.fillStyle = 'white';
      ctx.fillRect(0, 0, canvas.width, canvas.height);
      ctx.globalCompositeOperation = 'source-over';
      document.querySelector('.remote-participant') && document.querySelector('.remote-participant').classList.add('remote-participant__whiteboard-on');
      if (document.querySelector('.is-professor')) {
        document.querySelector('.canvas-options-nav').style.display = 'flex';
      }

      localDataTrack.send(JSON.stringify({
        action: 'whiteboard:show',
      }));
    } else {
      ctx.clearRect(0, 0, canvas.width, canvas.height);
      canvas.width = 0;
      canvas.height = 0;
      document.querySelector('.local-participant').style.zIndex = '0';
      document.querySelector('.canvas-options-nav').style.display = 'none';
      document.querySelector('.remote-participant') && document.querySelector('.remote-participant').classList.remove('remote-participant__whiteboard-on');

      localDataTrack.send(JSON.stringify({
        action: 'whiteboard:close',
      }));
    }
  }, [localDataTrack]);

  return (
    <div className="navbar-video">
      <div className="section section__interaction">
        <Button
          onclickFn={toggleVideo}
          effectFn={effectFn}
          type="cam"
          text="Vídeo"
          state={videoStop}
        />
        <Button
          onclickFn={toggleAudio}
          effectFn={effectFn}
          type="mic"
          text="Microfone"
          state={audioStop}
        />
        <Button
          onclickFn={toggleScreen}
          effectFn={effectFn}
          type="screen"
          text="Compartilhar a tela"
          state={screenShare}
        />
        <button ref={chatRef} onClick={handleChangeChatVisibility}>
          <span className="navbar-img" id="chat-img"></span>
          <span className="navbar-text" id="chat-text">Chat</span>
        </button>
        <button onClick={showChrono} className="chrono-btn">
          <span className="navbar-img" id="chrono-img"></span>
          <span className="navbar-text" id="chrono-text">Cronômetro</span>
        </button>
        <button onClick={showWhiteBoard} className="board-btn">
          <span className="navbar-img" id="board-img"></span>
          <span className="navbar-text" id="board-text">Quadro branco</span>
        </button>
      </div>
      <div className="section section__logout">
        <button onClick={finishLessonConfirmation}>
          <span className="navbar-img" id="logout-img"></span>
          <span className="navbar-text" id="logout-text">Sair da sala</span>
        </button>
      </div>
    </div>
  );
}

export default React.memo(NavBar);
