import React, { useEffect, useCallback, useState, useRef } from 'react';

import Drawing from './Drawing';

function Options({ canvasRef, localDataTrack, remoteDataTrack, isProfessor }) {

  const localDrawing = new Drawing(false, canvasRef.current, '#0d528a', 2);
  const remoteDrawing = new Drawing(false, canvasRef.current, '#96423c', 2);

  const [permission, setPermission] = useState(false);
  const [minMode, setMinMode] = useState(false);

  const permissionRef = useRef();
  const optionsRef = useRef();

  const fnSetup = useCallback((cursor, callback) => {
    deleteEventListeners();
    if (localDrawing.canvas.className.includes(cursor)) {
      return localDrawing.canvas.className = '';
    }

    localDrawing.canvas.classList.add(cursor);
    callback();
  }, [localDrawing]);

  const deleteEventListeners = useCallback(() => {
    localDrawing.canvas.onmousemove = null;
    localDrawing.canvas.onmousedown = null;
    localDrawing.canvas.onmouseup = null;
    localDrawing.canvas.onclick = null;
    window.onkeypress = null;
  }, [localDrawing]);

  const draw = useCallback(() => {
    fnSetup('pencil-now', () => {
      localDrawing.canvas.onmousedown = () => {
        localDrawing.mouseDown = true;
      };
  
      localDrawing.canvas.onmouseup = () => {
        localDrawing.mouseDown = false;
        localDrawing.resetCoords();
  
        localDataTrack.send(JSON.stringify({
          action: 'reset',
        }));
      };
  
      localDrawing.canvas.onmousemove = (event) => {
        if (!localDrawing.mouseDown) return;
        const canvasRect = localDrawing.canvas.getBoundingClientRect();
        
        localDrawing.x = (event.clientX - canvasRect.left);
        localDrawing.y = (event.clientY - canvasRect.top);
    
        localDrawing.action = 'drawLine';
        localDrawing.draw();
  
        localDataTrack.send(JSON.stringify({
          action: 'draw',
          fn: localDrawing.action,
          x: localDrawing.x / localDrawing.canvas.width,
          y: localDrawing.y / localDrawing.canvas.height
        }));
      };
    });
  }, [localDrawing]);

  const write = useCallback(() => {
    fnSetup('writing-now', () => {
      localDrawing.writeSetup((x, y) => {
        window.onkeypress = (event) => {
          localDrawing.write(event.key, x, y);

          localDataTrack.send(JSON.stringify({
            action: 'write',
            string: localDrawing.text,
            x: x / localDrawing.canvas.width,
            y: y / localDrawing.canvas.height
          }));
        }
      });
    });
  }, [localDrawing]);

  const erase = useCallback(() => {
    fnSetup('eraser-now', () => {
      localDrawing.canvas.onmousedown = () => {
        localDrawing.mouseDown = true;
      };
      localDrawing.canvas.onmouseup = () => {
        localDrawing.mouseDown = false;
      };

      localDrawing.canvas.onmousemove = (event) => {
        if (!localDrawing.mouseDown) return;
        localDrawing.erase(event.clientX, event.clientY, (x, y) => {
          localDataTrack.send(JSON.stringify({
            action: 'erase',
              x: x / localDrawing.canvas.width,
              y: y / localDrawing.canvas.height
          }));
        });
      }
    });
  }, [localDrawing]);

  const togglePermission = useCallback(() => {
    setPermission(!permission);
  }, [localDrawing, permission]);

  useEffect(() => {
    if (!document.querySelector('.is-professor')) return;
    if(!permission) {
      permissionRef.current.classList.remove('permission-on-img');
      localDataTrack.send(JSON.stringify({
        action: 'toggle-whiteboard-permission',
        permission: permission
      }));
    } else {
      permissionRef.current.classList.add('permission-on-img');
      localDataTrack.send(JSON.stringify({
        action: 'toggle-whiteboard-permission',
        permission: permission
      }));
    }
  }, [permission]);

  const clear = useCallback(() => {
    localDrawing.clear();
    localDataTrack.send(JSON.stringify({
      action: 'clear'
    }));
  }, [localDrawing]);

  const download = useCallback(() => {
    const link = document.createElement('a');
    link.download = 'canvas.jpg';
    link.href = localDrawing.canvas.toDataURL('image/jpg');
    link.click();
    link.remove();
  }, [localDrawing]);

  const toggleMode = useCallback(() => {
    setMinMode(!minMode);
  }, [localDrawing, minMode]);

  useEffect(() => {
    if (minMode) {
      optionsRef.current.classList.add('canvas-options-nav-minmode');
    } else {
      optionsRef.current.classList.remove('canvas-options-nav-minmode');
    }
  }, [minMode]);

  useEffect(() => {
    if (!remoteDataTrack) return;

    function remoteDraw(message) {
      remoteDrawing.x = message.x * remoteDrawing.canvas.width;
      remoteDrawing.y = message.y * remoteDrawing.canvas.height;
      remoteDrawing.action = message.fn;
      remoteDrawing.draw();
    }

    function remoteWrite(message) {
      const remoteX = message.x * remoteDrawing.canvas.width;
      const remoteY = message.y * remoteDrawing.canvas.height;
      const ctx = remoteDrawing.canvas2dContext;
      const fontSize = remoteDrawing
        .calcFontSize(
          remoteDrawing
            .diagonalSize(
              remoteDrawing.canvas.width,
              remoteDrawing.canvas.height
            )
        );
      ctx.font = `${fontSize}px Arial`;
      ctx.fillText(message.string, remoteX, remoteY);
    }

    function remoteErase(message) {
      const remoteX = message.x * remoteDrawing.canvas.width;
      const remoteY = message.y * remoteDrawing.canvas.height;

      const ctx = remoteDrawing.canvas2dContext;
      ctx.fillStyle = 'white';
      ctx.fillRect(remoteX, remoteY, 40, 40);
    }

    function remoteShow(elemQuery) {
      const ctx = remoteDrawing.canvas2dContext;
      const videoContainer = document.querySelector(elemQuery);
      document.querySelector('.local-participant').style.zIndex = '2';
      remoteDrawing.canvas.width = videoContainer.offsetWidth - 2;
      remoteDrawing.canvas.height = videoContainer.offsetHeight - 2;
      ctx.globalCompositeOperation = 'destination-over';
      ctx.fillStyle = 'white';
      ctx.fillRect(0, 0, remoteDrawing.canvas.width, remoteDrawing.canvas.height);
      ctx.globalCompositeOperation = 'source-over';
      document.querySelector('.remote-participant').classList.add('remote-participant__whiteboard-on');
      if (document.querySelector('.is-professor')) {
        document.querySelector('.canvas-options-nav').style.display = 'flex';
      }
    }

    function remoteClose() {
      const ctx = remoteDrawing.canvas2dContext;
      ctx.clearRect(0, 0, remoteDrawing.canvas.width, remoteDrawing.canvas.height);
      remoteDrawing.canvas.width = 0;
      remoteDrawing.canvas.height = 0;
      document.querySelector('.canvas-options-nav').style.display = 'none';
      document.querySelector('.remote-participant').classList.remove('remote-participant__whiteboard-on');
    }

    function allowStudent(message) {
      if (document.querySelector('.is-aluno')) {
        if (message.permission) {
          document.querySelector('.canvas-options-nav').style.display = 'flex';
        } else {
          document.querySelector('.canvas-options-nav').style.display = 'none';
          remoteDrawing.canvas.onmousemove = null;
          remoteDrawing.canvas.onmousedown = null;
          remoteDrawing.canvas.onmouseup = null;
          remoteDrawing.canvas.onclick = null;
          window.onkeypress = null;
          remoteDrawing.canvas.className = '';
        }
      }
    }

    remoteDataTrack.on('message', (data) => {
      const message = JSON.parse(data);

      switch(message.action) {
        case 'clear':
          remoteDrawing.clear();
          break;
        case 'reset':
          remoteDrawing.resetCoords();
          break;
        case 'draw':
          remoteDraw(message);
          break;
        case 'write':
          remoteWrite(message)
          break;
        case 'erase':
          remoteErase(message);
          break;
        case 'whiteboard:show':
          remoteShow('.video-container');
          break;
        case 'whiteboard:close':
          remoteClose();
          break;
        case 'toggle-whiteboard-permission':
          allowStudent(message);
          break;
        case 'screenshare:on':
        const remoteVideoElem = document.querySelector('.remote-participant video');
        remoteVideoElem.onloadeddata = (event) => {
          remoteDrawing.canvas.width = remoteVideoElem.offsetWidth - 2;
          remoteDrawing.canvas.height = remoteVideoElem.offsetHeight - 2;
          if (isProfessor) {
            document.querySelector('.canvas-options-nav').style.display = 'flex';
          }
        }
        break;
        case 'screenshare:off':
          remoteDrawing.canvas.width = 0;
          remoteDrawing.canvas.height = 0;
          document.querySelector('.canvas-options-nav').style.display = 'none';
      }
    });
  }, [remoteDataTrack]);


  return (
    <nav className="canvas-options-nav" ref={optionsRef}>
      <button className="pencil-btn" onClick={draw}>
        <span className="canvas-option__img" id="pencil-img"></span>
        <span className="canvas-option__text" id="pencil-text">Lápis</span>
      </button>
      <button className="writing-btn" onClick={write}>
        <span className="canvas-option__img" id="writing-img"></span>
        <span className="canvas-option__text" id="writing-text">Texto</span>
      </button>
      <button className="eraser-btn" onClick={erase}>
        <span className="canvas-option__img" id="eraser-img"></span>
        <span className="canvas-option__text" id="eraser-text">Borracha</span>
      </button>
      <button className="clearall-btn" onClick={clear}>
        <span className="canvas-option__img" id="clearall-img"></span>
        <span className="canvas-option__text" id="clearall-text">Limpar tela</span>
      </button>
      <button className="download-btn" onClick={download}>
        <span className="canvas-option__img" id="download-img"></span>
        <span className="canvas-option__text" id="download-text">Baixar imagem</span>
      </button>
      {
        document.querySelector('.is-professor') && <button className="permission-btn" onClick={togglePermission}>
          <span className="canvas-option__img" id="permission-img" ref={permissionRef}></span>
          <span className="canvas-option__text" id="permission-text">{permission ? 'Bloquear uso do aluno' : 'Liberar uso do aluno'}</span>
        </button>
      }
      <button className="mode-btn" onClick={toggleMode}>
        <span className="canvas-option__img" id="mode-img"></span>
        <span className="canvas-option__text" id="mode-text">{minMode? 'Maximizar' : 'Minimizar'}</span>
      </button>
    </nav>
  );
}

export default Options;
