import React, { useRef } from 'react';

import Options from './Options';

import './component.css';

function Canvas({ localDataTrack, remoteDataTrack }) {
  const canvasRef = useRef();

  return (
    <>
      <Options
        canvasRef={canvasRef}
        localDataTrack={localDataTrack}
        remoteDataTrack={remoteDataTrack}
      />
      <canvas
        ref={canvasRef}
        width={0}
        height={0}
      >
      </canvas>
    </>
  );
}

export default Canvas;