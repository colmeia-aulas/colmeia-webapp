import React, { memo, useRef, useState } from 'react';

import alarmSound from './alarm.mp3';

function ChronoPopup({ handlePopup }) {
  const lessonTimeRef = useRef();
  const alertTimeRef = useRef();

  const lessonFinishedSound = new Audio(alarmSound);

  const [reminderTimeout, setReminderTimeout] = useState(0);
  const [lessonFinishedTimeout, setLessonFinishedTimeout] = useState(0);
  const [timeLeftInterval, setTimeLeftInterval] = useState(0);
  const [lessonTime, setLessonTime] = useState(0);

  function startChrono() {
    function initialize() {
      const lessonTimeValue = Number(lessonTimeRef.current.value) || 90;
      const alertTimeValue = alertTimeRef.current.value ?
        Number(alertTimeRef.current.value) : 5;
      
      const alertTimeout = lessonTimeValue - alertTimeValue;
  
      setLessonTime(lessonTimeValue);
  
      const reminder = setTimeout(() => {
        lessonFinishedSound.play();
        handlePopup(`Falta ${alertTimeValue} minutos para acabar a aula!`, 3000);
        setReminderTimeout((reminder) => {
          clearTimeout(reminder);
          return 0;
        });
      }, 1000 * 60 * alertTimeout);
  
      const lessonFinished = setTimeout(() => {
        lessonFinishedSound.play();
        handlePopup('Tempo de aula definido acabou!', 4000);
        setLessonFinishedTimeout((lessonFinished) => {
          clearTimeout(lessonFinished);
          return 0;
        });
      }, 1000 * 60 * lessonTimeValue);
  
      const timeLeft = setInterval(() => {
        setLessonTime((lessonTime) => {
          if (lessonTime - 1 === 0) {
            setTimeLeftInterval((timeLeft) => {
              clearInterval(timeLeft);
              return 0;
            });
          }
          return lessonTime - 1;
        });
      }, 1000 * 60);
  
      setReminderTimeout(reminder);
      setLessonFinishedTimeout(lessonFinished);
      setTimeLeftInterval(timeLeft);
    }

    if (!reminderTimeout, !lessonFinishedTimeout, !timeLeftInterval) {
      initialize();
    } else {
      setReminderTimeout((reminder) => {
        clearTimeout(reminder);
        return 0;
      });
      setLessonFinishedTimeout((lessonFinished) => {
        clearTimeout(lessonFinished);
        return 0;
      });
      setTimeLeftInterval((timeLeft) => {
        clearInterval(timeLeft);
        return 0;
      });

      initialize();
    }
  }

  function closeChrono() {
    const component = document.querySelector('.chrono-popup');
    component.style.visibility = 'hidden';
  }

  return (
    <div className="chrono-popup">
      <div className="chrono-box">
        <div className="section">
          <label htmlFor="minute-input">
            Defina o tempo da aula:
          </label>
          <div className="lesson-time-box">
            <input
              ref={lessonTimeRef}
              id="minute-input"
              type="number"
              placeholder="90"
            />
            <span>minutos</span>
          </div>
        </div>
        <div className="section">
          <label htmlFor="alert-input">Deseja receber um alerta quando a aula estiver perto de acabar?</label>
          <div className="lesson-alert-box">
            <span>Me avise quando faltar </span>
            <input
              ref={alertTimeRef}
              id="alert-input"
              type="number"
              placeholder="5"
            />
            <span>minutes</span>
          </div>
        </div>
        <div className="section">
          <div className="buttons">
            <button
              className="cancel btn"
              onClick={closeChrono}
            >Fechar</button>
            <button
              className="confirm btn"
              onClick={startChrono}
            >
              Confirmar valores
            </button>
          </div>
        </div>
        { lessonTime > 0 ? <div className="section">
          <label>Restam {lessonTime} minutos de aula</label>
        </div> : <div></div>}
      </div>
    </div>
  );
}

export default memo(ChronoPopup);
