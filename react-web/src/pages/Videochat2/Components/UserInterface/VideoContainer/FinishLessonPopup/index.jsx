import React, { memo } from 'react';

function FinishLessonPopup({ popupFinishLessonRef, handleLessonFinished, stayOnLesson }) {
  return (
    <div className="finish-class-popup" ref={popupFinishLessonRef}>
      <div className="popup-box">
        <div className="section heading">
          <h3>Sair da sala</h3>
        </div>
        <div className="section info">
          <p>Tem certeza que você deseja sair da sala?</p>
          <p>A aula será interrompida, caso ela ainda esteja acontecendo.</p>
        </div>
        <div className="section buttons">
          <button className="confirm btn" onClick={handleLessonFinished}>Sair da sala</button>
          <button className="cancel btn" onClick={stayOnLesson}>Permanecer</button>
        </div>
      </div>
    </div>
  );
}

export default memo(FinishLessonPopup);
