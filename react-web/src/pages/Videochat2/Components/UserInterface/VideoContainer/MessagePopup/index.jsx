import React, { memo } from 'react';

function MessagePopup({ userMsg }) {
  return (
    <div
      className="popup-message"
      style={{ 'visibility': userMsg ? 'visible' : 'hidden' }}
    >
      {userMsg}
    </div>
  );
}

export default memo(MessagePopup);
