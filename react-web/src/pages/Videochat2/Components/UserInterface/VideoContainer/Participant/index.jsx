import React, { useEffect, useRef } from 'react';

function Participant({ isLocal, participant, refs, tracks, setRemoteDataTrack }) {
  const remoteVideoRef = useRef();
  const remoteAudioRef = useRef();

  const { localVideoRef, localAudioRef } = refs;

  let setLocalAudioTrack;
  let setLocalVideoTrack;

  if (tracks) {
    setLocalAudioTrack = tracks.setLocalAudioTrack;
    setLocalVideoTrack = tracks.setLocalVideoTrack;
  }

  const attachedVideoRef = isLocal ? localVideoRef : remoteVideoRef;
  const attachedAudioRef = isLocal ? localAudioRef : remoteAudioRef;

  useEffect(() => {
    if (!participant) return;

    function registerTrack(track) {
      console.log(track);
      if (track && track.kind === 'video') {
        if (track.name !== 'screen' && isLocal) {
          setLocalVideoTrack && setLocalVideoTrack(track);
        }
        track.attach(attachedVideoRef.current);

      } else if (track.kind === 'audio') {
        if (isLocal) {
          setLocalAudioTrack && setLocalAudioTrack(track);
        }

        track.attach(attachedAudioRef.current);
      } else if (track.kind === 'data') {
        if (!isLocal) {
          setRemoteDataTrack(track);
        }
      }
    }

    if (isLocal) {
      participant.tracks.forEach((publication) => {
        if (!publication.track) return;
        registerTrack(publication.track);
      });
    }

    participant.on('trackSubscribed', registerTrack);
    participant.on('trackUnsubscribed', (track) => {
      if (!track || track.kind === 'data') return;
      track.detach();
    });

    return () => {
      participant.tracks.forEach(({ track }) => {
        if (!track || track.kind === 'data') return;
        track.detach();
      });
      participant.removeAllListeners();
    }
  }, [participant]);

  return (
    <div className={ isLocal ? 'local-participant' : 'remote-participant' }>
      <video muted ref={attachedVideoRef}></video>
      <audio ref={attachedAudioRef}></audio>
    </div>
  );
}

export default Participant;
