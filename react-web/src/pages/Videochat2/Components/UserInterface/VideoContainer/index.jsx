import React, { useEffect, useCallback, useState, useContext, useRef } from 'react';

import Video from 'twilio-video';
import * as Sentry from '@sentry/react';

import { RoomContext } from '../../../index';
import helpers from '../../../utils/helpers';

import Participant from './Participant';

import './video-container.css';
import FinishLessonPopup from './FinishLessonPopup';
import MessagePopup from './MessagePopup';
import ChronoPopup from './ChronoPopup';
import Canvas from './Canvas';

function VideoContainer({ popupFinishLessonRef, setLocalDataTrack }) {
  const {
    token,
    roomName,
    username,
    videoState,
    audioState,
    screenState,
    isAtendimento,
    isProfessor,
    handleLessonFinished
  } = useContext(RoomContext);

  const { videoStop } = videoState;
  const { audioStop } = audioState;
  const { screenShare } = screenState;

  const [hasCamera, setHasCamera] = useState(false);
  const [room, setRoom] = useState(null);
  const [hasMicrophone, setHasMicrophone] = useState(false);
  const [askPermissions, setAskPermissions] = useState(false);
  const [hasDeviceChange, setHasDeviceChange] = useState(false);
  const [participants, setParticipants] = useState([]);
  const [userMsg, setUserMsg] = useState('');
  const [localAudioTrack, setLocalAudioTrack] = useState(null);
  const [localVideoTrack, setLocalVideoTrack] = useState(null);
  const [localScreenTrack, setLocalScreenTrack] = useState(null);
  const [remoteDataTrack, setRemoteDataTrack] = useState(null);
  const [mineDataTrack, setMineDataTrack] = useState(null);

  const isMobile = helpers.isMobile();

  const videoContainerRef = useRef();

  const localVideoRef = useRef();
  const localAudioRef = useRef();

  const askForMediaPermission = useCallback(() => {
    navigator.mediaDevices.getUserMedia({ audio: true, video: true })
      .then(mediaStream => mediaStream.getTracks().forEach(track => track.stop()))
      .catch((error) => console.log(error));
  }, []);

  const getAvailableDevices = useCallback((callback) => {

    navigator.mediaDevices.enumerateDevices().then((devices) => {
      const hasCamera = devices.some((device) => device.kind === 'videoinput');
      const hasMicrophone = devices.some((device) => device.kind === 'audioinput');

      setHasCamera(hasCamera);
      setHasMicrophone(hasMicrophone);
      if (callback) {
        callback();
      }
    }).catch((error) => {
      const msg = 'Não conseguiu achar microfone ou câmera';
      console.log(msg, error);
      Sentry.captureEvent(error);
      alert(`${msg}. Por favor reentre na sala e se persistir o problema, entre em contato com o atendimento: (61) 9 9805-2073`)
    });
  }, []);

  const toggleTrack = useCallback((track, action) => {
    track && track[action]();
  }, [localAudioRef, localVideoRef]);

  const unpublishTracks = useCallback((tracks) => {
    room && room.localParticipant[tracks].forEach(publication => {
      const track = publication.track;

      if (track && track.name !== 'screen') {
        track.stop();
        room.localParticipant.unpublishTrack(track);
      }
    });
  }, [room]);

  const stayOnLesson = useCallback(() => {
    popupFinishLessonRef.current.style.display = 'none';
  }, [popupFinishLessonRef]);

  const handlePopup = useCallback((msg, milliseconds = 2000) => {
    setUserMsg(msg);
    setTimeout(() => { setUserMsg(); }, milliseconds);
  }, []);
  
  useEffect(() => {
    if (isAtendimento) return;

    askForMediaPermission();
    getAvailableDevices(() => {
      setAskPermissions(true);
    });
  }, [token, roomName]);

  useEffect(() => {
    if (!isAtendimento) return;

    Video.connect(token, {
      name: roomName,
      video: false,
      audio: false
    })
      .then((room) => { setRoom(room); })
      .catch((error) => {
        console.log(error);
        alert('Não foi possível entrar em modo espectador. Por favor, tente novamente')
      });
  }, [isAtendimento]);

  useEffect(() => {
    if (!askPermissions) return;

    Video.connect(token, {
      name: roomName,
      video: hasCamera,
      audio: hasMicrophone
    })
      .then((room) => {
        setRoom(room);
        navigator.mediaDevices.ondevicechange = (event) => {
          console.log('DEVICE CHANGE', event);
          getAvailableDevices(() => {
            setHasDeviceChange(true);
          });
        }
      }).catch((error) => {
        console.log(error);
        Sentry.captureEvent(error);
        alert('Não foi possível se conectar a sala. Tente novamente ou entre em contato com o atendimento')
      });
  }, [askPermissions]);

  useEffect(() => {
    if (!hasDeviceChange) return;

    const audioTracksPublished = room.localParticipant.audioTracks.size;
    const videoTracksPublished = room.localParticipant.videoTracks.size;

    if (hasCamera && (videoTracksPublished === 0)) {
      Video.createLocalVideoTrack().then((stream) => {
        room.localParticipant.publishTrack(stream);
        stream.attach(localVideoRef.current);
      }).catch((error) => { console.log(error); Sentry.captureEvent(error); });
    } else if (!hasCamera) {
      unpublishTracks('videoTracks');
    }

    if (hasMicrophone && (audioTracksPublished === 0)) {
      Video.createLocalAudioTrack().then((stream) => {
        room.localParticipant.publishTrack(stream);
        stream.attach(localAudioRef.current);
      }).catch((error) => console.log(error));
    } else if (!hasMicrophone) {
      unpublishTracks('videoTracks');
    }
  }, [hasDeviceChange]);

  useEffect(() => {
    if (!room) return;

    function connect(participant) {
      if (participant.identity.includes('@colmeia')) return;
      setParticipants((participants) => [...participants, participant]);
    }

    function disconnect() {
      setRoom((room) => {
        const isConnected = room && room.localParticipant.state === 'connected';
        if (!isConnected) return null;

        room.localParticipant.tracks.forEach(({ track }) => {
          if (!track || track.kind === 'data') return;
          track.stop();
          room.localParticipant.unpublishTrack(track);
        });

        room.disconnect();
      });
    }

    const dataTrack = new Video.LocalDataTrack();
    setMineDataTrack(dataTrack);
    setLocalDataTrack(dataTrack);
    room.localParticipant.publishTrack(dataTrack);

    handlePopup(`Seja bem vindo (a)`);
    connect(room.localParticipant);
    room.participants.forEach(connect);
    room.on('participantConnected', (participant) => {
      if (participant.identity.includes('@colmeia')) return;
      handlePopup(`${participant.identity} entrou na sala`);
      connect(participant);
    });

    room.on('participantDisconnected', (participant) => {
      if (participant.identity.includes('@colmeia')) return;
      handlePopup(`${participant.identity} saiu na sala`);
      setParticipants((participants) => participants.filter(p => p !== participant));
    });

    window.addEventListener('beforeunload', disconnect);
    if (isMobile) {
      window.addEventListener('pagehide', disconnect);
    }

    room.on('disconnected', (room, error) => {
      room && room.localParticipant.tracks.forEach(({ track }) => {
        if (!track || track.kind === 'data') return;
        track.stop();
        room.localParticipant.unpublishTrack(track);
      });
      handleLessonFinished();
    });

    return () => {
      disconnect();
    }
  }, [room]);

  useEffect(() => {
    const onRemoteParticipantCss = 'video-container__on-remote-participant';
    if (participants.length > 1) {
      videoContainerRef.current.classList.add(onRemoteParticipantCss);
    } else {
      videoContainerRef.current.classList.remove(onRemoteParticipantCss);
    }
  }, [participants]);

  useEffect(() => {
    if (!videoStop) {
      toggleTrack(localVideoTrack, 'enable');
    } else {
      toggleTrack(localVideoTrack, 'disable');
    }
  }, [videoStop]);

  useEffect(() => {
    if (!audioStop) {
      toggleTrack(localAudioTrack, 'enable');
    } else {
      toggleTrack(localAudioTrack, 'disable');
    }
  }, [audioStop]);

  useEffect(() => {
    if (!screenShare) {
      if (!localScreenTrack && !localVideoTrack) return;

      Video.createLocalVideoTrack().then((stream) => {
        localScreenTrack.stop();
        room.localParticipant.unpublishTrack(localScreenTrack);
        setLocalVideoTrack(stream);
        document.querySelector('#cam-img').classList.remove('cam-img__blocked');
        room.localParticipant.publishTrack(stream);
        stream.attach(localVideoRef.current);
      }).catch((error) => console.log(error));
    } else {
      const canvas = document.querySelector('canvas');
      if (canvas.width !== 0) {
        const ctx = canvas.getContext('2d');
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        canvas.width = 0;
        canvas.height = 0;
        document.querySelector('.local-participant').style.zIndex = '0';
        document.querySelector('.canvas-options-nav').style.display = 'none';
        document.querySelector('.remote-participant') && document.querySelector('.remote-participant').classList.remove('remote-participant__whiteboard-on');

        mineDataTrack.send(JSON.stringify({
          action: 'whiteboard:close',
        }));
      }


      navigator.mediaDevices.getDisplayMedia().then((screenStream) => {
        const screenTrack = new Video.LocalVideoTrack(screenStream.getTracks()[0], { name: 'screen' });
        setLocalScreenTrack(screenTrack);
        document.querySelector('#cam-img').classList.add('cam-img__blocked');
        localVideoTrack.stop();
        room.localParticipant.unpublishTrack(localVideoTrack);
        room.localParticipant.publishTrack(screenTrack);
        screenTrack.attach(localVideoRef.current);
      }).catch((error) => {
        console.log(error);
        Sentry.captureEvent(error);
        alert('Infelizmente não foi possível compartilhar a tela. Por favor, entre em contato com o atendimento');
      });
    }
  }, [screenShare, room]);

  return (
    <div className="video-container" ref={videoContainerRef}>
      {participants.length && participants.map((participant) =>
        <Participant
          key={participant.sid}
          isLocal={participant.identity === username}
          participant={participant}
          refs={{ localAudioRef, localVideoRef }}
          setRemoteDataTrack={setRemoteDataTrack}
          tracks={participant.identity === username ? { setLocalAudioTrack, setLocalVideoTrack } : undefined }
        />
      )}
      <FinishLessonPopup
        popupFinishLessonRef={popupFinishLessonRef}
        handleLessonFinished={handleLessonFinished}
        stayOnLesson={stayOnLesson}
      />
      <MessagePopup userMsg={userMsg} />
      { isProfessor && <ChronoPopup handlePopup={handlePopup} /> }
      <Canvas
        localDataTrack={mineDataTrack}
        remoteDataTrack={remoteDataTrack}
      />
    </div>
  );

}

export default VideoContainer;
