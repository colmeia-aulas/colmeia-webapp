import React, { useRef, useCallback, useEffect, useState } from 'react';

import NavBar from './NavBar';
import LessonArea from './LessonArea';

import helpers from '../../utils/helpers';

import './user-interface.css';

function UserInterface() {
  const [chatVisibility, setChatVisibility] = useState(false);
  const [localDataTrack, setLocalDataTrack] = useState(null);
  
  const isMobile = helpers.isMobile();

  const chatRef = useRef();
  const popupFinishLessonRef = useRef();

  const onChatVisible = useCallback(() => {
    document.querySelector('.chat-container').style.display = 'flex';

    
    if (isMobile) {
      const localParticipant = document.querySelector('.local-participant');
      if (localParticipant) {
        localParticipant.setAttribute('style', 'display: none!important');
      }
    }
    
    if (document.querySelector('canvas') && document.querySelector('canvas').width != 0) {
      const canvas = document.querySelector('canvas');
      const videoContainer = document.querySelector('.video-container');
      canvas.width = videoContainer.offsetWidth;
      canvas.height = videoContainer.offsetHeight;

      const ctx = canvas.getContext('2d');
      ctx.globalCompositeOperation = 'destination-over';
      ctx.fillStyle = 'white';
      ctx.fillRect(0, 0, canvas.width, canvas.height);
      ctx.globalCompositeOperation = 'source-over';
    }
  }, [chatVisibility]);

  const onChatHidden = useCallback(() => {
    document.querySelector('.chat-container').style.display = 'none';
    if (isMobile) {
      const localParticipant = document.querySelector('.local-participant');
      if (localParticipant) {
        localParticipant.setAttribute('style', 'display: block!important');
      }
    }

    if (document.querySelector('canvas') && document.querySelector('canvas').width != 0) {
      const canvas = document.querySelector('canvas');
      const videoContainer = document.querySelector('.video-container');
      canvas.width = videoContainer.offsetWidth;
      canvas.height = videoContainer.offsetHeight;

      const ctx = canvas.getContext('2d');
      ctx.globalCompositeOperation = 'destination-over';
      ctx.fillStyle = 'white';
      ctx.fillRect(0, 0, canvas.width, canvas.height);
      ctx.globalCompositeOperation = 'source-over';
    }
  }, [chatVisibility]);

  const toggleChat = useCallback(() => {
    if (!chatVisibility) {
      onChatHidden();
    } else {
      onChatVisible();
    }
  }, [chatVisibility]);

  const handleChangeChatVisibility = useCallback(() => {
    setChatVisibility(!chatVisibility);
  }, [chatVisibility]);

  useEffect(() => {
    toggleChat();
  }, [chatVisibility]);
  
  return (
    <div className="user-interface">
      <NavBar
        handleChangeChatVisibility={handleChangeChatVisibility}
        chatRef={chatRef}
        popupFinishLessonRef={popupFinishLessonRef}
        localDataTrack={localDataTrack}
      />
      <LessonArea
        handleChangeChatVisibility={handleChangeChatVisibility}
        chatRef={chatRef}
        popupFinishLessonRef={popupFinishLessonRef}
        setLocalDataTrack={setLocalDataTrack}
      />
    </div>
  );
}

export default UserInterface;
