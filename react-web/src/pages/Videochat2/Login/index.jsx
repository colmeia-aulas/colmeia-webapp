import React, { useState, useCallback } from 'react';

import * as Sentry from '@sentry/react';
import axios from 'axios';

import TopBar from '../Components/TopBar';
import LoginForm from '../Components/LoginForm';
import Tips from '../Components/Tips';

import './login-component.css';

function Login({ handleLogin, roomName, isAtendimento, setUserType }) {
  const [token, setToken] = useState('');
  const [username, setUsername] = useState('');

  const generateToken = useCallback(async (username) => {
    const { data } = await axios.post('/video/token',
      {
        identity: isAtendimento ? username + '@colmeia' : username,
        room: roomName
      },
      {
        headers: { 'Content-Type': 'application/json' }
      }
    );

    Sentry.configureScope(function(scope) {
      scope.setUser({ username });
      scope.setTag("room", roomName);
    });
    Sentry.captureMessage(`Event: new login - ${roomName}:${username}`);

    setUsername(username);
    setToken(data.token);
  }, [roomName]);

  const enterRoom = useCallback(() => {
    handleLogin(token, username);
  }, [token]);

  return (
    <div className="login-component">
      <TopBar />
      {
        token
        ? <Tips enterRoom={enterRoom} />
        : <LoginForm generateToken={generateToken} setUserType={setUserType} />
      }
    </div>
  );
}

export default Login;
