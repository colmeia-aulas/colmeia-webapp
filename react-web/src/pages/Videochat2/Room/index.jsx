import React, { useEffect, useRef } from 'react';

import TopBar from '../Components/TopBar'
import UserInterface from '../Components/UserInterface';

import helpers from '../utils/helpers';
import './room-component.css';

function Room({ isAtendimento, isProfessor }) {
  const isMobile = helpers.isMobile();

  const roomRef = useRef();

  useEffect(() => {
    if (isMobile) {
      roomRef.current.classList.add('mobile-v');
    }

    if (isAtendimento) {
      roomRef.current.classList.add('is-atendimento');
    }

    if (isProfessor) {
      roomRef.current.classList.add('is-professor');
    } else {
      roomRef.current.classList.add('is-aluno');
    }
  }, []); 

  return (
    <div ref={roomRef} className='room-component'>
      { !isMobile && <TopBar /> }
      <UserInterface />
    </div>
  );
}

export default Room;
