import axios from 'axios';


describe('User id', () => {
  const debug = false;

  test('Check if user with associated id exists', async (done) => {
    function dataBuilder() {
      if (debug) {
        return {
          hostname: 'https://colmeia-lq.back4app.io/classes/_User',
          id: '4E6K7DEVxu',
          appId: 'H5h9AeZtIyrDlVYX9hI1ODxM9npPa3qohJnH5H7a',
          restAPIKey: 'LjbGK6uOnmIQoji4LQViJbo1jqL9NQetqcSp8bCQ'
        }
      } else {
        return {
          hostname: 'https://colmeia-prod-lq.back4app.io/classes/_User',
          id: 'UrxwyvUyuA',
          appId: 'ICT2Kq9bUvJwDEHFADoJehfyEZZtVh5MhPgtueTB',
          restAPIKey: 'bHErAUuRi5pOvQOfTxNPvRcpkeIwOOReha7IR0pT'
        }
      }
    }

    const { hostname, id, appId, restAPIKey } = dataBuilder();

    axios.get(`${hostname}/${id}`, {
      headers: {
        'X-Parse-Application-Id': appId,
        'X-Parse-REST-API-Key': restAPIKey,
      },
    })
      .then(({ data: user }) => {
        expect(user.objectId).toBe(id);
        expect(user.objectId.length).toBe(10);
        console.log(user);
        done();
      });
  }, 10000);
});