import React, { useState, useCallback, createContext, useEffect } from 'react';
import { useParams } from 'react-router-dom';

import Login from './Login';
import Room from './Room';
import Avaliation from './Avaliation';

import './index.css';

export const RoomContext = createContext();

function Main() {
  const [token, setToken] = useState('');
  const [username, setUsername] = useState('');
  const [lessonFinished, setLessonFinished] = useState(false);

  const [videoStop, setVideoStop] = useState(false);
  const [audioStop, setAudioStop] = useState(false);
  const [screenShare, setScreenShare] = useState(false);

  const [userType, setUserType] = useState('');

  const { id: roomName } = useParams();
  const isAtendimento = new URL(window.location.href).searchParams.has('atendimento');

  const handleLogin = useCallback((token, username) => {
    setToken(token);
    setUsername(username);
  }, []);

  const handleLessonFinished = useCallback(() => {
    setToken('');
    setLessonFinished(true);
  }, []);

  useEffect(() => {
    if (roomName === 'p2p' || roomName === 'sala1') return;
    fetch(`https://colmeia-prod-lq.back4app.io/classes/_User/${roomName}`,
      {
        headers: {
          'X-Parse-Application-Id': 'ICT2Kq9bUvJwDEHFADoJehfyEZZtVh5MhPgtueTB',
          'X-Parse-REST-API-Key': 'bHErAUuRi5pOvQOfTxNPvRcpkeIwOOReha7IR0pT',
        },
      }
    )
      .then((response) => response.json())
      .then((user) => {
        if (!user.objectId) {
          alert(`Sala especificada "${roomName}" não existe! Por favor verificar o link fornecido.`);
          document.querySelector('.login-box .section__button button').style.setProperty('pointer-events', 'none');
        }
      });
  }, []);

  if (!token && !lessonFinished) {
    return (
      <Login handleLogin={handleLogin} roomName={roomName} isAtendimento={isAtendimento} setUserType={setUserType}/>
    );
  }

  if (token) {
    const contextValue = {
      token,
      roomName,
      username,
      handleLessonFinished,
      isAtendimento,
      isProfessor: userType === 'professor',
      videoState: { videoStop, setVideoStop },
      audioState: { audioStop, setAudioStop },
      screenState: { screenShare, setScreenShare }
    }
    return (
      <RoomContext.Provider value={contextValue}>
        <Room isAtendimento={isAtendimento} isProfessor={contextValue.isProfessor}/>
      </RoomContext.Provider>
    );
  }

  if (lessonFinished) {
    return (
      <Avaliation username={username} roomName={roomName} />
    );
  }
}

export default Main;
