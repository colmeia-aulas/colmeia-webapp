const helpers = {
  isMobile: () => {
    return /Mobi|Android|iPad|iPhone/i.test(navigator.userAgent);
  }
};

export default helpers;
