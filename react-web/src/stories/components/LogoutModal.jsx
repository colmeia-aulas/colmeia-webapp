import React from 'react';
import PropTypes from 'prop-types';

function LogoutModal({ isModalOpen, confirmLogout, cancelLogout }) {
  return (
    <div className={isModalOpen ? 'modal-visible' : 'modal-hidden'}>
      <div className="confirmation-box">
        <h3 className="confirmation-box__title">Sair da sala</h3>
        <p className="confirmation-box__info">Tem certeza que você deseja sair da sala? A aula será interrompida, caso ela ainda esteja acontecendo.</p>
        <div className="btn-group">
          <button className="logout-btn" onClick={confirmLogout}>Sair da sala</button>
          <button className="cancel-logout-btn" onClick={cancelLogout}>Permanecer</button>
        </div>
      </div>
    </div>
  );
}

export default LogoutModal;

LogoutModal.propTypes = {
  isModalOpen: PropTypes.bool.isRequired,
  confirmLogout: PropTypes.func.isRequired,
  cancelLogout: PropTypes.func.isRequired,
}
