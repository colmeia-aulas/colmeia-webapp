import React from 'react';
import PropTypes from 'prop-types';

function StreamAvaliation({feedbackState, handleVote, handleStreamAvaliation}) {
  const { feedback, setFeedback } = feedbackState;

  return (
    <div className="stream-avaliation">
      <h3 className="aval-child">Aula finalizada</h3>
      <div className="vote aval-child">
        <h4>Avalie a qualidade da transmissão</h4>
        <div className="button-group">
          <button onClick={handleVote} className="thumbs-up">
            <img src="/img/aulaonline-thumbsup.png"></img>
          </button>
          <button onClick={handleVote} className="thumbs-down">
            <img src="/img/aulaonline-thumbsdown.png"></img>
          </button>
        </div>
      </div>
      <div className="feedback aval-child">
        <h4>Deixe seu comentário sobre a aula</h4>
        <textarea
          rows="6"
          value={feedback}
          placeholder="Escreva seu comentário aqui"
          onChange={(event) => setFeedback(event.target.value)}
        >
        </textarea>
      </div>
      <button
        className="feedback-btn"
        onClick={handleStreamAvaliation}
      >
        Enviar
      </button>
    </div>
  );
}

export default StreamAvaliation;

StreamAvaliation.propTypes = {
  feedbackState: PropTypes.shape({
    feedback: PropTypes.string.isRequired,
    setFeedback: PropTypes.func.isRequired,
  }).isRequired,
  handleVote: PropTypes.func.isRequired,
  handleStreamAvaliation: PropTypes.func.isRequired,
}
