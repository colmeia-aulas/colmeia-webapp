import React from 'react';
import PropTypes from 'prop-types';

function VoteBtn(imgSrc, btnTypeClass, handleVote) {
  return (
    <button onClick={handleVote} className={btnTypeClass}>
      <img src={imgSrc}></img>
    </button>
  );
}

export default VoteBtn;

VoteBtn.propTypes = {
  imgSrc: PropTypes.string.isRequired,
  btnTypeClass: PropTypes.string.isRequired,
  handle: PropTypes.func.isRequired,
}
