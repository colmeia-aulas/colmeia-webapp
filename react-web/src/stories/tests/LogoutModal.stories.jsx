import React from 'react';
import { action } from '@storybook/addon-actions';

import LogoutModal from '../components/LogoutModal';

export default {
  component: LogoutModal,
  title: 'Logout confirmation modal',
  excludeStories: /.*Data$/,
};

const logoutTrueData = {
  isModalOpen: true,
  confirmLogout: action('confirm logout'),
  cancelLogout: action('cancel logout'),
};

const logoutfalseData = {
  ...logoutTrueData,
  isModalOpen: false,
};

export const openedModal = () => <LogoutModal {...logoutTrueData} />;
export const closedModal = () => <LogoutModal {...logoutfalseData} />;

