import React from 'react';
import { action } from '@storybook/addon-actions';

import StreamAvaliation from '../components/StreamAvaliation';

export default {
  component: StreamAvaliation,
  title: 'Stream avaliation component',
  excludeStories: /.*Data$/,
};

const streamData = {
  feedbackState: { feedback: '', setFeedback: action('set feedback') },
  handleVote: action('handle vote'),
  handleStreamAvaliation: action('handle stream avaliation'),
}

export const Default = () => <StreamAvaliation {...streamData} />;