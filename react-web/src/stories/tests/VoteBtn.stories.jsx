import React from 'react';
import { action } from '@storybook/addon-actions';

import VoteBtn from '../components/VoteBtn';

export default {
  component: VoteBtn,
  title: 'Vote Button',
  excludeStories: /.*Data$/,
};



export const green = () => 
  <VoteBtn 
    btnTypeClass="thumbs-up" 
    imgSrc="/img/aulaonline-thumbsup.png"
    handleVote={action('clicou')}
  />;