const BAIRROS = {
  brasilia: {
    'todos-os-bairros': 'Todos os bairros',
    'aguas-claras': 'Águas Claras',
    'asa-norte': 'Asa Norte',
    'asa-sul': 'Asa Sul',
    brazlandia: 'Brazlândia',
    candangolandia: 'Candangolândia',
    ceilandia: 'Ceilândia',
    'cruzeiro-novo': 'Cruzeiro Novo',
    'cruzeiro-velho': 'Cruzeiro Velho',
    gama: 'Gama',
    'grande-colorado': 'Grande Colorado',
    'granja-do-torto': 'Granja do Torto',
    'guara-i': 'Guará I',
    'guara-ii': 'Guará II',
    itapoa: 'Itapoã',
    'jardim-botanico': 'Jardim Botânico',
    'lago-norte': 'Lago Norte',
    'lago-sul': 'Lago Sul',
    noroeste: 'Noroeste',
    'nucleo-bandeirante': 'Núcleo Bandeirante',
    octogonal: 'Octogonal',
    paranoa: 'Paranoá',
    'park-way': 'Park Way',
    planaltina: 'Planaltina',
    'recanto-das-emas': 'Recanto das Emas',
    'riacho-fundo-i': 'Riacho Fundo I',
    'riacho-fundo-ii': 'Riacho Fundo II',
    samambaia: 'Samambaia',
    'santa-maria': 'Santa Maria',
    'setor-de-mansoes-lago-norte': 'Setor de Mansões Lago Norte',
    'setor-militar': 'Setor Militar',
    sia: 'SIA',
    sig: 'SIG',
    'sobradinho-i': 'Sobradinho I',
    'sobradinho-ii': 'Sobradinho II',
    'sof-sul': 'SOF Sul',
    sudoeste: 'Sudoeste',
    'sao-sebastiao': 'São Sebastião',
    taguatinga: 'Taguatinga',
    taquari: 'Taquari',
    varjao: 'Varjão',
    'vicente-pires': 'Vicente Pires',
    'vila-planalto': 'Vila Planalto'
  },
  'sao-paulo': {
    'todos-os-bairros': 'Todos os bairros',
    'agua-branca': 'Água Branca',
    'alto-de-pinheiros': 'Alto de Pinheiros',
    'bela-vista': 'Bela Vista',
    butanta: 'Butantã',
    'campo-belo': 'Campo Belo',
    'cidade-ademar': 'Cidade Ademar',
    consolacao: 'Consolação',
    'itaim-bibi': 'Itaim Bibi',
    jabaquara: 'Jabaquara',
    'jardim-paulista': 'Jardim Paulista',
    moema: 'Moema',
    mooca: 'Mooca',
    morumbi: 'Morumbi',
    perdizes: 'Perdizes',
    pinheiros: 'Pinheiros',
    samambaia: 'Samambaia',
    'santo-amaro': 'Santo Amaro',
    saude: 'Saúde',
    'vila-mariana': 'Vila Mariana'
  }
};

const CIDADES = {
  brasilia: 'Brasília',
  'sao-paulo': 'São Paulo'
};

const MATERIAS = {
  'todas-as-materias': 'Todas as matérias',
  matematica: 'Matemática',
  portugues: 'Português',
  fisica: 'Física',
  quimica: 'Química',
  biologia: 'Biologia',
  historia: 'História',
  geografia: 'Geografia'
  // ingles: 'Inglês'
};
const ANOS = {
  '1o-ano-fundamental': '1º ano Fundamental',
  '2o-ano-fundamental': '2º ano Fundamental',
  '3o-ano-fundamental': '3º ano Fundamental',
  '4o-ano-fundamental': '4º ano Fundamental',
  '5o-ano-fundamental': '5º ano Fundamental',
  '6o-ano-fundamental': '6º ano Fundamental',
  '7o-ano-fundamental': '7º ano Fundamental',
  '8o-ano-fundamental': '8º ano Fundamental',
  '9o-ano-fundamental': '9º ano Fundamental',
  '1o-ano-medio': '1º ano Médio',
  '2o-ano-medio': '2º ano Médio',
  '3o-ano-medio': '3º ano Médio'
};

const ANOS_NIVEIS = {
  '1o-ano-fundamental': 'Fundamental 1',
  '2o-ano-fundamental': 'Fundamental 1',
  '3o-ano-fundamental': 'Fundamental 1',
  '4o-ano-fundamental': 'Fundamental 1',
  '5o-ano-fundamental': 'Fundamental 1',
  '6o-ano-fundamental': 'Fundamental 2',
  '7o-ano-fundamental': 'Fundamental 2',
  '8o-ano-fundamental': 'Fundamental 2',
  '9o-ano-fundamental': 'Fundamental 2',
  '1o-ano-medio': 'Médio',
  '2o-ano-medio': 'Médio',
  '3o-ano-medio': 'Médio'
};

const NIVEIS = {
  'fundamental-1': 'Fundamental 1',
  'fundamental-2': 'Fundamental 2',
  medio: 'Médio'
};

export default {
  CIDADES,
  BAIRROS,
  MATERIAS,
  ANOS,
  NIVEIS
};
