const express = require('express');
const addressController = require('../controllers/address');
const { isLoggedInAjax } = require('../middlewares/cookies');

const router = express.Router();

router
  .post('/reforco-escolar/endereco/buscar', isLoggedInAjax, addressController.find)
  .post('/reforco-escolar/endereco/adicionar', isLoggedInAjax, addressController.create)
  .post('/reforco-escolar/endereco/remover', isLoggedInAjax, addressController.delete);

module.exports = router;
