const express = require('express');
const campaignController = require('../controllers/campaigns');

let router = express.Router();

// All GET campaign landing pages go here!
router = campaignController.setupCampaignPages(router);

router
  .post('/calculadora-pas-lead', campaignController.leadCalculadora)
  .post('/acompanhamento-escolar', campaignController.leadAcompanhamento)
  .post('/landing-campanha', campaignController.lead)
  .get(
    [
      '/aula-particular/:id',
      '/aula-particular/:id/:end',
      '/reforco-escolar/:id',
      '/reforco-escolar/:id/:end',
      '/acompanhamento-escolar/fundamental-1/:id',
      '/ebook-1/:id',
      '/ebook-1/:id/:end',
      '/ebook-2/:id',
      '/cartao/:id'
    ],
    campaignController.setupCampaignPagesWithId
  );

module.exports = router;
