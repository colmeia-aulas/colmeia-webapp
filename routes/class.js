const express = require('express');
const multer = require('multer');
const { multerConfig } = require('../config/libs');

const {
  initializeCookies,
  estaNoFluxo,
  estaNoFluxoAjax,
  isLoggedIn,
  isLoggedInAjax
} = require('../middlewares/cookies');
const { validateParamsBusca } = require('../middlewares/class');
const classController = require('../controllers/class');

const upload = multer(multerConfig);

const router = express.Router();

router
  .get(
    [
      '/reforco-escolar/:materia/:cidade/:bairro/:ano',
      '/reforco/:materia/:cidade/:bairro/:ano',
      '/aula-particular/:materia/:cidade/:bairro/:ano',
      '/professor-particular/:materia/:cidade/:bairro/:ano'
    ],
    initializeCookies,
    validateParamsBusca,
    classController.buscaProfessores
  )

  .get('/reforco-escolar/agendar', classController.newAgendamento)

  .get('/reforco-escolar/resumo', estaNoFluxo, isLoggedIn, classController.summary)

  .post('/reforco-escolar/horarios/escolher', initializeCookies, classController.selectTime)

  .post(
    '/reforco-escolar/verificar-codigo',
    isLoggedInAjax,
    estaNoFluxoAjax,
    classController.verifyCode
  )

  .post(
    '/reforco-escolar/marcar-aula',
    isLoggedInAjax,
    estaNoFluxoAjax,
    upload.any(),
    classController.scheduleClass
  )

  .post('/reforco-escolar/aula/cancelar', isLoggedInAjax, classController.requestClassCancel)
  .post('/reforco-escolar/aula/cancelar/confirmar', isLoggedInAjax, classController.cancelClass);

module.exports = router;
