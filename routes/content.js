const express = require('express');
const contentController = require('../controllers/content');
const miscController = require('../controllers/misc');
const blogController = require('../controllers/blog');

let router = express.Router();

// static pages
router = contentController.setupLandingPages(router);

router
  .post('/calculadora-pas', miscController.calculateScorePAS)
  // ?: Verificar necessidade da requisição
  .post('/email-marketing', miscController.emailMarketing)
  .post('/newsletter', miscController.newsletter)
  .post('/novo-agendamento', miscController.emailNovoAgendamento)
  .post('/novo-agendamento-idiomas', miscController.emailNovoAgendamentoIdiomas)
  .get('/vagas', miscController.allJobsController)
  .get('/vagas/:id', miscController.jobController)
  .get('/blog', blogController.main)
  .get('/blog/:id', blogController.article);

module.exports = router;
