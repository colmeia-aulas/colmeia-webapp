const express = require('express');

const formsController = require('../controllers/forms');

const router = express.Router();

router
  .get('/formulario', formsController.get)
  .get('/cadastro-comunidade-estudos', formsController.restrictLP)
  .post('/googlesheets', formsController.sendToGoogleSheets)
  .post('/streamfeedback', formsController.sendStreamFeedback);

module.exports = router;
