const express = require('express');
const miscController = require('../controllers/misc');

const router = express.Router();

router
  .get('/cadastro-online', miscController.cadastroOnline)
  .get('/cliente-online', miscController.clienteOnline)
  .get('/video-test', miscController.videoTest)
  .post('/video/token', miscController.twilioAuth)
  .get('/comunidade-de-estudos', miscController.restrictPage)
  .get('/comunidade-de-estudos/:id', miscController.restrictPage)
  .get('/comunidade-de-estudos/pdf/:id', miscController.restrictPdf)
  .get('/aulao-live', miscController.live)
  .get('/favicon.ico', miscController.favicon)
  .get('/politica-de-privacidade', miscController.privacyPolicy)
  .get('/curriculo/:profId', miscController.curriculo)
  .get(
    '/A8qrUDbW1Z8RxcyLyBnHyFeH9Vhq375rVOda7OYzz16LTaWv7zgdEkdViNfnlrKNMZ4TftmCjvibsApxfPHvUrkWj7pr7HpPNEZk',
    miscController.dataStudio
  )
  .post('/resize-image', miscController.resizeImage);

module.exports = router;
