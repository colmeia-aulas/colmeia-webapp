const express = require('express');

const router = express.Router();

router
  .get('*', (req, res, next) => next(new Error('NOT_FOUND_GET')))
  .post('*', (req, res, next) => next(new Error('NOT_FOUND')))
  .delete('*', (req, res, next) => next(new Error('NOT_FOUND')))
  .put('*', (req, res, next) => next(new Error('NOT_FOUND')))
  .patch('*', (req, res, next) => next(new Error('NOT_FOUND')));

module.exports = router;
