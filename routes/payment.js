const express = require('express');
const paymentController = require('../controllers/payment');
const { isLoggedInAjax } = require('../middlewares/cookies');

const router = express.Router();

router
  .post('/reforco-escolar/pagamento/buscar', isLoggedInAjax, paymentController.find)
  .post('/reforco-escolar/pagamento/adicionar', isLoggedInAjax, paymentController.create)
  .post('/reforco-escolar/pagamento/remover', isLoggedInAjax, paymentController.delete);

module.exports = router;
