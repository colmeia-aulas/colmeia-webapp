const path = require('path');
const express = require('express'); // config

const router = express.Router();

// router.get(['/app', '/app/*'], (req, res, next) => {
router.get('*', (req, res, next) => {
  const url = req.originalUrl;

  if (!url.startsWith('/app')) return next();

  return res.sendFile(path.join(__dirname, '../react-web', 'build', 'index.html'));
});

module.exports = router;
