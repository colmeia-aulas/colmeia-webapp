const express = require('express');
const multer = require('multer');
const { multerConfig } = require('../config/libs');
const { isProfLoggedIn, isProfLoggedInAjax } = require('../middlewares/cookies');
const tutorsController = require('../controllers/tutors');

const upload = multer(multerConfig);
const router = express.Router();

router
  .get(['/professores', '/professor', '/seja-um-professor'], tutorsController.showHome)
  // /cadastro should come first!
  .get('/professor/cadastro', isProfLoggedIn, tutorsController.showRegisterPage)
  .get('/professor/:id', tutorsController.getProfessor)
  .post('/professor/cadastro/update', isProfLoggedInAjax, tutorsController.update)
  .post(
    '/professor/cadastro/update/final',
    isProfLoggedInAjax,
    upload.any(),
    tutorsController.updateFinal
  );

module.exports = router;
