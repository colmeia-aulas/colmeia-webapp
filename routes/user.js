const express = require('express');
const multer = require('multer');
const { multerConfig } = require('../config/libs');
const { initializeCookies, isLoggedIn, isLoggedInAjax } = require('../middlewares/cookies');
const userController = require('../controllers/user');

const upload = multer(multerConfig);

const router = express.Router();

// Show routes
router
  .get('/cadastro', initializeCookies, userController.showCadastroExpress)
  .get('/aluno/entrar', initializeCookies, userController.showLogin)
  .get('/aluno/registrar', initializeCookies, userController.showRegister)
  .get('/aluno/recuperar-senha', initializeCookies, userController.showPasswordReset)

  .get('/aluno/perfil', isLoggedIn, userController.showProfile)
  .get('/aluno/agenda', isLoggedIn, userController.showSchedule)
  .get('/aluno/historico', isLoggedIn, userController.showHistory)

  .get('/aluno/sair', userController.postLogout);

// Get routes
router
  .get('/aluno/perfil/buscar', isLoggedIn, userController.getProfile)
  .get('/aluno/agenda/buscar', isLoggedIn, userController.getSchedule)
  .get('/aluno/historico/buscar', isLoggedIn, userController.getHistory);

// Post routes
router
  .post('/aluno/entrar', userController.postLogin)
  .post('/aluno/registrar', userController.postRegister)
  .post('/aluno/update', isLoggedInAjax, userController.update)
  .post('/aluno/recuperar-senha', initializeCookies, userController.postPasswordReset)

  .post('/aluno/perfil/foto', isLoggedInAjax, upload.any(), userController.postProfilePicture)
  .post('/aluno/pre-cadastro/email', userController.emailPreRegister);

module.exports = router;
