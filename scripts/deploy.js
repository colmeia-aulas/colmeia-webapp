const { spawn } = require('child_process');
const { readFileSync, writeFileSync } = require('fs');

const run = () => {
  const version = readFileSync('version', 'utf8');
  console.log('> Application deploy started.');
  const ebProcess = spawn('eb', ['deploy', 'Custom-env', '-l', version]);
  ebProcess.stdout.on('data', data => process.stdout.write(data));
  ebProcess.on('close', () => {
    writeFileSync('version', Number(version) + 1);
    console.log('> Application deploy finished.');
  });
};

run();
