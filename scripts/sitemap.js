const sm = require('sitemap');
const path = require('path');
const { writeFileSync } = require('fs');
const { CONST } = require('../public/js/constantes');
const getBlogPostsUrls = require('../controllers/blog').getBlogPostsUrls;

const { BAIRROS, MATERIAS_ALL, ANOS } = CONST;

const generateUrls = async () => {
  const today = new Date().toISOString().slice(0, 10);

  const urls = [
    {
      url: '/',
      changefreq: 'monthly',
      priority: 1,
      lastMod: today
    },
    {
      url: '/politica-de-privacidade',
      changefreq: 'monthly',
      priority: 0.5,
      lastMod: today
    },
    {
      url: '/favicon.ico',
      changefreq: 'monthly',
      priority: 0.5,
      lastMod: today
    },
    {
      url: '/conheca-a-colmeia',
      changefreq: 'monthly',
      priority: 0.9,
      lastMod: today
    },
    {
      url: '/calculadora-pas',
      changefreq: 'monthly',
      priority: 0.9,
      lastMod: today
    },
    {
      url: '/perguntas-frequentes',
      changefreq: 'monthly',
      priority: 0.5,
      lastMod: today
    },
    {
      url: '/quem-somos',
      changefreq: 'monthly',
      priority: 0.5,
      lastMod: today
    },
    {
      url: '/aula-particular-sp',
      changefreq: 'monthly',
      priority: 0.5,
      lastMod: today
    },
    {
      url: '/acompanhamento-escolar',
      changefreq: 'monthly',
      priority: 0.5,
      lastMod: today
    },
    {
      url: '/aula-particular/ingles',
      changefreq: 'monthly',
      priority: 1,
      lastMod: today
    },
    {
      url: '/aula-particular/certificacao-ingles',
      changefreq: 'monthly',
      priority: 1,
      lastMod: today
    },
    {
      url: '/aula-particular/recuperacao',
      changefreq: 'monthly',
      priority: 1,
      lastMod: today
    },
    {
      url: '/reforco-escolar/fantasma',
      changefreq: 'monthly',
      priority: 1,
      lastMod: today
    },
    {
      url: '/aula-particular/matematica',
      changefreq: 'monthly',
      priority: 1,
      lastMod: today
    },
    {
      url: '/aula-particular/portugues',
      changefreq: 'monthly',
      priority: 1,
      lastMod: today
    },
    {
      url: '/aula-particular/brasilia',
      changefreq: 'monthly',
      priority: 1,
      lastMod: today
    },
    {
      url: '/aula-particular/sao-paulo',
      changefreq: 'monthly',
      priority: 1,
      lastMod: today
    },
    {
      url: '/reforco-escolar/matematica',
      changefreq: 'monthly',
      priority: 1,
      lastMod: today
    },
    {
      url: '/reforco-escolar/portugues',
      changefreq: 'monthly',
      priority: 1,
      lastMod: today
    },
    {
      url: '/reforco-escolar/brasilia',
      changefreq: 'monthly',
      priority: 1,
      lastMod: today
    },
    {
      url: '/reforco-escolar/sao-paulo',
      changefreq: 'monthly',
      priority: 1,
      lastMod: today
    },
    {
      url: '/seja-um-professor',
      changefreq: 'monthly',
      priority: 1
    },
    {
      url: '/professor',
      changefreq: 'monthly',
      priority: 1
    },
    {
      url: '/professores',
      changefreq: 'monthly',
      priority: 1
    },
    {
      url: '/blog',
      changefreq: 'monthly',
      priority: 1
    }
  ];

  Object.keys(BAIRROS).forEach(cidade => {
    Object.keys(BAIRROS[cidade]).forEach(bairro => {
      Object.keys(MATERIAS_ALL).forEach(materia => {
        Object.keys(ANOS).forEach(ano => {
          urls.push(
            {
              url: `/reforco-escolar/${materia}/${cidade}/${bairro}/${ano}/`,
              changefreq: 'always',
              priority: 0.9,
              lastMod: today
            },
            {
              url: `/reforco/${materia}/${cidade}/${bairro}/${ano}/`,
              changefreq: 'always',
              priority: 0.9,
              lastMod: today
            },
            {
              url: `/aula-particular/${materia}/${cidade}/${bairro}/${ano}/`,
              changefreq: 'always',
              priority: 0.9,
              lastMod: today
            },
            {
              url: `/professor-particular/${materia}/${cidade}/${bairro}/${ano}/`,
              changefreq: 'always',
              priority: 0.9,
              lastMod: today
            }
          );
        });

        ['fundamental', 'medio'].forEach(nivel => {
          urls.push(
            {
              url: `/reforco-escolar/${materia}/${cidade}/${bairro}/${nivel}/`,
              changefreq: 'always',
              priority: 0.9,
              lastMod: today
            },
            {
              url: `/reforco/${materia}/${cidade}/${bairro}/${nivel}/`,
              changefreq: 'always',
              priority: 0.9,
              lastMod: today
            },
            {
              url: `/aula-particular/${materia}/${cidade}/${bairro}/${nivel}/`,
              changefreq: 'always',
              priority: 0.9,
              lastMod: today
            },
            {
              url: `/professor-particular/${materia}/${cidade}/${bairro}/${nivel}/`,
              changefreq: 'always',
              priority: 0.9,
              lastMod: today
            }
          );
        });
      });
    });
  });

  const blogPostsUrls = await getBlogPostsUrls();
  blogPostsUrls.forEach(blogPostUrl => {
    urls.push({
      url: `/blog/${blogPostUrl}/`,
      changefreq: 'monthly',
      priority: 0.5,
      lastMod: today
    });
  });

  return urls;
};

generateUrls().then(urls => {
  const sitemap = sm.createSitemap({
    hostname: 'https://aulascolmeia.com.br',
    cacheTime: 600000, // 600 sec cache period
    urls
  });

  try {
    writeFileSync(path.join(__dirname, '..', 'public', 'sitemap.xml'), sitemap.toString());
    console.log('Sitemap written to public/sitemap.xml');
  } catch (error) {
    console.log(error);
  }
});
