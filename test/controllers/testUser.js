const chai = require('chai');
const userController = require('../../controllers/user.js');
const Parse = require('../../config/parse.js');

const assert = chai.assert;

function check(done, f) {
  try {
    f();
    done();
  } catch (e) {
    done(e);
  }
}

function makeid(length) {
  let result = '';
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  const charactersLength = characters.length;
  for (let i = 0; i < length; i += 1) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

const req = {};

module.exports.tests = () => {
  describe('Teste login de user', () => {
    it('Teste de login válido', done => {
      const userData = {
        body: {
          email: 'teste@teste.com',
          password: 'teste123'
        },
        session: {}
      };
      const resHandler = {
        json(data) {
          req.session = userData.session;
          check(done, () => {
            assert.equal(data.user.email, userData.body.email);
          });
        }
      };
      const errorHandler = error => {
        check(done, () => {
          assert.fail(error);
        });
      };
      userController.postLogin(userData, resHandler, errorHandler);
    });

    it('Teste de login com user vs senha errados', done => {
      const userData = {
        body: {
          email: 'teste@teste.com',
          password: 'teste1234'
        },
        session: {}
      };
      const resHandler = {
        json() {
          check(done, () => {
            assert.fail('User returned');
          });
        }
      };
      const errorHandler = error => {
        check(done, () => {
          assert.equal(error.code, 101);
        });
      };
      userController.postLogin(userData, resHandler, errorHandler);
    });
  });
  describe('Testes de dados do usuário', () => {
    it('Pegar perfil do usuário', done => {
      const resHandler = {
        json(data) {
          check(done, () => {
            assert.isTrue(data.success);
            assert.isDefined(data.perfil.nome);
            assert.isDefined(data.perfil.telefone);
            assert.isArray(data.enderecos.ends);
            assert.isObject(data.cartaoFavorito);
          });
        }
      };
      const errorHandler = error => {
        check(done, () => {
          assert.fail(error);
        });
      };
      userController.getProfile(req, resHandler, errorHandler);
    });
    it('Pegar agenda do usuário', done => {
      const resHandler = {
        json(data) {
          check(done, () => {
            assert.isArray(data.aulas);
          });
        }
      };
      const errorHandler = error => {
        check(done, () => {
          assert.fail(error);
        });
      };
      userController.getSchedule(req, resHandler, errorHandler);
    });
    it('Pegar histórico do usuário', done => {
      const resHandler = {
        json(data) {
          check(done, () => {
            assert.isArray(data.aulas);
          });
        }
      };
      const errorHandler = error => {
        check(done, () => {
          assert.fail(error);
        });
      };
      userController.getHistory(req, resHandler, errorHandler);
    });
  });
  describe('Teste logout', () => {
    it('Teste de logout realizado', done => {
      const resHandler = {
        redirect() {
          check(done, () => {
            assert.deepEqual(req.session, {
              user: {},
              token: undefined,
              aula: {},
              nextPage: '/'
            });
          });
        }
      };
      const errorHandler = error => {
        check(done, () => {
          assert.fail(error);
        });
      };
      userController.postLogout(req, resHandler, errorHandler);
    });
  });
  describe('Teste de registro de usuário', () => {
    it('Teste de registro realizado', done => {
      const id = makeid(16);
      const userData = {
        body: {
          email: `teste+${id}@teste.com`,
          username: `teste+${id}@teste.com`,
          password: 'colmeia123',
          nome: id,
          telefone: '+5555555555555',
          ga_id: id,
          digits_id: id
        },
        session: {}
      };
      const resHandler = {
        json(data) {
          check(done, () => {
            assert.isString(userData.session.token);
            assert.isObject(userData.session.user);
            assert.isTrue(data.success);
          });
          const User = Parse.Object.extend('_User');
          const user = new User();
          user.id = data.user.id;
          user.destroy();
        }
      };
      const errorHandler = error => {
        check(done, () => {
          assert.fail(error);
        });
      };
      userController.postRegister(userData, resHandler, errorHandler);
    });
  });
};
