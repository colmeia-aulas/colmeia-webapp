const { values } = require('../data/calculadoraWeights.json');

const cursos = [
  {
    id: 1,
    nome: 'Administração',
    campus: 1,
    nota: 73.224
  },
  {
    id: 2,
    nome: 'Administração Noturno',
    campus: 1,
    nota: 47.41
  },
  {
    id: 3,
    nome: 'Agronomia',
    campus: 1,
    nota: 31.18
  },
  {
    id: 4,
    nome: 'Arquitetura e Urbanismo',
    campus: 1,
    nota: 67.03
  },
  {
    id: 5,
    nome: 'Arquitetura e Urbanismo Noturno',
    campus: 1,
    nota: 30.183
  },
  {
    id: 6,
    nome: 'Arquivologia Noturno',
    campus: 1,
    nota: -17.637
  },
  {
    id: 7,
    nome: 'Artes Cênicas',
    campus: 1,
    nota: -4.86
  },
  {
    id: 8,
    nome: 'Artes Visuais',
    campus: 1,
    nota: 13.106
  },
  {
    id: 9,
    nome: 'Bacharelado em Química',
    campus: 1,
    nota: 44.867
  },
  {
    id: 10,
    nome: 'Biblioteconomia',
    campus: 1,
    nota: -17.562
  },
  {
    id: 11,
    nome: 'Biotecnologia',
    campus: 1,
    nota: 119.946
  },
  {
    id: 12,
    nome: 'Ciência da Computação',
    campus: 1,
    nota: 104.193
  },
  {
    id: 13,
    nome: 'Ciência Política',
    campus: 1,
    nota: 74.365
  },
  {
    id: 14,
    nome: 'Ciências Ambientais Noturno',
    campus: 1,
    nota: 7.692
  },
  {
    id: 15,
    nome: 'Ciências Biológicas',
    campus: 1,
    nota: 81.098
  },
  {
    id: 16,
    nome: 'Ciências Contábeis',
    campus: 1,
    nota: 49.066
  },
  {
    id: 17,
    nome: 'Ciências Contábeis Noturno',
    campus: 1,
    nota: 23.256
  },
  {
    id: 18,
    nome: 'Ciências Econômicas',
    campus: 1,
    nota: 107.892
  },
  {
    id: 87,
    nome: 'Ciências Naturais',
    campus: 2,
    nota: -61.498
  },
  {
    id: 89,
    nome: 'Ciências Naturais Noturno',
    campus: 2,
    nota: -64.082
  },
  {
    id: 19,
    nome: 'Ciências Sociais',
    campus: 1,
    nota: 28.202
  },
  {
    id: 20,
    nome: 'Computação Noturno',
    campus: 1,
    nota: -0.779
  },
  {
    id: 21,
    nome: 'Comunicação Organizacional Noturno',
    campus: 1,
    nota: 55.182
  },
  {
    id: 22,
    nome: 'Comunicação Social',
    campus: 1,
    nota: 85.418
  },
  {
    id: 23,
    nome: 'Design',
    campus: 1,
    nota: 79.027
  },
  {
    id: 24,
    nome: 'Direito',
    campus: 1,
    nota: 140.735
  },
  {
    id: 25,
    nome: 'Direito Noturno',
    campus: 1,
    nota: 122.994
  },
  {
    id: 26,
    nome: 'Educação Física (Bacharelado)',
    campus: 1,
    nota: 20.742
  },
  {
    id: 27,
    nome: 'Educação Física (Licenciatura)',
    campus: 1,
    nota: 10.616
  },
  {
    id: 91,
    nome: 'Enfermagem',
    campus: 3,
    nota: 48.878
  },
  {
    id: 28,
    nome: 'Enfermagem',
    campus: 1,
    nota: 54.159
  },
  {
    id: 29,
    nome: 'Engenharia Ambiental',
    campus: 1,
    nota: 641.661
  },
  {
    id: 30,
    nome: 'Engenharia Civil',
    campus: 1,
    nota: 127.896
  },
  {
    id: 31,
    nome: 'Engenharia de Computação',
    campus: 1,
    nota: 129.881
  },
  {
    id: 32,
    nome: 'Engenharia de Produção Noturno',
    campus: 1,
    nota: 101.073
  },
  {
    id: 33,
    nome: 'Engenharia de Redes de Comunicação',
    campus: 1,
    nota: 62.742
  },
  {
    id: 34,
    nome: 'Engenharia Elétrica',
    campus: 1,
    nota: 129.276
  },
  {
    id: 35,
    nome: 'Engenharia Florestal',
    campus: 1,
    nota: 21.833
  },
  {
    id: 36,
    nome: 'Engenharia Mecânica',
    campus: 1,
    nota: 141.927
  },
  {
    id: 37,
    nome: 'Engenharia Mecatrônica',
    campus: 1,
    nota: 118.922
  },
  {
    id: 38,
    nome: 'Engenharia Química',
    campus: 1,
    nota: 96.62
  },
  {
    id: 85,
    nome: 'Engenharias',
    campus: 2,
    nota: 60.982
  },
  {
    id: 39,
    nome: 'Estatística',
    campus: 1,
    nota: 78.705
  },
  {
    id: 90,
    nome: 'Farmácia',
    campus: 3,
    nota: 40.225
  },
  {
    id: 40,
    nome: 'Farmácia',
    campus: 1,
    nota: 51.8
  },
  {
    id: 41,
    nome: 'Farmácia Noturno',
    campus: 1,
    nota: 17.91
  },
  {
    id: 42,
    nome: 'Filosofia',
    campus: 1,
    nota: 28.978
  },
  {
    id: 43,
    nome: 'Filosofia Noturno',
    campus: 1,
    nota: 398.591
  },
  {
    id: 44,
    nome: 'Física',
    campus: 1,
    nota: 77.341
  },
  {
    id: 92,
    nome: 'Fisioterapia',
    campus: 3,
    nota: 32.952
  },
  {
    id: 95,
    nome: 'Fonoaudiologia',
    campus: 3,
    nota: 17.507
  },
  {
    id: 45,
    nome: 'Geofísica',
    campus: 1,
    nota: -13.023
  },
  {
    id: 46,
    nome: 'Geografia',
    campus: 1,
    nota: 24.514
  },
  {
    id: 47,
    nome: 'Geologia',
    campus: 1,
    nota: 11.61
  },
  {
    id: 88,
    nome: 'Gestão Ambiental',
    campus: 2,
    nota: -72.403
  },
  {
    id: 48,
    nome: 'Gestão de Agronegócio Noturno',
    campus: 1,
    nota: -39.456
  },
  {
    id: 49,
    nome: 'Gestão de Políticas Públicas Noturno',
    campus: 1,
    nota: 27.429
  },
  {
    id: 86,
    nome: 'Gestão do Agronegócio',
    campus: 2,
    nota: -69.446
  },
  {
    id: 50,
    nome: 'História',
    campus: 1,
    nota: 74.551
  },
  {
    id: 51,
    nome: 'História Noturno',
    campus: 1,
    nota: 36.147
  },
  {
    id: 52,
    nome: 'Jornalismo',
    campus: 1,
    nota: 112.338
  },
  {
    id: 53,
    nome: 'Letras – Português do Brasil como Segunda Língua',
    campus: 1,
    nota: 0.266
  },
  {
    id: 56,
    nome: 'Letras – Tradução Espanhol Noturno',
    campus: 1,
    nota: -39.344
  },
  {
    id: 54,
    nome: 'Letras – Tradução – Francês',
    campus: 1,
    nota: -24.332
  },
  {
    id: 55,
    nome: 'Letras – Tradução – Inglês',
    campus: 1,
    nota: 43.893
  },
  {
    id: 57,
    nome: 'Licenciatura em Ciências Biológicas Noturno',
    campus: 1,
    nota: 57.713
  },
  {
    id: 58,
    nome: 'Licenciatura em Física Noturno',
    campus: 1,
    nota: 2.413
  },
  {
    id: 59,
    nome: 'Licenciatura em Matemática Noturno',
    campus: 1,
    nota: 17.477
  },
  {
    id: 60,
    nome: 'Licenciatura em Química Noturno',
    campus: 1,
    nota: 24
  },
  {
    id: 61,
    nome: 'Língua e Literatura Japonesa Noturno',
    campus: 1,
    nota: -54.042
  },
  {
    id: 62,
    nome: 'Língua Espanhola e Literatura Espanhola e Hispano-Americana Noturno',
    campus: 1,
    nota: -50.57
  },
  {
    id: 63,
    nome: 'Língua Estrangeira Aplicada – Multilinguismo e Sociedade da Informação',
    campus: 1,
    nota: 46.148
  },
  {
    id: 64,
    nome: 'Língua Francesa e Respectiva Literatura',
    campus: 1,
    nota: -21.616
  },
  {
    id: 65,
    nome: 'Língua Inglesa e Respectiva Literatura',
    campus: 1,
    nota: 32.993
  },
  {
    id: 66,
    nome: 'Língua Portuguesa e Respectiva Literatura',
    campus: 1,
    nota: 65.983
  },
  {
    id: 67,
    nome: 'Língua Portuguesa e Respectiva Literatura (Licenciatura) Noturno',
    campus: 1,
    nota: 1.84
  },
  {
    id: 68,
    nome: 'Matemática',
    campus: 1,
    nota: 62.04
  },
  {
    id: 69,
    nome: 'Medicina',
    campus: 1,
    nota: 175.052
  },
  {
    id: 70,
    nome: 'Medicina Veterinária',
    campus: 1,
    nota: 80.188
  },
  {
    id: 71,
    nome: 'Museologia',
    campus: 1,
    nota: -50.754
  },
  {
    id: 72,
    nome: 'Música',
    campus: 1,
    nota: 51.217
  },
  {
    id: 73,
    nome: 'Nutrição',
    campus: 1,
    nota: 89.06
  },
  {
    id: 74,
    nome: 'Odontologia',
    campus: 1,
    nota: 113.933
  },
  {
    id: 75,
    nome: 'Pedagogia',
    campus: 1,
    nota: 9.745
  },
  {
    id: 76,
    nome: 'Pedagogia Noturno',
    campus: 1,
    nota: 9.97
  },
  {
    id: 77,
    nome: 'Psicologia',
    campus: 1,
    nota: 108.276
  },
  {
    id: 78,
    nome: 'Química Tecnológica',
    campus: 1,
    nota: 47.656
  },
  {
    id: 79,
    nome: 'Relações Internacionais',
    campus: 1,
    nota: 107.153
  },
  {
    id: 94,
    nome: 'Saúde Coletiva',
    campus: 3,
    nota: -23.094
  },
  {
    id: 80,
    nome: 'Saúde Coletiva Noturno',
    campus: 1,
    nota: -15.54
  },
  {
    id: 81,
    nome: 'Serviço Social',
    campus: 1,
    nota: 39.527
  },
  {
    id: 82,
    nome: 'Serviço Social Noturno',
    campus: 1,
    nota: 3.164
  },
  {
    id: 83,
    nome: 'Teoria, Crítica e História da Arte Noturno',
    campus: 1,
    nota: -9.475
  },
  {
    id: 93,
    nome: 'Terapia Ocupacional',
    campus: 3,
    nota: -1.12
  },
  {
    id: 84,
    nome: 'Turismo',
    campus: 1,
    nota: 1.074
  }
];

module.exports.calculateArg = (pas1, pas2, pas3) => {
  const calcEtapa = (etapa, scoreBrutoEtapa, scoreRedacao, lingua) => {
    const valoresDaEtapa = values[etapa];

    const notaParte2 =
      ((scoreBrutoEtapa - valoresDaEtapa.Parte2.media - valoresDaEtapa[lingua].media) * 10.0) /
      valoresDaEtapa.Parte2.desvio;

    const notaRedacao =
      ((scoreRedacao - valoresDaEtapa.Redacao.media) * 10.0) / valoresDaEtapa.Redacao.desvio;

    const result = 0.828 * notaParte2 + 0.1 * notaRedacao;

    return result;
  };

  const p1 = calcEtapa('PAS1', parseFloat(pas1.escore), parseFloat(pas1.redacao), pas1.idioma);
  const p2 = calcEtapa('PAS2', parseFloat(pas2.escore), parseFloat(pas2.redacao), pas2.idioma);
  const p3 = calcEtapa('PAS3', parseFloat(pas3.escore), parseFloat(pas3.redacao), pas3.idioma);

  const argumento = 3 * p3 + 2 * p2 + 1 * p1;

  return argumento;
};
