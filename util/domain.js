module.exports.createDomainWhiteList = () => {
  // TODO: Liberar base parse de produção no modo debug
  const domainName = 'aulascolmeia.com.br';
  const protocols = ['http', 'https'];
  const prefixes = ['', 'www.', 'blog.', 'oferta.'];
  const domains = [];

  protocols.forEach(protocol => {
    prefixes.forEach(prefix => {
      domains.push(`${protocol}://${prefix}${domainName}`);
    });
  });

  return domains;
};
